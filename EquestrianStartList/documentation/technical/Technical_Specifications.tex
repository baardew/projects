\documentclass[a4paper, 10pt]{article}
\usepackage{color}
\usepackage{graphicx}
\usepackage[colorlinks=false]{hyperref}
\usepackage{todonotes}
\usepackage{tikz-uml}

% Manual setup to replicate layout of 11pt version
\setlength{\topmargin}{4pt}
\setlength{\evensidemargin}{22pt}
\setlength{\oddsidemargin}{22pt}
\setlength{\textwidth}{400pt}
\setlength{\marginparwidth}{103pt}
\setlength{\marginparpush}{5pt}

% Custom font setup:
% - Roman:    Charter
% - Math:     Charter Math
% - Sans:     Fira Sans
% - Teletype: Source Code Pro
\usepackage[bitstream-charter]{mathdesign}
\usepackage[scaled=.9,lining]{FiraSans}
\usepackage[scaled=.85]{sourcecodepro}

\hypersetup{
  colorlinks,
  citecolor=black,
  filecolor=black,
  linkcolor=black,
  urlcolor=black
}

\begin{document}
\title{Equestrian Start List}
\author{Follo Western Riders}

\thispagestyle{empty}
\textcolor{black}{\Huge\textbf{Equestrian Start List}}
\vfill
\textcolor{black}{\Huge\textbf{Technical Specifications}}
\clearpage

\setcounter{page}{2}
\Huge{Equestrian Start List}
\normalsize

\tableofcontents

\clearpage
\section{Overview}
Equestrian Start List is a desktop application designed to simplify the creation of equestrians and classes as well as printing of start lists and results.

\subsection{Technologies}
The project is implemented in Java, with the following required libraries:
\begin{itemize}
\item DocumentRenderer
\item SystemLogging
\item JUnit
\item Mockito
\item Powermockito
\end{itemize}
All the libraries are present in the \texttt{lib/} and \texttt{test/lib} folders. The tesing libraries (JUnit, Mockito and Powermockito) have additional dependencies, which are also found in \texttt{test/lib}.

\subsection{File Formats}
The file format for storage utilizes serialization and deserialization provided by Java. Locale specific resource bundles and print layout are all integrated into the sourcecode.

\subsection{MVC Design Pattern}
\label{sec:MVC Design Pattern}
The design implementation uses the MVC design pattern with the following specifications:

\begin{figure}[!h]
  \includegraphics[width=\textwidth]{Figures/MVC_Design_Pattern.pdf}
  \caption{MVC Design Pattern and communication links}
\end{figure}

\begin{figure}[!h]
  \includegraphics[width=\textwidth]{Figures/MVC_Class_Communication.pdf}
  \caption{MVC Design Pattern and class communication links with resource accesses}
\end{figure}

Classes are only allowed to have knowledge of other classes in the same design enviroment (\emph{Model}, \emph{View} or \emph{Controller}) and in addition these following ways:
\begin{itemize}
\item \emph{Controller} classes have access to \emph{Model} classes
\item \emph{View} classes have access to \emph{Controller} classes
\item \emph{View} classes have access to \emph{Model} classes iff it is an enum for, e.g., type specification (which would also be used in \emph{Controller} classes).
\end{itemize}

\subsection{Coding Conventions}
By default java coding conventions are used, unless otherwise specified. General exceptions to this rule are:
\begin{itemize}
\item All classes that implements the \texttt{Serializable} interface are not to specify the \texttt{serialVersionUID}, i.e., it is to be set by the compiler.
\item Objects can only be created/instantiated in the following places:
  \begin{enumerate}
  \item Constructor of \emph{Model} classes
  \item Constructor of \emph{Controller} classes
  \item Factory classes
  \item Anywhere in \emph{View} classes
  \end{enumerate}
\item Any errors in the program that are not recoverable, caused by un-initialized classes or otherwise incorrect behavior are to throw a \texttt{RuntimeException} with a corresponding error message. This results in a program crash and loss of all changed not submitted to storage.
\end{itemize}
Creating/instantiating \emph{Model} or \emph{Controller} objects are to be done in factory classes only.

\subsection{Object Identification and Linking}
Identification of objects are to be made by the \texttt{String} representation obtained from \emph{Model} classes. For example, this includes querying for assistant classes (Section~\ref{sec:Assistant Classes} by a \emph{Model} objects \texttt{String} representation or to link \emph{Model} objects in the \emph{Controller} by finding objects based on the \texttt{String} representation to then retrieve the actual object (and thereby call the approperiate setter(s) to link the objects).

\begin{figure}[!h]
  \includegraphics[width=\textwidth]{Figures/Object_Identification.pdf}
  \caption{Object fetching and identification callgraph}
  \label{fig:Object Identification}
\end{figure}

\begin{figure}[!h]
  \includegraphics[width=\textwidth]{Figures/Object_Linking.pdf}
  \caption{Linking \emph{Model} objects from \texttt{String} representations \texttt{e} and \texttt{c}}
  \label{fig:Object Linking}
\end{figure}

\subsection{Factory Pattern}
The project uses a factory patter for instantiating objects, with the factory using a singleton class (i.e., static methods). Optionally, an \texttt{initialize()} method can be required and a \texttt{RuntimeException} is to be thrown if the factory's initalize method has not been called.

\subsection{Testing Methodology}
Provided tests are found for \textit{Model} and \textit{Controller} classes, but not for \texttt{View} classes. All methods are to be tested, with the exception of optionally testing single-line methods (e.g., set or get a single variable or an if test in a void method to prevent \texttt{NullpointerException}) and integrations to other code or libraries (e.g., print command for \texttt{DocumentPrinter.printDocument(String)}).

\clearpage

\section{Model}
Classes are presented in Figure~\ref{fig:Model Classes}
\begin{figure}[!h]
  \includegraphics[width=\textwidth]{Figures/Model_Classes.pdf}
  \caption{\emph{Model} classes}
  \label{fig:Model Classes}
\end{figure}

\subsection{Design Conventions}
According to the MCV design pattern described in Section~\ref{sec:MVC Design Pattern}, the design of all \emph{Model} classes must comply with the following requirements:
\begin{itemize}
\item A constructor is provided that takes no arguments and used to construct a valid object (i.e., no null objects for class members and all variables are initialized to default values).
\item A copy constructor that performs deep copy of class member objects, but referenced objects inside member variables are not to be copied (i.e., shallow copy).
\item The method \texttt{toString()} should be overridden and return a list-presentable object.
\item The class implements the \texttt{Serializable} interface and is serializable.
\item Setter methods should always return a boolean, defining whether the input is valid for the class/object or not.
\end{itemize}
Exceptions to these requirements are factory classes used to instantiate objects and enums.

\subsection{Summit}
\label{sec:Summit Class}
The \texttt{Summit} class is the model object that contains all instances of both \texttt{Equestrian} and \texttt{Competition} objects. Even though \texttt{Equestrian} and \texttt{Competition} classes can have linkage to one another, they are still to be found in the \texttt{Summit} class. \texttt{Result} objects, are only found in \texttt{Equestrian} objects and are therefore not found in the \texttt{Summit} class.

\clearpage

\section{Controller}
Classes are presented in Figure~\ref{fig:Controller Classes}
\begin{figure}[!h]
  \includegraphics[width=\textwidth]{Figures/Controller_Classes.pdf}
  \caption{\emph{Controller} classes}
  \label{fig:Controller Classes}
\end{figure}

\subsection{Design Conventions}
According to the MCV design pattern described in Section~\ref{sec:MVC Design Pattern}, the design of all \emph{Controller} classes must comply with the following requirements:
\begin{itemize}
\item Setters are to return a valid and correct instance of the \texttt{ControllerStatus} enum. This should also be used for other classes interacting with GUI (unless boolean would be better, e.g., \texttt{save()}).
\end{itemize}

\subsection{Secretary Class}
\label{sec:Secretary Class}
The \texttt{Secretary} class contains an instance of the \texttt{Summit} class (cf. Section~\ref{sec:Summit Class}). It is used to manage and control all \emph{Model} object accesses, including the creation of \emph{Assistant Classes} (cf. Section~\ref{sec:Assistant Classes}). It also contains other resources required in the \emph{Controller} part of the program, with the exception of singleton/factory classes, which are initialized on construction. The \texttt{Secretary} class is also responsible for storing and loading saved summits.

\subsection{Assistant Classes}
\label{sec:Assistant Classes}
Assistant classes encapsulates a single \emph{Model} object and is responsible for accessing (set/get) of member variables for the \emph{Model} object. Assistant classes are only allowed to change the encapsulated \emph{Model} object by changing any of its fields or replacing the object by copy constructing and linkage replacement, but is not allowed to create or delete it to/from any containing data structures. Creating or deleting objects, as well as linking between different \emph{Model} objects, are to be performed by a dedicated \emph{Controller} class. Additionally:
\begin{itemize}
\item The constructor takes the model object to be the assistant for as argument.
\item Assistant class must override \texttt{toString()}, which returns the \emph{Model} object's \texttt{toString()} value.
\end{itemize}

\begin{figure}[h]
  \includegraphics[width=\textwidth]{Figures/Assistant_Design_Layout.pdf}
  \caption{Assistant classes design layout}
\end{figure}

\subsection{Printing}
Printing is done by using Java's internal rendering of HTML tags and then printing it. As a result, the creation of the tables to be printed are created using HTML with CSS 1. Figure~\ref{fig:Printing Flow} shows an example for this flow.
\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=.5\textwidth]{Figures/Printing_Flow.pdf}
  \end{center}
  \caption{The flow for printing out tables}
  \label{fig:Printing Flow}
\end{figure}

\clearpage

\section{View}
Classes are presented in Figure~\ref{fig:View Classes}.
\begin{figure}[!h]
  \includegraphics[width=\textwidth]{Figures/View_Classes.pdf}
  \caption{\emph{View} classes}
  \label{fig:View Classes}
\end{figure}

\subsection{Design Conventions}
According to the MCV design pattern described in Section~\ref{sec:MVC Design Pattern}, the design of all \emph{View} classes must comply with the following requirements:
\begin{itemize}
\item Classes are to have \texttt{ApplicationFrame} class as the first parameter to any constructor.
\item Classes are to use the language locale provided by the \texttt{ApplicationFrame}, by either accessing the global language resource bundle from the \texttt{ApplicationFrame} or load another one based on the locale defined by the \texttt{ApplicationFrame}'s locale.
\end{itemize}

\subsection{ApplicationFrame}
\label{sec:ApplicationFrame}
The \texttt{ApplicationFrame} class is responsible for managing GUI resources (access to the \texttt{Secretary} described in Section~\ref{sec:Secretary Class} and resource bundles/locales), as well as containing all the menu and panel implementations. It also contains the implementation of the window stack (cf. Section~\ref{sec:Window Stack}. For every implemented menu or panel (or other window types) are to have access to this class, which is an initiated class but used as a singleton. Instantiation is done in the \texttt{main()} method during program launch.

\subsection{Template Classes}
Two template classes (i.e., \texttt{abstract class}) are provided to support two types of interfaces: One for button menus (\texttt{TemplateMenu}) and the other for list controls with a custom data management panel (\texttt{TemplatePanel}).

Moreover, the class \texttt{StringJList} provides a JList panel for \texttt{String} items, which can be used to list \emph{Model} classes' \texttt{toString()} value.

\subsection{Window Stack}
\label{sec:Window Stack}
The GUI uses a single \texttt{JFrame} instance (\texttt{ApplicationFrame}, cf. Section~\ref{sec:ApplicationFrame}) that contains the \texttt{JPanel}s used to display the respective information (for example, from subclasses of \texttt{TemplatePanel} or \texttt{TemplateMenu}). This implementation then has menu items that opens a new panel or menu push the new panel or menu on top of the window stack for display as in Figure~\ref{fig:Sequence Diagram Open Panel}. Likewise, when closing a panel or menu, the stack is popped and the previous menu or panel item is displayed as in Figure~\ref{fig:Sequence Diagram Close Panel}. For both operations, the control is transferred to the new panel or menu after the \texttt{ActionEvent} for the current method invocation has returned.
\begin{center}
  \begin{figure}[!h]
    \includegraphics[width=\textwidth]{Figures/Menu_Open.pdf}
    \caption{Sequence diagram for opening a new panel.}
    \label{fig:Sequence Diagram Open Panel}
  \end{figure}
\end{center}

\begin{center}
  \begin{figure}[!h]
    \includegraphics[width=\textwidth]{Figures/Panel_Close.pdf}
    \caption{Sequence diagram for closing a panel.}
    \label{fig:Sequence Diagram Close Panel}
  \end{figure}
\end{center}

\subsection{Program Launch}
On program launch, the main method intatiates a new \texttt{ApplicationFrame}, the \texttt{JFrame} that holds all the GUI elements (cf. Section~\ref{sec:Window Stack}) and pushes the \texttt{StartMenu} menu. This menu is then responsible for setting up the \texttt{Secretary}, the main class for managing and referencing all data objects, and on completion, removes itself from the window stack.
\begin{center}
  \begin{figure}[!h]
    \includegraphics[width=\textwidth]{Figures/Program_Launch.pdf}
    \caption{Sequence diagram for program launch.}
  \end{figure}
\end{center}

\clearpage

\maketitle
\thispagestyle{empty}
\begin{center}
  v. 1.0\\
  Manual created with LaTeX\\
  Equstrian Start List developed in Java\\
  Technical documentation graphs created with UMLet
\end{center}
\end{document}
