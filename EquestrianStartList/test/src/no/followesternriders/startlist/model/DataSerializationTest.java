package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

import testutils.TestUtil;

public class DataSerializationTest {

    private final String fileResult = "testResult.ser";
    private final String fileEquestrian = "testEquestrian.ser";
    private final String fileCompetition = "testCompetitionser.ser";
    private final String fileSummit = "testSummit.ser";
    private final String fileFull = "fileFull.ser";
    private final String SERDIR = "test/serialization/";

    @Before
    public void teardown() {
        new File(SERDIR + fileResult).delete();
        new File(SERDIR + fileEquestrian).delete();
    }

    @Test
    public void serialization_result() {
        Result testResult = new Result();
        testResult.setResult("serialize_result");

        String status = TestUtil.serialize(fileResult, testResult);
        if (status != null)
            Assert.fail(status);

        Result retResult = TestUtil.<Result>deserialize(fileResult);
        if (retResult == null)
            Assert.fail("Could not deserialize");

        Assert.assertEquals(testResult.getResult(), retResult.getResult());
    }

    @Test
    public void serialization_equestrian() {
        Equestrian testEquestrian = new Equestrian();
        testEquestrian.setRiderName("serialization_equestrian_rider");
        testEquestrian.setHorseName("serialization_equestrian_horse");
        testEquestrian.setStartNumber(42);

        String status = TestUtil.serialize(fileEquestrian, testEquestrian);
        if (status != null)
            Assert.fail(status);

        Equestrian retEquestrian = TestUtil.<Equestrian>deserialize(fileEquestrian);
        if (retEquestrian == null)
            Assert.fail("Could not deserialize");

        Assert.assertEquals(testEquestrian.getRiderName(), retEquestrian.getRiderName());
        Assert.assertEquals(testEquestrian.getHorseName(), retEquestrian.getHorseName());
        Assert.assertEquals(testEquestrian.getStartNumber(), retEquestrian.getStartNumber());
    }

    @Test
    public void serialization_competition() {
        Competition testCompetition = new Competition();
        testCompetition.setName("serialization_competition_name");
        testCompetition.setDetails("serialization_competition_details");
        testCompetition.setStartPrice(12);
        testCompetition.setStartOrder(57);
        testCompetition.setRandomizeStartList(true);
        testCompetition.setResultType(ResultType.Score);
        testCompetition.setStartTime("serialization_competition_startTime");

        String status = TestUtil.serialize(fileCompetition, testCompetition);
        if (status != null)
            Assert.fail(status);

        Competition retCompetition = TestUtil.<Competition>deserialize(fileCompetition);
        if (retCompetition == null)
            Assert.fail("Could not deserialize");

        Assert.assertEquals(testCompetition.getName(), retCompetition.getName());
        Assert.assertEquals(testCompetition.getDetails(), retCompetition.getDetails());
        Assert.assertEquals(testCompetition.getStartPrice(), retCompetition.getStartPrice());
        Assert.assertEquals(testCompetition.getResultType(), retCompetition.getResultType());
        Assert.assertEquals(testCompetition.getStartTime(), retCompetition.getStartTime());
        Assert.assertEquals(testCompetition.getStartOrder(), retCompetition.getStartOrder());
        Assert.assertEquals(testCompetition.getRandomizeStartList(), retCompetition.getRandomizeStartList());
    }

    @Test
    public void serialization_summit() {
        Summit testSummit = new Summit();
        testSummit.setName("serialization_summit_name");

        String status = TestUtil.serialize(fileSummit, testSummit);
        if (status != null)
            Assert.fail(status);

        Summit retSummit = TestUtil.<Summit>deserialize(fileSummit);
        if (retSummit == null)
            Assert.fail("Could not deserialize");

        Assert.assertEquals(testSummit.getName(), retSummit.getName());
    }

    @Test
    public void serialization_full() {
        Summit testSummit = new Summit();
        testSummit.setName("serialization_summit_name");

        Result testResult = new Result();
        testResult.setResult("serialize_result");

        Competition testCompetition = new Competition();
        testCompetition.setName("serialization_competition_name");
        testCompetition.setDetails("serialization_competition_details");
        testCompetition.setStartPrice(12);
        testCompetition.setStartOrder(57);
        testCompetition.setRandomizeStartList(true);
        testCompetition.setResultType(ResultType.Score);
        testCompetition.setStartTime("serialization_competition_startTime");

        Equestrian testEquestrian = new Equestrian();
        testEquestrian.setRiderName("serialization_equestrian_rider");
        testEquestrian.setHorseName("serialization_equestrian_horse");

        testEquestrian.addCompetition(testCompetition);
        testEquestrian.updateResult(testCompetition, testResult);

        testCompetition.addEquestrian(testEquestrian);

        testSummit.addCompetition(testCompetition);
        testSummit.addEquestrian(testEquestrian);

        String status = TestUtil.serialize(fileFull, testSummit);
        if (status != null)
            Assert.fail(status);

        Summit retSummit = TestUtil.<Summit>deserialize(fileFull);
        if (retSummit == null)
            Assert.fail("Could not deserialize");

        Competition retCompetition = retSummit.getCompetition(testCompetition.toString());
        Equestrian retEquestrian = retSummit.getEquestrian(testEquestrian.toString());
        Result retResult = retEquestrian.getResult(retCompetition);

        Assert.assertEquals(testResult.getResult(), retResult.getResult());
        Assert.assertEquals(testEquestrian.getRiderName(), retEquestrian.getRiderName());
        Assert.assertEquals(testEquestrian.getHorseName(), retEquestrian.getHorseName());
        Assert.assertEquals(testEquestrian.getStartNumber(), retEquestrian.getStartNumber());
        Assert.assertEquals(testSummit.getName(), retSummit.getName());
        Assert.assertEquals(testCompetition.getName(), retCompetition.getName());
        Assert.assertEquals(testCompetition.getDetails(), retCompetition.getDetails());
        Assert.assertEquals(testCompetition.getStartPrice(), retCompetition.getStartPrice());
        Assert.assertEquals(testCompetition.getResultType(), retCompetition.getResultType());
        Assert.assertEquals(testCompetition.getStartTime(), retCompetition.getStartTime());
        Assert.assertEquals(testCompetition.getStartOrder(), retCompetition.getStartOrder());
        Assert.assertEquals(testCompetition.getRandomizeStartList(), retCompetition.getRandomizeStartList());
    }
}
