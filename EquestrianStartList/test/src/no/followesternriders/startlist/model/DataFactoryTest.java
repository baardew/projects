package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.DataFactory;

import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.mockito.Mockito;

public class DataFactoryTest {

    private Summit summit;
    private ResourceBundle locale;

    @Before
    public void init() {
        locale = ResourceBundle.getBundle("LanguageBundle", new Locale("en"));
        summit = new Summit();
        DataFactory.initialize(summit, locale);
    }

    @After
    public void teardown() {
        summit = null;
        DataFactory.initialize(null, null);
        locale = null;
    }

    @Test
    public void createEquestrian_uninitializedFactory_throwsException() {
        // Initializing facotry to null is the same as uninitialized
        DataFactory.initialize(null, null);
        boolean threwException = false;

        try {
            DataFactory.createEquestrian();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DataFactory not initialized");
        }

        if (!threwException)
            Assert.fail("No exceptions or wrong exception thrown");
    }

    @Test
    public void createEquestrian_emptySummit_returnValidObject() {
        Equestrian ret = DataFactory.createEquestrian();
        Assert.assertEquals(locale.getString("defaultRiderName") + "1", ret.getRiderName());
        Assert.assertEquals(locale.getString("defaultHorseName") + "1", ret.getHorseName());
        Assert.assertEquals(1, ret.getStartNumber());
    }

    @Test
    public void createEquestrian_nonEmptyEntry_returnValidObject() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(locale.getString("defaultRiderName") + "1" + locale.getString("defaultHorseName") + "1");
        Mockito.when(testEquestrian.getRiderName()).thenReturn("Named" + locale.getString("defaultRiderName") + "1");
        Mockito.when(testEquestrian.getHorseName()).thenReturn("Named" + locale.getString("defaultHorseName") + "1");
        summit.addEquestrian(testEquestrian);

        Equestrian ret = DataFactory.createEquestrian();
        Assert.assertEquals(locale.getString("defaultRiderName") + "1", ret.getRiderName());
        Assert.assertEquals(locale.getString("defaultHorseName") + "1", ret.getHorseName());
        Assert.assertEquals(2, ret.getStartNumber());
    }

    @Test
    public void createEquestrian_existingNewEntry_returnValidObject() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(locale.getString("defaultRiderName") + "1" + locale.getString("defaultHorseName") + "1");
        Mockito.when(testEquestrian.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "1");
        Mockito.when(testEquestrian.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "1");
        summit.addEquestrian(testEquestrian);

        Equestrian ret = DataFactory.createEquestrian();
        Assert.assertEquals(locale.getString("defaultRiderName") + "2", ret.getRiderName());
        Assert.assertEquals(locale.getString("defaultHorseName") + "2", ret.getHorseName());
        Assert.assertEquals(2, ret.getStartNumber());
    }

    @Test
    public void createEquestrian_existingEntries_returnRiderNameNumberMax() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian1.toString()).thenReturn(locale.getString("defaultRiderName") + "1" + locale.getString("defaultHorseName") + "1");
        Mockito.when(testEquestrian1.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "1");
        Mockito.when(testEquestrian1.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "1");
        summit.addEquestrian(testEquestrian1);

        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian2.toString()).thenReturn(locale.getString("defaultRiderName") + "2" + locale.getString("defaultHorseName") + "2");
        Mockito.when(testEquestrian2.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "GenericName2");
        Mockito.when(testEquestrian2.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "2");
        summit.addEquestrian(testEquestrian2);

        Equestrian testEquestrian3 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian3.toString()).thenReturn(locale.getString("defaultRiderName") + "3" + locale.getString("defaultHorseName") + "3");
        Mockito.when(testEquestrian3.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "3");
        Mockito.when(testEquestrian3.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "GenericName3");
        summit.addEquestrian(testEquestrian3);

        Equestrian ret = DataFactory.createEquestrian();
        Assert.assertEquals(locale.getString("defaultRiderName") + "4", ret.getRiderName());
        Assert.assertEquals(locale.getString("defaultHorseName") + "4", ret.getHorseName());
        Assert.assertEquals(4, ret.getStartNumber());
    }

    @Test
    public void createEquestrian_existingEntries_returnHorseNameNumberMax() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian1.toString()).thenReturn(locale.getString("defaultRiderName") + "1" + locale.getString("defaultHorseName") + "1");
        Mockito.when(testEquestrian1.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "1");
        Mockito.when(testEquestrian1.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "1");
        summit.addEquestrian(testEquestrian1);

        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian2.toString()).thenReturn(locale.getString("defaultRiderName") + "2" + locale.getString("defaultHorseName") + "2");
        Mockito.when(testEquestrian2.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "2");
        Mockito.when(testEquestrian2.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "GenericName2");
        summit.addEquestrian(testEquestrian2);

        Equestrian testEquestrian3 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian3.toString()).thenReturn(locale.getString("defaultRiderName") + "3" + locale.getString("defaultHorseName") + "3");
        Mockito.when(testEquestrian3.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "GenericName3");
        Mockito.when(testEquestrian3.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "3");
        summit.addEquestrian(testEquestrian3);

        Equestrian ret = DataFactory.createEquestrian();
        Assert.assertEquals(locale.getString("defaultRiderName") + "4", ret.getRiderName());
        Assert.assertEquals(locale.getString("defaultHorseName") + "4", ret.getHorseName());
        Assert.assertEquals(4, ret.getStartNumber());
    }

    @Test
    public void createEquestrian_existingEntryHole_returnMaxIndex() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian1.toString()).thenReturn(locale.getString("defaultRiderName") + "1" + locale.getString("defaultHorseName") + "1");
        Mockito.when(testEquestrian1.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "GenericName1");
        Mockito.when(testEquestrian1.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "GenericName1");
        summit.addEquestrian(testEquestrian1);

        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian2.toString()).thenReturn(locale.getString("defaultRiderName") + "2" + locale.getString("defaultHorseName") + "2");
        Mockito.when(testEquestrian2.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "2");
        Mockito.when(testEquestrian2.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "2");
        summit.addEquestrian(testEquestrian2);

        Equestrian ret = DataFactory.createEquestrian();
        Assert.assertEquals(locale.getString("defaultRiderName") + "3", ret.getRiderName());
        Assert.assertEquals(locale.getString("defaultHorseName") + "3", ret.getHorseName());
        Assert.assertEquals(3, ret.getStartNumber());
    }

    @Test
    public void createEquestrian_existingEntryHole_returnIndex() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian1.toString()).thenReturn(locale.getString("defaultRiderName") + "1" + locale.getString("defaultHorseName") + "1");
        Mockito.when(testEquestrian1.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "1");
        Mockito.when(testEquestrian1.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "1");
        summit.addEquestrian(testEquestrian1);

        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian2.toString()).thenReturn(locale.getString("defaultRiderName") + "GenericName1" + locale.getString("defaultHorseName") + "GenericName1");
        Mockito.when(testEquestrian2.getRiderName()).thenReturn(locale.getString("defaultRiderName") + "GenericName1");
        Mockito.when(testEquestrian2.getHorseName()).thenReturn(locale.getString("defaultHorseName") + "GenericName1");
        summit.addEquestrian(testEquestrian2);

        Equestrian ret = DataFactory.createEquestrian();
        Assert.assertEquals(locale.getString("defaultRiderName") + "2", ret.getRiderName());
        Assert.assertEquals(locale.getString("defaultHorseName") + "2", ret.getHorseName());
        Assert.assertEquals(3, ret.getStartNumber());
    }

    @Test
    public void cloneEquestrian_nullEntry_returnNull() {
        Assert.assertNull(DataFactory.cloneEquestrian(null));
    }

    @Test
    public void cloneEquestrian_entry_returnObject() {
        Equestrian testEquestrian = new Equestrian();
        Assert.assertNotNull(DataFactory.cloneEquestrian(testEquestrian));
    }

    @Test
    public void cloneEquestrian_entry_returnNew() {
        Equestrian testEquestrian = new Equestrian();
        Equestrian ret = DataFactory.cloneEquestrian(testEquestrian);
        Assert.assertNotSame(testEquestrian, ret);
    }

    @Test
    public void createCompetition_uninitializedFactory_throwsException() {
        // Initializing facotry to null is the same as uninitialized
        DataFactory.initialize(null, null);
        boolean threwException = false;

        try {
            DataFactory.createCompetition();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DataFactory not initialized");
        }

        if (!threwException)
            Assert.fail("No exceptions or wrong exception thrown");
    }

    @Test
    public void createCompetition_emptySummit_returnValidObject() {
        Competition ret = DataFactory.createCompetition();
        Assert.assertEquals(locale.getString("defaultCompetitionName") + "1", ret.getName());
        Assert.assertEquals(1, ret.getStartOrder());
    }

    @Test
    public void createCompetition_nonEmptyEntry_returnValidObject() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(locale.getString("defaultCompetitionName") + "1");
        Mockito.when(testCompetition.getName()).thenReturn(locale.getString("defaultCompetitionName") + "GenericName1");
        summit.addCompetition(testCompetition);

        Competition ret = DataFactory.createCompetition();
        Assert.assertEquals(locale.getString("defaultCompetitionName") + "1", ret.getName());
        Assert.assertEquals(2, ret.getStartOrder());
    }

    @Test
    public void createCompetition_existingNewEntry_returnValidObject() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(locale.getString("defaultCompetitionName") + "1");
        Mockito.when(testCompetition.getName()).thenReturn(locale.getString("defaultCompetitionName") + "1");
        summit.addCompetition(testCompetition);

        Competition ret = DataFactory.createCompetition();
        Assert.assertEquals(locale.getString("defaultCompetitionName") + "2", ret.getName());
        Assert.assertEquals(2, ret.getStartOrder());
    }

    @Test
    public void createCompetition_existingEntries_returnNumberMax() {
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition1.toString()).thenReturn(locale.getString("defaultCompetitionName") + "1");
        Mockito.when(testCompetition1.getName()).thenReturn(locale.getString("defaultCompetitionName") + "1");
        summit.addCompetition(testCompetition1);

        Competition testCompetition2 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition2.toString()).thenReturn(locale.getString("defaultCompetitionName") + "2");
        Mockito.when(testCompetition2.getName()).thenReturn(locale.getString("defaultCompetitionName") + "2");
        summit.addCompetition(testCompetition2);

        Competition ret = DataFactory.createCompetition();
        Assert.assertEquals(locale.getString("defaultCompetitionName") + "3", ret.getName());
        Assert.assertEquals(3, ret.getStartOrder());
    }

    @Test
    public void createCompetition_existingEntries_returnNumberIndexed() {
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition1.toString()).thenReturn(locale.getString("defaultCompetitionName") + "1");
        Mockito.when(testCompetition1.getName()).thenReturn(locale.getString("defaultCompetitionName") + "1");
        summit.addCompetition(testCompetition1);

        Competition testCompetition2 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition2.toString()).thenReturn(locale.getString("defaultCompetitionName") + "GenericName1");
        Mockito.when(testCompetition2.getName()).thenReturn(locale.getString("defaultCompetitionName") + "GenericName1");
        summit.addCompetition(testCompetition2);

        Competition ret = DataFactory.createCompetition();
        Assert.assertEquals(locale.getString("defaultCompetitionName") + "2", ret.getName());
        Assert.assertEquals(3, ret.getStartOrder());
    }

    @Test
    public void createCompetition_existingEntryHole_returnMaxIndex() {
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition1.toString()).thenReturn(locale.getString("defaultCompetitionName") + "GenericCompetition");
        Mockito.when(testCompetition1.getName()).thenReturn(locale.getString("defaultCompetitionName") + "GenericCompetition");
        summit.addCompetition(testCompetition1);

        Competition testCompetition2 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition2.toString()).thenReturn(locale.getString("defaultCompetitionName") + "2");
        Mockito.when(testCompetition2.getName()).thenReturn(locale.getString("defaultCompetitionName") + "2");
        summit.addCompetition(testCompetition2);

        Competition ret = DataFactory.createCompetition();
        Assert.assertEquals(locale.getString("defaultCompetitionName") + "3", ret.getName());
        Assert.assertEquals(3, ret.getStartOrder());
    }

    @Test
    public void cloneCompetition_nullEntry_returnNull() {
        Assert.assertNull(DataFactory.cloneCompetition(null));
    }

    @Test
    public void cloneCompetition_entry_returnObject() {
        Competition testCompetition = new Competition();
        Assert.assertNotNull(DataFactory.cloneCompetition(testCompetition));
    }

    @Test
    public void cloneCompetition_entry_returnNew() {
        Competition testCompetition = new Competition();
        Competition ret = DataFactory.cloneCompetition(testCompetition);
        Assert.assertNotSame(testCompetition, ret);
    }

    @Test
    public void createResult_uninitializedFactory_throwsException() {
        // Initializing facotry to null is the same as uninitialized
        DataFactory.initialize(null, null);
        boolean threwException = false;

        try {
            DataFactory.createResult();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DataFactory not initialized");
        }

        if (!threwException)
            Assert.fail("No exceptions or wrong exception thrown");
    }

    @Test
    public void createResult_initializedFactory_returnObject() {
        Assert.assertNotNull(DataFactory.createResult());
    }
}
