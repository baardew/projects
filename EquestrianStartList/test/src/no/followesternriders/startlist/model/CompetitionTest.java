package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.Result;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.mockito.Mockito;

public class CompetitionTest {

    private Competition competition;

    @Before
    public void initialize() {
        competition = new Competition();
    }

    @After
    public void teardown() {
        competition = null;
    }

    @Test
    public void setName_null_returnFalse() {
        Assert.assertFalse(competition.setName(null));
    }

    @Test
    public void setName_empty_returnFalse() {
        Assert.assertFalse(competition.setName(new String("")));
    }

    @Test
    public void setName_valid_returnTrue() {
        Assert.assertTrue(competition.setName(new String("setName_valid_returnTrue")));
    }

    @Test
    public void setName_valid_set() {
        String testName = new String("setName_valid_returnTrue");
        competition.setName(testName);
        Assert.assertEquals(testName, competition.getName());
    }

    @Test
    public void setDetails_null_returnFalse() {
        Assert.assertFalse(competition.setDetails(null));
    }

    @Test
    public void setDetails_empty_returnTrue() {
        Assert.assertTrue(competition.setDetails(new String("")));
    }

    @Test
    public void setDetails_valid_returnTrue() {
        Assert.assertTrue(competition.setDetails(new String("setDetails_valid_returnTrue")));
    }

    @Test
    public void setDetails_valid_set() {
        String testDetails = new String("setDetails_valid_returnTrue");
        competition.setDetails(testDetails);
        Assert.assertEquals(testDetails, competition.getDetails());
    }

    @Test
    public void setStartTime_null_returnFalse() {
        Assert.assertFalse(competition.setStartTime(null));
    }

    @Test
    public void setStartTime_empty_returnTrue() {
        Assert.assertTrue(competition.setStartTime(new String("")));
    }

    @Test
    public void setStartTime_valid_returnTrue() {
        Assert.assertTrue(competition.setStartTime(new String("setStartTime_valid_returnTrue")));
    }

    @Test
    public void setStartTime_valid_set() {
        String testStartTime = new String("setStartTime_valid_returnTrue");
        competition.setStartTime(testStartTime);
        Assert.assertEquals(testStartTime, competition.getStartTime());
    }

    @Test
    public void addEquestrian_null_returnFalse() {
        Assert.assertFalse(competition.addEquestrian(null));
    }

    @Test
    public void addEquestrian_notPresent_returnTrue() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Assert.assertTrue(competition.addEquestrian(testEquestrian));
    }

    @Test
    public void addEquestrian_present_returnFalse() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        competition.addEquestrian(testEquestrian);
        Assert.assertFalse(competition.addEquestrian(testEquestrian));
    }

    @Test
    public void addEquestrian_multiEntry_correctlyset() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Equestrian testEquestrian3 = Mockito.mock(Equestrian.class);

        competition.addEquestrian(testEquestrian1);
        competition.addEquestrian(testEquestrian2);
        competition.addEquestrian(testEquestrian3);

        ArrayList<Equestrian> ret = competition.getEquestrians();
        Assert.assertEquals(3, ret.size());
        Assert.assertSame(testEquestrian1, ret.get(0));
        Assert.assertSame(testEquestrian2, ret.get(1));
        Assert.assertSame(testEquestrian3, ret.get(2));
    }

    @Test
    public void addEquestrian_singleEntry_correctlySet() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        competition.addEquestrian(testEquestrian);
        ArrayList<Equestrian> ret = competition.getEquestrians();
        Assert.assertEquals(1, ret.size());
        Assert.assertSame(testEquestrian, ret.get(0));
    }

    @Test
    public void removeEquestrian_null_returnFalse() {
        Assert.assertFalse(competition.removeEquestrian(null));
    }

    @Test
    public void removeEquestrian_notFound_returnFalse() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Assert.assertFalse(competition.removeEquestrian(testEquestrian));
    }

    @Test
    public void removeEquestrian_found_returnTrue() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        competition.addEquestrian(testEquestrian);
        Assert.assertTrue(competition.removeEquestrian(testEquestrian));
    }

    @Test
    public void removeEquestrian_found_removed() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        competition.addEquestrian(testEquestrian);
        competition.removeEquestrian(testEquestrian);
        ArrayList<Equestrian> ret = competition.getEquestrians();
        Assert.assertEquals(0, ret.size());
    }

    @Test
    public void removeEquestrian_multiple_removedCorrect() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Equestrian testEquestrian3 = Mockito.mock(Equestrian.class);

        competition.addEquestrian(testEquestrian1);
        competition.addEquestrian(testEquestrian2);
        competition.addEquestrian(testEquestrian3);
        competition.removeEquestrian(testEquestrian2);

        ArrayList<Equestrian> ret = competition.getEquestrians();
        Assert.assertEquals(2, ret.size());
        Assert.assertSame(testEquestrian1, ret.get(0));
        Assert.assertSame(testEquestrian3, ret.get(1));
    }

    @Test
    public void setStartTime_negative_returnFalse() {
        Assert.assertFalse(competition.setStartPrice(-1));
    }

    @Test
    public void setStartTime_positive_returnTrue() {
        Assert.assertTrue(competition.setStartPrice(1));
    }

    @Test
    public void setStartTime_zero_returnTrue() {
        Assert.assertTrue(competition.setStartPrice(0));
    }

    @Test
    public void setStartOrder_negative_returnFalse() {
        Assert.assertFalse(competition.setStartOrder(-1));
    }

    @Test
    public void setStartOrder_positive_returnTrue() {
        Assert.assertTrue(competition.setStartOrder(1));
    }

    @Test
    public void setStartOrder_zero_returnFalse() {
        Assert.assertFalse(competition.setStartOrder(0));
    }


    @Test
    public void setResultType_null_returnFalse() {
        Assert.assertFalse(competition.setResultType(null));
    }

    @Test
    public void setResultType_valid_returnFalse() {
        Assert.assertTrue(competition.setResultType(ResultType.Text));
    }

    @Test
    public void toString_correct() {
        String testName = new String("toString_correct");
        competition.setName(testName);
        Assert.assertEquals(testName, competition.toString());
    }
}
