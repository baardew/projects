package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Result;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class ResultTest {

    Result result;

    @Before
    public void initialize() {
        result = new Result();
    }

    @After
    public void teardown() {
        result = null;
    }

    @Test
    public void setResult_null_returnFalse() {
        Assert.assertFalse(result.setResult(null));
    }

    @Test
    public void setResult_empty_returnTrue() {
        Assert.assertTrue(result.setResult(new String("")));
    }

    @Test
    public void setResult_valid_returnTrue() {
        Assert.assertTrue(result.setResult(new String("setResult_valid_returnTrue")));
    }

    @Test
    public void setResult_valid_set() {
        String testResult = new String("setResult_valid_set");
        result.setResult(testResult);
        Assert.assertEquals(testResult, result.getResult());
    }

    @Test
    public void toString_correct() {
        String testResult = new String("toString_correct");
        result.setResult(testResult);
        Assert.assertEquals(testResult, result.toString());
    }
}
