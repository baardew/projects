package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.Result;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.mockito.Mockito;

public class EquestrianTest {

    private Equestrian equestrian;

    @Before
    public void initialize() {
        equestrian = new Equestrian();
    }

    @After
    public void teardown() {
        equestrian = null;
    }

    @Test
    public void setRider_null_returnFalse() {
        Assert.assertFalse(equestrian.setRiderName(null));
    }

    @Test
    public void setRider_empty_returnFalse() {
        Assert.assertFalse(equestrian.setRiderName(new String("")));
    }

    @Test
    public void setRider_valid_returnTrue() {
        String testName = "setRider_valid_returnTrue";
        Assert.assertTrue(equestrian.setRiderName(testName));
    }

    @Test
    public void setRider_valid_set() {
        String testName = "setRider_valid_set";
        equestrian.setRiderName(testName);
        Assert.assertEquals(testName, equestrian.getRiderName());
    }

    @Test
    public void setHorse_null_returnFalse() {
        Assert.assertFalse(equestrian.setHorseName(null));
    }

    @Test
    public void setHorse_empty_returnFalse() {
        Assert.assertFalse(equestrian.setHorseName(new String("")));
    }

    @Test
    public void setHorse_valid_returnTrue() {
        String testName = "setHorse_valid_returnTrue";
        Assert.assertTrue(equestrian.setHorseName(testName));
    }

    @Test
    public void setHorse_valid_set() {
        String testName = "setHorse_valid_set";
        equestrian.setHorseName(testName);
        Assert.assertEquals(testName, equestrian.getHorseName());
    }

    @Test
    public void setStartNumber_negative_returnFalse() {
        Assert.assertFalse(equestrian.setStartNumber(-15));
    }

    @Test
    public void setStartNumber_zero_returnFalse() {
        Assert.assertFalse(equestrian.setStartNumber(0));
    }

    @Test
    public void setStartNumber_valid_returnTrue() {
        Assert.assertTrue(equestrian.setStartNumber(15));
    }

    @Test
    public void setStartNumber_valid_set() {
        int testNumber = 26;
        equestrian.setStartNumber(testNumber);
        Assert.assertEquals(testNumber, equestrian.getStartNumber());
    }

    @Test
    public void addCompetition_null_returnFalse() {
        try {
            Assert.assertFalse(equestrian.addCompetition(null));
        } catch (Throwable e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void addCompetition_singleEntry_sucess() {
        Competition testCompetition = Mockito.mock(Competition.class);

        Assert.assertTrue(equestrian.addCompetition(testCompetition));

        ArrayList<Competition> comp = equestrian.getCompetitions();
        Assert.assertEquals(1, comp.size());
        Assert.assertSame(testCompetition, comp.get(0));
    }

    @Test
    public void addCompetition_duplicateEntry_returnFalse() {
        Competition testCompetition = Mockito.mock(Competition.class);

        Assert.assertTrue(equestrian.addCompetition(testCompetition));
        Assert.assertFalse(equestrian.addCompetition(testCompetition));

        ArrayList<Competition> comp = equestrian.getCompetitions();
        Assert.assertEquals(1, comp.size());
        Assert.assertSame(testCompetition, comp.get(0));
    }

    @Test
    public void addCompetition_multiEntry_returnTrue() {
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Competition testCompetition2 = Mockito.mock(Competition.class);
        Competition testCompetition3 = Mockito.mock(Competition.class);

        Assert.assertTrue(equestrian.addCompetition(testCompetition1));
        Assert.assertTrue(equestrian.addCompetition(testCompetition2));
        Assert.assertTrue(equestrian.addCompetition(testCompetition3));

        ArrayList<Competition> comp = equestrian.getCompetitions();
        Assert.assertEquals(3, comp.size());
        Assert.assertSame(testCompetition1, comp.get(0));
        Assert.assertSame(testCompetition2, comp.get(1));
        Assert.assertSame(testCompetition3, comp.get(2));
    }

    @Test
    public void removeCompetition_null_returnFalse() {
        try {
            Assert.assertFalse(equestrian.removeCompetition(null));
        } catch (Throwable e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void removeCompetition_multiEntry_removedCorrect() {
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Competition testCompetition2 = Mockito.mock(Competition.class);
        Competition testCompetition3 = Mockito.mock(Competition.class);

        Assert.assertTrue(equestrian.addCompetition(testCompetition1));
        Assert.assertTrue(equestrian.addCompetition(testCompetition2));
        Assert.assertTrue(equestrian.addCompetition(testCompetition3));
        Assert.assertTrue(equestrian.removeCompetition(testCompetition2));

        ArrayList<Competition> comp = equestrian.getCompetitions();
        Assert.assertEquals(2, comp.size());
        Assert.assertSame(testCompetition1, comp.get(0));
        Assert.assertSame(testCompetition3, comp.get(1));
    }

    @Test
    public void removeCompetition_noEntry_returnFalse() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Assert.assertFalse(equestrian.removeCompetition(testCompetition));
    }

    @Test
    public void updateResult_nullResult_returnFalse() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Assert.assertFalse(equestrian.updateResult(testCompetition, null));
    }

    @Test
    public void updateResult_nullCompetition_returnFalse() {
        Result testResult = Mockito.mock(Result.class);
        Assert.assertFalse(equestrian.updateResult(null, testResult));
    }

    @Test
    public void updateResult_valid_returnTrue() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Result testResult = Mockito.mock(Result.class);
        Assert.assertTrue(equestrian.updateResult(testCompetition, testResult));
    }

    @Test
    public void getResult_noResult_returnNull() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Assert.assertNull(equestrian.getResult(testCompetition));
    }

    @Test
    public void getResult_nullCompetition_returnNull() {
        Assert.assertNull(equestrian.getResult(null));
    }

    @Test
    public void getResult_present_returnResult() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Result testResult = Mockito.mock(Result.class);
        equestrian.updateResult(testCompetition, testResult);

        Result ret = equestrian.getResult(testCompetition);
        Assert.assertNotNull(ret);
        Assert.assertSame(testResult, ret);
    }

    @Test
    public void toString_correct() {
        String testRiderName = new String("stringRiderName");
        String testHorseName = new String("stringHorseName");
        equestrian.setRiderName(testRiderName);
        equestrian.setHorseName(testHorseName);

        String objectString = testRiderName + " & " + testHorseName;
        Assert.assertEquals(objectString, equestrian.toString());
    }
}
