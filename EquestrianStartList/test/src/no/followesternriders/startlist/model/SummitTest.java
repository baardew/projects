package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.mockito.Mockito;

public class SummitTest {

    private Summit summit;

    @Before
    public void initialize() {
        summit = new Summit();
    }

    @After
    public void teardown() {
        summit = null;
    }

    @Test
    public void setName_null_returnFalse() {
        Assert.assertFalse(summit.setName(null));
    }

    @Test
    public void setName_empty_returnFalse() {
        Assert.assertFalse(summit.setName(new String("")));
    }

    @Test
    public void setName_valid_returnTrue() {
        Assert.assertTrue(summit.setName(new String("setName_valid_returnTrue")));
    }

    @Test
    public void setName_valid_set() {
        String testName = "setName_valid_set";
        summit.setName(testName);
        Assert.assertEquals(testName, summit.getName());
    }

    @Test
    public void addEquestrian_null_returnFalse() {
        Assert.assertFalse(summit.addEquestrian(null));
    }

    @Test
    public void addEquestrian_singleEntry_returnTrue() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(new String("addEquestrian_singleEntry_returnTrue"));
        Assert.assertTrue(summit.addEquestrian(testEquestrian));
    }

    @Test
    public void addEquestrian_duplicateEntry_returnFalse() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(new String("addEquestrian_duplicateEntry_returnFalse"));

        summit.addEquestrian(testEquestrian);
        Assert.assertFalse(summit.addEquestrian(testEquestrian));
    }

    @Test
    public void addEquestrian_multiEntry_returnTrue() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian1.toString()).thenReturn(new String("multiEntry1"));

        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian2.toString()).thenReturn(new String("multiEntry2"));

        Equestrian testEquestrian3 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian3.toString()).thenReturn(new String("multiEntry3"));

        summit.addEquestrian(testEquestrian1);
        summit.addEquestrian(testEquestrian2);

        Assert.assertTrue(summit.addEquestrian(testEquestrian3));
    }

    @Test
    public void addEquestrian_present_returnFalse() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(new String("addEquestrian_present_returnFalse"));

        summit.addEquestrian(testEquestrian);
        Assert.assertFalse(summit.addEquestrian(testEquestrian));
    }

    @Test
    public void removeEquestrian_null_returnFalse() {
        Assert.assertFalse(summit.removeEquestrian(null));
    }

    @Test
    public void removeEquestrian_notPresent_returnFalse() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(new String("removeEquestrian_notPresent_returnFalse"));

        Assert.assertFalse(summit.removeEquestrian(testEquestrian));
    }

    @Test
    public void removeEquestrian_present_returnTrue() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(new String("removeEquestrian_present_returnTrue"));
        summit.addEquestrian(testEquestrian);

        Assert.assertTrue(summit.removeEquestrian(testEquestrian));
    }

    @Test
    public void getEquestrian_null_returnNull() {
        Assert.assertNull(summit.getEquestrian(null));
    }


    @Test
    public void getEquestrian_notPresent_returnNull() {
        String testObjectString = new String("getEquestrian_notPresent_returnNull");
        Assert.assertNull(summit.getEquestrian(testObjectString));
    }

    @Test
    public void getEquestrian_present_returnObject() {
        String testObjectString = new String("getEquestrian_present_returnObject");
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(testObjectString);
        summit.addEquestrian(testEquestrian);

        Equestrian ret = summit.getEquestrian(testObjectString);
        Assert.assertNotNull(ret);
        Assert.assertSame(testEquestrian, ret);
    }

    @Test
    public void addCompetition_null_returnFalse() {
        Assert.assertFalse(summit.addCompetition(null));
    }

    @Test
    public void addCompetition_singleEntry_returnTrue() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(new String("addCompetition_singleEntry_returnTrue"));
        Assert.assertTrue(summit.addCompetition(testCompetition));
    }

    @Test
    public void addCompetition_duplicateEntry_returnFalse() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(new String("addCompetition_duplicateEntry_returnFalse"));

        summit.addCompetition(testCompetition);
        Assert.assertFalse(summit.addCompetition(testCompetition));
    }

    @Test
    public void addCompetition_multiEntry_returnTrue() {
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition1.toString()).thenReturn(new String("multiEntry1"));

        Competition testCompetition2 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition2.toString()).thenReturn(new String("multiEntry2"));

        Competition testCompetition3 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition3.toString()).thenReturn(new String("multiEntry3"));

        summit.addCompetition(testCompetition1);
        summit.addCompetition(testCompetition2);

        Assert.assertTrue(summit.addCompetition(testCompetition3));
    }

    @Test
    public void addCompetition_present_returnFalse() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(new String("addCompetition_present_returnFalse"));

        summit.addCompetition(testCompetition);
        Assert.assertFalse(summit.addCompetition(testCompetition));
    }

    @Test
    public void removeCompetition_null_returnFalse() {
        Assert.assertFalse(summit.removeCompetition(null));
    }

    @Test
    public void removeCompetition_notPresent_returnFalse() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(new String("removeCompetition_notPresent_returnFalse"));

        Assert.assertFalse(summit.removeCompetition(testCompetition));
    }

    @Test
    public void removeCompetition_present_returnTrue() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(new String("removeCompetition_present_returnTrue"));
        summit.addCompetition(testCompetition);

        Assert.assertTrue(summit.removeCompetition(testCompetition));
    }

    @Test
    public void getCompetition_null_returnNull() {
        Assert.assertNull(summit.getCompetition(null));
    }


    @Test
    public void getCompetition_notPresent_returnNull() {
        String testObjectString = new String("getCompetition_notPresent_returnNull");
        Assert.assertNull(summit.getCompetition(testObjectString));
    }

    @Test
    public void getCompetition_present_returnObject() {
        String testObjectString = new String("getCompetition_present_returnObject");
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(testObjectString);
        summit.addCompetition(testCompetition);

        Competition ret = summit.getCompetition(testObjectString);
        Assert.assertNotNull(ret);
        Assert.assertSame(testCompetition, ret);
    }

    @Test
    public void toString_correct() {
        String testString = new String("toString_correct");
        summit.setName(testString);
        Assert.assertEquals(testString, summit.toString());
    }

    @Test
    public void getEquestrians_empty_notNull() {
        Equestrian[] ret = summit.getEquestrians();
        Assert.assertNotNull(ret);
    }

    @Test
    public void getEquestrians_elements_returnAllElements() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian1.toString()).thenReturn(new String("multiEntry1"));
        summit.addEquestrian(testEquestrian1);

        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian2.toString()).thenReturn(new String("multiEntry2"));
        summit.addEquestrian(testEquestrian2);

        Equestrian testEquestrian3 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian3.toString()).thenReturn(new String("multiEntry3"));
        summit.addEquestrian(testEquestrian3);

        Equestrian[] ret = summit.getEquestrians();
        Assert.assertEquals(3, ret.length);

        boolean allReturned =
            (ret[0] == testEquestrian1 || ret[1] == testEquestrian1 || ret[2] == testEquestrian1)
            && (ret[0] == testEquestrian2 || ret[1] == testEquestrian2 || ret[2] == testEquestrian2)
            &&
            (ret[0] == testEquestrian3 || ret[1] == testEquestrian3 || ret[2] == testEquestrian3);
        Assert.assertTrue(allReturned);
    }

    @Test
    public void getEquestrians_elements_sortedByString() {
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian1.toString()).thenReturn(new String("multiEntry1"));

        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian2.toString()).thenReturn(new String("multiEntry2"));

        Equestrian testEquestrian3 = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian3.toString()).thenReturn(new String("multiEntry3"));

        summit.addEquestrian(testEquestrian2);
        summit.addEquestrian(testEquestrian1);
        summit.addEquestrian(testEquestrian3);

        Equestrian[] ret = summit.getEquestrians();
        Assert.assertSame(ret[0], testEquestrian1);
        Assert.assertSame(ret[1], testEquestrian2);
        Assert.assertSame(ret[2], testEquestrian3);
    }

    @Test
    public void getCompetitions_empty_notNull() {
        Competition[] ret = summit.getCompetitions();
        Assert.assertNotNull(ret);
    }

    @Test
    public void getCompetitions_elements_returnAllElements() {
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition1.toString()).thenReturn(new String("multiEntry1"));
        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        summit.addCompetition(testCompetition1);

        Competition testCompetition2 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition2.toString()).thenReturn(new String("multiEntry2"));
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        summit.addCompetition(testCompetition2);

        Competition testCompetition3 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition3.toString()).thenReturn(new String("multiEntry3"));
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(3);
        summit.addCompetition(testCompetition3);

        Competition[] ret = summit.getCompetitions();
        Assert.assertEquals(3, ret.length);

        boolean allReturned =
            (ret[0] == testCompetition1 || ret[1] == testCompetition1 || ret[2] == testCompetition1)
            && (ret[0] == testCompetition2 || ret[1] == testCompetition2 || ret[2] == testCompetition2)
            &&
            (ret[0] == testCompetition3 || ret[1] == testCompetition3 || ret[2] == testCompetition3);
        Assert.assertTrue(allReturned);
    }

    @Test
    public void getCompetitions_elements_sortedByStartOrder() {
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition1.toString()).thenReturn(new String("multiEntry1"));
        Mockito.when(testCompetition1.getStartOrder()).thenReturn(2);

        Competition testCompetition2 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition2.toString()).thenReturn(new String("multiEntry2"));
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(1);

        Competition testCompetition3 = Mockito.mock(Competition.class);
        Mockito.when(testCompetition3.toString()).thenReturn(new String("multiEntry3"));
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);

        summit.addCompetition(testCompetition1);
        summit.addCompetition(testCompetition2);
        summit.addCompetition(testCompetition3);

        Competition[] ret = summit.getCompetitions();
        Assert.assertSame(ret[0], testCompetition2);
        Assert.assertSame(ret[1], testCompetition1);
        Assert.assertSame(ret[2], testCompetition3);
    }
}
