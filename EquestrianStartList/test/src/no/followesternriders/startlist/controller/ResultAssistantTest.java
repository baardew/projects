package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.ResultAssistant;
import no.followesternriders.startlist.controller.ControllerStatus;

import no.followesternriders.startlist.model.Result;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.mockito.Mockito;

public class ResultAssistantTest {

    private Result mockResult;
    private ResultAssistant assistant;

    @Before
    public void initialize() {
        mockResult = Mockito.mock(Result.class);
        assistant = new ResultAssistant(mockResult);
    }

    @After
    public void teardown() {
        mockResult = null;
        assistant = null;
    }

    @Test
    public void setResult_null_returnInvalid() {
        Mockito.when(mockResult.setResult(Mockito.anyString())).thenReturn(false);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setResult(null));
    }

    @Test
    public void setResult_valid_returnSuccess() {
        String testResult = "setResult_valid_returnSuccess";
        Mockito.when(mockResult.setResult(Mockito.anyString())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Success, assistant.setResult(testResult));
    }

}
