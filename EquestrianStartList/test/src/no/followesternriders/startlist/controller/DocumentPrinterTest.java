package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.DocumentPrinter;
import no.followesternriders.startlist.controller.ControllerStatus;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.ResultType;

import testutils.TestUtil;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.invocation.InvocationOnMock;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DocumentPrinter.class)
public class DocumentPrinterTest {

    private Summit mockSummit;
    private final String testCompetition = "TestCompetition";
    private final String markupTestFolder = "test/HTMLTestMarkupPages/";
    private final String bgColor = "LightGreen";
    private ResourceBundle locale;

    @Before
    public void initialize() {
        locale = ResourceBundle.getBundle("LanguageBundle", new Locale("en"));
        mockSummit = Mockito.mock(Summit.class);
        Mockito.when(mockSummit.getName()).thenReturn("TEST SUMMIT");
        DocumentPrinter.initialize(mockSummit, locale, bgColor);
        DocumentPrinter.setLogoImage("logo");

        PowerMockito.spy(DocumentPrinter.class);
        try {
            PowerMockito.doNothing().when(DocumentPrinter.class, "printDocument", Mockito.anyString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        DocumentPrinter.setEnumerateResults(false);
    }

    @After
    public void teardown() {
        DocumentPrinter.initialize(null, null, null);
        locale = null;
        mockSummit = null;
    }

    private void verifyPrintCall(String expected, int times) {
        PowerMockito.verifyStatic(Mockito.times(times));
        try {
            Method printMethod = TestUtil.getPrivateMethod(new DocumentPrinter(), "printDocument", String.class);
            TestUtil.invokePrivateMethod(new DocumentPrinter(), printMethod, expected);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    private String readFile(String file) {
        try {
            String ls = System.getProperty("line.separator");
            BufferedReader reader = new BufferedReader(new FileReader(markupTestFolder + file));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null ) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            reader.close();

            return stringBuilder.toString();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void mockSummit(boolean withResult, boolean noResult, boolean textResult) {
        Equestrian[] equestrians = new Equestrian[3];
        equestrians[0] = mockEquestrian(3, "Doffen", "Gamma");
        equestrians[1] = mockEquestrian(1, "Ole", "Alpha");
        equestrians[2] = mockEquestrian(2, "Dole", "Beta");

        Competition competition = Mockito.mock(Competition.class);
        ArrayList<Equestrian> listEquestrians = new ArrayList<Equestrian>(Arrays.asList(equestrians));
        Mockito.when(competition.getName()).thenReturn("Test Competition");
        Mockito.when(competition.getDetails()).thenReturn("Some details here.");
        Mockito.when(competition.getEquestrians()).thenReturn(listEquestrians);

        if (textResult)
            Mockito.when(competition.getResultType()).thenReturn(ResultType.Text);
        else
            Mockito.when(competition.getResultType()).thenReturn(ResultType.Score);

        if (withResult) {
            if (noResult)
                mockResult(equestrians[0], competition, "No Result");
            else
                mockResult(equestrians[0], competition, "77");

            mockResult(equestrians[1], competition, "55");
            mockResult(equestrians[2], competition, "66");
        }

        Mockito.when(mockSummit.getEquestrians()).thenReturn(equestrians);
        Mockito.when(mockSummit.getCompetition(testCompetition)).thenReturn(competition);
    }

    private Equestrian mockEquestrian(int startNo, String riderName, String horseName) {
        Equestrian equestrian = Mockito.mock(Equestrian.class);
        Mockito.when(equestrian.getStartNumber()).thenReturn(startNo);
        Mockito.when(equestrian.getRiderName()).thenReturn(riderName);
        Mockito.when(equestrian.getHorseName()).thenReturn(horseName);
        return equestrian;
    }

    private void mockResult(Equestrian mockEquestrian, Competition mockCompetition, String resultValue) {
        Result result = Mockito.mock(Result.class);
        Mockito.when(result.getResult()).thenReturn(resultValue);
        Mockito.when(mockEquestrian.getResult(mockCompetition)).thenReturn(result);
    }

    @Test
    public void initialize_nullSummit_throwExceptionsOnCalls() {
        DocumentPrinter.initialize(null, locale, bgColor);
        boolean threwException = false;

        try {
            DocumentPrinter.printStartNumbers();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printStartNumbers: No exceptions or wrong exception thrown");

        threwException = false;
        try {
            DocumentPrinter.printCompetition(testCompetition);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printCompetition: No exceptions or wrong exception thrown");

        threwException = false;
        try {
            DocumentPrinter.printResults(testCompetition, null);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printResults: No exceptions or wrong exception thrown");


        threwException = false;
        try {
            DocumentPrinter.printStartList();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printStartList: No exceptions or wrong exception thrown");
    }

    @Test
    public void initialize_nullLocale_throwExceptionsOnCalls() {
        DocumentPrinter.initialize(mockSummit, null, bgColor);
        boolean threwException = false;

        try {
            DocumentPrinter.printStartNumbers();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printStartNumbers: No exceptions or wrong exception thrown");

        threwException = false;
        try {
            DocumentPrinter.printCompetition(testCompetition);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printCompetition: No exceptions or wrong exception thrown");

        threwException = false;
        try {
            DocumentPrinter.printResults(testCompetition, null);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printResults: No exceptions or wrong exception thrown");

        threwException = false;
        try {
            DocumentPrinter.printStartList();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printStartList: No exceptions or wrong exception thrown");
    }

    @Test
    public void initialize_nullBGColor_throwExceptionsOnCalls() {
        DocumentPrinter.initialize(mockSummit, locale, null);
        boolean threwException = false;

        try {
            DocumentPrinter.printStartNumbers();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printStartNumbers: No exceptions or wrong exception thrown");

        threwException = false;
        try {
            DocumentPrinter.printCompetition(testCompetition);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printCompetition: No exceptions or wrong exception thrown");

        threwException = false;
        try {
            DocumentPrinter.printResults(testCompetition, null);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printResults: prNo exceptions or wrong exception thrown");

        threwException = false;
        try {
            DocumentPrinter.printStartList();
        } catch (Throwable e) {
            threwException = e.getMessage().equals("DocumentPrinter not initialized");
        }

        if (!threwException)
            Assert.fail("printStartList: prNo exceptions or wrong exception thrown");
    }

    @Test
    public void printStartNumbers_empty_noPrint() {
        Mockito.when(mockSummit.getEquestrians()).thenReturn(new Equestrian[0]);
        DocumentPrinter.printStartNumbers();
        verifyPrintCall("", 0);
    }

    @Test
    public void printStartNumbers_empty_returnEmpty() {
        Mockito.when(mockSummit.getEquestrians()).thenReturn(new Equestrian[0]);
        Assert.assertEquals(ControllerStatus.Empty, DocumentPrinter.printStartNumbers());
    }

    @Test
    public void printStartNumbers_filled_doPrint() {
        mockSummit(true, false, false);
        DocumentPrinter.printStartNumbers();
        verifyPrintCall(readFile("startNumbers.html"), 1);
    }

    @Test
    public void printStartNumbers_filled_returnSuccess() {
        mockSummit(true, false, false);
        Assert.assertEquals(ControllerStatus.Success, DocumentPrinter.printStartNumbers());
    }

    @Test
    public void printCompetition_nonExistent_noPrint() {
        String testString = "nonExistant_competition";
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(null);
        DocumentPrinter.printCompetition(testString);
    }

    @Test
    public void printCompetition_nonExistent_returnInvalid() {
        String testString = "nonExistant_competition";
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(null);
        Assert.assertEquals(ControllerStatus.Invalid, DocumentPrinter.printCompetition(testString));
    }

    @Test
    public void printCompetition_noEquestrians_noPrint() {
        String testString = "emptyCompetition";
        Competition competition = Mockito.mock(Competition.class);
        Mockito.when(competition.getName()).thenReturn("Test Competition");
        Mockito.when(competition.getDetails()).thenReturn("Some details here.");
        Mockito.when(competition.getEquestrians()).thenReturn(new ArrayList<Equestrian>());
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(competition);

        DocumentPrinter.printCompetition(testString);
        verifyPrintCall("", 0);
    }

    @Test
    public void printCompetition_noEquestrians_returnEmpty() {
        String testString = "emptyCompetition";
        Competition competition = Mockito.mock(Competition.class);
        Mockito.when(competition.getName()).thenReturn("Test Competition");
        Mockito.when(competition.getDetails()).thenReturn("Some details here.");
        Mockito.when(competition.getEquestrians()).thenReturn(new ArrayList<Equestrian>());
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(competition);
        Assert.assertEquals(ControllerStatus.Empty, DocumentPrinter.printCompetition(testString));
    }

    @Test
    public void printCompetition_filled_doPrint() {
        mockSummit(true, false, false);
        DocumentPrinter.printCompetition(testCompetition);
        verifyPrintCall(readFile("competitionList.html"), 1);
    }

    @Test
    public void printCompetition_filled_returnSuccess() {
        mockSummit(true, false, false);
        Assert.assertEquals(ControllerStatus.Success, DocumentPrinter.printCompetition(testCompetition));
    }

    @Test
    public void printResults_nonExistent_noPrint() {
        String testString = "nonExistant_competition";
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(null);
        DocumentPrinter.printResults(testString, null);
        verifyPrintCall("", 0);
    }

    @Test
    public void printResults_nonExistent_returnInvalid() {
        String testString = "nonExistant_competition";
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(null);
        Assert.assertEquals(ControllerStatus.Invalid, DocumentPrinter.printResults(testString, null));
    }

    @Test
    public void printResults_noEquestrians_noPrint() {
        String testString = "emptyCompetition";
        Competition competition = Mockito.mock(Competition.class);
        Mockito.when(competition.getName()).thenReturn("Test Competition");
        Mockito.when(competition.getDetails()).thenReturn("Some details here.");
        Mockito.when(competition.getEquestrians()).thenReturn(new ArrayList<Equestrian>());
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(competition);

        DocumentPrinter.printResults(testString, null);
        verifyPrintCall("", 0);
    }

    @Test
    public void printResults_noEquestrians_returnEmpty() {
        String testString = "emptyCompetition";
        Competition competition = Mockito.mock(Competition.class);
        Mockito.when(competition.getName()).thenReturn("Test Competition");
        Mockito.when(competition.getDetails()).thenReturn("Some details here.");
        Mockito.when(competition.getEquestrians()).thenReturn(new ArrayList<Equestrian>());
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(competition);

        Assert.assertEquals(ControllerStatus.Empty, DocumentPrinter.printResults(testString, null));
    }

    @Test
    public void printResults_filled_doPrint() {
        mockSummit(true, false, false);
        DocumentPrinter.printResults(testCompetition, null);
        verifyPrintCall(readFile("resultList.html"), 1);
    }

    @Test
    public void printResults_filled_returnSuccess() {
        mockSummit(true, false, false);
        Assert.assertEquals(ControllerStatus.Success, DocumentPrinter.printResults(testCompetition, null));
    }

    @Test
    public void printResults_filledEnumerated_doPrint() {
        mockSummit(true, false, false);
        DocumentPrinter.setEnumerateResults(true);
        DocumentPrinter.printResults(testCompetition, null);
        verifyPrintCall(readFile("resultListEnumerated.html"), 1);
    }

    @Test
    public void printResults_filledEnumerated_returnSuccess() {
        mockSummit(true, false, false);
        DocumentPrinter.setEnumerateResults(true);
        Assert.assertEquals(ControllerStatus.Success, DocumentPrinter.printResults(testCompetition, null));
    }

    @Test
    public void printResults_filledEnumeratedNoResult_doPrint() {
        mockSummit(true, true, false);
        DocumentPrinter.setEnumerateResults(true);
        DocumentPrinter.printResults(testCompetition, null);
        verifyPrintCall(readFile("resultListEnumeratedNoResult.html"), 1);
    }

    @Test
    public void printResults_textResultType_doPrintNoEnumerate() {
        mockSummit(true, false, true);
        DocumentPrinter.setEnumerateResults(true);
        DocumentPrinter.printResults(testCompetition, null);
        verifyPrintCall(readFile("resultList.html"), 1);
    }

    @Test
    public void printResults_filledEnumeratedNoResult_returnSuccess() {
        mockSummit(true, true, false);
        DocumentPrinter.setEnumerateResults(true);
        Assert.assertEquals(ControllerStatus.Success, DocumentPrinter.printResults(testCompetition, null));
    }

    @Test
    public void printResults_filledNoResult_doPrintWithEmptyResult() {
        mockSummit(false, false, false);
        DocumentPrinter.printResults(testCompetition, null);
        verifyPrintCall(readFile("resultListNoResult.html"), 1);
    }

    @Test
    public void printResults_filledNoResult_returnSuccess() {
        mockSummit(false, false, false);
        Assert.assertEquals(ControllerStatus.Success, DocumentPrinter.printResults(testCompetition, null));
    }

    @Test
    public void printStartList_filled_doPrint() {
        Equestrian[] equestrians = new Equestrian[3];
        equestrians[0] = mockEquestrian(3, "Doffen", "Gamma");
        equestrians[1] = mockEquestrian(1, "Ole", "Alpha");
        equestrians[2] = mockEquestrian(2, "Dole", "Beta");
        ArrayList<Equestrian> listEquestrians = new ArrayList<Equestrian>(Arrays.asList(equestrians));

        Competition competition1 = Mockito.mock(Competition.class);
        Mockito.when(competition1.getStartOrder()).thenReturn(1);
        Mockito.when(competition1.getName()).thenReturn("Competition ONE");
        Mockito.when(competition1.getStartTime()).thenReturn("TIME");
        Mockito.when(competition1.getDetails()).thenReturn("Some details here.");
        Mockito.when(competition1.getEquestrians()).thenReturn(listEquestrians);

        Competition competition2 = Mockito.mock(Competition.class);
        Mockito.when(competition2.getStartOrder()).thenReturn(2);
        Mockito.when(competition2.getStartTime()).thenReturn("TIME");
        Mockito.when(competition2.getName()).thenReturn("Competition TWO");
        Mockito.when(competition2.getDetails()).thenReturn("Some details here.");
        Mockito.when(competition2.getEquestrians()).thenReturn(listEquestrians);

        Competition competition3 = Mockito.mock(Competition.class);
        Mockito.when(competition3.getStartOrder()).thenReturn(3);
        Mockito.when(competition3.getStartTime()).thenReturn("TIME");
        Mockito.when(competition3.getName()).thenReturn("Competition THREE");
        Mockito.when(competition3.getDetails()).thenReturn("Some details here.");
        Mockito.when(competition3.getEquestrians()).thenReturn(listEquestrians);

        Competition[] listCompetitions = new Competition[3];
        listCompetitions[0] = (competition1);
        listCompetitions[1] = (competition2);
        listCompetitions[2] = (competition3);
        Mockito.when(mockSummit.getCompetitions()).thenReturn(listCompetitions);

        DocumentPrinter.printStartList();
        verifyPrintCall(readFile("startList.html"), 1);
    }
}
