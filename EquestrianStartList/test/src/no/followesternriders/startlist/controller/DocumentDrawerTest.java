package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.DocumentDrawer;

import java.util.ArrayList;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.mockito.Mockito;

public class DocumentDrawerTest {

    private DocumentDrawer documentDrawer;

    private final String markupTestFolder = "test/HTMLTestMarkupPages/";

    private final String defaultTitle = "DOCUMENT TITLE";
    private final String defaultSummitName = "EquestrianStartList";
    private final String defaultImageFilename = "logo";
    private final String defaultDetails = "timestamp";
    private final String defaultBGColor = "LightGreen";

    private final String[] tableHeader = {"Column1", "ColumnA", "ColumnAlpha"};
    private final String[][] tableEntries = {{"1", "2", "3"}, {"A", "B", "C"}, {"Alpha", "Beta", "Gamma"}};

    private ArrayList<String[]> table;

    @Before
    public void initialize() {
        table = new ArrayList<String[]>();
        table.add(tableEntries[0]);
        table.add(tableEntries[1]);
        table.add(tableEntries[2]);

        documentDrawer = new DocumentDrawer(defaultTitle, defaultSummitName, defaultImageFilename, defaultDetails, defaultBGColor);
    }

    @After
    public void teardown() {
        documentDrawer = null;
        table = null;
    }

    @Test
    public void emptyDocument_correctMarkup() {
        Assert.assertEquals(readFile("empty.html"), documentDrawer.getHTML());
    }

    @Test
    public void noImage_correctMarkup() {
        documentDrawer = new DocumentDrawer(defaultTitle, defaultSummitName, "", defaultDetails, defaultBGColor);
        Assert.assertEquals(readFile("noImage.html"), documentDrawer.getHTML());
    }

    @Test
    public void addTable_nullHeader_returnFalse() {
        Assert.assertFalse(documentDrawer.addTable(null, table));
    }

    @Test
    public void addTable_nullTable_returnFalse() {
        Assert.assertFalse(documentDrawer.addTable(tableHeader, null));
    }

    @Test
    public void addTable_tableSizeHeaderDifference_returnFalse() {
        String[] header = {"MissingHeaders"};
        Assert.assertFalse(documentDrawer.addTable(header, table));
    }

    @Test
    public void addTable_headerSizeTableDifference_returnFalse() {
        String[] smallLine = {"MissingEntries"};
        table.add(smallLine);
        Assert.assertFalse(documentDrawer.addTable(tableHeader, table));
    }

    @Test
    public void addTable_emptyHeader_returnFalse() {
        String[] header = {};
        Assert.assertFalse(documentDrawer.addTable(header, table));
    }

    @Test
    public void addTable_emptyTable_returnFalse() {
        ArrayList<String[]> table = new ArrayList<String[]>();
        Assert.assertFalse(documentDrawer.addTable(tableHeader, table));
    }

    @Test
    public void addTable_small_returTrue() {
        Assert.assertTrue(documentDrawer.addTable(tableHeader, table));
    }

    @Test
    public void addTable_small_correctHTML() {
        table = new ArrayList<String[]>();
        table.add(tableEntries[0]);
        documentDrawer.addTable(tableHeader, table);
        Assert.assertEquals(readFile("smallTable.html"), documentDrawer.getHTML());
    }

    @Test
    public void addTable_large_correctHTML() {
        documentDrawer.addTable(tableHeader, table);
        Assert.assertEquals(readFile("largeTable.html"), documentDrawer.getHTML());
    }

    private String readFile(String file) {
        try {
            String ls = System.getProperty("line.separator");
            BufferedReader reader = new BufferedReader(new FileReader(markupTestFolder + file));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null ) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

            reader.close();
            return stringBuilder.toString();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
