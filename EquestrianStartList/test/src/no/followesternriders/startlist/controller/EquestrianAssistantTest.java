package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.Secretary;
import no.followesternriders.startlist.controller.CompetitionAssistant;
import no.followesternriders.startlist.controller.ControllerStatus;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.DataFactory;

import testutils.TestVariableAnswer;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DataFactory.class)
public class EquestrianAssistantTest {

    private EquestrianAssistant assistant;
    private Summit mockSummit;
    private Equestrian mockEquestrian;
    private Equestrian cloneMockEquestrian;

    @Before
    public void initialize() {
        mockSummit = Mockito.mock(Summit.class);
        mockEquestrian = Mockito.mock(Equestrian.class);
        cloneMockEquestrian = Mockito.mock(Equestrian.class);
        assistant = new EquestrianAssistant(mockSummit, mockEquestrian);

        PowerMockito.mockStatic(DataFactory.class);
        Mockito.when(DataFactory.cloneEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(cloneMockEquestrian);
    }

    @After
    public void teardown() {
        assistant = null;
        mockSummit = null;
        mockEquestrian = null;
        cloneMockEquestrian = null;
    }

    @Test
    public void setRiderName_null_returnInvalid() {
        Mockito.when(cloneMockEquestrian.setRiderName(Mockito.anyString())).thenReturn(false);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setRiderName(null));
    }

    @Test
    public void setRiderName_valid_returnSuccess() {
        String testName = "setRiderName_valid_returnSuccess";
        Mockito.when(cloneMockEquestrian.setRiderName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);

        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.removeEquestrian(mockEquestrian)).thenReturn(true);
        Mockito.when(testCompetition.addEquestrian(cloneMockEquestrian)).thenReturn(true);
        ArrayList<Competition> competitions = new ArrayList<Competition>();
        competitions.add(testCompetition);
        Mockito.when(cloneMockEquestrian.getCompetitions()).thenReturn(competitions);


        Assert.assertEquals(ControllerStatus.Success, assistant.setRiderName(testName));
    }

    @Test
    public void setRiderName_invalid_returnInvalid() {
        String testName = "setRiderName_invalid_returnInvalid";
        Mockito.when(cloneMockEquestrian.setRiderName(Mockito.anyString())).thenReturn(false);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setRiderName(testName));
    }

    @Test
    public void setRiderName_exists_returnExists() {
        String testName = "setRiderName_exists_returnExists";
        Mockito.when(cloneMockEquestrian.setRiderName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(false);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Exists, assistant.setRiderName(testName));
    }

    @Test
    public void setRiderName_exists_notChanged() {
        String testNameWrong = "setRiderName_exists_notChanged_Wrong";
        String testNameCorrect = "setRiderName_exists_notChanged_Correct";
        TestVariableAnswer<Boolean, String> nameTest = new TestVariableAnswer<Boolean, String>(false, testNameCorrect);
        Mockito.when(mockEquestrian.setRiderName(Mockito.anyString())).thenAnswer(nameTest);
        Mockito.when(cloneMockEquestrian.setRiderName(Mockito.anyString())).thenReturn(false);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(false);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        assistant.setRiderName(testNameWrong);
        Assert.assertEquals(testNameCorrect, nameTest.getVariable());
    }

    @Test
    public void setRiderName_existsNoRemove_throwException() {
        String testName = "setRiderName_existsNoRemove_throwExceptions";
        Mockito.when(cloneMockEquestrian.setRiderName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(false);
        try {
            assistant.setRiderName(testName);
        } catch (Exception e) {
            return;
        }

        Assert.fail("Did not throw exception on error");
    }


    @Test
    public void setRiderName_failedNotificationRemove_throwException() {
        String testName = "setRiderName_failedNotificationRemove_throwException";
        Mockito.when(cloneMockEquestrian.setRiderName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);

        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.removeEquestrian(mockEquestrian)).thenReturn(false);
        Mockito.when(testCompetition.addEquestrian(cloneMockEquestrian)).thenReturn(true);
        ArrayList<Competition> competitions = new ArrayList<Competition>();
        competitions.add(testCompetition);
        Mockito.when(cloneMockEquestrian.getCompetitions()).thenReturn(competitions);

        try {
            assistant.setRiderName(testName);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Should throw exception");
    }

    @Test
    public void setRiderName_failedNotificationAdd_throwException() {
        String testName = "setRiderName_failedNotificationAdd_throwException";
        Mockito.when(cloneMockEquestrian.setRiderName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);


        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.removeEquestrian(mockEquestrian)).thenReturn(true);
        Mockito.when(testCompetition.addEquestrian(cloneMockEquestrian)).thenReturn(false);
        ArrayList<Competition> competitions = new ArrayList<Competition>();
        competitions.add(testCompetition);
        Mockito.when(cloneMockEquestrian.getCompetitions()).thenReturn(competitions);

        try {
            assistant.setRiderName(testName);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Should throw exception");
    }

    @Test
    public void setHorseName_null_returnInvalid() {
        Mockito.when(cloneMockEquestrian.setHorseName(Mockito.anyString())).thenReturn(false);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setHorseName(null));
    }

    @Test
    public void setHorseName_valid_returnSuccess() {
        String testName = "setHorseName_valid_returnSuccess";
        Mockito.when(cloneMockEquestrian.setHorseName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);

        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.removeEquestrian(mockEquestrian)).thenReturn(true);
        Mockito.when(testCompetition.addEquestrian(cloneMockEquestrian)).thenReturn(true);
        ArrayList<Competition> competitions = new ArrayList<Competition>();
        competitions.add(testCompetition);
        Mockito.when(cloneMockEquestrian.getCompetitions()).thenReturn(competitions);

        Assert.assertEquals(ControllerStatus.Success, assistant.setHorseName(testName));
    }

    @Test
    public void setHorseName_invalid_returnInvalid() {
        String testName = "setHorseName_invalid_returnInvalid";
        Mockito.when(cloneMockEquestrian.setHorseName(Mockito.anyString())).thenReturn(false);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setHorseName(testName));
    }

    @Test
    public void setHorseName_exists_returnExists() {
        String testName = "setHorseName_exists_returnExists";
        Mockito.when(cloneMockEquestrian.setHorseName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(false);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Exists, assistant.setHorseName(testName));
    }

    @Test
    public void setHorseName_exists_notChanged() {
        String testNameWrong = "setHorseName_exists_notChanged_Wrong";
        String testNameCorrect = "setHorseName_exists_notChanged_Correct";
        TestVariableAnswer<Boolean, String> nameTest = new TestVariableAnswer<Boolean, String>(false, testNameCorrect);
        Mockito.when(mockEquestrian.setHorseName(Mockito.anyString())).thenAnswer(nameTest);
        Mockito.when(cloneMockEquestrian.setHorseName(Mockito.anyString())).thenReturn(false);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(false);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        assistant.setHorseName(testNameWrong);
        Assert.assertEquals(testNameCorrect, nameTest.getVariable());
    }

    @Test
    public void setHorseName_existsNoRemove_throwException() {
        String testName = "setHorseName_existsNoRemove_throwExceptions";
        Mockito.when(cloneMockEquestrian.setHorseName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(false);
        try {
            assistant.setHorseName(testName);
        } catch (Exception e) {
            return;
        }

        Assert.fail("Did not throw exception on error");
    }

    @Test
    public void setHorseName_failedNotificationRemove_throwException() {
        String testName = "setHorseName_failedNotificationRemove_throwException";
        Mockito.when(cloneMockEquestrian.setHorseName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);

        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.removeEquestrian(mockEquestrian)).thenReturn(false);
        Mockito.when(testCompetition.addEquestrian(cloneMockEquestrian)).thenReturn(true);
        ArrayList<Competition> competitions = new ArrayList<Competition>();
        competitions.add(testCompetition);
        Mockito.when(cloneMockEquestrian.getCompetitions()).thenReturn(competitions);

        try {
            assistant.setHorseName(testName);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Should throw exception");
    }

    @Test
    public void setHorseName_failedNotificationAdd_throwException() {
        String testName = "setHorseName_failedNotificationAdd_throwException";
        Mockito.when(cloneMockEquestrian.setHorseName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);


        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.removeEquestrian(mockEquestrian)).thenReturn(true);
        Mockito.when(testCompetition.addEquestrian(cloneMockEquestrian)).thenReturn(false);
        ArrayList<Competition> competitions = new ArrayList<Competition>();
        competitions.add(testCompetition);
        Mockito.when(cloneMockEquestrian.getCompetitions()).thenReturn(competitions);

        try {
            assistant.setHorseName(testName);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Should throw exception");
    }

    @Test
    public void getCompetitions_empty_returnEmptyArray() {
        Mockito.when(mockEquestrian.getCompetitions()).thenReturn(new ArrayList<Competition>());
        String[] ret = assistant.getCompetitions();
        Assert.assertNotNull(ret);
        Assert.assertEquals(0, ret.length);
    }

    @Test
    public void getCompetitions_filled_returnAllElements() {
        Competition[] competitions = new Competition[3];
        competitions[0] = Mockito.mock(Competition.class);
        competitions[1] = Mockito.mock(Competition.class);
        competitions[2] = Mockito.mock(Competition.class);
        Mockito.when(competitions[0].toString()).thenReturn("competition_a");
        Mockito.when(competitions[1].toString()).thenReturn("competition_c");
        Mockito.when(competitions[2].toString()).thenReturn("competition_b");
        ArrayList<Competition> listedCompetitions = new ArrayList<Competition>(Arrays.asList(competitions));
        Mockito.when(mockEquestrian.getCompetitions()).thenReturn(listedCompetitions);

        Assert.assertEquals(3, assistant.getCompetitions().length);
    }

    @Test
    public void getCompetitions_filled_sorted() {
        Competition[] competitions = new Competition[3];
        competitions[0] = Mockito.mock(Competition.class);
        competitions[1] = Mockito.mock(Competition.class);
        competitions[2] = Mockito.mock(Competition.class);
        Mockito.when(competitions[0].toString()).thenReturn("competition_a");
        Mockito.when(competitions[1].toString()).thenReturn("competition_c");
        Mockito.when(competitions[2].toString()).thenReturn("competition_b");
        ArrayList<Competition> listedCompetitions = new ArrayList<Competition>(Arrays.asList(competitions));
        Mockito.when(mockEquestrian.getCompetitions()).thenReturn(listedCompetitions);

        String[] ret = assistant.getCompetitions();
        Assert.assertEquals("competition_a", ret[0]);
        Assert.assertEquals("competition_b", ret[1]);
        Assert.assertEquals("competition_c", ret[2]);
    }

    @Test
    public void getTotalStartPrice_filled_returnSum() {
        Competition[] competitions = new Competition[3];
        competitions[0] = Mockito.mock(Competition.class);
        competitions[1] = Mockito.mock(Competition.class);
        competitions[2] = Mockito.mock(Competition.class);
        Mockito.when(competitions[0].toString()).thenReturn("competition_a");
        Mockito.when(competitions[1].toString()).thenReturn("competition_c");
        Mockito.when(competitions[2].toString()).thenReturn("competition_b");
        Mockito.when(competitions[0].getStartPrice()).thenReturn(1);
        Mockito.when(competitions[1].getStartPrice()).thenReturn(2);
        Mockito.when(competitions[2].getStartPrice()).thenReturn(3);

        ArrayList<Competition> listedCompetitions = new ArrayList<Competition>(Arrays.asList(competitions));
        Mockito.when(mockEquestrian.getCompetitions()).thenReturn(listedCompetitions);

        Assert.assertEquals("6", assistant.getTotalStartPrice());
    }

    @Test
    public void getTotalStartPrice_empty_returnSum() {
        ArrayList<Competition> listedCompetitions = new ArrayList<Competition>();
        Mockito.when(mockEquestrian.getCompetitions()).thenReturn(listedCompetitions);
        Assert.assertEquals("0", assistant.getTotalStartPrice());
    }
}
