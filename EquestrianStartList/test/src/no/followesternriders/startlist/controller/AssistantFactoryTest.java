package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.AssistantFactory;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import org.mockito.Mockito;

public class AssistantFactoryTest {

    private Summit summit;

    @Before
    public void init() {
        summit = new Summit();
        AssistantFactory.initialize(summit);
    }

    @After
    public void teardown() {
        summit = null;
        AssistantFactory.initialize(null);
    }

    @Test
    public void createEquestrianAssistant_uninitializedFactory_throwsException() {
        // Initializing facotry to null is the same as uninitialized
        AssistantFactory.initialize(null);
        boolean threwException = false;
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);

        try {
            AssistantFactory.createEquestrianAssistant(testEquestrian);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("AssistantFactory not initialized");
        }

        if (!threwException)
            Assert.fail("No exceptions or wrong exception thrown");
    }

    @Test
    public void createEquestrianAssistant_nullObject_returnNull() {
        Assert.assertNull(AssistantFactory.createEquestrianAssistant(null));
    }

    @Test
    public void createEquestrianAssistant_withObject_returnObject() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Assert.assertNotNull(AssistantFactory.createEquestrianAssistant(testEquestrian));
    }

    @Test
    public void createCompetitionAssistant_uninitializedFactory_throwsException() {
        // Initializing facotry to null is the same as uninitialized
        AssistantFactory.initialize(null);
        boolean threwException = false;
        Competition testCompetition = Mockito.mock(Competition.class);

        try {
            AssistantFactory.createCompetitionAssistant(testCompetition);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("AssistantFactory not initialized");
        }

        if (!threwException)
            Assert.fail("No exceptions or wrong exception thrown");
    }

    @Test
    public void createCompetitionAssistant_nullObject_returnNull() {
        Assert.assertNull(AssistantFactory.createCompetitionAssistant(null));
    }

    @Test
    public void createCompetitionAssistant_withObject_returnObject() {
        Competition testCompetition = Mockito.mock(Competition.class);
        Assert.assertNotNull(AssistantFactory.createCompetitionAssistant(testCompetition));
    }

    @Test
    public void createResultAssistant_uninitializedFactory_throwsException() {
        // Initializing facotry to null is the same as uninitialized
        AssistantFactory.initialize(null);
        boolean threwException = false;
        Result testResult = Mockito.mock(Result.class);

        try {
            AssistantFactory.createResultAssistant(testResult);
        } catch (Throwable e) {
            threwException = e.getMessage().equals("AssistantFactory not initialized");
        }

        if (!threwException)
            Assert.fail("No exceptions or wrong exception thrown");
    }

    @Test
    public void createResultAssistant_nullObject_returnNull() {
        Assert.assertNull(AssistantFactory.createResultAssistant(null));
    }

    @Test
    public void createResultAssistant_withObject_returnObject() {
        Result testResult = Mockito.mock(Result.class);
        Assert.assertNotNull(AssistantFactory.createResultAssistant(testResult));
    }
}
