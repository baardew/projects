package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.Secretary;
import no.followesternriders.startlist.controller.CompetitionAssistant;
import no.followesternriders.startlist.controller.ControllerStatus;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.DataFactory;
import no.followesternriders.startlist.model.ResultType;
import no.followesternriders.startlist.model.Result;

import testutils.TestVariableAnswer;

import java.util.Arrays;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DataFactory.class)
public class CompetitionAssistantTest {

    private CompetitionAssistant assistant;
    private Summit mockSummit;
    private Competition mockCompetition;
    private Competition cloneMockCompetition;

    @Before
    public void initialize() {
        mockSummit = Mockito.mock(Summit.class);
        mockCompetition = Mockito.mock(Competition.class);
        cloneMockCompetition = Mockito.mock(Competition.class);
        assistant = new CompetitionAssistant(mockSummit, mockCompetition);

        PowerMockito.mockStatic(DataFactory.class);
        Mockito.when(DataFactory.cloneCompetition(Mockito.<Competition>anyObject())).thenReturn(cloneMockCompetition);
    }

    @After
    public void teardown() {
        assistant = null;
        mockSummit = null;
        mockCompetition = null;
        cloneMockCompetition = null;
    }

    @Test
    public void setName_null_returnInvalid() {
        Mockito.when(cloneMockCompetition.setName(Mockito.anyString())).thenReturn(false);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setName(null));
    }

    @Test
    public void setName_valid_returnSuccess() {
        String testName = "setName_valid_returnSuccess";
        Mockito.when(cloneMockCompetition.setName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeCompetition(Mockito.<Competition>anyObject())).thenReturn(true);

        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.removeCompetition(mockCompetition)).thenReturn(true);
        Mockito.when(testEquestrian.addCompetition(cloneMockCompetition)).thenReturn(true);
        ArrayList<Equestrian> equestrians = new ArrayList<Equestrian>();
        equestrians.add(testEquestrian);
        Mockito.when(cloneMockCompetition.getEquestrians()).thenReturn(equestrians);

        Assert.assertEquals(ControllerStatus.Success, assistant.setName(testName));
    }

    @Test
    public void setName_invalid_returnInvalid() {
        String testName = "setName_invalid_returnInvalid";
        Mockito.when(cloneMockCompetition.setName(Mockito.anyString())).thenReturn(false);
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setName(testName));
    }

    @Test
    public void setName_exists_returnExists() {
        String testName = "setName_exists_returnExists";
        Mockito.when(cloneMockCompetition.setName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(false);
        Mockito.when(mockSummit.removeCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Exists, assistant.setName(testName));
    }

    @Test
    public void setName_exists_notChanged() {
        String testNameWrong = "setName_exists_notChanged_Wrong";
        String testNameCorrect = "setName_exists_notChanged_Correct";
        TestVariableAnswer<Boolean, String> nameTest = new TestVariableAnswer<Boolean, String>(false, testNameCorrect);
        Mockito.when(mockCompetition.setName(Mockito.anyString())).thenAnswer(nameTest);
        Mockito.when(cloneMockCompetition.setName(Mockito.anyString())).thenReturn(false);
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(false);
        Mockito.when(mockSummit.removeCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        assistant.setName(testNameWrong);
        Assert.assertEquals(testNameCorrect, nameTest.getVariable());
    }

    @Test
    public void setName_existsNoRemove_throwException() {
        String testName = "setName_existsNoRemove_throwExceptions";
        Mockito.when(cloneMockCompetition.setName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeCompetition(Mockito.<Competition>anyObject())).thenReturn(false);
        try {
            assistant.setName(testName);
        } catch (Exception e) {
            return;
        }

        Assert.fail("Did not throw exception on error");
    }

    @Test
    public void setName_failedNotificationRemove_throwException() {
        String testName = "setName_failedNotificationRemove_throwException";
        Mockito.when(cloneMockCompetition.setName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeCompetition(Mockito.<Competition>anyObject())).thenReturn(true);

        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.removeCompetition(mockCompetition)).thenReturn(false);
        Mockito.when(testEquestrian.addCompetition(cloneMockCompetition)).thenReturn(true);
        ArrayList<Equestrian> equestrians = new ArrayList<Equestrian>();
        equestrians.add(testEquestrian);
        Mockito.when(cloneMockCompetition.getEquestrians()).thenReturn(equestrians);

        try {
            assistant.setName(testName);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Should throw exception");
    }

    @Test
    public void setName_failedNotificationAdd_throwException() {
        String testName = "setName_failedNotificationAdd_throwException";
        Mockito.when(cloneMockCompetition.setName(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Mockito.when(mockSummit.removeCompetition(Mockito.<Competition>anyObject())).thenReturn(true);

        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.removeCompetition(mockCompetition)).thenReturn(true);
        Mockito.when(testEquestrian.addCompetition(cloneMockCompetition)).thenReturn(false);
        ArrayList<Equestrian> equestrians = new ArrayList<Equestrian>();
        equestrians.add(testEquestrian);
        Mockito.when(cloneMockCompetition.getEquestrians()).thenReturn(equestrians);

        try {
            assistant.setName(testName);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Should throw exception");
    }

    @Test
    public void setDetails_null_returnInvalid() {
        Mockito.when(mockCompetition.setDetails(Mockito.anyString())).thenReturn(false);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setDetails(null));
    }

    @Test
    public void setDetails_valid_returnSuccess() {
        String testDetails = "setDetails_valid_returnSuccess";
        Mockito.when(mockCompetition.setDetails(Mockito.anyString())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Success, assistant.setDetails(testDetails));
    }

    @Test
    public void setRandomizeStartList_invalid_returnInvalid() {
        Mockito.when(mockCompetition.setRandomizeStartList(Mockito.anyBoolean())).thenReturn(false);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setRandomizeStartList(true));
    }

    @Test
    public void setRandomizeStartList_valid_returnSuccess() {
        Mockito.when(mockCompetition.setRandomizeStartList(Mockito.anyBoolean())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Success, assistant.setRandomizeStartList(true));
    }

    @Test
    public void setStartTime_null_returnInvalid() {
        Mockito.when(mockCompetition.setStartTime(Mockito.anyString())).thenReturn(false);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setStartTime(null));
    }

    @Test
    public void setStartTime_valid_returnSuccess() {
        String testStartTime = "setStartTime_valid_returnSuccess";
        Mockito.when(mockCompetition.setStartTime(Mockito.anyString())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Success, assistant.setStartTime(testStartTime));
    }

    @Test
    public void setStartPrice_null_returnInvalid() {
        Mockito.when(mockCompetition.setStartPrice(Mockito.anyInt())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setStartPrice(null));
    }

    @Test
    public void setStartPrice_invalidInput_returnInvalid() {
        String testStartPrice = "-1";
        Mockito.when(mockCompetition.setStartPrice(Mockito.anyInt())).thenReturn(false);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setStartPrice(testStartPrice));
    }

    @Test
    public void setStartPrice_notANumber_returnInvalid() {
        String testStartPrice = "setStartPrice_invalidInput_returnInvalid";
        Mockito.when(mockCompetition.setStartPrice(Mockito.anyInt())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setStartPrice(testStartPrice));
    }

    @Test
    public void setStartPrice_valid_returnSuccess() {
        String testStartPrice = "1";
        Mockito.when(mockCompetition.setStartPrice(Mockito.anyInt())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Success, assistant.setStartPrice(testStartPrice));
    }

    @Test
    public void setStartPrice_noValue_returnSuccessSetToZero() {
        String testStartPrice = "";
        Mockito.when(mockCompetition.setStartPrice(Mockito.anyInt())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Success, assistant.setStartPrice(testStartPrice));
        Mockito.verify(mockCompetition).setStartPrice(0);
    }

    @Test
    public void getEquestrians_empty_returnEmptyArray() {
        Mockito.when(mockCompetition.getEquestrians()).thenReturn(new ArrayList<Equestrian>());
        String[] ret = assistant.getEquestrians();
        Assert.assertNotNull(ret);
        Assert.assertEquals(0, ret.length);
    }

    @Test
    public void getEquestrians_filled_returnAllElements() {
        Equestrian[] equestrians = new Equestrian[3];
        equestrians[0] = Mockito.mock(Equestrian.class);
        equestrians[1] = Mockito.mock(Equestrian.class);
        equestrians[2] = Mockito.mock(Equestrian.class);
        Mockito.when(equestrians[0].toString()).thenReturn("equestrian_a");
        Mockito.when(equestrians[1].toString()).thenReturn("equestrian_c");
        Mockito.when(equestrians[2].toString()).thenReturn("equestrian_b");
        ArrayList<Equestrian> listedEquestrians = new ArrayList<Equestrian>(Arrays.asList(equestrians));
        Mockito.when(mockCompetition.getEquestrians()).thenReturn(listedEquestrians);

        Assert.assertEquals(3, assistant.getEquestrians().length);
    }

    @Test
    public void getEquestrians_filled_sorted() {
        Equestrian[] equestrians = new Equestrian[3];
        equestrians[0] = Mockito.mock(Equestrian.class);
        equestrians[1] = Mockito.mock(Equestrian.class);
        equestrians[2] = Mockito.mock(Equestrian.class);
        Mockito.when(equestrians[0].toString()).thenReturn("equestrian_a");
        Mockito.when(equestrians[1].toString()).thenReturn("equestrian_c");
        Mockito.when(equestrians[2].toString()).thenReturn("equestrian_b");
        ArrayList<Equestrian> listedEquestrians = new ArrayList<Equestrian>(Arrays.asList(equestrians));
        Mockito.when(mockCompetition.getEquestrians()).thenReturn(listedEquestrians);

        String[] ret = assistant.getEquestrians();
        Assert.assertEquals("equestrian_a", ret[0]);
        Assert.assertEquals("equestrian_b", ret[1]);
        Assert.assertEquals("equestrian_c", ret[2]);
    }

    @Test
    public void setResultType_null_returnInvalid() {
        Mockito.when(mockCompetition.setResultType(null)).thenReturn(false);
        Mockito.when(mockCompetition.getResultType()).thenReturn(ResultType.Text);
        Assert.assertEquals(ControllerStatus.Invalid, assistant.setResultType(null));
    }

    @Test
    public void setResultType_same_returnExists() {
        Mockito.when(mockCompetition.setResultType(Mockito.<ResultType>anyObject())).thenReturn(true);
        Mockito.when(mockCompetition.getResultType()).thenReturn(ResultType.Text);
        Assert.assertEquals(ControllerStatus.Exists, assistant.setResultType(ResultType.Text));
    }

    @Test
    public void setResultType_changed_returnSuccessClearResult() {
        String correctResult = "";
        String wrongResult = "wrong result: should be empty";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Result testResult = Mockito.mock(Result.class);
        TestVariableAnswer<Boolean, String> resultValueTest = new TestVariableAnswer<Boolean, String>(false, wrongResult);
        Mockito.when(testResult.setResult(Mockito.anyString())).thenAnswer(resultValueTest);
        Mockito.when(mockCompetition.getResultType()).thenReturn(ResultType.Score);
        ArrayList<Equestrian> listEquestrians = new ArrayList<Equestrian>();
        listEquestrians.add(testEquestrian);
        Mockito.when(mockCompetition.getEquestrians()).thenReturn(listEquestrians);
        Mockito.when(testEquestrian.getResult(mockCompetition)).thenReturn(testResult);

        Mockito.when(mockCompetition.setResultType(Mockito.<ResultType>anyObject())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Success, assistant.setResultType(ResultType.Text));
        Assert.assertEquals(correctResult, resultValueTest.getVariable());
    }

    @Test
    public void setResultType_nullResult_returnSuccess() {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(mockCompetition.getResultType()).thenReturn(ResultType.Score);
        ArrayList<Equestrian> listEquestrians = new ArrayList<Equestrian>();
        listEquestrians.add(testEquestrian);
        Mockito.when(mockCompetition.getEquestrians()).thenReturn(listEquestrians);
        Mockito.when(testEquestrian.getResult(mockCompetition)).thenReturn(null);

        Mockito.when(mockCompetition.setResultType(Mockito.<ResultType>anyObject())).thenReturn(true);
        Assert.assertEquals(ControllerStatus.Success, assistant.setResultType(ResultType.Text));
    }
}
