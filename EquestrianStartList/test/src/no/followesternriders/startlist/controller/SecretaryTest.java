package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.Secretary;
import no.followesternriders.startlist.controller.ResultAssistant;
import no.followesternriders.startlist.controller.CompetitionAssistant;
import no.followesternriders.startlist.controller.EquestrianAssistant;
import no.followesternriders.startlist.controller.AssistantFactory;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.DataFactory;

import testutils.TestUtil;
import testutils.TestVariableAnswer;

import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.invocation.InvocationOnMock;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DataFactory.class, AssistantFactory.class})
public class SecretaryTest {

    private final String testFile = "test/files/Summit.esl";

    private Secretary secretary;
    private Summit mockSummit;
    private ResourceBundle locale;

    @Before
    public void initialize() {
        locale = ResourceBundle.getBundle("LanguageBundle", new Locale("en"));
        File projectFile = new File(testFile);
        secretary = new Secretary(projectFile, Secretary.Constructor.CREATE, locale);
        mockSummit = Mockito.mock(Summit.class);
        TestUtil.<Summit>setPrivateField(secretary, "summit", mockSummit);
        PowerMockito.mockStatic(DataFactory.class);
        PowerMockito.mockStatic(AssistantFactory.class);
    }

    @After
    public void teardown() {
        secretary = null;
        mockSummit = null;
        locale = null;
        new File(testFile).delete();
    }

    private Equestrian mockEquestrianCreation(String testString) {
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(testEquestrian.toString()).thenReturn(testString);
        Mockito.when(DataFactory.createEquestrian()).thenReturn(testEquestrian);
        return testEquestrian;
    }

    private Competition mockCompetitionCreation(String testString) {
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(testCompetition.toString()).thenReturn(testString);
        Mockito.when(DataFactory.createCompetition()).thenReturn(testCompetition);
        return testCompetition;
    }

    @Test
    public void constructor_create_fileCreated() {
        File projectFile = new File(testFile);
        Assert.assertTrue(projectFile.exists());
    }

    @Test
    public void constructor_openNotFound_createFile() {
        secretary = null;
        new File(testFile).delete();
        File projectFile = new File(testFile);
        secretary = new Secretary(projectFile, Secretary.Constructor.OPEN, locale);
        Assert.assertTrue(projectFile.exists());
    }

    @Test
    public void createEquestrian_valid_returnString() {
        mockEquestrianCreation("createEquestrian_valid_returnString");
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Assert.assertNotNull(secretary.createEquestrian());
    }

    @Test
    public void createEquestrian_valid_addToSummit() {
        Equestrian testEquestrian = mockEquestrianCreation("createEquestrian_valid_addToSummit");
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        secretary.createEquestrian();
        Mockito.verify(mockSummit).addEquestrian(testEquestrian);
    }

    @Test
    public void createEquestrian_valid_returnCorrectString() {
        String testString = "createEquestrian_valid_returnCorrectString";
        mockEquestrianCreation(testString);
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(true);
        Assert.assertEquals(testString, secretary.createEquestrian());
    }

    @Test
    public void createEquestrian_insertionFailed_returnNull() {
        mockEquestrianCreation("createEquestrian_valid_returnCorrectString");
        Mockito.when(mockSummit.addEquestrian(Mockito.<Equestrian>anyObject())).thenReturn(false);
        Assert.assertNull(secretary.createEquestrian());
    }

    @Test
    public void createEquestrian_creationFailed_returnNull() {
        Mockito.when(DataFactory.createEquestrian()).thenReturn(null);
        Assert.assertNull(secretary.createEquestrian());
    }

    @Test
    public void createCompetition_valid_returnString() {
        mockCompetitionCreation("createCompetition_valid_returnString");
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Assert.assertNotNull(secretary.createCompetition());
    }

    @Test
    public void createCompetition_valid_addToSummit() {
        Competition testCompetition = mockCompetitionCreation("createCompetition_valid_addToSummit");
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        secretary.createCompetition();
        Mockito.verify(mockSummit).addCompetition(testCompetition);
    }

    @Test
    public void createCompetition_valid_returnCorrectString() {
        String testString = "createCompetition_valid_returnCorrectString";
        mockCompetitionCreation(testString);
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(true);
        Assert.assertEquals(testString, secretary.createCompetition());
    }

    @Test
    public void createCompetition_insertionFailed_returnNull() {
        mockCompetitionCreation("createCompetition_valid_returnCorrectString");
        Mockito.when(mockSummit.addCompetition(Mockito.<Competition>anyObject())).thenReturn(false);
        Assert.assertNull(secretary.createCompetition());
    }

    @Test
    public void createCompetition_creationFailed_returnNull() {
        Mockito.when(DataFactory.createCompetition()).thenReturn(null);
        Assert.assertNull(secretary.createCompetition());
    }

    @Test
    public void startCompetitionEarlier_first_noChange() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionEarlier_first_noChange1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionEarlier_first_noChange2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionEarlier_first_noChange3");

        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_first_noChange1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_first_noChange2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_first_noChange3")).thenReturn(testCompetition3);

        TestVariableAnswer<Boolean, Integer> testVariable1 = new TestVariableAnswer<Boolean, Integer>(false, 1);
        TestVariableAnswer<Boolean, Integer> testVariable2 = new TestVariableAnswer<Boolean, Integer>(true, 2);
        TestVariableAnswer<Boolean, Integer> testVariable3 = new TestVariableAnswer<Boolean, Integer>(true, 3);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(testVariable1.getVariable());
        Mockito.when(testCompetition1.setStartOrder(0)).thenAnswer(testVariable1);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(testVariable2.getVariable());
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable2);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(testVariable3.getVariable());
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable3);

        secretary.startCompetitionEarlier("startCompetitionEarlier_first_noChange1");
        Assert.assertEquals(1, (int)testVariable1.getVariable());
        Assert.assertEquals(2, (int)testVariable2.getVariable());
        Assert.assertEquals(3, (int)testVariable3.getVariable());
    }

    @Test
    public void startCompetitionEarlier_inLine_correctChange() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionEarlier_inLine_correctChange1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionEarlier_inLine_correctChange2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionEarlier_inLine_correctChange3");

        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_inLine_correctChange1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_inLine_correctChange2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_inLine_correctChange3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        TestVariableAnswer<Boolean, Integer> testVariable1 = new TestVariableAnswer<Boolean, Integer>(true, 1);
        TestVariableAnswer<Boolean, Integer> testVariable2 = new TestVariableAnswer<Boolean, Integer>(true, 2);
        TestVariableAnswer<Boolean, Integer> testVariable3 = new TestVariableAnswer<Boolean, Integer>(true, 3);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(testVariable1.getVariable());
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable1);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(testVariable2.getVariable());
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable2);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(testVariable3.getVariable());
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable3);

        secretary.startCompetitionEarlier("startCompetitionEarlier_inLine_correctChange2");
        Assert.assertEquals(2, (int)testVariable1.getVariable());
        Assert.assertEquals(1, (int)testVariable2.getVariable());
        Assert.assertEquals(3, (int)testVariable3.getVariable());
    }

    @Test
    public void startCompetitionEarlier_first_returnNoChange() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionEarlier_first_returnNoChange1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionEarlier_first_returnNoChange2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionEarlier_first_returnNoChange3");

        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_first_returnNoChange1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_first_returnNoChange2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_first_returnNoChange3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.NoChange, secretary.startCompetitionEarlier("startCompetitionEarlier_first_returnNoChange1"));
    }

    @Test
    public void startCompetitionEarlier_nullObject_returnMissing() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionEarlier_nullObject_returnNoChange1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionEarlier_nullObject_returnNoChange2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionEarlier_nullObject_returnNoChange3");

        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_nullObject_returnNoChange1")).thenReturn(null);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_nullObject_returnNoChange2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_nullObject_returnNoChange3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.Missing, secretary.startCompetitionEarlier("startCompetitionEarlier_nullObject_returnNoChange1"));
    }

    @Test
    public void startCompetitionEarlier_success_returnSuccess() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionEarlier_success_returnSuccess1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionEarlier_success_returnSuccess2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionEarlier_success_returnSuccess3");

        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_success_returnSuccess1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_success_returnSuccess2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_success_returnSuccess3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.Success, secretary.startCompetitionEarlier("startCompetitionEarlier_success_returnSuccess2"));
    }

    @Test
    public void startCompetitionEarlier_invalidSelf_returnInvalid() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionEarlier_invalidSelf_returnInvalid1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionEarlier_invalidSelf_returnInvalid2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionEarlier_invalidSelf_returnInvalid3");

        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalidSelf_returnInvalid1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalidSelf_returnInvalid2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalidSelf_returnInvalid3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(false);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.Invalid, secretary.startCompetitionEarlier("startCompetitionEarlier_invalidSelf_returnInvalid2"));
    }

    @Test
    public void startCompetitionEarlier_invalidOther_returnInvalid() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionEarlier_invalidOther_returnInvalid1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionEarlier_invalidOther_returnInvalid2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionEarlier_invalidOther_returnInvalid3");

        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalidOther_returnInvalid1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalidOther_returnInvalid2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalidOther_returnInvalid3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(false);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.Invalid, secretary.startCompetitionEarlier("startCompetitionEarlier_invalidOther_returnInvalid2"));
    }

    @Test
    public void startCompetitionEarlier_invalidNoRecovery_throwException() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionEarlier_invalid_returnInvalid1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionEarlier_invalid_returnInvalid2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionEarlier_invalid_returnInvalid3");

        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalid_returnInvalid1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalid_returnInvalid2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionEarlier_invalid_returnInvalid3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(false);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(1)).thenReturn(true);
        Mockito.when(testCompetition2.setStartOrder(2)).thenReturn(false);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        try {
            secretary.startCompetitionEarlier("startCompetitionEarlier_invalid_returnInvalid2");
        } catch (Exception e) {
            return;
        }

        Assert.fail("Did not throw exception on failed recovery");
    }

    @Test
    public void startCompetitionLater_last_noChange() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionLater_last_noChange1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionLater_last_noChange2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionLater_last_noChange3");

        Mockito.when(mockSummit.getCompetition("startCompetitionLater_last_noChange1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_last_noChange2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_last_noChange3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        TestVariableAnswer<Boolean, Integer> testVariable1 = new TestVariableAnswer<Boolean, Integer>(true, 1);
        TestVariableAnswer<Boolean, Integer> testVariable2 = new TestVariableAnswer<Boolean, Integer>(true, 2);
        TestVariableAnswer<Boolean, Integer> testVariable3 = new TestVariableAnswer<Boolean, Integer>(false, 3);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(testVariable1.getVariable());
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable1);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(testVariable2.getVariable());
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable2);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(testVariable3.getVariable());
        Mockito.when(testCompetition3.setStartOrder(3)).thenAnswer(testVariable3);

        secretary.startCompetitionLater("startCompetitionLater_last_noChange3");
        Assert.assertEquals(1, (int)testVariable1.getVariable());
        Assert.assertEquals(2, (int)testVariable2.getVariable());
        Assert.assertEquals(3, (int)testVariable3.getVariable());
    }

    @Test
    public void startCompetitionLater_inLine_correctChange() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionLater_inLine_correctChange1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionLater_inLine_correctChange2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionLater_inLine_correctChange3");

        Mockito.when(mockSummit.getCompetition("startCompetitionLater_inLine_correctChange1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_inLine_correctChange2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_inLine_correctChange3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        TestVariableAnswer<Boolean, Integer> testVariable1 = new TestVariableAnswer<Boolean, Integer>(true, 1);
        TestVariableAnswer<Boolean, Integer> testVariable2 = new TestVariableAnswer<Boolean, Integer>(true, 2);
        TestVariableAnswer<Boolean, Integer> testVariable3 = new TestVariableAnswer<Boolean, Integer>(true, 3);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(testVariable1.getVariable());
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable1);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(testVariable2.getVariable());
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable2);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(testVariable3.getVariable());
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenAnswer(testVariable3);

        secretary.startCompetitionLater("startCompetitionLater_inLine_correctChange2");
        Assert.assertEquals(1, (int)testVariable1.getVariable());
        Assert.assertEquals(3, (int)testVariable2.getVariable());
        Assert.assertEquals(2, (int)testVariable3.getVariable());
    }

    @Test
    public void startCompetitionLater_last_returnNoChange() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionLater_last_returnNoChange1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionLater_last_returnNoChange2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionLater_last_returnNoChange3");

        Mockito.when(mockSummit.getCompetition("startCompetitionLater_last_returnNoChange1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_last_returnNoChange2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_last_returnNoChange3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.NoChange, secretary.startCompetitionLater("startCompetitionLater_last_returnNoChange3"));
    }

    @Test
    public void startCompetitionLater_nullObject_returnMissing() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionLater_nullObject_returnNoChange1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionLater_nullObject_returnNoChange2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionLater_nullObject_returnNoChange3");

        Mockito.when(mockSummit.getCompetition("startCompetitionLater_nullObject_returnNoChange1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_nullObject_returnNoChange2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_nullObject_returnNoChange3")).thenReturn(null);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.Missing, secretary.startCompetitionLater("startCompetitionLater_nullObject_returnNoChange3"));
    }

    @Test
    public void startCompetitionLater_success_returnSuccess() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionLater_success_returnSuccess1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionLater_success_returnSuccess2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionLater_success_returnSuccess3");

        Mockito.when(mockSummit.getCompetition("startCompetitionLater_success_returnSuccess1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_success_returnSuccess2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_success_returnSuccess3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.Success, secretary.startCompetitionLater("startCompetitionLater_success_returnSuccess2"));
    }

    @Test
    public void startCompetitionLater_invalidSelf_returnInvalid() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionLater_invalidSelf_returnInvalid1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionLater_invalidSelf_returnInvalid2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionLater_invalidSelf_returnInvalid3");

        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalidSelf_returnInvalid1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalidSelf_returnInvalid2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalidSelf_returnInvalid3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(false);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(true);

        Assert.assertEquals(ControllerStatus.Invalid, secretary.startCompetitionLater("startCompetitionLater_invalidSelf_returnInvalid2"));
    }

    @Test
    public void startCompetitionLater_invalidOther_returnInvalid() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionLater_invalidOther_returnInvalid1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionLater_invalidOther_returnInvalid2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionLater_invalidOther_returnInvalid3");

        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalidOther_returnInvalid1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalidOther_returnInvalid2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalidOther_returnInvalid3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(false);

        Assert.assertEquals(ControllerStatus.Invalid, secretary.startCompetitionLater("startCompetitionLater_invalidOther_returnInvalid2"));
    }

    @Test
    public void startCompetitionLater_invalidNoRecovery_throwException() {
        Competition testCompetition1 = mockCompetitionCreation("startCompetitionLater_invalid_returnInvalid1");
        Competition testCompetition2 = mockCompetitionCreation("startCompetitionLater_invalid_returnInvalid2");
        Competition testCompetition3 = mockCompetitionCreation("startCompetitionLater_invalid_returnInvalid3");

        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalid_returnInvalid1")).thenReturn(testCompetition1);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalid_returnInvalid2")).thenReturn(testCompetition2);
        Mockito.when(mockSummit.getCompetition("startCompetitionLater_invalid_returnInvalid3")).thenReturn(testCompetition3);

        Competition[] testCompetitions = new Competition[3];
        testCompetitions[0] = testCompetition1;
        testCompetitions[1] = testCompetition2;
        testCompetitions[2] = testCompetition3;
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);

        Mockito.when(testCompetition1.getStartOrder()).thenReturn(1);
        Mockito.when(testCompetition1.setStartOrder(Mockito.anyInt())).thenReturn(true);
        Mockito.when(testCompetition2.getStartOrder()).thenReturn(2);
        Mockito.when(testCompetition2.setStartOrder(3)).thenReturn(true);
        Mockito.when(testCompetition2.setStartOrder(2)).thenReturn(false);
        Mockito.when(testCompetition3.getStartOrder()).thenReturn(3);
        Mockito.when(testCompetition3.setStartOrder(Mockito.anyInt())).thenReturn(false);

        try {
            secretary.startCompetitionLater("startCompetitionLater_invalid_returnInvalid2");
        } catch (Exception e) {
            return;
        }

        Assert.fail("Did not throw exception on failed recovery");
    }

    @Test
    public void getEquestrian_notFound_returnNull() {
        String testString = "getEquestrian_notFound_returnNull";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        EquestrianAssistant testAssistant = Mockito.mock(EquestrianAssistant.class);
        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(null);
        Mockito.when(AssistantFactory.createEquestrianAssistant(null)).thenReturn(null);
        Assert.assertNull(secretary.getEquestrian(testString));
    }

    @Test
    public void getEquestrian_creationFailed_returnNull() {
        String testString = "getEquestrian_notFound_returnNull";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(testEquestrian);
        Mockito.when(AssistantFactory.createEquestrianAssistant(null)).thenReturn(null);
        Assert.assertNull(secretary.getEquestrian(testString));
    }

    @Test
    public void getEquestrian_found_returnObject() {
        String testString = "getEquestrian_found_returnObject";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        EquestrianAssistant testAssistant = Mockito.mock(EquestrianAssistant.class);
        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(testEquestrian);
        Mockito.when(AssistantFactory.createEquestrianAssistant(testEquestrian)).thenReturn(testAssistant);
        Assert.assertNotNull(secretary.getEquestrian(testString));
    }

    @Test
    public void getCompetition_notFound_returnNull() {
        String testString = "getCompetition_notFound_returnNull";
        Competition testCompetition = Mockito.mock(Competition.class);
        CompetitionAssistant testAssistant = Mockito.mock(CompetitionAssistant.class);
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(null);
        Mockito.when(AssistantFactory.createCompetitionAssistant(null)).thenReturn(null);
        Assert.assertNull(secretary.getCompetition(testString));
    }

    @Test
    public void getCompetition_creationFailed_returnNull() {
        String testString = "getCompetition_notFound_returnNull";
        Competition testCompetition = Mockito.mock(Competition.class);
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(testCompetition);
        Mockito.when(AssistantFactory.createCompetitionAssistant(null)).thenReturn(null);
        Assert.assertNull(secretary.getCompetition(testString));
    }

    @Test
    public void getCompetition_found_returnObject() {
        String testString = "getCompetition_found_returnObject";
        Competition testCompetition = Mockito.mock(Competition.class);
        CompetitionAssistant testAssistant = Mockito.mock(CompetitionAssistant.class);
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(testCompetition);
        Mockito.when(AssistantFactory.createCompetitionAssistant(testCompetition)).thenReturn(testAssistant);
        Assert.assertNotNull(secretary.getCompetition(testString));
    }

    @Test
    public void getResult_equestrianNotFound_returnNull() {
        String testStringCompetition = "getResult_equestrianNotFound_returnNull_competition";
        String testStringEquestrian = "getResult_equestrianNotFound_returnNull_equestrian";
        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(null);
        Assert.assertNull(secretary.getResult(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void getResult_competitionNotFound_returnNull() {
        String testStringCompetition = "getResult_competitionNotFound_returnNull_competition";
        String testStringEquestrian = "getResult_competitionNotFound_returnNull_equestrian";

        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(null);

        Assert.assertNull(secretary.getResult(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void getResult_noResult_createAndStoreObject() {
        String testStringCompetition = "getResult_noResult_createAndStoreObject_competition";
        String testStringEquestrian = "getResult_noResult_createAndStoreObject_equestrian";

        Result testResult = Mockito.mock(Result.class);
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);
        ResultAssistant testAssistant = Mockito.mock(ResultAssistant.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.getResult(testCompetition)).thenReturn(null);
        Mockito.when(DataFactory.createResult()).thenReturn(testResult);
        Mockito.when(testEquestrian.updateResult(testCompetition, testResult)).thenReturn(true);
        Mockito.when(AssistantFactory.createResultAssistant(testResult)).thenReturn(testAssistant);

        Assert.assertSame(testAssistant, secretary.getResult(testStringEquestrian, testStringCompetition));
        Mockito.verify(testEquestrian).updateResult(testCompetition, testResult);
    }

    @Test
    public void getResult_noResultCreationFailure_returnNull() {
        String testStringCompetition = "getResult_noResultCreationFailure_returnNull_competition";
        String testStringEquestrian = "getResult_noResultCreationFailure_returnNull_equestrian";

        Result testResult = Mockito.mock(Result.class);
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.getResult(testCompetition)).thenReturn(null);
        Mockito.when(DataFactory.createResult()).thenReturn(null);

        Assert.assertNull(secretary.getResult(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void getResult_noResultInsertionFailure_createAndStoreObject() {
        String testStringCompetition = "getResult_noResultInsertionFailure_createAndStoreObject_competition";
        String testStringEquestrian = "getResult_noResultInsertionFailure_createAndStoreObject_equestrian";

        Result testResult = Mockito.mock(Result.class);
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.getResult(testCompetition)).thenReturn(null);
        Mockito.when(DataFactory.createResult()).thenReturn(testResult);
        Mockito.when(testEquestrian.updateResult(testCompetition, testResult)).thenReturn(false);

        Assert.assertNull(secretary.getResult(testStringEquestrian, testStringCompetition));
        Mockito.verify(testEquestrian).updateResult(testCompetition, testResult);
    }

    @Test
    public void getResult_noResultAssistantCreationFailure_createAndStoreObject() {
        String testStringCompetition = "getResult_noResultAssistantCreationFailure_createAndStoreObject_competition";
        String testStringEquestrian = "getResult_noResultAssistantCreationFailure_createAndStoreObject_equestrian";

        Result testResult = Mockito.mock(Result.class);
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.getResult(testCompetition)).thenReturn(null);
        Mockito.when(DataFactory.createResult()).thenReturn(testResult);
        Mockito.when(testEquestrian.updateResult(testCompetition, testResult)).thenReturn(true);
        Mockito.when(AssistantFactory.createResultAssistant(testResult)).thenReturn(null);

        Assert.assertNull(secretary.getResult(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void getResult_hasResult_returnObject() {
        String testStringCompetition = "getResult_hasResult_returnObject_competition";
        String testStringEquestrian = "getResult_hasResult_returnObject_equestrian";

        Result testResult = Mockito.mock(Result.class);
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);
        ResultAssistant testAssistant = Mockito.mock(ResultAssistant.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.getResult(testCompetition)).thenReturn(testResult);
        Mockito.when(AssistantFactory.createResultAssistant(testResult)).thenReturn(testAssistant);

        Assert.assertSame(testAssistant, secretary.getResult(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void addCompetitionToEquestrian_equestrianNotFound_returnFalse() {
        String testStringCompetition = "addCompetitionToEquestrian_equestrianNotFound_returnFalse_competition";
        String testStringEquestrian = "addCompetitionToEquestrian_equestrianNotFound_returnFalse_equestrian";
        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(null);
        Assert.assertFalse(secretary.addCompetitionToEquestrian(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void addCompetitionToEquestrian_competitionNotFound_returnFalse() {
        String testStringCompetition = "addCompetitionToEquestrian_competitionNotFound_returnFalse_competition";
        String testStringEquestrian = "addCompetitionToEquestrian_competitionNotFound_returnFalse_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(null);
        Assert.assertFalse(secretary.addCompetitionToEquestrian(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void addCompetitionToEquestrian_failedAdd_returnFalse() {
        String testStringCompetition = "addCompetitionToEquestrian_failedAdd_returnFalse_competition";
        String testStringEquestrian = "addCompetitionToEquestrian_failedAdd_returnFalse_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.addCompetition(testCompetition)).thenReturn(false);
        Mockito.when(testCompetition.addEquestrian(testEquestrian)).thenReturn(true);

        Assert.assertFalse(secretary.addCompetitionToEquestrian(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void addCompetitionToEquestrian_failedSecondInsert_returnFalse() {
        String testStringCompetition = "addCompetitionToEquestrian_failedSecondInsert_returnFalse_competition";
        String testStringEquestrian = "addCompetitionToEquestrian_failedSecondInsert_returnFalse_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.addCompetition(testCompetition)).thenReturn(true);
        Mockito.when(testCompetition.addEquestrian(testEquestrian)).thenReturn(false);
        Mockito.when(testEquestrian.removeCompetition(testCompetition)).thenReturn(true);

        Assert.assertFalse(secretary.addCompetitionToEquestrian(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void addCompetitionToEquestrian_failedSecondInsertRevert_throwException() {
        String testStringCompetition = "addCompetitionToEquestrian_failedSecondInsertRevert_throwException_competition";
        String testStringEquestrian = "addCompetitionToEquestrian_failedSecondInsertRevert_throwException_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.addCompetition(testCompetition)).thenReturn(true);
        Mockito.when(testCompetition.addEquestrian(testEquestrian)).thenReturn(false);
        Mockito.when(testEquestrian.removeCompetition(testCompetition)).thenReturn(false);

        try {
            secretary.addCompetitionToEquestrian(testStringEquestrian, testStringCompetition);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Should throw exception");
    }

    @Test
    public void addCompetitionToEquestrian_successAndAdd_returnTrue() {
        String testStringCompetition = "addCompetitionToEquestrian_failedAdd_returnFalse_competition";
        String testStringEquestrian = "addCompetitionToEquestrian_failedAdd_returnFalse_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.addCompetition(testCompetition)).thenReturn(true);
        Mockito.when(testCompetition.addEquestrian(testEquestrian)).thenReturn(true);

        Assert.assertTrue(secretary.addCompetitionToEquestrian(testStringEquestrian, testStringCompetition));
        Mockito.verify(testEquestrian).addCompetition(testCompetition);
        Mockito.verify(testCompetition).addEquestrian(testEquestrian);
    }

    @Test
    public void removeCompetitionFromEquestrian_equestrianNotFound_returnFalse() {
        String testStringCompetition = "removeCompetitionFromEquestrian_equestrianNotFound_returnFalse_competition";
        String testStringEquestrian = "removeCompetitionFromEquestrian_equestrianNotFound_returnFalse_equestrian";
        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(null);
        Assert.assertFalse(secretary.removeCompetitionFromEquestrian(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void removeCompetitionFromEquestrian_competitionNotFound_returnFalse() {
        String testStringCompetition = "removeCompetitionFromEquestrian_competitionNotFound_returnFalse_competition";
        String testStringEquestrian = "removeCompetitionFromEquestrian_competitionNotFound_returnFalse_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(null);
        Assert.assertFalse(secretary.removeCompetitionFromEquestrian(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void removeCompetitionFromEquestrian_failedRemove_returnFalse() {
        String testStringCompetition = "removeCompetitionFromEquestrian_failedRemove_returnFalse_competition";
        String testStringEquestrian = "removeCompetitionFromEquestrian_failedRemove_returnFalse_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.removeCompetition(testCompetition)).thenReturn(false);

        Assert.assertFalse(secretary.removeCompetitionFromEquestrian(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void removeCompetitionFromEquestrian_failedCompetitionRemove_returnFalse() {
        String testStringCompetition = "removeCompetitionFromEquestrian_failedCompetitionRemove_returnFalse_competition";
        String testStringEquestrian = "removeCompetitionFromEquestrian_failedCompetitionRemove_returnFalse_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.removeCompetition(testCompetition)).thenReturn(true);
        Mockito.when(testCompetition.removeEquestrian(testEquestrian)).thenReturn(false);
        Mockito.when(testEquestrian.addCompetition(testCompetition)).thenReturn(true);

        Assert.assertFalse(secretary.removeCompetitionFromEquestrian(testStringEquestrian, testStringCompetition));
    }

    @Test
    public void removeCompetitionFromEquestrian_failedRevert_throwException() {
        String testStringCompetition = "removeCompetitionFromEquestrian_failedRevert_throwException_competition";
        String testStringEquestrian = "removeCompetitionFromEquestrian_failedRevert_throwException_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.removeCompetition(testCompetition)).thenReturn(true);
        Mockito.when(testCompetition.removeEquestrian(testEquestrian)).thenReturn(false);
        Mockito.when(testEquestrian.addCompetition(testCompetition)).thenReturn(false);

        try {
            secretary.removeCompetitionFromEquestrian(testStringEquestrian, testStringCompetition);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Should throw exception");
    }

    @Test
    public void removeCompetitionFromEquestrian_successAndRemove_returnTrue() {
        String testStringCompetition = "removeCompetitionFromEquestrian_failedRemove_returnFalse_competition";
        String testStringEquestrian = "removeCompetitionFromEquestrian_failedRemove_returnFalse_equestrian";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition = Mockito.mock(Competition.class);

        Mockito.when(mockSummit.getEquestrian(testStringEquestrian)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetition(testStringCompetition)).thenReturn(testCompetition);
        Mockito.when(testEquestrian.removeCompetition(testCompetition)).thenReturn(true);
        Mockito.when(testCompetition.removeEquestrian(testEquestrian)).thenReturn(true);

        Assert.assertTrue(secretary.removeCompetitionFromEquestrian(testStringEquestrian, testStringCompetition));
        Mockito.verify(testEquestrian).removeCompetition(testCompetition);
        Mockito.verify(testCompetition).removeEquestrian(testEquestrian);
    }

    @Test
    public void deleteCompetition_notFound_returnFalse() {
        String testString = "deleteCompetition_notFound_returnFalse";
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(null);
        Assert.assertFalse(secretary.deleteCompetition(testString));
    }

    @Test
    public void deleteCompetition_foundNoEquestrians_returnTrue() {
        String testString = "deleteCompetition_foundNoEquestrians_returnTrue";
        Competition testCompetition = Mockito.mock(Competition.class);
        Equestrian[] testEquestrians = {};
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(testCompetition);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);
        Mockito.when(mockSummit.removeCompetition(testCompetition)).thenReturn(true);
        Assert.assertTrue(secretary.deleteCompetition(testString));
    }

    @Test
    public void deleteCompetition_validRemove_returnTrue() {
        String testString = "deleteCompetition_validRemove_returnTrue";
        Competition testCompetition = Mockito.mock(Competition.class);
        Equestrian[] testEquestrians = {};
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(testCompetition);
        Mockito.when(mockSummit.removeCompetition(testCompetition)).thenReturn(true);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);

        Assert.assertTrue(secretary.deleteCompetition(testString));
        Mockito.verify(mockSummit).removeCompetition(testCompetition);
    }

    @Test
    public void deleteCompetition_invalidRemove_throwsException() {
        String testString = "deleteCompetition_invalidRemove_throwsException";
        Competition testCompetition = Mockito.mock(Competition.class);
        Equestrian[] testEquestrians = {};
        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(testCompetition);
        Mockito.when(mockSummit.removeCompetition(testCompetition)).thenReturn(false);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);

        try {
            secretary.deleteCompetition(testString);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Did not throw exception on error");
    }

    @Test
    public void deleteCompetition_foundWithEquestrians_returnTrue() {
        String testString = "deleteCompetition_foundWithEquestrians_returnTrue";
        Competition testCompetition = Mockito.mock(Competition.class);
        Equestrian testEquestrian0 = Mockito.mock(Equestrian.class);
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Equestrian[] testEquestrians = {testEquestrian0, testEquestrian1, testEquestrian2};

        Mockito.when(mockSummit.getCompetition(testString)).thenReturn(testCompetition);
        Mockito.when(testEquestrian0.removeCompetition(testCompetition)).thenReturn(true);
        Mockito.when(testEquestrian1.removeCompetition(testCompetition)).thenReturn(false);
        Mockito.when(testEquestrian2.removeCompetition(testCompetition)).thenReturn(true);
        Mockito.when(mockSummit.removeCompetition(testCompetition)).thenReturn(true);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);

        Assert.assertTrue(secretary.deleteCompetition(testString));
    }

    @Test
    public void deleteEquestrian_notFound_returnFalse() {
        String testString = "deleteEquestrian_notFound_returnFalse";
        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(null);
        Assert.assertFalse(secretary.deleteEquestrian(testString));
    }

    @Test
    public void deleteEquestrian_foundNoCompetitions_returnTrue() {
        String testString = "deleteEquestrian_foundNoCompetitions_returnTrue";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition[] testCompetitions = {};
        Equestrian[] testEquestrians = {};
        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);
        Mockito.when(mockSummit.removeEquestrian(testEquestrian)).thenReturn(true);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);

        Assert.assertTrue(secretary.deleteEquestrian(testString));
    }

    @Test
    public void deleteEquestrian_validRemove_returnTrue() {
        String testString = "deleteEquestrian_validRemove_returnTrue";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition[] testCompetitions = {};
        Equestrian[] testEquestrians = {};
        Mockito.when(testEquestrian.setStartNumber(Mockito.anyInt())).thenReturn(true);
        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.removeEquestrian(testEquestrian)).thenReturn(true);
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);

        Assert.assertTrue(secretary.deleteEquestrian(testString));
        Mockito.verify(mockSummit).removeEquestrian(testEquestrian);
    }

    @Test
    public void deleteEquestrian_invalidRemove_throwsException() {
        String testString = "deleteEquestrian_invalidRemove_throwsException";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition[] testCompetitions = {};
        Equestrian[] testEquestrians = {};
        Mockito.when(testEquestrian.setStartNumber(Mockito.anyInt())).thenReturn(true);
        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(testEquestrian);
        Mockito.when(mockSummit.removeEquestrian(testEquestrian)).thenReturn(false);
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);

        try {
            secretary.deleteEquestrian(testString);
        } catch (Throwable e) {
            return;
        }

        Assert.fail("Did not throw exception on error");
    }

    @Test
    public void deleteEquestrian_foundWithCompetitions_returnTrue() {
        String testString = "deleteEquestrian_foundWithCompetitions_returnTrue";
        Equestrian testEquestrian = Mockito.mock(Equestrian.class);
        Competition testCompetition0 = Mockito.mock(Competition.class);
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Competition testCompetition2 = Mockito.mock(Competition.class);
        Competition[] testCompetitions = {testCompetition0, testCompetition1, testCompetition2};
        Equestrian[] testEquestrians = {};

        Mockito.when(testEquestrian.setStartNumber(Mockito.anyInt())).thenReturn(true);
        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(testEquestrian);
        Mockito.when(testCompetition0.removeEquestrian(testEquestrian)).thenReturn(true);
        Mockito.when(testCompetition1.removeEquestrian(testEquestrian)).thenReturn(false);
        Mockito.when(testCompetition2.removeEquestrian(testEquestrian)).thenReturn(true);
        Mockito.when(mockSummit.removeEquestrian(testEquestrian)).thenReturn(true);
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);

        Assert.assertTrue(secretary.deleteEquestrian(testString));
    }

    @Test
    public void deleteEquestrian_updateStartNumbers_returnTrue() {
        String testString = "deleteEquestrian_updateStartNumbers_returnTrue";
        Equestrian testEquestrian0 = Mockito.mock(Equestrian.class);
        Equestrian testEquestrian1 = Mockito.mock(Equestrian.class);
        Equestrian testEquestrian2 = Mockito.mock(Equestrian.class);
        Competition testCompetition0 = Mockito.mock(Competition.class);
        Competition testCompetition1 = Mockito.mock(Competition.class);
        Competition testCompetition2 = Mockito.mock(Competition.class);
        Competition[] testCompetitions = {};
        Equestrian[] testEquestrians = {testEquestrian0, testEquestrian2};

        TestVariableAnswer<Boolean, Integer> keepNumber = new TestVariableAnswer<Boolean, Integer>(true, 1);
        TestVariableAnswer<Boolean, Integer> updateNumber = new TestVariableAnswer<Boolean, Integer>(true, 3);

        Mockito.when(testEquestrian0.getStartNumber()).thenReturn(1);
        Mockito.when(testEquestrian1.getStartNumber()).thenReturn(2);
        Mockito.when(testEquestrian2.getStartNumber()).thenReturn(3);
        Mockito.when(testEquestrian0.setStartNumber(Mockito.anyInt())).thenAnswer(keepNumber);
        Mockito.when(testEquestrian2.setStartNumber(Mockito.anyInt())).thenAnswer(updateNumber);


        Mockito.when(mockSummit.getEquestrian(testString)).thenReturn(testEquestrian1);
        Mockito.when(mockSummit.removeEquestrian(testEquestrian1)).thenReturn(true);
        Mockito.when(mockSummit.getCompetitions()).thenReturn(testCompetitions);
        Mockito.when(mockSummit.getEquestrians()).thenReturn(testEquestrians);

        Assert.assertTrue(secretary.deleteEquestrian(testString));
        Assert.assertEquals(1, keepNumber.getVariable().intValue());
        Assert.assertEquals(2, updateNumber.getVariable().intValue());
    }

    @Test
    public void getEquestrians_empty_returnEmptyArray() {
        Mockito.when(mockSummit.getEquestrians()).thenReturn(new Equestrian[0]);
        String[] ret = secretary.getEquestrians();
        Assert.assertNotNull(ret);
        Assert.assertEquals(0, ret.length);
    }

    @Test
    public void getEquestrians_filled_returnAllElements() {
        Equestrian[] equestrians = new Equestrian[3];
        equestrians[0] = Mockito.mock(Equestrian.class);
        equestrians[1] = Mockito.mock(Equestrian.class);
        equestrians[2] = Mockito.mock(Equestrian.class);
        Mockito.when(equestrians[0].toString()).thenReturn("equestrian_a");
        Mockito.when(equestrians[1].toString()).thenReturn("equestrian_c");
        Mockito.when(equestrians[2].toString()).thenReturn("equestrian_b");
        Mockito.when(mockSummit.getEquestrians()).thenReturn(equestrians);

        Assert.assertEquals(3, secretary.getEquestrians().length);
    }


    @Test
    public void getCompetitions_empty_returnEmptyArray() {
        Mockito.when(mockSummit.getCompetitions()).thenReturn(new Competition[0]);
        String[] ret = secretary.getCompetitions();
        Assert.assertNotNull(ret);
        Assert.assertEquals(0, ret.length);
    }

    @Test
    public void getCompetitions_filled_returnAllElements() {
        Competition[] competitions = new Competition[3];
        competitions[0] = Mockito.mock(Competition.class);
        competitions[1] = Mockito.mock(Competition.class);
        competitions[2] = Mockito.mock(Competition.class);
        Mockito.when(competitions[0].toString()).thenReturn("competition_a");
        Mockito.when(competitions[1].toString()).thenReturn("competition_c");
        Mockito.when(competitions[2].toString()).thenReturn("competition_b");
        Mockito.when(mockSummit.getCompetitions()).thenReturn(competitions);

        Assert.assertEquals(3, secretary.getCompetitions().length);
    }
}
