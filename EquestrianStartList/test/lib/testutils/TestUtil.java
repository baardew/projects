/**
 * Testutil: Utilities for testing with MLog.
 *
 * A class for predefined routines that can be used for testing purposes.
 * All methods are static to allow for universial use.
 *
 * @author baardew
 * @date 2015
 */

package testutils;

import java.io.IOException;
import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.InvalidClassException;
import java.io.NotSerializableException;

import java.lang.reflect.Method;
import java.lang.reflect.Field;

import java.awt.Component;
import java.awt.Point;

@SuppressWarnings("unchecked")
public abstract class TestUtil {

    private final static String SERDIR = "test/serialization/";

    /**
     * Get a private method object for a membered function. Can also be protected or public.
     *
     * @param obj The object that has the method
     * @param name The name of the function
     * @param args The parameters to the function, given as class types only (int, String, Object etc...)
     * @return A Method containing the requested member function or null if not found.
     *
     * @note Testing private methods should not be done. However, this method is included in order
     * to simplyfy this process if it ever becomes neccessary.
     */
    public static Method getPrivateMethod(Object obj, String name, Class<?> ... args) {
        Method method = null;
        try {
            method = obj.getClass().getDeclaredMethod(name, args);
            method.setAccessible(true);
        } catch (Exception e) {
            throw new TestutilException("Method '" + name + "' does not exists in " + obj.getClass());
        }
        return method;
    }

    /**
     * Invokes a Method object with the given parameters.
     * The object method is retrieved using getPrivateMethod.
     *
     * @param obj The owner object which has the method
     * @param method The obj's method to invoke
     * @param args The argument list to the function
     * @return The return value from the method.
     */
    public static <V> V invokePrivateMethod(Object obj, Method method, Object ... args) {
        try {
            return (V)method.invoke(obj, args);
        } catch (Exception e) {
            throw new TestutilException(e.getCause().getMessage());
        }
    }

    /**
     * Get a private field object for a membered (private) variable. Can also be protected or public.
     *
     * @param obj The object that has the field
     * @param name The name of the field
     * @return An Object that is the requested field
     *
     * @note Using private fields should not be used for testing. However,
     * this method is included in order to simplyfy this process if it ever becomes neccessary.
     */
    public static <V> V getPrivateField(Object obj, String name) {
        Field field = null;
        try {
            field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return (V)field.get(obj);
        } catch (Exception e) {
            throw new TestutilException("Variable '" + name + "' does not exists in " + obj.getClass());
        }
    }

    /**
     * Set a private field object for a membered (private) variable. Can also be protected or public.
     *
     * @param obj The object that has the field
     * @param name The name of the field
     * @return An Object that is the requested field
     *
     * @note Using private fields should not be used for testing. However,
     * this method is included in order to simplyfy this process if it ever becomes neccessary.
     */
    public static <V> void setPrivateField(Object obj, String name, V value) {
        Field field = null;
        try {
            field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(obj, value);
        } catch (Exception e) {
            throw new TestutilException("Unable to assign to variable '" + name + "' in " + obj.getClass());
        }
    }

    /**
     * Serialize an object.
     *
     * @param file The file name to serialize to.
     * @param Serializable The object to be serialized.
     * @return null on success or an error message
     */
    public static String serialize(String file, Serializable object) {
        String status = null;
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(SERDIR + file));
            out.writeObject(object);
            out.close();
        } catch (InvalidClassException e) {
            status = "Something is wrong with the class";
        } catch (NotSerializableException e) {
            status = "The class does not implement " + Serializable.class;
        } catch (IOException e) {
            throw new TestutilException("Serialization I/O error");
        }

        return status;
    }

    /**
     * Deserialize an object.
     *
     * @param file The file name to deserialize from.
     * @return The resulting object or null on error.
     */
    public static <V> V deserialize(String file) {
        Object object = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(SERDIR + file));
            object = in.readObject();
            in.close();
        } catch(ClassNotFoundException e) {
            throw new TestutilException("Serialization class definition error");
        } catch(IOException e) {
            throw new TestutilException("Serialization I/O error");
        }

        return (V)object;
    }
}

class TestutilException extends RuntimeException {
    private static final long serialVersionUID = 2014;

    public TestutilException(String message) {
        super(message);
    }
}
