package testutils;

import org.mockito.stubbing.Answer;
import org.mockito.invocation.InvocationOnMock;

public class TestVariableAnswer<A, V> implements Answer<A> {
    private V variable;
    private A answer;

    public TestVariableAnswer(A answer) {
        this.answer = answer;
    }

    public TestVariableAnswer(A answer, V variable) {
        this.answer = answer;
        this.variable = variable;
    }

    public V getVariable() {
        return variable;
    }

    @SuppressWarnings("unchecked")
    public A answer(InvocationOnMock invocation) {
        Object[] args = invocation.getArguments();
        variable = (V)args[0];
        return answer;
    }
}
