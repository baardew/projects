package no.followesternriders.startlist;

import no.followesternriders.startlist.view.ApplicationFrame;
import no.followesternriders.startlist.view.StartMenu;

import com.oracle.blogs.nickstephen.CrashDialog;
import com.oracle.blogs.nickstephen.StdOutErrLevel;
import com.oracle.blogs.nickstephen.PlainFormatter;
import com.oracle.blogs.nickstephen.EventQueueLogger;
import com.oracle.blogs.nickstephen.LoggingOutputStream;

import java.awt.Toolkit;
import java.awt.EventQueue;

import javax.swing.UIManager;

import javax.swing.UIManager.LookAndFeelInfo;

import java.io.PrintStream;
import java.io.IOException;

import java.util.Locale;
import java.util.ResourceBundle;

import java.util.logging.Logger;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

public class EquestrianStartList {
    public static void main(String[] args) {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // Do not change set, use defaults
        }

        ApplicationFrame af;

        if (!(args.length != 0 && args[0].equals("-test"))) {
            enableLogging("");
            af = new ApplicationFrame(ResourceBundle.getBundle("LanguageBundle", new Locale("no_NO")));
        } else {
            af = new ApplicationFrame(ResourceBundle.getBundle("LanguageBundle", new Locale("en")));
        }

        af.pushDisplay(new StartMenu(af));
        af.setVisible();
    }

    /**
     * Maps System.out and System.err to a looging file
     *
     * @source https://blogs.oracle.com/nickstephen/entry/java_redirecting_system_out_and
     */
    public static void enableLogging(String logDirectory) {
        // Initialize crash reporting
        String errorMessage =
            "The program got an exception while performing an operation."
            + "\n\n\n"
            + "The cause of this can be found in the file 'EquestrianStartList.log'\n"
            + "located alongside the Equestrian Start List program.\n"
            + "Report this issue by attaching the log file to an e-mail and\n"
            + "send it to dev@wintherproduksjoner.no.";

        CrashDialog.initialize("Equestrian Start List has Crashed", errorMessage);

        // Enable logging of exceptions in events
        EventQueue queue = Toolkit.getDefaultToolkit().getSystemEventQueue();
        queue.push(new EventQueueLogger());

        // initialize logging to go to rolling log file
        LogManager logManager = LogManager.getLogManager();
        logManager.reset();

        // log file max size 32K, 1 rolling file, restart on open
        Handler fileHandler = null;
        try {
            fileHandler = new FileHandler(logDirectory + "EquestrianStartList.log", 320000, 1, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileHandler.setFormatter(new PlainFormatter());
        Logger.getLogger("").addHandler(fileHandler);

        // Rebind stdout/stderr to logger
        Logger logger;
        LoggingOutputStream los;

        logger = Logger.getLogger("stdout");
        los = new LoggingOutputStream(logger, StdOutErrLevel.STDOUT);
        System.setOut(new PrintStream(los, true));

        logger = Logger.getLogger("stderr");
        los = new LoggingOutputStream(logger, StdOutErrLevel.STDERR);
        System.setErr(new PrintStream(los, true));

        // Make all exceptions go to logger/screen
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread t, Throwable e) {
                    System.err.print("Exception in thread \"" + t.getName() + "\":");
                    e.printStackTrace();
                    CrashDialog.execute();
                }
            });
    }
}
