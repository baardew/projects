package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ApplicationFrame;
import no.followesternriders.startlist.view.StringItemSelection;
import no.followesternriders.startlist.view.PrintSettingsMenu;
import no.followesternriders.startlist.view.ResultTypeFactory;

import no.followesternriders.startlist.controller.DocumentPrinter;
import no.followesternriders.startlist.controller.CompetitionAssistant;
import no.followesternriders.startlist.controller.ControllerStatus;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import javax.swing.JComboBox;

import java.awt.GridLayout;
import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class PrintMenu extends TemplateMenu {

    private JButton startNumbers;
    private JButton competition;
    private JButton result;
    private JButton settings;
    private JButton startList;

    public PrintMenu(ApplicationFrame applicationFrame) {
        super(applicationFrame, true);

        startNumbers = new JButton(applicationFrame.getResourceBundle().getString("startNumber"));
        competition = new JButton(applicationFrame.getResourceBundle().getString("competition"));
        result = new JButton(applicationFrame.getResourceBundle().getString("result"));
        settings = new JButton(applicationFrame.getResourceBundle().getString("settings"));
        startList = new JButton(applicationFrame.getResourceBundle().getString("startList"));

        startNumbers.addActionListener(this);
        competition.addActionListener(this);
        result.addActionListener(this);
        settings.addActionListener(this);
        startList.addActionListener(this);

        addButton(startList);
        addButton(startNumbers);
        addButton(competition);
        addButton(result);
        addButton(settings);
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == startNumbers)
            printStartNumbers();
        else if (event.getSource() == competition)
            printCompetition();
        else if (event.getSource() == result)
            printResults();
        else if (event.getSource() == settings)
            settingsMenu();
        else if (event.getSource() == startList)
            printStartList();
    }

    private void printStartNumbers() {
        applicationFrame.reportOnError(DocumentPrinter.printStartNumbers(), "startNumbers", "startPrint");
    }

    private void printStartList() {
        applicationFrame.reportOnError(DocumentPrinter.printStartList(), "startList", "startList");
    }

    private void printCompetition() {
        StringItemSelection item = new StringItemSelection(applicationFrame.getSecretary().getCompetitions(), applicationFrame.getResourceBundle().getString("selectCompetition"));
        applicationFrame.reportOnError(DocumentPrinter.printCompetition(item.getSelectedItem()), "competition", "competitionPrint");
    }

    private void printResults() {
        StringItemSelection item = new StringItemSelection(applicationFrame.getSecretary().getCompetitions(), applicationFrame.getResourceBundle().getString("selectCompetition"));
        CompetitionAssistant competition = applicationFrame.getSecretary().getCompetition(item.getSelectedItem());
        if (competition == null) {
            applicationFrame.reportOnError(ControllerStatus.Invalid, "results", "resultPrint");
            return;
        }

        ResultPlacement placement = ResultTypeFactory.createResultTypePanel(competition.getResultType());
        applicationFrame.reportOnError(DocumentPrinter.printResults(item.getSelectedItem(), placement.getResultSorter()), "results", "resultPrint");
    }

    private void settingsMenu() {
        PrintSettingsMenu printSettings = new PrintSettingsMenu(applicationFrame, DocumentPrinter.getLogoImage(), "");
        applicationFrame.pushDisplay(printSettings);
    }
}
