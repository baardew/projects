package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ResultPlacement;

import java.util.Comparator;
import java.util.ResourceBundle;

import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.GridLayout;

public class ScoreResult extends ResultPlacement {

    private JTextField result;

    public ScoreResult(ResourceBundle locale) {
        super(locale);
        result = new JTextField();
    }

    public JPanel getInputRepresentation() {
        JPanel panel = new JPanel(new GridLayout(1, 1));
        panel.add(result);
        return panel;
    }

    public void setValue(String result) {
        this.result.setText(result);
    }

    public String getValue() {
        return result.getText();
    }

    public boolean hasValidInput() {
        try {
            Integer.parseInt(result.getText());            
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public Comparator<String[]> getResultSorter() {
        return new Comparator<String[]>() {
            public int compare(String[] a, String[] b) {
                int ia;
                int ib;
                
                if (a[resultIndex].equals(noResult) && b[resultIndex].equals(noResult))
                    return 0;

                try {
                    ia = Integer.parseInt(a[resultIndex]);
                } catch (NumberFormatException e) {
                    return 1;
                }

                try {
                    ib = Integer.parseInt(a[resultIndex]);
                } catch (NumberFormatException e) {
                    return -1;
                }
                
                return ib - ia;
            }
        };
    }

    public void setFieldsEnabled(boolean enabled) {
        result.setEnabled(enabled);
    }
}
