package no.followesternriders.startlist.view;

import no.followesternriders.startlist.model.ResultType;

import no.followesternriders.startlist.view.ApplicationFrame;

import no.followesternriders.startlist.view.TemplatePanel;
import no.followesternriders.startlist.view.StringJList;

import no.followesternriders.startlist.controller.Secretary;
import no.followesternriders.startlist.controller.CompetitionAssistant;
import no.followesternriders.startlist.controller.ControllerStatus;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JComponent;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;

import javax.swing.event.ListSelectionEvent;

import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class CompetitionPanel extends TemplatePanel {

    private JButton newCompetition;
    private JButton deleteCompetition;
    private JButton moveCompetitionUp;
    private JButton moveCompetitionDown;

    private JButton updateInformation;

    private JTextField name;
    private JTextField startTime;
    private JTextField startPrice;
    private JTextArea information;
    private JComboBox<String> resultType;
    private StringJList equestrians;
    private JCheckBox randomizeStartList;

    private boolean newCompetitionInstance;

    public CompetitionPanel(ApplicationFrame applicationFrame) {
        super(applicationFrame, false);

        newCompetition = new JButton(applicationFrame.getResourceBundle().getString("new"));
        newCompetition.addActionListener(this);
        addMenuItem(newCompetition);
        deleteCompetition = new JButton(applicationFrame.getResourceBundle().getString("remove"));
        deleteCompetition.addActionListener(this);
        addMenuItem(deleteCompetition);
        moveCompetitionUp = new JButton(applicationFrame.getResourceBundle().getString("moveUp"));
        moveCompetitionUp.addActionListener(this);
        addMenuItem(moveCompetitionUp);
        moveCompetitionDown = new JButton(applicationFrame.getResourceBundle().getString("moveDown"));
        moveCompetitionDown.addActionListener(this);
        addMenuItem(moveCompetitionDown);

        // Content panels
        JPanel content = new JPanel(new GridLayout());
        JTabbedPane contentPanes = new JTabbedPane();
        content.add(contentPanes);
        setContentPanel(content);

        // Content.Details
        JPanel details = new JPanel(new GridBagLayout());
        details.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.gridwidth = GridBagConstraints.RELATIVE;
        c.gridx = 0;
        c.gridy = 0;
        c.ipady = 10;

        name = new JTextField();
        startTime = new JTextField();
        startPrice = new JTextField();
        information = new JTextArea();
        resultType = new JComboBox<String>(getResultTypes());
        randomizeStartList = new JCheckBox(applicationFrame.getResourceBundle().getString("randomizeStartList"));

        c.weightx = 0.1;
        details.add(new JLabel(applicationFrame.getResourceBundle().getString("name")), c);
        c.weightx = 0.9;
        c.gridx = 1;
        details.add(name, c);

        c.weightx = 0.1;
        c.gridy = 1;
        c.gridx = 0;
        details.add(new JLabel(applicationFrame.getResourceBundle().getString("startTime")), c);
        c.weightx = 0.9;
        c.weightx = 1;
        c.gridx = 1;
        details.add(startTime, c);

        c.weightx = 0.1;
        c.gridy = 2;
        c.gridx = 0;
        details.add(new JLabel(applicationFrame.getResourceBundle().getString("startPrice")), c);
        c.weightx = 0.9;
        c.weightx = 1;
        c.gridx = 1;
        details.add(startPrice, c);

        c.weightx = 0.1;
        c.gridy = 3;
        c.gridx = 0;
        details.add(new JLabel(applicationFrame.getResourceBundle().getString("resultType")), c);
        c.weightx = 0.9;
        c.weightx = 1;
        c.gridx = 1;
        details.add(resultType, c);

        c.weightx = 0.1;
        c.gridy = 4;
        c.gridx = 0;
        c.weightx = 0.9;
        c.weightx = 1;
        c.gridx = 1;
        details.add(randomizeStartList, c);

        c.weightx = 0.1;
        c.gridy = 5;
        c.gridx = 0;
        c.weighty = 0;
        JPanel topInformationText = new JPanel(new BorderLayout());
        topInformationText.add(new JLabel(applicationFrame.getResourceBundle().getString("information")), BorderLayout.NORTH);
        details.add(topInformationText, c);
        c.weightx = 0.9;
        c.weightx = 1;
        c.gridx = 1;
        c.weighty = 1.0;
        details.add(new JScrollPane(information), c);

        JPanel submitPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        updateInformation = new JButton(applicationFrame.getResourceBundle().getString("submit"));
        updateInformation.addActionListener(this);
        submitPanel.add(updateInformation);

        JPanel detailsTopPlacement = new JPanel(new BorderLayout());
        detailsTopPlacement.add(details, BorderLayout.CENTER);
        detailsTopPlacement.add(submitPanel, BorderLayout.SOUTH);
        contentPanes.addTab(applicationFrame.getResourceBundle().getString("details"), detailsTopPlacement);

        // Content.Participants
        JPanel participants = new JPanel(new BorderLayout());
        participants.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        equestrians = new StringJList(null);
        participants.add(equestrians, BorderLayout.CENTER);
        contentPanes.addTab(applicationFrame.getResourceBundle().getString("equestrians"), participants);

        // Insert table data
        for (String competition : applicationFrame.getSecretary().getCompetitions()) {
            newCompetitionInstance = true;
            itemList.addItem(competition);
        }

        setFieldsEnabled(false);
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == newCompetition)
            createCompetition();
        else if (event.getSource() == deleteCompetition)
            deleteCompetition();
        else if (event.getSource() == updateInformation)
            submitInformation();
        else if (event.getSource() == moveCompetitionUp)
            moveSelectionUp();
        else if (event.getSource() == moveCompetitionDown)
            moveSelectionDown();
    }

    public void valueChanged(ListSelectionEvent event) {
        if (event.getValueIsAdjusting() || newCompetitionInstance) {
            newCompetitionInstance = false;
            return;
        }

        CompetitionAssistant assistant = applicationFrame.getSecretary().getCompetition(itemList.getSelectedItem());
        if (assistant == null) {
            clearCompetitionDisplay();
            return;
        }

        displayCompetition(assistant);
    }

    private void moveSelectionUp() {
        ControllerStatus ret = applicationFrame.getSecretary().startCompetitionEarlier(itemList.getSelectedItem());
        if (ret != ControllerStatus.Success) {
            applicationFrame.reportOnError(ret, "competition", "moveCompetition");
            return;
        }

        itemList.moveSelectedUp();
    }

    private void moveSelectionDown() {
        ControllerStatus ret = applicationFrame.getSecretary().startCompetitionLater(itemList.getSelectedItem());
        if (ret != ControllerStatus.Success) {
            applicationFrame.reportOnError(ret, "competition", "moveCompetition");
            return;
        }

        itemList.moveSelectedDown();
    }

    private void createCompetition() {
        String competitionObject = applicationFrame.getSecretary().createCompetition();
        CompetitionAssistant competition = applicationFrame.getSecretary().getCompetition(competitionObject);
        newCompetitionInstance = true;
        itemList.addItem(competitionObject);
    }

    private void deleteCompetition() {
        String competitionObject = itemList.getSelectedItem();
        if (competitionObject == null)
            return;

        CompetitionAssistant competition = applicationFrame.getSecretary().getCompetition(competitionObject);
        if (applicationFrame.getSecretary().deleteCompetition(competitionObject))
            itemList.removeSelectedItem();
    }

    private void submitInformation() {
        CompetitionAssistant assistant = applicationFrame.getSecretary().getCompetition(itemList.getSelectedItem());
        if (assistant == null) {
            clearCompetitionDisplay();
            return;
        }

        newCompetitionInstance = true;
        itemList.removeSelectedItem();

        if (!assistant.getName().equals(name.getText()))
            applicationFrame.reportOnError(assistant.setName(name.getText()), "competition", "name");
        applicationFrame.reportOnError(assistant.setStartTime(startTime.getText()), "competition", "startTime");
        applicationFrame.reportOnError(assistant.setStartPrice(startPrice.getText()), "competition", "startPrice");
        applicationFrame.reportOnError(assistant.setDetails(information.getText()), "competition", "details");
        applicationFrame.reportOnError(assistant.setRandomizeStartList(randomizeStartList.isSelected()), "competition", "randomizeStartList");

        ResultType type = getSelectedResultType();
        if (assistant.getResultType() != type) {
            int ret = JOptionPane.showConfirmDialog(this,
                                                    applicationFrame.getResourceBundle().getString("resultTypeChangeWarning"),
                                                    applicationFrame.getResourceBundle().getString("competition"),
                                                    JOptionPane.YES_NO_OPTION,
                                                    JOptionPane.WARNING_MESSAGE);
            if (ret == 0)
                applicationFrame.reportOnError(assistant.setResultType(type), "competition", "resultType");
        }

        itemList.addItem(assistant.toString());
        applicationFrame.updateNotification(updateInformation);
        clearCompetitionDisplay();
    }

    private void clearCompetitionDisplay() {
        name.setText("");
        startTime.setText("");
        information.setText("");
        startPrice.setText("");
        resultType.setSelectedItem(ResultType.Text);
        equestrians.removeAllItems();
        randomizeStartList.setSelected(false);
        setFieldsEnabled(false);
    }

    private void displayCompetition(CompetitionAssistant competition) {
        ResourceBundle resultBundle = ResourceBundle.getBundle("ResultTypesBundle", applicationFrame.getResourceBundle().getLocale());
        name.setText(competition.getName());
        startTime.setText(competition.getStartTime());
        startPrice.setText(competition.getStartPrice());
        resultType.setSelectedItem(resultBundle.getString(competition.getResultType().toString()));
        information.setText(competition.getDetails());
        randomizeStartList.setSelected(competition.getRandomizeStartList());
        setFieldsEnabled(true);

        equestrians.removeAllItems();
        for (String equestrianObject : competition.getEquestrians())
            equestrians.addItem(equestrianObject);
    }

    private void setFieldsEnabled(boolean enabled) {
        name.setEnabled(enabled);
        startTime.setEnabled(enabled);
        information.setEnabled(enabled);
        startPrice.setEnabled(enabled);
        resultType.setEnabled(enabled);
        moveCompetitionUp.setEnabled(enabled);
        moveCompetitionDown.setEnabled(enabled);
        randomizeStartList.setEnabled(enabled);
    }

    private String[] getResultTypes() {
        ResourceBundle bundle = ResourceBundle.getBundle("ResultTypesBundle", applicationFrame.getResourceBundle().getLocale());
        ArrayList<String> list = new ArrayList<String>();

        for (String key : bundle.keySet())
            list.add(bundle.getString(key));

        String[] items = new String[list.size()];
        list.toArray(items);
        return items;
    }

    private ResultType getSelectedResultType() {
        ResourceBundle bundle = ResourceBundle.getBundle("ResultTypesBundle", applicationFrame.getResourceBundle().getLocale());
        String selection = (String)resultType.getSelectedItem();

        for (String key : bundle.keySet())
            if (selection.equals(bundle.getString(key)))
                return ResultType.valueOf(key);

        throw new RuntimeException("Could not find the ResultType from list");
    }
}
