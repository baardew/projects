package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.TemplateMenu;
import no.followesternriders.startlist.view.ApplicationFrame;
import no.followesternriders.startlist.view.MainMenu;

import no.followesternriders.startlist.controller.Secretary;

import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class StartMenu extends TemplateMenu {

    private JButton newSummit;
    private JButton openSummit;
    private JButton exit;

    public StartMenu(ApplicationFrame applicationFrame) {
        super(applicationFrame, false);

        newSummit = new JButton(applicationFrame.getResourceBundle().getString("new"));
        openSummit = new JButton(applicationFrame.getResourceBundle().getString("open"));
        exit = new JButton(applicationFrame.getResourceBundle().getString("quit"));

        newSummit.addActionListener(this);
        openSummit.addActionListener(this);
        exit.addActionListener(this);

        addButton(newSummit);
        addButton(openSummit);
        addButton(exit);
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == newSummit)
            openNewSummit();
        else if (event.getSource() == openSummit)
            openExistingSummit();
        else if (event.getSource() == exit)
            exit();
    }

    private void exit() {
        applicationFrame.dispose();
        System.exit(0);
    }

    private void openNewSummit() {
        File projectFile = getProjectFile(Secretary.Constructor.CREATE);
        if (projectFile == null)
            return;

        applicationFrame.setSecretary(new Secretary(projectFile, Secretary.Constructor.CREATE, applicationFrame.getResourceBundle()));
        applicationFrame.pushDisplay(new MainMenu(applicationFrame));
    }

    private void openExistingSummit() {
        File projectFile = getProjectFile(Secretary.Constructor.OPEN);
        if (projectFile == null)
            return;

        applicationFrame.setSecretary(new Secretary(projectFile, Secretary.Constructor.OPEN, applicationFrame.getResourceBundle()));
        applicationFrame.pushDisplay(new MainMenu(applicationFrame));
    }

    private File getProjectFile(Secretary.Constructor constructor) {
        JFileChooser fileFinder = new JFileChooser();
        fileFinder.setFileFilter(new FileNameExtensionFilter(applicationFrame.getResourceBundle().getString("fileFormatName"), "esl"));
        int action;

        if (constructor == Secretary.Constructor.OPEN)
            action = fileFinder.showOpenDialog(applicationFrame);
        else
            action = fileFinder.showSaveDialog(applicationFrame);

        if (action != JFileChooser.APPROVE_OPTION)
            return null;

        if (constructor == Secretary.Constructor.OPEN)
            return fileFinder.getSelectedFile();

        if (fileFinder.getSelectedFile().getName().endsWith(".esl"))
            return new File(fileFinder.getSelectedFile().getPath());

        return new File(fileFinder.getSelectedFile().getPath().concat(".esl"));
    }
}
