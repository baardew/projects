package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ResultTypeFactory;

import no.followesternriders.startlist.controller.Secretary;
import no.followesternriders.startlist.controller.ControllerStatus;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

import java.util.Stack;
import java.util.ResourceBundle;

@SuppressWarnings("serial")
public class ApplicationFrame extends JFrame implements ActionListener {

    private int screenWidth, screenHeight;
    private JPanel centerPanel;
    private JPanel centerComponent;

    private Stack<JPanel> frameStack;

    private Secretary secretary;

    private ResourceBundle locale;

    public ApplicationFrame(ResourceBundle bundle) {
        super(bundle.getString("programName"));
        locale = bundle;
        frameStack = new Stack<JPanel>();
        secretary = null;

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        this.screenWidth = (int)screen.getWidth();
        this.screenHeight = (int)screen.getHeight();

        centerComponent = null;
        centerPanel = new JPanel(new GridLayout());
        add(centerPanel);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    saveAndExit();
                }
            });

        loadAndSetSwingLocale();

        ResultTypeFactory.initialize(bundle);
    }

    public void saveAndExit() {
        if (secretary != null && !secretary.save()) {
            JOptionPane.showMessageDialog(this,
                                          locale.getString("saveErrorBody"),
                                          locale.getString("saveErrorTitle"),
                                          JOptionPane.OK_OPTION);
            return;
        }

        dispose();
        System.exit(0);
    }

    public void setSecretary(Secretary secretary) {
        if (this.secretary != null)
            throw new RuntimeException("Attempting to set Secretary when already set");

        this.secretary = secretary;
    }

    public Secretary getSecretary() {
        return secretary;
    }

    public ResourceBundle getResourceBundle() {
        return locale;
    }

    public void setVisible() {
        setMinimumSize(new Dimension(960, 720));
        pack();
        setLocation((screenWidth / 2) - (getWidth() / 2), (screenHeight / 2) - (getHeight() / 2));
        setVisible(true);
    }

    public void pushDisplay(JPanel panel) {
        frameStack.push(panel);
        setPanel(frameStack.peek());
    }

    public void popDisplay() {
        frameStack.pop();
        setPanel(frameStack.peek());
    }

    public JButton getBackButton() {
        JButton backButton = new JButton(locale.getString("back"));
        backButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent event) {
                    popDisplay();
                }
            });
        return backButton;
    }

    public void reportOnError(ControllerStatus status, String titleKey, String messageKey) {
        if (status == ControllerStatus.Success)
            return;

        if (status == ControllerStatus.Invalid) {
            JOptionPane.showMessageDialog(this,
                                          locale.getString(messageKey + "." + status),
                                          locale.getString(titleKey),
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        JOptionPane.showMessageDialog(this,
                                      locale.getString(messageKey + "." + status),
                                      locale.getString(titleKey),
                                      JOptionPane.WARNING_MESSAGE);
    }

    public void updateNotification(final JButton button) {
        button.setText(locale.getString("submitted"));
        button.setForeground(Color.GREEN.darker());

        new Thread() {
            public void run() {
                try{
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}

                button.setText(locale.getString("submit"));
                button.setForeground(Color.BLACK);
            }
        }.start();
    }

    private void setPanel(JPanel panel) {
        if (centerComponent != null) {
            centerPanel.remove(centerComponent);
            centerPanel.revalidate();
        }

        centerComponent = panel;
        centerPanel.add(panel);
        centerPanel.revalidate();
        repaint();
    }

    private void loadAndSetSwingLocale() {
        ResourceBundle bundle = ResourceBundle.getBundle("CustomSwingComponentBundle", locale.getLocale());

        for (String key : bundle.keySet())
            UIManager.getDefaults().put(key, bundle.getString(key));
    }

    public void actionPerformed(ActionEvent event) {
        // Nothing here
    }
}
