package no.followesternriders.startlist.view;

import javax.swing.JOptionPane;
import javax.swing.JComboBox;

public class StringItemSelection {

    private JComboBox<String> items;
    private String title;

    public StringItemSelection(String[] items, String title) {
        this.title = title;
        this.items = new JComboBox<String>(items);
    }

    public String getSelectedItem() {
        int confirm = JOptionPane.showOptionDialog(null,
                                                   items,
                                                   title,
                                                   JOptionPane.OK_CANCEL_OPTION,
                                                   JOptionPane.PLAIN_MESSAGE,
                                                   null,
                                                   null,
                                                   null);
        if (confirm != 0)
            return null;

        return (String)items.getSelectedItem();
    }
}
