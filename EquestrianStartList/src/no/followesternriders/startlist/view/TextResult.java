package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ResultPlacement;

import com.davekoelle.alphanum.AlphanumComparator;

import java.util.Comparator;
import java.util.ResourceBundle;

import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.GridLayout;

public class TextResult extends ResultPlacement {

    private JTextField result;

    public TextResult(ResourceBundle locale) {
        super(locale);
        result = new JTextField();
    }

    public JPanel getInputRepresentation() {
        JPanel panel = new JPanel(new GridLayout(1, 1));
        panel.add(result);
        return panel;
    }

    public void setValue(String result) {
        this.result.setText(result);
    }

    public String getValue() {
        return result.getText();
    }

    public boolean hasValidInput() {
        return true;
    }

    public Comparator<String[]> getResultSorter() {
        return new Comparator<String[]>() {
            private AlphanumComparator comp = new AlphanumComparator();
            public int compare(String[] a, String[] b) {
                return comp.compare(a[resultIndex], b[resultIndex]);
            }
        };
    }

    public void setFieldsEnabled(boolean enabled) {
        result.setEnabled(enabled);
    }
}
