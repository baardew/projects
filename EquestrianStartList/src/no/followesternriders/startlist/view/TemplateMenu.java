package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ApplicationFrame;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public abstract class TemplateMenu extends JPanel implements ActionListener {

    private JPanel buttons;
    private Dimension buttonMinimumSize;

    protected ApplicationFrame applicationFrame;

    public TemplateMenu(ApplicationFrame applicationFrame, boolean haveBackButton) {
        this.applicationFrame = applicationFrame;

        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.Y_AXIS));

        buttons = new JPanel();
        buttons.setLayout(new GridLayout(0, 1, 0, 5));
        buttonMinimumSize = new Dimension(192, 48);

        JPanel content = new JPanel();
        content.add(buttons);
        menuPanel.add(Box.createVerticalGlue());
        menuPanel.add(content);

        setLayout(new BorderLayout());
        add(menuPanel, BorderLayout.CENTER);

        if (haveBackButton) {
            JPanel rightAlign = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            rightAlign.add(applicationFrame.getBackButton());
            add(rightAlign, BorderLayout.NORTH);
        }
    }

    protected void addButton(JButton button) {
        button.setPreferredSize(buttonMinimumSize);
        buttons.add(button);
    }

    public abstract void actionPerformed(ActionEvent event);
}
