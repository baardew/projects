package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ApplicationFrame;
import no.followesternriders.startlist.view.TemplatePanel;
import no.followesternriders.startlist.view.StringJList;
import no.followesternriders.startlist.view.StringItemSelection;

import no.followesternriders.startlist.controller.EquestrianAssistant;

import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BorderFactory;

import javax.swing.event.ListSelectionEvent;

import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class EquestrianPanel extends TemplatePanel {

    private JButton newEquestrian;
    private JButton deleteEquestrian;

    private JButton addCompetition;
    private JButton removeCompetition;

    private JButton updateInformation;

    private JTextField riderName;
    private JTextField horseName;
    private JLabel startNumber;
    private StringJList competitions;
    private JTextField totalStartPrice;

    private boolean newEquestrianInstance;

    public EquestrianPanel(ApplicationFrame applicationFrame) {
        super(applicationFrame, false);

        newEquestrian = new JButton(applicationFrame.getResourceBundle().getString("new"));
        newEquestrian.addActionListener(this);
        addMenuItem(newEquestrian);
        deleteEquestrian = new JButton(applicationFrame.getResourceBundle().getString("remove"));
        deleteEquestrian.addActionListener(this);
        addMenuItem(deleteEquestrian);

        // Content panels
        JPanel content = new JPanel(new GridLayout());
        JTabbedPane contentPanes = new JTabbedPane();
        content.add(contentPanes);
        setContentPanel(content);

        // Content.Details
        JPanel details = new JPanel(new GridBagLayout());
        details.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.gridwidth = GridBagConstraints.RELATIVE;
        c.gridx = 0;
        c.gridy = 0;
        c.ipady = 10;

        riderName = new JTextField();
        horseName = new JTextField();
        startNumber = new JLabel();

        c.weightx = 0.1;
        details.add(new JLabel(applicationFrame.getResourceBundle().getString("riderName")), c);
        c.weightx = 0.9;
        c.gridx = 1;
        details.add(riderName, c);

        c.weightx = 0.1;
        c.gridy = 1;
        c.gridx = 0;
        details.add(new JLabel(applicationFrame.getResourceBundle().getString("horseName")), c);
        c.weightx = 0.9;
        c.weightx = 1;
        c.gridx = 1;
        details.add(horseName, c);

        c.weightx = 0.1;
        c.gridy = 2;
        c.gridx = 0;
        details.add(new JLabel(applicationFrame.getResourceBundle().getString("startNumber")), c);
        c.weightx = 0.9;
        c.weightx = 1;
        c.gridx = 1;
        details.add(startNumber, c);

        JPanel submitPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        updateInformation = new JButton(applicationFrame.getResourceBundle().getString("submit"));
        updateInformation.addActionListener(this);
        submitPanel.add(updateInformation);

        JPanel detailsTopPlacement = new JPanel(new BorderLayout());
        detailsTopPlacement.add(details, BorderLayout.NORTH);
        detailsTopPlacement.add(submitPanel, BorderLayout.SOUTH);
        contentPanes.addTab(applicationFrame.getResourceBundle().getString("details"), detailsTopPlacement);

        // Content.Classes
        JPanel classes = new JPanel(new BorderLayout());
        classes.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JPanel classButtons = new JPanel(new GridLayout(1, 0, 5, 5));
        JPanel classMenu = new JPanel(new FlowLayout());

        JPanel competitionPrice = new JPanel(new BorderLayout());
        totalStartPrice = new JTextField("0");
        totalStartPrice.setEditable(false);
        competitionPrice.add(new JLabel(applicationFrame.getResourceBundle().getString("totalStartPrice")), BorderLayout.WEST);
        competitionPrice.add(totalStartPrice, BorderLayout.CENTER);

        addCompetition = new JButton(applicationFrame.getResourceBundle().getString("new"));
        addCompetition.addActionListener(this);
        removeCompetition = new JButton(applicationFrame.getResourceBundle().getString("remove"));
        removeCompetition.addActionListener(this);

        classButtons.add(addCompetition);
        classButtons.add(removeCompetition);
        classMenu.add(classButtons);

        competitions = new StringJList(null);

        classes.add(classMenu, BorderLayout.NORTH);
        classes.add(competitions, BorderLayout.CENTER);
        classes.add(competitionPrice, BorderLayout.SOUTH);

        contentPanes.addTab(applicationFrame.getResourceBundle().getString("competitions"), classes);

        // Insert table data
        for (String equestrian : applicationFrame.getSecretary().getEquestrians()) {
            newEquestrianInstance = true;
            itemList.addItem(equestrian);
        }

        setFieldsEnabled(false);
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == newEquestrian)
            createEquestrian();
        else if (event.getSource() == deleteEquestrian)
            deleteEquestrian();
        else if (event.getSource() == addCompetition)
            addCompetition();
        else if (event.getSource() == removeCompetition)
            removeCompetition();
        else if (event.getSource() == updateInformation)
            submitInformation();
    }

    public void valueChanged(ListSelectionEvent event) {
        if (event.getValueIsAdjusting() || newEquestrianInstance) {
            newEquestrianInstance = false;
            return;
        }

        EquestrianAssistant assistant = applicationFrame.getSecretary().getEquestrian(itemList.getSelectedItem());
        if (assistant == null) {
            clearEquestrianDisplay();
            return;
        }

        displayEquestrian(assistant);
    }

    private void createEquestrian() {
        String equestrianObject = applicationFrame.getSecretary().createEquestrian();
        EquestrianAssistant equestrian = applicationFrame.getSecretary().getEquestrian(equestrianObject);
        newEquestrianInstance = true;
        itemList.addItem(equestrianObject);
    }

    private void deleteEquestrian() {
        String equestrianObject = itemList.getSelectedItem();
        if (equestrianObject == null)
            return;

        if (applicationFrame.getSecretary().deleteEquestrian(equestrianObject))
            itemList.removeSelectedItem();
    }

    private void addCompetition() {
        StringItemSelection item = new StringItemSelection(applicationFrame.getSecretary().getCompetitions(), applicationFrame.getResourceBundle().getString("selectCompetition"));
        String competitionObject = item.getSelectedItem();
        if (competitionObject == null)
            return;

        if (applicationFrame.getSecretary().addCompetitionToEquestrian(itemList.getSelectedItem(), competitionObject))
            competitions.addItem(competitionObject);

        EquestrianAssistant equestrian = applicationFrame.getSecretary().getEquestrian(itemList.getSelectedItem());
        if (equestrian != null)
            totalStartPrice.setText(equestrian.getTotalStartPrice());
    }

    private void removeCompetition() {
        String competitionObject = competitions.getSelectedItem();
        if (competitionObject == null)
            return;

        if (applicationFrame.getSecretary().removeCompetitionFromEquestrian(itemList.getSelectedItem(), competitionObject))
            competitions.removeSelectedItem();

        totalStartPrice.setText(applicationFrame.getSecretary().getEquestrian(itemList.getSelectedItem()).getTotalStartPrice());
    }

    private void submitInformation() {
        EquestrianAssistant assistant = applicationFrame.getSecretary().getEquestrian(itemList.getSelectedItem());
        if (assistant == null) {
            clearEquestrianDisplay();
            return;
        }

        newEquestrianInstance = true;
        itemList.removeSelectedItem();

        if (!assistant.getRiderName().equals(riderName.getText()))
            applicationFrame.reportOnError(assistant.setRiderName(riderName.getText()), "equestrian", "riderName");
        if (!assistant.getHorseName().equals(horseName.getText()))
            applicationFrame.reportOnError(assistant.setHorseName(horseName.getText()), "equestrian", "horseName");

        itemList.addItem(assistant.toString());
        applicationFrame.updateNotification(updateInformation);
        clearEquestrianDisplay();
    }

    private void clearEquestrianDisplay() {
        riderName.setText("");
        horseName.setText("");
        startNumber.setText("");
        competitions.removeAllItems();
        totalStartPrice.setText("");
        setFieldsEnabled(false);
    }

    private void displayEquestrian(EquestrianAssistant equestrian) {
        riderName.setText(equestrian.getRiderName());
        horseName.setText(equestrian.getHorseName());
        startNumber.setText(Integer.toString(equestrian.getStartNumber()));
        totalStartPrice.setText(equestrian.getTotalStartPrice());
        setFieldsEnabled(true);

        competitions.removeAllItems();
        for (String competitionObject : equestrian.getCompetitions())
            competitions.addItem(competitionObject);
    }

    private void setFieldsEnabled(boolean enabled) {
        riderName.setEnabled(enabled);
        horseName.setEnabled(enabled);
        addCompetition.setEnabled(enabled);
        removeCompetition.setEnabled(enabled);
    }
}
