package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.StringJList;
import no.followesternriders.startlist.view.ApplicationFrame;

import javax.swing.JPanel;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JLayeredPane;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import javax.swing.border.TitledBorder;

import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public abstract class TemplatePanel extends JPanel implements ActionListener, ListSelectionListener {

    private JPanel menuItems;
    private JPanel contentPanel;
    private JPanel menuBar;
    private TitledBorder contentBorder;

    protected StringJList itemList;
    protected ApplicationFrame applicationFrame;

    public TemplatePanel(ApplicationFrame applicationFrame, boolean useBorder) {
        this.applicationFrame = applicationFrame;

        contentPanel = null;
        itemList = new StringJList(this);

        JPanel menuItemContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
        menuItemContainer.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 15));
        menuItems = new JPanel(new GridLayout(1, 0, 5, 5));
        menuItemContainer.add(menuItems);

        JPanel backMenu = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 5));
        backMenu.add(applicationFrame.getBackButton());

        menuBar = new JPanel(new BorderLayout());
        menuBar.add(menuItemContainer, BorderLayout.CENTER);
        menuBar.add(backMenu, BorderLayout.EAST);

        JPanel menu = new JPanel(new BorderLayout());
        menu.add(menuBar, BorderLayout.NORTH);
        menu.add(itemList, BorderLayout.CENTER);

        if (useBorder) {
            contentBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 3),
                                                             "<title>",
                                                             TitledBorder.DEFAULT_JUSTIFICATION,
                                                             TitledBorder.DEFAULT_POSITION,
                                                             new Font(null, Font.PLAIN, 21));

        } else {
            contentBorder = null;
        }

        setLayout(new GridLayout(2, 1, 0, 5));
        add(menu);

        setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));
    }

    protected void addMenuItem(JComponent item) {
        if (menuItems == null)
            throw new RuntimeException("Adding a menu item with a custom menu panel is not possible");

        menuItems.add(item);
    }

    protected void setMenuItem(JPanel panel) {
        menuItems = null;
        panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 15));
        menuBar.add(panel, BorderLayout.CENTER);
    }

    protected void setContentPanel(JPanel content) {
        if (contentPanel != null)
            remove(contentPanel);

        if (contentBorder != null) {
            content.setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));

            JPanel borderPanel = new JPanel(new GridLayout(1, 1, 0, 0));
            borderPanel.add(content);
            borderPanel.setBorder(contentBorder);

            contentPanel = borderPanel;
            add(borderPanel);
        } else {
            contentPanel = content;
            add(content);
        }
    }

    protected void setContentTitle(String title) {
        if (contentBorder != null) {
            contentBorder.setTitle(title);
            repaint();
        }
    }

    public abstract void actionPerformed(ActionEvent event);
    public abstract void valueChanged(ListSelectionEvent event);
}
