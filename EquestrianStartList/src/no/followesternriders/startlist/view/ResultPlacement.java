package no.followesternriders.startlist.view;

import java.util.Comparator;
import java.util.ResourceBundle;

import javax.swing.JPanel;

public abstract class ResultPlacement {

    protected final ResourceBundle locale;
    protected final int resultIndex = 3;

    protected final String noResult;

    public ResultPlacement(ResourceBundle locale) {
        this.locale = locale;
        noResult = locale.getString("noResult");
    }

    public abstract JPanel getInputRepresentation();
    public abstract void setValue(String result);
    public abstract String getValue();
    public abstract boolean hasValidInput();
    public abstract Comparator<String[]> getResultSorter();
    public abstract void setFieldsEnabled(boolean enabled);
}
