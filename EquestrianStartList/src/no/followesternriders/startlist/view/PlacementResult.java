package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ResultPlacement;

import java.util.Comparator;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import java.awt.GridLayout;

public class PlacementResult extends ResultPlacement {

    private JComboBox<String> result;
    private String[] placements;

    private final int listSize = 30;

    public PlacementResult(ResourceBundle locale) {
        super(locale);
        placements = getPlacementsList();
        result = new JComboBox<String>(placements);
        placements = getPlacementsList();
    }

    public JPanel getInputRepresentation() {
        JPanel panel = new JPanel(new GridLayout(1, 1));
        panel.add(result);
        return panel;
    }

    public void setValue(String result) {
        this.result.setSelectedItem(result);
    }

    public String getValue() {
        return (String)result.getSelectedItem();
    }

    public boolean hasValidInput() {
        return true;
    }

    public Comparator<String[]> getResultSorter() {
        return new Comparator<String[]>() {
            private String[] placementList = getPlacementsList();

            public int compare(String[] a, String[] b) {
                int ia = 0;
                int ib = 0;

                for (int i = 0; i < listSize; i++) {
                    if (a[resultIndex].equals(placementList[i])) {
                        ia = i;
                        break;
                    }
                }

                for (int i = 0; i < listSize; i++) {
                    if (b[resultIndex].equals(placementList[i])) {
                        ib = i;
                        break;
                    }
                }

                return ia -ib;
            }
        };
    }

    private String[] getPlacementsList() {
        ResourceBundle list = ResourceBundle.getBundle("PlacementBundle", locale.getLocale());
        String[] items = new String[listSize];

        for (int i = 0; i < listSize; i++)
            items[i] = list.getString(Integer.toString(i + 1));

        return items;
    }

    public void setFieldsEnabled(boolean enabled) {
        result.setEnabled(enabled);
    }
}
