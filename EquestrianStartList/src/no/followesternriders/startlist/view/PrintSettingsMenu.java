package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ApplicationFrame;

import no.followesternriders.startlist.controller.DocumentPrinter;

import java.util.ResourceBundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.BorderFactory;

import java.io.File;

import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class PrintSettingsMenu extends JPanel implements ActionListener {

    private String imageSource;
    private JTextField imageDisplay;
    private JComboBox<String> tableHeaderColor;
    private JCheckBox enumerateCompetitionResults;
    private ResourceBundle locale;

    private ApplicationFrame applicationFrame;

    private JButton updateInformation;
    private JButton imageSourceOpen;

    public PrintSettingsMenu(ApplicationFrame applicationFrame, String imageSource, String tableHeaderColor) {
        this.applicationFrame = applicationFrame;
        this.locale = applicationFrame.getResourceBundle();
        this.imageDisplay = new JTextField(extractFilename(imageSource));
        this.tableHeaderColor = new JComboBox<String>(getColorOptions());
        this.enumerateCompetitionResults = new JCheckBox(locale.getString("enumerateCompetitionResults"));
        this.enumerateCompetitionResults.setSelected(DocumentPrinter.getEnumerateResults());
        this.tableHeaderColor.setSelectedItem(getStoredColor());

        imageDisplay.setEditable(false);

        JPanel backMenu = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 5));
        backMenu.add(applicationFrame.getBackButton());

        JPanel submitPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        updateInformation = new JButton(applicationFrame.getResourceBundle().getString("submit"));
        updateInformation.addActionListener(this);
        submitPanel.add(updateInformation);

        JPanel settingsPanel = new JPanel(new GridLayout());
        settingsPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 0;
        c.ipady = 10;

        c.weightx = 0.1;
        settingsPanel.add(new JLabel(locale.getString("image")), c);
        c.weightx = 0.8;
        c.gridx = 1;
        settingsPanel.add(this.imageDisplay, c);
        c.weightx = 0.1;
        c.gridx = 2;
        imageSourceOpen = new JButton(locale.getString("open"));
        imageSourceOpen.addActionListener(this);
        settingsPanel.add(imageSourceOpen, c);

        c.weightx = 0.1;
        c.gridy = 1;
        c.gridx = 0;
        settingsPanel.add(new JLabel(locale.getString("tableHeaderColor")), c);
        c.weightx = 0.9;
        c.gridx = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        settingsPanel.add(this.tableHeaderColor, c);

        c.weightx = 1;
        c.gridy = 2;
        settingsPanel.add(this.enumerateCompetitionResults, c);

        setBorder(BorderFactory.createEmptyBorder(10, 150, 10, 150));
        setLayout(new BorderLayout());
        add(backMenu, BorderLayout.NORTH);
        add(settingsPanel, BorderLayout.CENTER);
        add(submitPanel, BorderLayout.SOUTH);
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == updateInformation)
            updateSettings();
        else if (event.getSource() == imageSourceOpen)
            findImage();
    }

    private void updateSettings() {
        DocumentPrinter.setLogoImage(imageSource);
        DocumentPrinter.setEnumerateResults(enumerateCompetitionResults.isSelected());
        DocumentPrinter.setBGColor(getSelectedColor());

        applicationFrame.updateNotification(updateInformation);
    }

    private void findImage() {
        JFileChooser fileFinder = new JFileChooser();
        fileFinder.setFileFilter(new FileNameExtensionFilter(locale.getString("imageFiles"), "jpg", "jpeg", "png"));
        int action;
        action = fileFinder.showOpenDialog(this);
        if (action != JFileChooser.APPROVE_OPTION)
            return;

        imageSource = fileFinder.getSelectedFile().getPath();
        imageDisplay.setText(fileFinder.getSelectedFile().getName());
    }

    private String getSelectedColor() {
        ResourceBundle colorList = ResourceBundle.getBundle("ColorBundle", locale.getLocale());
        Enumeration<String> colorBundle = colorList.getKeys();
        String bgColor = null;

        while (colorBundle.hasMoreElements()) {
            String key = colorBundle.nextElement();
            if (colorList.getString(key).equals((String)tableHeaderColor.getSelectedItem())) {
                bgColor = key;
            }
        }

        return bgColor;
    }

    private String getStoredColor() {
        ResourceBundle colorList = ResourceBundle.getBundle("ColorBundle", locale.getLocale());
        Enumeration<String> colorBundle = colorList.getKeys();
        String bgColor = null;

        while (colorBundle.hasMoreElements()) {
            String key = colorBundle.nextElement();
            if (key.equals(DocumentPrinter.getBGColor())) {
                bgColor = colorList.getString(key);
            }
        }

        return bgColor;
    }

    private String[] getColorOptions() {
        ResourceBundle colorList = ResourceBundle.getBundle("ColorBundle", locale.getLocale());

        ArrayList<String> list = new ArrayList<String>();
        Enumeration<String> colorBundle = colorList.getKeys();

        while (colorBundle.hasMoreElements())
            list.add(colorList.getString(colorBundle.nextElement()));

        String[] colors = new String[list.size()];
        list.toArray(colors);
        Arrays.sort(colors);
        return colors;
    }

    private String extractFilename(String name) {
        if (name == null || name.length() == 0)
            return "";

        return new File(name).getName();
    }
}
