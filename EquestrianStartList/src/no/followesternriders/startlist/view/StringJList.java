package no.followesternriders.startlist.view;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;

import javax.swing.text.Position.Bias;

import javax.swing.event.ListSelectionListener;

import java.awt.BorderLayout;

@SuppressWarnings("serial")
public class StringJList extends JPanel {

    private DefaultListModel<String> model;
    private JList<String> list;

    public StringJList(ListSelectionListener listener) {
        setLayout(new BorderLayout());

        list = new JList<String>();

        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(-1);

        if (listener != null)
            list.addListSelectionListener(listener);

        model = new DefaultListModel<String>();
        list.setModel(model);

        add(new JScrollPane(list));
    }

    public boolean addItem(String item) {
        if (model.contains(item))
            return false;

        model.addElement(item);
        return true;
    }

    public void removeSelectedItem() {
        int index = list.getSelectedIndex();
        if (index == -1)
            return;

        model.removeElementAt(index);
    }

    public void removeAllItems() {
        list.clearSelection();
        model.clear();
    }

    public void clearSelection() {
        list.clearSelection();
    }

    public String getSelectedItem() {
        int index = list.getSelectedIndex();
        if (index == -1)
            return null;

        return list.getSelectedValue();
    }

    public void moveSelectedUp() {
        int index = list.getSelectedIndex();
        String item = getSelectedItem();

        if (index <= 0)
            return;

        model.removeElementAt(index);
        model.insertElementAt(item, index - 1);
    }

    public void moveSelectedDown() {
        int index = list.getSelectedIndex();
        String item = getSelectedItem();

        if (index == -1 || index + 1 >= model.size())
            return;

        model.removeElementAt(index);
        model.insertElementAt(item, index + 1);
    }
}
