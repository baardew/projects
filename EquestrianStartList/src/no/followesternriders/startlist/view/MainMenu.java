package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ApplicationFrame;

import no.followesternriders.startlist.view.TemplateMenu;
import no.followesternriders.startlist.view.EquestrianPanel;
import no.followesternriders.startlist.view.CompetitionPanel;
import no.followesternriders.startlist.view.ResultPanel;
import no.followesternriders.startlist.view.PrintMenu;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;

import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class MainMenu extends TemplateMenu {

    private JButton competition;
    private JButton equestrian;
    private JButton result;
    private JButton print;
    private JButton about;
    private JButton exit;

    public MainMenu(ApplicationFrame applicationFrame) {
        super(applicationFrame, false);

        competition = new JButton(applicationFrame.getResourceBundle().getString("competitions"));
        equestrian = new JButton(applicationFrame.getResourceBundle().getString("equestrians"));
        result = new JButton(applicationFrame.getResourceBundle().getString("results"));
        print = new JButton(applicationFrame.getResourceBundle().getString("print"));
        about = new JButton(applicationFrame.getResourceBundle().getString("about"));
        exit = new JButton(applicationFrame.getResourceBundle().getString("exit"));

        competition.addActionListener(this);
        equestrian.addActionListener(this);
        result.addActionListener(this);
        print.addActionListener(this);
        about.addActionListener(this);
        exit.addActionListener(this);

        addButton(competition);
        addButton(equestrian);
        addButton(result);
        addButton(print);
        addButton(about);
        addButton(exit);
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == competition)
            applicationFrame.pushDisplay(new CompetitionPanel(applicationFrame));
        else if (event.getSource() == equestrian)
            applicationFrame.pushDisplay(new EquestrianPanel(applicationFrame));
        else if (event.getSource() == result)
            applicationFrame.pushDisplay(new ResultPanel(applicationFrame));
        else if (event.getSource() == print)
            applicationFrame.pushDisplay(new PrintMenu(applicationFrame));
        else if (event.getSource() == about)
            displayAbout();
        else if (event.getSource() == exit)
            applicationFrame.saveAndExit();
    }

    private void displayAbout() {
        JPanel textarea = new JPanel(new GridLayout(0, 1, 5, 5));

        textarea.add(new JLabel(applicationFrame.getResourceBundle().getString("aboutTitle"), JLabel.CENTER));
        textarea.add(new JLabel(applicationFrame.getResourceBundle().getString("aboutVersion"), JLabel.CENTER));
        textarea.add(new JLabel(" "));
        textarea.add(new JLabel(applicationFrame.getResourceBundle().getString("aboutDeveloper") + ": Bård Eirik Winther"));
        textarea.add(new JLabel(applicationFrame.getResourceBundle().getString("aboutSourceCode") + ": bitbucket.org/baardew/projects"));

        JOptionPane.showMessageDialog(null,
                                      textarea,
                                      applicationFrame.getResourceBundle().getString("about"),
                                      JOptionPane.INFORMATION_MESSAGE);

    }
}
