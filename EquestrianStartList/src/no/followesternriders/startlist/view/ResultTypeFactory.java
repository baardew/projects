package no.followesternriders.startlist.view;

import no.followesternriders.startlist.model.ResultType;

import no.followesternriders.startlist.view.ResultPlacement;
import no.followesternriders.startlist.view.PlacementResult;
import no.followesternriders.startlist.view.ScoreResult;
import no.followesternriders.startlist.view.TextResult;

import java.util.ResourceBundle;

public class ResultTypeFactory {

    private static ResourceBundle locale = null;
    
    public static void initialize(ResourceBundle locale) {
        ResultTypeFactory.locale = locale;
    }
    
    public static ResultPlacement createResultTypePanel(ResultType type) {
        if (locale == null)
            throw new RuntimeException("ResultTypeFactory not initialized");
        
        if (type == ResultType.Text)
            return new TextResult(locale);
        else if (type == ResultType.Score)
            return new ScoreResult(locale);
        else if (type == ResultType.Placement)
            return new PlacementResult(locale);
        return null;
    }
}
