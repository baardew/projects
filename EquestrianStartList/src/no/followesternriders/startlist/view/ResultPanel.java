package no.followesternriders.startlist.view;

import no.followesternriders.startlist.view.ApplicationFrame;
import no.followesternriders.startlist.view.TemplatePanel;
import no.followesternriders.startlist.view.StringJList;
import no.followesternriders.startlist.view.ResultPlacement;
import no.followesternriders.startlist.view.ResultTypeFactory;

import no.followesternriders.startlist.model.ResultType;

import no.followesternriders.startlist.controller.ResultAssistant;
import no.followesternriders.startlist.controller.CompetitionAssistant;
import no.followesternriders.startlist.controller.ControllerStatus;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.BorderFactory;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.FlowLayout;

import javax.swing.event.ListSelectionEvent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

@SuppressWarnings("serial")
public class ResultPanel extends TemplatePanel {

    private JPanel scorePanel;
    private ResultPlacement score;
    private JComboBox<String> competitions;

    private JButton updateInformation;

    private boolean newResultInstance;

    public ResultPanel(ApplicationFrame applicationFrame) {
        super(applicationFrame, true);

        JPanel menuItems = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.gridwidth = GridBagConstraints.RELATIVE;

        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0;
        menuItems.add(new JLabel(applicationFrame.getResourceBundle().getString("competition")), c);

        c.weightx = 1;
        c.gridx = 1;
        // Include insertion of data
        competitions = new JComboBox<String>(applicationFrame.getSecretary().getCompetitions());
        competitions.addActionListener(this);
        menuItems.add(competitions, c);
        setMenuItem(menuItems);

        JPanel details = new JPanel(new GridBagLayout());
        scorePanel = new JPanel(new GridLayout(1, 1, 10, 10));
        score = ResultTypeFactory.createResultTypePanel(ResultType.Text);
        scorePanel.add(score.getInputRepresentation());
        c.weightx = 0.1;
        c.gridx = 0;
        details.add(new JLabel(applicationFrame.getResourceBundle().getString("result")), c);
        c.weightx = 0.9;
        c.gridx = 1;
        details.add(scorePanel, c);

        JPanel submitPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        updateInformation = new JButton(applicationFrame.getResourceBundle().getString("submit"));
        updateInformation.addActionListener(this);
        submitPanel.add(updateInformation);

        JPanel detailsTopPlacement = new JPanel(new BorderLayout());
        detailsTopPlacement.add(details, BorderLayout.NORTH);
        detailsTopPlacement.add(submitPanel, BorderLayout.SOUTH);
        setContentPanel(detailsTopPlacement);

        setContentTitle(" ");
        setFieldsEnabled(false);
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == updateInformation)
            submitInformation();
        else if (event.getSource() == competitions)
            displayEquestrians((String)competitions.getSelectedItem());
    }

    public void ItemListener(ItemEvent event) {
        if (event.getStateChange() != ItemEvent.SELECTED)
            return;
    }


    public void valueChanged(ListSelectionEvent event) {
        if (event.getValueIsAdjusting()) {
            newResultInstance = false;
            return;
        }

        ResultAssistant assistant = applicationFrame.getSecretary().getResult(itemList.getSelectedItem(), (String)competitions.getSelectedItem());
        if (assistant == null) {
            clearResultDisplay();
            return;
        }

        displayResult(assistant);
    }

    private void submitInformation() {
        ResultAssistant assistant = applicationFrame.getSecretary().getResult(itemList.getSelectedItem(), (String)competitions.getSelectedItem());
        if (assistant == null) {
            clearResultDisplay();
            return;
        }

        if (!score.hasValidInput()) {
            applicationFrame.reportOnError(ControllerStatus.Invalid, "result", "resultValue");
        } else {
            applicationFrame.reportOnError(assistant.setResult(score.getValue()), "result", "resultValue");
        }

        displayResult(assistant);
        applicationFrame.updateNotification(updateInformation);
        clearResultDisplay();
    }

    private void clearResultDisplay() {
        itemList.clearSelection();
        updateScorePanel(null);
        setContentTitle(" ");
        setFieldsEnabled(false);
    }

    private void displayResult(ResultAssistant result) {
        CompetitionAssistant competition = applicationFrame.getSecretary().getCompetition((String)competitions.getSelectedItem());
        score = ResultTypeFactory.createResultTypePanel(competition.getResultType());
        updateScorePanel(score);
        score.setValue(result.getResult());
        setContentTitle(itemList.getSelectedItem());
        setFieldsEnabled(true);
    }

    private void displayEquestrians(String competitionObject) {
        itemList.removeAllItems();
        CompetitionAssistant assistant = applicationFrame.getSecretary().getCompetition(competitionObject);
        for (String equestrianObject : assistant.getEquestrians()) {
            newResultInstance = true;
            itemList.addItem(equestrianObject);
        }
    }

    private void updateScorePanel(ResultPlacement panel) {
        scorePanel.removeAll();
        if (panel == null)
            scorePanel.add(ResultTypeFactory.createResultTypePanel(ResultType.Text).getInputRepresentation());
        else
            scorePanel.add(panel.getInputRepresentation());
        scorePanel.revalidate();
        scorePanel.repaint();
    }

    private void setFieldsEnabled(boolean enabled) {
        score.setFieldsEnabled(enabled);
    }
}
