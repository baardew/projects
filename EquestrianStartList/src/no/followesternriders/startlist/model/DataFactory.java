package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;

import java.util.ResourceBundle;

public class DataFactory {

    private static Summit summit = null;
    private static ResourceBundle locale = null;

    public static void initialize(Summit summit, ResourceBundle locale) {
        DataFactory.summit = summit;
        DataFactory.locale = locale;
    }

    public static Equestrian createEquestrian() {
        if (summit == null)
            throw new RuntimeException("DataFactory not initialized");

        Equestrian equestrian = new Equestrian();

        int riderNo = getRiderNameNumber();
        int horseNo = getHorseNameNumber();
        int maxNo = Math.max(riderNo, horseNo) + 1;

        equestrian.setRiderName(new String(locale.getString("defaultRiderName") + maxNo));
        equestrian.setHorseName(new String(locale.getString("defaultHorseName") + maxNo));
        equestrian.setStartNumber(getStartNumber());
        return equestrian;
    }

    public static Equestrian cloneEquestrian(Equestrian equestrian) {
        if (equestrian == null)
            return null;

        return new Equestrian(equestrian);
    }

    public static Competition createCompetition() {
        if (summit == null)
            throw new RuntimeException("DataFactory not initialized");

        Competition competition = new Competition();
        int maxNo = getCompetitionNameNumber() + 1;
        competition.setName(locale.getString("defaultCompetitionName") + maxNo);
        competition.setStartOrder(getStartOrder());

        return competition;
    }

    public static Competition cloneCompetition(Competition competition) {
        if (competition == null)
            return null;

        return new Competition(competition);
    }

    public static Result createResult() {
        if (summit == null)
            throw new RuntimeException("DataFactory not initialized");

        return new Result();
    }

    private static int getRiderNameNumber() {
        int maxNumber = 0;

        for (Equestrian eq : summit.getEquestrians()) {
            String nameString = new String(eq.getRiderName());
            if (nameString.startsWith(locale.getString("defaultRiderName"))) {
                nameString = nameString.substring(locale.getString("defaultRiderName").length(), nameString.length());

                int newNumber = maxNumber;
                try {
                    newNumber = Integer.parseInt(nameString);
                } catch (NumberFormatException e) {
                    // Ignore
                }

                if (newNumber > maxNumber)
                    maxNumber = newNumber;
            }
        }

        return maxNumber;
    }

    private static int getHorseNameNumber() {
        int maxNumber = 0;

        for (Equestrian eq : summit.getEquestrians()) {
            String nameString = new String(eq.getHorseName());
            if (nameString.startsWith(locale.getString("defaultHorseName"))) {
                nameString = nameString.substring(locale.getString("defaultHorseName").length(), nameString.length());

                int newNumber = maxNumber;
                try {
                    newNumber = Integer.parseInt(nameString);
                } catch (NumberFormatException e) {
                    // Ignore
                }

                if (newNumber > maxNumber)
                    maxNumber = newNumber;
            }
        }

        return maxNumber;
    }

    private static int getStartNumber() {
        return summit.getEquestrians().length + 1;
    }

    private static int getStartOrder() {
        return summit.getCompetitions().length + 1;
    }

    private static int getCompetitionNameNumber() {
        int maxNumber = 0;

        for (Competition com : summit.getCompetitions()) {
            String nameString = new String(com.getName());
            if (nameString.startsWith(locale.getString("defaultCompetitionName"))) {
                nameString = nameString.substring(locale.getString("defaultCompetitionName").length(), nameString.length());

                int newNumber = maxNumber;
                try {
                    newNumber = Integer.parseInt(nameString);
                } catch (NumberFormatException e) {
                    // Ignore
                }

                if (newNumber > maxNumber)
                    maxNumber = newNumber;
            }
        }

        return maxNumber;
    }

}
