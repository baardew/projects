package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.Result;

import java.util.HashMap;
import java.util.ArrayList;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Equestrian implements Serializable {

    private String riderName;
    private String horseName;
    private int startNumber;
    private ArrayList<Competition> competitions;
    private HashMap<Competition, Result> results;

    public Equestrian() {
        riderName = new String();
        horseName = new String();
        startNumber = 1;
        competitions = new ArrayList<Competition>();
        results = new HashMap<Competition, Result>();
    }

    public Equestrian(Equestrian equestrian) {
        riderName = new String(equestrian.riderName);
        horseName = new String(equestrian.horseName);
        startNumber = equestrian.startNumber;
        competitions = new ArrayList<Competition>(equestrian.competitions);
        results = new HashMap<Competition, Result>(equestrian.results);
    }

    public boolean addCompetition(Competition competition) {
        if (competition == null || competitions.contains(competition))
            return false;

        return competitions.add(competition);
    }

    public boolean removeCompetition(Competition competition) {
        return competitions.remove(competition);
    }

    public ArrayList<Competition> getCompetitions() {
        return competitions;
    }

    public boolean updateResult(Competition competition, Result result) {
        if (competition == null || result == null)
            return false;

        results.put(competition, result);
        return true;
    }

    public Result getResult(Competition competition) {
        return results.get(competition);
    }

    public boolean setRiderName(String name) {
        if (name == null || name.length() == 0)
            return false;

        riderName = name;
        return true;
    }

    public String getRiderName() {
        return riderName;
    }

    public boolean setHorseName(String name) {
        if (name == null || name.length() == 0)
            return false;

        horseName = name;
        return true;
    }

    public String getHorseName() {
        return horseName;
    }

    public boolean setStartNumber(int number) {
        if (number <= 0)
            return false;

        startNumber = number;
        return true;
    }

    public int getStartNumber() {
        return startNumber;
    }

    public String toString() {
        return riderName + " & " + horseName;
    }
}
