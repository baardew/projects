package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;

import com.davekoelle.alphanum.AlphanumComparator;

import java.util.HashMap;
import java.util.Arrays;
import java.util.Comparator;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Summit implements Serializable {

    private String name;
    private HashMap<String, Equestrian> equestrians;
    private HashMap<String, Competition> competitions;

    public Summit() {
        name = new String();
        equestrians = new HashMap<String, Equestrian>();
        competitions = new HashMap<String, Competition>();
    }

    public boolean addEquestrian(Equestrian equestrian) {
        if (equestrian == null)
            return false;

        if (equestrians.containsKey(equestrian.toString()))
            return false;

        equestrians.put(equestrian.toString(), equestrian);
        return true;
    }

    public boolean removeEquestrian(Equestrian equestrian) {
        if (equestrian == null)
            return false;

        return equestrians.remove(equestrian.toString()) != null;
    }

    public Equestrian getEquestrian(String objectString) {
        return equestrians.get(objectString);
    }

    public Equestrian[] getEquestrians() {
        Equestrian[] listed = new Equestrian[equestrians.size()];

        int index = 0;
        for (Equestrian eq : equestrians.values())
            listed[index++] = eq;

        Arrays.sort(listed, new Comparator<Equestrian>() {
                private AlphanumComparator comp = new AlphanumComparator();
                public int compare(Equestrian e1, Equestrian e2) {
                    return comp.compare(e1.toString(), e2.toString());
                }
            });

        return listed;
    }

    public boolean addCompetition(Competition competition) {
        if (competition == null)
            return false;

        if (competitions.containsKey(competition.toString()))
            return false;

        competitions.put(competition.toString(), competition);
        return true;
    }

    public boolean removeCompetition(Competition competition) {
        if (competition == null)
            return false;

        return competitions.remove(competition.toString()) != null;
    }

    public Competition getCompetition(String objectString) {
        return competitions.get(objectString);
    }

    public Competition[] getCompetitions() {
        Competition[] listed = new Competition[competitions.size()];

        int index = 0;
        for (Competition eq : competitions.values())
            listed[index++] = eq;

        Arrays.sort(listed, new Comparator<Competition>() {
                private AlphanumComparator comp = new AlphanumComparator();
                public int compare(Competition e1, Competition e2) {
                    return e1.getStartOrder() - e2.getStartOrder();
                }
            });

        return listed;
    }

    public boolean setName(String name) {
        if (name == null || name.length() == 0)
            return false;

        this.name = name;
        return true;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
