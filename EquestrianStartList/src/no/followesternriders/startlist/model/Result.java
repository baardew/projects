package no.followesternriders.startlist.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Result implements Serializable {

    private String result;

    public Result() {
        result = new String();
    }

    public boolean setResult(String result) {
        if (result == null)
            return false;

        this.result = result;
        return true;
    }

    public String getResult() {
        return result;
    }

    public String toString() {
        return result;
    }
}
