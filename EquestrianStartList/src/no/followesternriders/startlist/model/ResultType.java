package no.followesternriders.startlist.model;

public enum ResultType {
    Placement, Score, Text;
}
