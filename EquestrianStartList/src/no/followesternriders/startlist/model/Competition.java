package no.followesternriders.startlist.model;

import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.ResultType;

import java.util.ArrayList;
import java.util.Random;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Competition implements Serializable {

    private String name;
    private String details;
    private String startTime;
    private int startPrice;
    private int startOrder;
    private int randomSeed;
    private boolean randomizeStartList;
    private ResultType resultType;
    private ArrayList<Equestrian> equestrians;

    public Competition() {
        name = new String();
        details = new String();
        startTime = new String();
        startPrice = 0;
        startOrder = 0;
        randomizeStartList = false;
        randomSeed = new Random().nextInt();
        resultType = ResultType.Text;
        equestrians = new ArrayList<Equestrian>();
    }

    public Competition(Competition competition) {
        name = new String(competition.name);
        details = new String(competition.details);
        startTime = new String(competition.startTime);
        startPrice = competition.startPrice;
        resultType = competition.resultType;
        startOrder = competition.startOrder;
        randomSeed = competition.randomSeed;
        randomizeStartList = competition.randomizeStartList;
        equestrians = new ArrayList<Equestrian>(competition.equestrians);
    }

    public boolean addEquestrian(Equestrian equestrian) {
        if (equestrian == null || equestrians.contains(equestrian))
            return false;

        return equestrians.add(equestrian);
    }

    public boolean removeEquestrian(Equestrian equestrian) {
        return equestrians.remove(equestrian);
    }

    public ArrayList<Equestrian> getEquestrians() {
        return equestrians;
    }

    public boolean setName(String name) {
        if (name == null || name.length() == 0)
            return false;

        this.name = name;
        return true;
    }

    public String getName() {
        return name;
    }

    public boolean setDetails(String details) {
        if (details == null)
            return false;

        this.details = details;
        return true;
    }

    public String getDetails() {
        return details;
    }

    public boolean setStartTime(String startTime) {
        if (startTime == null)
            return false;

        this.startTime = startTime;
        return true;
    }

    public boolean setStartPrice(int startPrice) {
        if (startPrice < 0)
            return false;

        this.startPrice = startPrice;
        return true;
    }

    public boolean setStartOrder(int startOrder) {
        if (startOrder <= 0)
            return false;

        this.startOrder = startOrder;
        return true;
    }

    public boolean setRandomizeStartList(boolean randomizeStart) {
        randomizeStartList = randomizeStart;
        return true;
    }

    public boolean getRandomizeStartList() {
        return randomizeStartList;
    }

    public boolean setResultType(ResultType type) {
        if (type == null)
            return false;

        resultType = type;
        return true;
    }

    public String getStartTime() {
        return startTime;
    }

    public String toString() {
        return name;
    }

    public int getStartPrice() {
        return startPrice;
    }

    public int getStartOrder() {
        return startOrder;
    }

    public int getRandomSeed() {
        return randomSeed;
    }

    public ResultType getResultType() {
        return resultType;
    }
}
