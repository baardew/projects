package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.DocumentDrawer;
import no.followesternriders.startlist.controller.ControllerStatus;
import no.followesternriders.startlist.controller.GenericArrayShuffler;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.ResultType;

import com.moneydance.modules.features.detailedBudget.DocumentRenderer;

import java.io.Reader;
import java.io.StringReader;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.Locale;
import java.util.Arrays;
import java.util.Random;

import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTMLDocument;

public class DocumentPrinter {

    private static Summit summit = null;
    private static String logoImage = "";
    private static String bgColor = "";
    private static ResourceBundle locale = null;
    private static boolean enumerateResults = false;


    private static String noResult = "";
    private static String[] startNumbersHeader = new String[3];
    private static String[] competitionHeader = startNumbersHeader;
    private static String[] resultHeader = new String[4];
    private static String[] startListHeader = new String[4];

    public static void initialize(Summit summit, ResourceBundle locale, String bgColor) {
        if (summit == null || locale == null || bgColor == null) {
            DocumentPrinter.summit = null;
            DocumentPrinter.locale = null;
            DocumentPrinter.bgColor = null;
            return;
        }

        DocumentPrinter.summit = summit;
        DocumentPrinter.locale = locale;
        DocumentPrinter.bgColor = bgColor;

        DocumentPrinter.noResult = locale.getString("noResult");

        competitionHeader[0] = locale.getString("startNumber");
        competitionHeader[1] = locale.getString("rider");
        competitionHeader[2] = locale.getString("horse");
        competitionHeader = startNumbersHeader;

        resultHeader[0] = locale.getString("startNumber");
        resultHeader[1] = locale.getString("rider");
        resultHeader[2] = locale.getString("horse");
        resultHeader[3] = locale.getString("result");

        startListHeader[0] = new String();
        startListHeader[1] = locale.getString("startNumber");
        startListHeader[2] = locale.getString("rider");
        startListHeader[3] = locale.getString("horse");
    }

    public static ControllerStatus setLogoImage(String src) {
        if (src == null)
            return ControllerStatus.Invalid;

        DocumentPrinter.logoImage = src;
        return ControllerStatus.Success;
    }

    public static String getLogoImage() {
        return DocumentPrinter.logoImage;
    }

    public static ControllerStatus setEnumerateResults(boolean doEnumerate) {
        DocumentPrinter.enumerateResults = doEnumerate;
        return ControllerStatus.Success;
    }

    public static boolean getEnumerateResults() {
        return DocumentPrinter.enumerateResults;
    }

    public static ControllerStatus setBGColor(String color) {
        if (color == null)
            return ControllerStatus.Invalid;

        DocumentPrinter.bgColor = color;
        return ControllerStatus.Success;
    }

    public static String getBGColor() {
        return DocumentPrinter.bgColor;
    }

    public static ControllerStatus printStartNumbers() {
        if (summit == null || locale == null)
            throw new RuntimeException("DocumentPrinter not initialized");

        Equestrian[] equestrians = summit.getEquestrians();
        if (equestrians.length == 0)
            return ControllerStatus.Empty;

        ArrayList<String[]> table = getEquestrianTable(Arrays.asList(equestrians), 3, 0);

        sortTable(table, null, 0);

        DocumentDrawer drawer = new DocumentDrawer(locale.getString("startNumbers"), summit.getName(), logoImage, new String(), bgColor);
        drawer.addTable(startNumbersHeader, table);
        printDocument(drawer.getHTML());
        return ControllerStatus.Success;
    }

    public static ControllerStatus printStartList() {
        if (summit == null || locale == null)
            throw new RuntimeException("DocumentPrinter not initialized");

        DocumentDrawer drawer = new DocumentDrawer("", summit.getName(), "", "", bgColor);

        if (summit.getCompetitions().length == 0)
            return ControllerStatus.Empty;

        for (Competition competition : summit.getCompetitions()) {
            startListHeader[0] = competition.getName();

            List<Equestrian> equestrians = competition.getEquestrians();
            ArrayList<String[]> table = getEquestrianTable(equestrians, 4, 1);

            if (competition.getRandomizeStartList()) {
                String[][] shuffleTable = new String[table.size()][4];
                table.toArray(shuffleTable);
                table.clear();
                GenericArrayShuffler.<String[]>shuffle(shuffleTable, new Random(competition.getRandomSeed()));
                for (int i = 0; i < shuffleTable.length; i++)
                    table.add(shuffleTable[i]);
            } else {
                sortTable(table, null, 1);
            }

            table.get(0)[0] = competition.getStartTime();
            drawer.addPartialTable(startListHeader, table);
        }

        printDocument(drawer.getHTML());
        return ControllerStatus.Success;
    }

    public static ControllerStatus printCompetition(String competitionObject) {
        if (summit == null || locale == null)
            throw new RuntimeException("DocumentPrinter not initialized");

        Competition competition = summit.getCompetition(competitionObject);
        if (competition == null)
            return ControllerStatus.Invalid;

        List<Equestrian> equestrians = competition.getEquestrians();
        if (equestrians.size() == 0)
            return ControllerStatus.Empty;

        ArrayList<String[]> table = getEquestrianTable(equestrians, 3, 0);
        if (competition.getRandomizeStartList()) {
            String[][] shuffleTable = new String[table.size()][];
            table.toArray(shuffleTable);
            table.clear();
            GenericArrayShuffler.<String[]>shuffle(shuffleTable, new Random(competition.getRandomSeed()));
            for (int i = 0; i < shuffleTable.length; i++)
                table.add(shuffleTable[i]);
        } else {
            sortTable(table, null, 0);
        }

        DocumentDrawer drawer = new DocumentDrawer(competition.getName(), summit.getName(), logoImage, competition.getDetails(), bgColor);
        drawer.addTable(competitionHeader, table);
        printDocument(drawer.getHTML());
        return ControllerStatus.Success;
    }

    public static ControllerStatus printResults(String competitionObject, Comparator<String[]> sortMethod) {
        if (summit == null || locale == null)
            throw new RuntimeException("DocumentPrinter not initialized");

        Competition competition = summit.getCompetition(competitionObject);
        if (competition == null)
            return ControllerStatus.Invalid;

        List<Equestrian> equestrians = competition.getEquestrians();
        if (equestrians.size() == 0)
            return ControllerStatus.Empty;

        ArrayList<String[]> table = getEquestrianTable(equestrians, 4, 0);
        addEquestrianResults(table, equestrians, competition);
        sortTable(table, sortMethod, 0);

        if (enumerateResults && competition.getResultType() != ResultType.Text)
            enumerateResults(table);

        DocumentDrawer drawer = new DocumentDrawer(competition.getName(), summit.getName(), logoImage, competition.getDetails(), bgColor);
        drawer.addTable(resultHeader, table);
        printDocument(drawer.getHTML());
        return ControllerStatus.Success;
    }

    private static ArrayList<String[]> getEquestrianTable(List<Equestrian> equestrians, int rowLength, int offset) {
        ArrayList<String[]> table = new ArrayList<String[]>(equestrians.size());

        for (Equestrian equestrian : equestrians) {
            String[] row = new String[rowLength];

            for (int i = 0; i < offset; i++)
                row[i] = new String();

            row[0 + offset] = Integer.toString(equestrian.getStartNumber());
            row[1 + offset] = equestrian.getRiderName();
            row[2 + offset] = equestrian.getHorseName();

            table.add(row);
        }

        return table;
    }

    private static void addEquestrianResults(ArrayList<String[]> table, List<Equestrian> equestrians, Competition competition) {
        for (int i = 0; i < table.size(); i++) {
            Result result = equestrians.get(i).getResult(competition);
            table.get(i)[3] = (result == null ? noResult : result.getResult());
        }
    }

    private static void sortTable(ArrayList<String[]> table, Comparator<String[]> sortingMethod, final int offset) {
        if (sortingMethod == null) {
            // Default sorting is by start number
            sortingMethod = new Comparator<String[]>() {
                public int compare(String[] s1, String[] e2) {
                    return Integer.parseInt(s1[0 + offset]) - Integer.parseInt(e2[0 + offset]);
                }
            };
        }

        Collections.sort(table, sortingMethod);
    }

    private static void enumerateResults(ArrayList<String[]> table) {
        ResourceBundle enumerateList = ResourceBundle.getBundle("PlacementBundle", locale.getLocale());

        for (int i = 0; i < table.size(); i++)
            table.get(i)[3] = (table.get(i)[3].equals(noResult) ? noResult : enumerateList.getString(Integer.toString(i + 1)));
    }

    private static void printDocument(String page) {
        DocumentRenderer render = new DocumentRenderer();
        Reader stringReader = new StringReader(page);
        HTMLEditorKit htmlKit = new HTMLEditorKit();
        HTMLDocument htmlDoc = (HTMLDocument)htmlKit.createDefaultDocument();

        try {
            htmlKit.read(stringReader, htmlDoc, 0);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        render.print(htmlDoc);
    }
}
