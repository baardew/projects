package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.ControllerStatus;

import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.DataFactory;

import com.davekoelle.alphanum.AlphanumComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class EquestrianAssistant {

    private final Summit summit;
    private Equestrian equestrian;

    public EquestrianAssistant(Summit summit, Equestrian equestrian) {
        this.summit = summit;
        this.equestrian = equestrian;
    }

    public ControllerStatus setRiderName(String name) {
        Equestrian entry = DataFactory.cloneEquestrian(equestrian);

        if (!entry.setRiderName(name))
            return ControllerStatus.Invalid;

        if (!summit.addEquestrian(entry))
            return ControllerStatus.Exists;

        if (!summit.removeEquestrian(equestrian))
            throw new RuntimeException("Equestrian object not found, but should be present.");

        if (!notifyCompetitions(equestrian, entry))
            throw new RuntimeException("Could not update Competition object(s), but should be possible");

        equestrian = entry;

        return ControllerStatus.Success;
    }

    public ControllerStatus setHorseName(String name) {
        Equestrian entry = DataFactory.cloneEquestrian(equestrian);

        if (!entry.setHorseName(name))
            return ControllerStatus.Invalid;

        if (!summit.addEquestrian(entry))
            return ControllerStatus.Exists;

        if (!summit.removeEquestrian(equestrian))
            throw new RuntimeException("Equestrian object not found, but should be present.");

        if (!notifyCompetitions(equestrian, entry))
            throw new RuntimeException("Could not update Competition object(s), but should be possible");

        equestrian = entry;

        return ControllerStatus.Success;
    }

    public String getRiderName() {
        return equestrian.getRiderName();
    }

    public String getHorseName() {
        return equestrian.getHorseName();
    }

    public int getStartNumber() {
        return equestrian.getStartNumber();
    }

    public String getTotalStartPrice() {
        int total = 0;

        for (Competition competition : equestrian.getCompetitions())
            total += competition.getStartPrice();

        return Integer.toString(total);
    }

    public String toString() {
        return equestrian.toString();
    }

    public String[] getCompetitions() {
        ArrayList<Competition> competitions = equestrian.getCompetitions();
        Collections.sort(competitions, new Comparator<Competition>() {
                private AlphanumComparator comp = new AlphanumComparator();
                public int compare(Competition e1, Competition e2) {
                    return comp.compare(e1.toString(), e2.toString());
                }
            });

        String[] listedCompetitions = new String[competitions.size()];
        for (int i = 0; i < competitions.size(); i++)
            listedCompetitions[i] = competitions.get(i).toString();

        return listedCompetitions;

    }

    private boolean notifyCompetitions(Equestrian oldEquestrian, Equestrian newEquestrian) {
        for (Competition competition : newEquestrian.getCompetitions()) {
            if (!competition.removeEquestrian(oldEquestrian))
                return false;

            if (!competition.addEquestrian(newEquestrian))
                return false;
        }

        return true;
    }
}
