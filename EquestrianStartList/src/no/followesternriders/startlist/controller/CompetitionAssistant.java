package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.ControllerStatus;
import no.followesternriders.startlist.controller.GenericArrayShuffler;


import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.DataFactory;
import no.followesternriders.startlist.model.ResultType;
import no.followesternriders.startlist.model.Result;

import com.davekoelle.alphanum.AlphanumComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class CompetitionAssistant {

    private final Summit summit;
    private Competition competition;

    public CompetitionAssistant(Summit summit, Competition competition) {
        this.summit = summit;
        this.competition = competition;
    }

    public ControllerStatus setName(String name) {
        Competition entry = DataFactory.cloneCompetition(competition);

        if (!entry.setName(name))
            return ControllerStatus.Invalid;

        if (!summit.addCompetition(entry))
            return ControllerStatus.Exists;

        if (!summit.removeCompetition(competition))
            throw new RuntimeException("Competition object not found, but should be present.");

        if (!notifyEquestrians(competition, entry))
            throw new RuntimeException("Could not update Equestrian object(s), but should be possible");

        competition = entry;

        return ControllerStatus.Success;
    }

    public ControllerStatus setDetails(String details) {
        if (!competition.setDetails(details))
            return ControllerStatus.Invalid;

        return ControllerStatus.Success;
    }

    public ControllerStatus setStartTime(String startTime) {
        if (!competition.setStartTime(startTime))
            return ControllerStatus.Invalid;

        return ControllerStatus.Success;
    }

    public ControllerStatus setStartPrice(String startPrice) {
        if (startPrice == null)
            return ControllerStatus.Invalid;

        if (startPrice.length() == 0) {
            if (!competition.setStartPrice(0))
                return ControllerStatus.Invalid;

            return ControllerStatus.Success;
        }

        int price = 0;
        try {
            price = Integer.parseInt(startPrice);
        } catch (NumberFormatException e) {
            return ControllerStatus.Invalid;
        }

        if (!competition.setStartPrice(price))
            return ControllerStatus.Invalid;

        return ControllerStatus.Success;
    }

    public ControllerStatus setRandomizeStartList(boolean randomizeStart) {
        if (!competition.setRandomizeStartList(randomizeStart))
            return ControllerStatus.Invalid;

        return ControllerStatus.Success;
    }

    public String getName() {
        return competition.getName();
    }

    public String getDetails() {
        return competition.getDetails();
    }

    public String getStartTime() {
        return competition.getStartTime();
    }

    public String getStartPrice() {
        return Integer.toString(competition.getStartPrice());
    }

    public boolean getRandomizeStartList() {
        return competition.getRandomizeStartList();
    }

    public String[] getEquestrians() {
        ArrayList<Equestrian> equestrians = competition.getEquestrians();

        if (!competition.getRandomizeStartList()) {
            Collections.sort(equestrians, new Comparator<Equestrian>() {
                    private AlphanumComparator comp = new AlphanumComparator();
                    public int compare(Equestrian e1, Equestrian e2) {
                        return comp.compare(e1.toString(), e2.toString());
                    }
                });
        }

        String[] listedEquestrians = new String[equestrians.size()];

        for (int i = 0; i < equestrians.size(); i++)
            listedEquestrians[i] = equestrians.get(i).toString();

        if (competition.getRandomizeStartList())
            GenericArrayShuffler.<String>shuffle(listedEquestrians, new Random(competition.getRandomSeed()));

        return listedEquestrians;
    }

    public ControllerStatus setResultType(ResultType type) {
        if (competition.getResultType() == type)
            return ControllerStatus.Exists;

        if (!competition.setResultType(type))
            return ControllerStatus.Invalid;

        for (Equestrian equestrian : competition.getEquestrians()) {
            Result result = equestrian.getResult(competition);
            if (result != null)
                result.setResult("");
        }

        return ControllerStatus.Success;
    }

    public ResultType getResultType() {
        return competition.getResultType();
    }

    public String toString() {
        return competition.toString();
    }

    private boolean notifyEquestrians(Competition oldCompetition, Competition newCompetition) {
        for (Equestrian equestrian : newCompetition.getEquestrians()) {
            if (!equestrian.removeCompetition(oldCompetition))
                return false;

            if (!equestrian.addCompetition(newCompetition))
                return false;
        }

        return true;
    }
}
