package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.ResultAssistant;
import no.followesternriders.startlist.controller.EquestrianAssistant;
import no.followesternriders.startlist.controller.CompetitionAssistant;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;

public class AssistantFactory {

    private static Summit summit = null;

    public static void initialize(Summit summit) {
        AssistantFactory.summit = summit;
    }

    public static EquestrianAssistant createEquestrianAssistant(Equestrian equestrian) {
        if (summit == null)
            throw new RuntimeException("AssistantFactory not initialized");

        if (equestrian == null)
            return null;

        return new EquestrianAssistant(summit, equestrian);
    }

    public static CompetitionAssistant createCompetitionAssistant(Competition competition) {
        if (summit == null)
            throw new RuntimeException("AssistantFactory not initialized");

        if (competition == null)
            return null;

        return new CompetitionAssistant(summit, competition);
    }

    public static ResultAssistant createResultAssistant(Result result) {
        if (summit == null)
            throw new RuntimeException("AssistantFactory not initialized");

        if (result == null)
            return null;

        return new ResultAssistant(result);
    }
}
