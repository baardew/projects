package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.model.Result;

import no.followesternriders.startlist.controller.ControllerStatus;

public class ResultAssistant {

    private final Result result;

    public ResultAssistant(Result result) {
        this.result = result;
    }

    public ControllerStatus setResult(String value) {
        if (!result.setResult(value))
            return ControllerStatus.Invalid;

        return ControllerStatus.Success;
    }

    public String getResult() {
        return result.getResult();
    }
}
