package no.followesternriders.startlist.controller;

import java.io.File;
import java.io.IOException;
import java.io.EOFException;
import java.io.Serializable;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.ObjectInputStream;

import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.ObjectOutputStream;

import java.util.ArrayList;

/**
 * @source Adapted from no.wintherproduksjoner.synthservice.db.engine
 */
public class FileHandleLite<T> {

    private final String filename;

    private ObjectInputStream in = null;
    private ObjectOutputStream out = null;

    public FileHandleLite(String filename) {
        if (filename == null || filename.length() == 0)
            throw new RuntimeException("File is '" + filename + "'");

        this.filename = filename;
        in = null;
        out = null;
    }

    public boolean write(T obj) {
        if (in != null)
            return false;

        try {
            if (out == null)
                out = new ObjectOutputStream(new FileOutputStream(getFilename().concat(".tmp")));

            out.writeObject(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @SuppressWarnings("unchecked")
    public T read() {
        if (out != null)
            return null;

        T obj = null;
        try {
            in = new ObjectInputStream(new FileInputStream(getFilename()));
            obj = (T)in.readObject();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }

        return obj;
    }

    public boolean closeFailedWrite() {
        if (out == null)
            return false;

        try {
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean close() {
        if (in != null) {
            try {
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

            in = null;
        }

        if (out != null) {
            try {
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

            File original = new File(getFilename());
            File updated = new File(getFilename().concat(".tmp"));
            original.delete();
            updated.renameTo(new File(getFilename()));

            out = null;
        }

        return true;
    }

    private String getFilename() {
        return filename;
    }
}
