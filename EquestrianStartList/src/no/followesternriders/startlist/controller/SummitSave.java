package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.model.Summit;

import java.io.Serializable;

/**
 * This class is a simple placeholder class for values that needs to be stored
 * to file. No getters and setters, as this is a simple 'object holder' class.
 * A constructor is provided for programming simplicity.
 */
@SuppressWarnings("serial")
public class SummitSave implements Serializable {
    public Summit summit;
    public String logoImage;
    public String tableColor;
    public boolean enumerateResults;

    public SummitSave(Summit summit, String logoImage, String tableColor, boolean enumerateResults) {
        this.summit = summit;
        this.logoImage = logoImage;
        this.tableColor = tableColor;
        this.enumerateResults = enumerateResults;
    }
}
