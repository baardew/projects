package no.followesternriders.startlist.controller;

public enum ControllerStatus {
    /**
     * Acceptance code
     */
    Success,

    /**
     * Error codes
     */
    Exists,
    Invalid,
    Missing,
    Empty,
    NoChange,
}
