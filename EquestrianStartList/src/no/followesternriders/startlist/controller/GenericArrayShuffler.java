package no.followesternriders.startlist.controller;

import java.util.List;
import java.util.Random;
import java.util.Collections;

/**
 * java.util.Collection.shuffle(list, random) does not provide consistent shuffeling.
 * This class is then created to overcome this shortcoming.
 */
public class GenericArrayShuffler {

    public static <E> void shuffle(E[] list, Random rand) {
        for (int i = 0; i < list.length / 2; i++) {
            int a = getNextIndex(rand, list.length);
            int b = getNextIndex(rand, list.length);

            swap(list, a, b);
        }
    }

    private static int getNextIndex(Random rand, int max) {
        int num = rand.nextInt();
        if (num < 0)
            num *= -1;

        return num % max;
    }

    private static <E> void swap(E[] list, int a, int b) {
        E tmp = list[a];
        list[a] = list[b];
        list[b] = tmp;
    }
}
