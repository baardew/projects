package no.followesternriders.startlist.controller;

import java.util.Stack;
import java.util.List;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

public class DocumentDrawer {

    private StringBuilder html;
    private int indentationLevel;
    private Stack<String> tagStack;
    private String bgColor = "LightGreen";
    private boolean startedNewTable;

    private final String indentation = "    ";
    private final String styleFile = "/HTMLDocument.css";

    public DocumentDrawer(String title, String summitName, String imageFilename, String details, String bgColor) {
        this.bgColor = bgColor;
        indentationLevel = 0;
        startedNewTable = false;

        html = new StringBuilder();
        tagStack = new Stack<String>();

        initializeDocument();
        createTitle(title, summitName, imageFilename, details);
    }

    public boolean addTable(String[] header, List<String[]> table) {
        openTagNewline("table");

        if (!addHeader(header))
            return false;

        if (!addTable(table, header.length))
            return false;

        closeTagNewline();
        return true;
    }

    public boolean addPartialTable(String[] header, List<String[]> table) {
        if (!startedNewTable) {
            openTagNewline("table");
            startedNewTable = true;
        }

        if (!addHeader(header))
            return false;

        if (!addTable(table, header.length))
            return false;

        return true;
    }

    public String getHTML() {
        closeAllTags();
        return html.toString();
    }

    private boolean addHeader(String[] header) {
        if (header == null || header.length == 0)
            return false;

        openTagNewline("tr");

        addTagWithStyledContentBGColor("td", "border-width: 1px 1px 2px 1px; text-align: center; font-weight: bold;", header[0]);
        for (int i = 1; i < header.length; i++)
            addTagWithStyledContentBGColor("td", "border-width: 1px 1px 2px 0px; text-align: center; font-weight: bold;", header[i]);

        closeTagNewline();

        return true;
    }

    private boolean addTable(List<String[]> table, int headerLength) {
        if (table == null || table.size() == 0)
            return false;

        String[] row;

        for (int i = 0; i < table.size() - 1; i++) {
            openTagNewline("tr");

            row = table.get(i);
            if (row.length != headerLength)
                return false;

            addTagWithStyledContent("td", "border-width: 1px 1px 0px 1px", row[0]);
            for (int j = 1; j < row.length; j++)
                addTagWithStyledContent("td", "border-width: 1px 1px 0px 0px", row[j]);

            closeTagNewline();
        }

        row = table.get(table.size() - 1);
        if (row.length != headerLength)
            return false;

        openTagNewline("tr");
        addTagWithStyledContent("td", "border-width: 1px 1px 1px 1px", row[0]);
        for (int j = 1; j < row.length; j++)
            addTagWithStyledContent("td", "border-width: 1px 1px 1px 0px", row[j]);

        closeTagNewline();

        return true;
    }

    private void initializeDocument() {
        html.append("<!DOCTYPE html>\n");

        openTagNewline("html");
        openTagNewline("head");
        openTagNewline("style");

        readStyle();

        closeTagNewline();
        closeTagNewline();

        openTagNewline("body");
    }

    private void createTitle(String title, String summitName, String imageFilename, String details) {

        addTagWithContent("h1", title);
        addImage(imageFilename);
        addTagWithContent("h3", summitName);
        addTagWithContent("p", details);
    }

    private void readStyle() {
        try {
            InputStream in = getClass().getResourceAsStream(styleFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null)
                appendIndentationHTML(line + "\n");

            reader.close();

        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void appendIndentationHTML(String markup) {
        for (int i = 0; i < indentationLevel; i++)
            html.append(indentation);

        appendHTML(markup);
    }

    private void appendHTML(String markup) {
        html.append(markup);
    }

    private void openTagNewline(String tag) {
        tagStack.push(tag);
        appendIndentationHTML("<" + tag + ">\n");
        indentationLevel++;
    }

    private void openTag(String tag) {
        appendIndentationHTML("<" + tag + ">");
        tagStack.push(tag);
    }

    private void openTagWithStyle(String tag, String style) {
        appendIndentationHTML("<" + tag + " style=\"" + style + "\">");
        tagStack.push(tag);
    }

    private void openTagWithStyleBGColor(String tag, String style) {
        appendIndentationHTML("<" + tag + " bgcolor=\"" + bgColor + "\" style=\"" + style + "\">");
        tagStack.push(tag);
    }

    private void addContent(String content) {
        appendHTML(content);
    }

    private void addTagWithContent(String tag, String content) {
        openTag(tag);
        addContent(content);
        closeTag();
    }

    private void addTagWithStyledContent(String tag, String style, String content) {
        openTagWithStyle(tag, style);
        addContent(content);
        closeTag();
    }

    private void addTagWithStyledContentBGColor(String tag, String style, String content) {
        openTagWithStyleBGColor(tag, style);
        addContent(content);
        closeTag();
    }

    private void addImage(String src) {
        if (src.length() == 0)
            return;

        appendIndentationHTML("<img src=\"" + src + "\"/>\n");
    }

    private void closeTag() {
        String tag = tagStack.pop();
        appendHTML("</" + tag + ">\n");
    }

    private void closeTagNewline() {
        String tag = tagStack.pop();

        indentationLevel--;
        appendIndentationHTML("</" + tag + ">\n");
    }

    private void closeAllTags() {
        startedNewTable = false;
        while (!tagStack.isEmpty())
            closeTagNewline();
    }
}
