package no.followesternriders.startlist.controller;

import no.followesternriders.startlist.controller.ResultAssistant;
import no.followesternriders.startlist.controller.CompetitionAssistant;
import no.followesternriders.startlist.controller.EquestrianAssistant;
import no.followesternriders.startlist.controller.AssistantFactory;
import no.followesternriders.startlist.controller.DocumentPrinter;
import no.followesternriders.startlist.controller.FileHandleLite;
import no.followesternriders.startlist.controller.SummitSave;
import no.followesternriders.startlist.controller.DocumentPrinter;
import no.followesternriders.startlist.controller.ControllerStatus;

import no.followesternriders.startlist.model.Summit;
import no.followesternriders.startlist.model.Equestrian;
import no.followesternriders.startlist.model.Competition;
import no.followesternriders.startlist.model.Result;
import no.followesternriders.startlist.model.DataFactory;

import java.io.File;
import java.io.IOException;

import java.util.ResourceBundle;
import java.util.Arrays;
import java.util.Comparator;

public class Secretary {

    public enum Constructor {
        CREATE,
        OPEN,
    }

    private Summit summit;
    private File summitFile;
    private ResourceBundle locale;

    public Secretary(File summitFile, Constructor constructor, ResourceBundle locale) {
        this.summitFile = summitFile;
        this.locale = locale;

        if (constructor == Constructor.CREATE) {
            constructNewSummit();
        } else {
            constructExistingSummit();
        }

        DataFactory.initialize(summit, locale);
        AssistantFactory.initialize(summit);
        DocumentPrinter.initialize(summit, locale, "#FFFFFF");
    }

    public boolean save() {
        FileHandleLite<SummitSave> file = new FileHandleLite<SummitSave>(summitFile.getPath());
        SummitSave save = new SummitSave(summit, DocumentPrinter.getLogoImage(), DocumentPrinter.getBGColor(), DocumentPrinter.getEnumerateResults());

        if (!file.write(save)) {
            if (!file.closeFailedWrite())
                throw new RuntimeException("Failed to resolve erronous save");
            return false;
        }

        if (!file.close())
            throw new RuntimeException("Closing summit file error");

        return true;
    }

    public boolean addCompetitionToEquestrian(String equestrianObject, String competitionObject) {
        Equestrian equestrian = summit.getEquestrian(equestrianObject);
        if (equestrian == null)
            return false;

        Competition competition = summit.getCompetition(competitionObject);
        if (competition == null)
            return false;

        if (!equestrian.addCompetition(competition))
            return false;

        if (!competition.addEquestrian(equestrian)) {
            if (!equestrian.removeCompetition(competition))
                throw new RuntimeException("Could not revert insertion of competition");

            return false;
        }

        return true;
    }

    public boolean removeCompetitionFromEquestrian(String equestrianObject, String competitionObject) {
        Equestrian equestrian = summit.getEquestrian(equestrianObject);
        if (equestrian == null)
            return false;

        Competition competition = summit.getCompetition(competitionObject);
        if (competition == null)
            return false;

        if (!equestrian.removeCompetition(competition))
            return false;

        if (!competition.removeEquestrian(equestrian)) {
            if (!equestrian.addCompetition(competition))
                throw new RuntimeException("Could not revert removal of competition");

            return false;
        }

        return true;
    }

    public ControllerStatus startCompetitionEarlier(String competitionObject) {
        Competition competition = summit.getCompetition(competitionObject);
        if (competition == null)
            return ControllerStatus.Missing;

        if (competition.getStartOrder() == 1)
            return ControllerStatus.NoChange;

        int orderIndex = competition.getStartOrder() - 1;
        for (Competition comp : summit.getCompetitions()) {
            if (comp.getStartOrder() == orderIndex) {
                if (!competition.setStartOrder(orderIndex))
                    return ControllerStatus.Invalid;

                if (!comp.setStartOrder(orderIndex + 1)) {
                    if (!competition.setStartOrder(orderIndex + 1))
                        throw new RuntimeException("Could not revert to previous start order");

                    return ControllerStatus.Invalid;
                }

                return ControllerStatus.Success;
            }
        }

        throw new RuntimeException("Competition not first in start order, but only one Competition in Summit");
    }

    public ControllerStatus startCompetitionLater(String competitionObject) {
        Competition competition = summit.getCompetition(competitionObject);
        if (competition == null)
            return ControllerStatus.Missing;

        if (competition.getStartOrder() == summit.getCompetitions().length)
            return ControllerStatus.NoChange;

        int orderIndex = competition.getStartOrder() + 1;
        for (Competition comp : summit.getCompetitions()) {
            if (comp.getStartOrder() == orderIndex) {
                if (!competition.setStartOrder(orderIndex))
                    return ControllerStatus.Invalid;

                if (!comp.setStartOrder(orderIndex - 1)) {
                    if (!competition.setStartOrder(orderIndex - 1))
                        throw new RuntimeException("Could not revert to previous start order");

                    return ControllerStatus.Invalid;
                }

                return ControllerStatus.Success;
            }
        }

        throw new RuntimeException("Competition not first in start order, but only one Competition in Summit");
    }

    public String createEquestrian() {
        Equestrian equestrian = DataFactory.createEquestrian();
        if (equestrian == null)
            return null;

        if (!summit.addEquestrian(equestrian))
            return null;

        return equestrian.toString();
    }

    public String[] getEquestrians() {
        Equestrian[] equestrians = summit.getEquestrians();
        String[] equestrianObjects = new String[equestrians.length];

        for (int i = 0; i < equestrians.length; i++)
            equestrianObjects[i] = equestrians[i].toString();

        return equestrianObjects;
    }

    public String createCompetition() {
        Competition competition = DataFactory.createCompetition();
        if (competition == null)
            return null;

        if (!summit.addCompetition(competition))
            return null;

        return competition.toString();
    }

    public boolean deleteCompetition(String competitionObject) {
        Competition competition = summit.getCompetition(competitionObject);
        if (competition == null)
            return false;

        for (Equestrian equestrian : summit.getEquestrians())
            equestrian.removeCompetition(competition);

        if (!summit.removeCompetition(competition))
            throw new RuntimeException("Could not remove Competition even though it exists");

        return true;
    }

    public boolean deleteEquestrian(String equestrianObject) {
        Equestrian equestrian = summit.getEquestrian(equestrianObject);
        if (equestrian == null)
            return false;

        for (Competition competition : summit.getCompetitions())
            competition.removeEquestrian(equestrian);

        if (!summit.removeEquestrian(equestrian))
            throw new RuntimeException("Could not remove Equestrian even though it exists");

        if (!updateStartNumbers())
            throw new RuntimeException("Could not update start numbers");

        return true;
    }

    public String[] getCompetitions() {
        Competition[] competitions = summit.getCompetitions();
        String[] competitionObjects = new String[competitions.length];

        for (int i = 0; i < competitions.length; i++)
            competitionObjects[i] = competitions[i].toString();

        return competitionObjects;
    }

    public ResultAssistant getResult(String equestrianObject, String competitionObject) {
        Equestrian equestrian = summit.getEquestrian(equestrianObject);
        if (equestrian == null)
            return null;

        Competition competition = summit.getCompetition(competitionObject);
        if (competition == null)
            return null;

        Result result = equestrian.getResult(competition);
        if (result == null) {
            result = DataFactory.createResult();
            if (result == null)
                return null;

            if (!equestrian.updateResult(competition, result))
                return null;
        }

        return AssistantFactory.createResultAssistant(result);
    }

    public CompetitionAssistant getCompetition(String competitionObject) {
        Competition competition = summit.getCompetition(competitionObject);
        return AssistantFactory.createCompetitionAssistant(competition);
    }

    public EquestrianAssistant getEquestrian(String equestrianObject) {
        Equestrian equestrian = summit.getEquestrian(equestrianObject);
        return AssistantFactory.createEquestrianAssistant(equestrian);
    }

    private boolean updateStartNumbers() {
        Equestrian[] equestrians = summit.getEquestrians();
        Arrays.sort(equestrians, new Comparator<Equestrian>() {
                public int compare(Equestrian a, Equestrian b) {
                    return a.getStartNumber() - b.getStartNumber();
                }
            });

        for (int i = 0; i < equestrians.length; i++) {
            if (!equestrians[i].setStartNumber(i + 1))
                return false;
        }

        return true;
    }

    private void constructNewSummit() {
        try {
            summitFile.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        summit = new Summit();
        summit.setName(summitFile.getName().substring(0, summitFile.getName().length() - 4));

        // Ensure that there is data in the file after creation
        save();
    }

    private void constructExistingSummit() {
        if (!summitFile.exists()) {
            constructNewSummit();
            return;
        }

        FileHandleLite<SummitSave> file = new FileHandleLite<SummitSave>(summitFile.getPath());
        SummitSave save = file.read();
        file.close();

        summit = save.summit;
        DocumentPrinter.setLogoImage(save.logoImage);
        DocumentPrinter.setBGColor(save.tableColor);
        DocumentPrinter.setEnumerateResults(save.enumerateResults);

        if (summit == null)
            throw new RuntimeException("Invalid summit file '" + summitFile.getPath() + "'");
    }
}
