#include <string.h>
#include <stdio.h>
#include <stdlib.h>

// square object
struct object {
    int x; // X pos
    int y; // Y pos
    int w; // width
    int h; // height
    int s; // speed (movement)
    int d; // direction (up = 0, down = 1, left = 2, right = 3)
    int v; // direction velocity
};

#define DIM_X 30
#define DIM_Y 30

static char monitor[DIM_X][DIM_Y];

void setObject(struct object *obj, int x, int y, int w, int h, int d, int v)
{
    obj->x = x;
    obj->y = y;
    obj->w = w;
    obj->y = y;
    obj->h = h;
    obj->s = v;
    obj->d = d;
    obj->v = v;
}

static int fails = 0;

void draw(int frame, struct object *argv, int args)
{
    memset(*monitor, 0, DIM_X * DIM_Y);

    int x, y, i, fail = 0;
    for (i = 0; i < args; i++) {
        struct object *o = argv + i;
        for (x = o->x; x < o->x + o->w; x++)
            for (y = o->y; y < o->y + o->h; y++) {
                monitor[x][y] = monitor[x][y] == '\0' ? o->s + 48 : '|';
		if (monitor[x][y] == '|') fail = 1;
	    }
    }
/*
    printf("Frame: %d\n", frame);
    for (y = 0; y < DIM_Y; y++) {
        for (x = 0; x < DIM_X; x++) {
            if (monitor[x][y] == '\0')
                printf(". ");
            else
                printf("%c ", monitor[x][y]);
        }
        printf("\b\n");
    }
*/
    if (fail) fails++;
    
    printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bHITS = %d, FRAMES = %d :: %d%%", fails, frame, (fails*100)/(frame+1));

}

void animate(struct object *argv, int args)
{
    int x, y, i;
    for (i = 0; i < args; i++) {
        struct object *o = argv + i;

	int check;
	
        switch (o->d) {
        case 0:
	    check = o->y - o->s;
	    if (check < 0)
		o->s += check; // check is negative
	    o->y -= o->s;
            break;
        case 1:
	    check = o->y + o->s + o->h;
	    if (check >= DIM_Y)
		o->s -= check - DIM_Y;
	    o->y += o->s;
            break;
        case 2:
	    check = o->x - o->s;
	    if (check < 0)
		o->s += check; // check is negative
	    o->x -= o->s;
            break;
        case 3:
	    check = o->x + o->s + o->w;
	    if (check >= DIM_X)
		o->s -= check - DIM_X;
	    o->x += o->s;
            break;
        default:
            break;
        }
    }    
}

// Square objects, so simple detection
int hit(struct object *a, struct object *b)
{
    int xa, ya, xb, yb;
    for (xa = a->x; xa < a->x + a->w; xa++)
	for (ya = a->y; ya < a->y + a->h; ya++)
	    for (xb = b->x; xb < b->x + b->w; xb++)
		for (yb = b->y; yb < b->y + b->h; yb++)
		    if (xa == xb && ya == yb)
			return 1;
    
    return 0;
}

//#define MOVE(i, x, y, w, h, d, v, s, b) printf("Obj[%d] [%d][%d] x [%d][%d] : %d * --%d--> |%d| {%d}\n", i, x, y, x+w, y+h, d, v, s, b);
#define MOVE(i, x, y, w, h, d, v, s, b)

void collide(struct object *argv, int args)
{
    // For every object check collision
    int i;
    for (i = 0; i < args; i++) {
	struct object *me = argv + i;
	me->s = me->v;
	struct object next;
	memcpy(&next, me, sizeof(struct object));
	
	animate(&next, 1);
	
	// n^2 collision detection, check every object with every other object
	int j, sub = 0;
	for (j = 0; j < args; j++) {
	    // Not compare to self
	    if (j == i)
		continue;
	    
	    // Compute middle-space if both move in complete parallell
	    struct object *other = argv + j;
	    struct object step;
	    memcpy(&step, other, sizeof(struct object));	    
	    
	    // Check for both estimated move and no move
	    while (hit(&next, &step) || hit(&next, other)) {
		sub++;
		memcpy(&step, other, sizeof(struct object));
		memcpy(&next, me, sizeof(struct object));
		
		next.s = me->v / sub;
		step.s = other->v / sub;

		animate(&next, 1);
		animate(&step, 1);

		if (next.s == 0)
		    break;
	    }
	}
	
	memcpy(me, &next, sizeof(struct object));
	MOVE(i, me->x, me->y, me->w, me->h, me->d, me->v, me->s, sub);
	me->d = rand() % 4;
    }
}

int main(void)
{
    struct object obj[7];

    // obj, x, y, w, h, d, v
    setObject(obj,    0, 0, 4, 4, 3, 1);
    setObject(obj+1, DIM_X-4, DIM_Y-4, 4, 4, 2, 2);
    setObject(obj+2, DIM_X-3, 0, 3, 3, 0, 3);
    setObject(obj+3, 0, DIM_Y-5, 1, 5, 1, 4);
    setObject(obj+4, DIM_X/2, DIM_Y/2, 3, 1, 4, 5);
    setObject(obj+5, 10, 10, 3, 2, 4, 6);
    setObject(obj+6, 20, 20, 6, 3, 4, 7);

    int f = 0;
    draw(f++, obj, 7);
    sleep(1);

    while (1) {
	collide(obj, 7);
        draw(f++, obj, 7);
//	sleep(1);
    }

    return 0;
}
