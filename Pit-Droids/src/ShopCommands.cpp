#include "Shop.hpp"
#include "Display.hpp"
#include "DisplayText.hpp"
#include "ShopCommands.hpp"

class FactoryMenu : public Menu
{
public:
    FactoryMenu(Shop &shop) : Menu(shop), selection{0} { }
    void processCommand(const char cmd) override;

    const std::string commands =
        DisplayText::Color::Yellow + "******" + DisplayText::Color::None + " List of Commands: "
        + DisplayText::Color::Yellow + "******" + DisplayText::Color::None + "\n\n"
        + DisplayText::Color::Blue + "   l " + DisplayText::Color::None + " - Display current factory details\n"
        + DisplayText::Color::Blue + "   m " + DisplayText::Color::None + " - Move droids to another location\n"
        + DisplayText::Color::Blue + "   w " + DisplayText::Color::None + " - Sell droids in storage\n"
        + DisplayText::Color::Blue + "   u " + DisplayText::Color::None + " - Upgrade\n"
        + DisplayText::Color::Blue + "   a " + DisplayText::Color::None + " - Upgrade all droids to factory level\n"
        + DisplayText::Color::Blue + "   s " + DisplayText::Color::None + " - Select factory number\n"
        + DisplayText::Color::Blue + "   b " + DisplayText::Color::None + " - Back to previous menu\n"
        + DisplayText::Color::Blue + "   ? " + DisplayText::Color::None + " - Display list of commands\n"
        + "\n"
        ;

private:
    size_t selection;

    void moveDroids();
    void sellDroids();
    void upgradeDroids();
    void upgrade();
};

class MainMenu : public Menu {
public:
    MainMenu(Shop &shop) : Menu(shop) { }
    void processCommand(const char cmd) override;

private:
    const std::string commands =
        DisplayText::Color::Yellow + "******" + DisplayText::Color::None + " List of Commands: "
        + DisplayText::Color::Yellow + "******" + DisplayText::Color::None + "\n\n"
        + DisplayText::Color::Blue + "   n " + DisplayText::Color::None + " - Continue to next day\n"
        + DisplayText::Color::Blue + "   l " + DisplayText::Color::None + " - Display factories\n"
        + DisplayText::Color::Blue + "   $ " + DisplayText::Color::None + " - Display status\n"
        + DisplayText::Color::Blue + "   f " + DisplayText::Color::None + " - Factory management\n"
        + DisplayText::Color::Blue + "   b " + DisplayText::Color::None + " - Buy a new level 0 factory\n"
        + DisplayText::Color::Blue + "   q " + DisplayText::Color::None + " - Quit\n"
        + DisplayText::Color::Blue + "   ? " + DisplayText::Color::None + " - Display list of commands\n"
        + "\n"
        ;
};

#ifdef SHOP_COMMANDS_IMPL
void FactoryMenu::upgrade()
{
    Factory &factory = shop.getFactories().at(selection);
    shop.addCash(-factory.upgradeLevel());
}

void FactoryMenu::sellDroids()
{
    // Get inputs
    int level = shop.getDisplay().getInt(std::string{"Level"});
    int count = shop.getDisplay().getInt(std::string{"Count"});
    Factory &factory = shop.getFactories().at(selection);

    if ((int)factory.storageCount() - count < 0) {
        shop.getDisplay().show(std::string{"Factory does not have quantity\n"});
        return;
    }

    size_t quantity = 0;
    for (auto& d : factory.getStorage())
        if (d->getLevel() == level)
            ++quantity;

    if (quantity < (size_t)count) {
        shop.getDisplay().show(std::string{"Factory does not have quantity\n"});
        return;
    }

    std::vector<shared_ptr<Droid>> droids = factory.getWorkers();

    // Sell droids
    for (; count > 0; --count) {
        auto d = factory.removeStorage(level);
        shop.addCash(d->sellValue());
    }
}

void FactoryMenu::upgradeDroids()
{
    Factory &factory = shop.getFactories().at(selection);

    for (auto& d : factory.getStorage())
        while (d->getLevel() < factory.getLevel() )
            shop.addCash(d->upgradeLevel());

    for (auto& d : factory.getWorkers())
        while (d->getLevel() < factory.getLevel() )
            shop.addCash(d->upgradeLevel());
}

void FactoryMenu::moveDroids()
{
    // Get inputs
    int level = shop.getDisplay().getInt(std::string{"Level"});
    int count = shop.getDisplay().getInt(std::string{"Count"});
    int target = shop.getDisplay().getInt(std::string{"To factory"});
    char src_location = shop.getDisplay().getChar(std::string{"Move from [(w)orker/(s)torage]"});
    char dst_location = shop.getDisplay().getChar(std::string{"Move to [(w)orker/(s)torage]"});
    Factory &src_factory = shop.getFactories().at(selection);

    // Validate inputs
    if ((size_t)target >= shop.getFactories().size()) {
        shop.getDisplay().show(std::string{"Invalid target factory \n"});
        return;
    }

    Factory &dst_factory = shop.getFactories().at(target);

    if (src_location != 'w' && src_location != 's') {
        shop.getDisplay().show(std::string{"Invalid source factory location\n"});
        return;
    }

    if (dst_location != 'w' && dst_location != 's') {
        shop.getDisplay().show(std::string{"Invalid destination factory location\n"});
        return;
    }

    if (dst_location == 's' && dst_factory.storageCount() + count > dst_factory.storageCapacity()) {
        shop.getDisplay().show(std::string{"Destination factory does not have capacity\n"});
        return;
    }

    if (dst_location == 'w' && dst_factory.workerCount() + count > dst_factory.workerCapacity()) {
        shop.getDisplay().show(std::string{"Destination factory does not have capacity\n"});
        return;
    }

    // Get workers and check src dst capacity
    std::function<void(shared_ptr<Droid>)> dst_function;
    std::function<shared_ptr<Droid>(int)> src_function;

    std::vector<shared_ptr<Droid>> droids;

    if (src_location == 's') {
        droids = src_factory.getStorage();
        src_function = std::bind(&Factory::removeStorage, &src_factory, std::placeholders::_1);
    } else {
        droids = src_factory.getWorkers();
        src_function = std::bind(&Factory::removeWorker, &src_factory, std::placeholders::_1);
    }

    if (dst_location == 's')
        dst_function = std::bind(&Factory::addStorage, &dst_factory, std::placeholders::_1);
    else
        dst_function = std::bind(&Factory::addWorker, &dst_factory, std::placeholders::_1);

    size_t quantity = 0;
    for (auto& d : droids)
        if (d->getLevel() == level)
            ++quantity;

    if (quantity < (size_t)count) {
        shop.getDisplay().show(std::string{"Source factory does not have quantity\n"});
        return;
    }

    // Move droids
    for (; count > 0; --count)
        dst_function(src_function(level));
}


void FactoryMenu::processCommand(const char cmd)
{
    switch (cmd) {
    case 'l':
        shop.getDisplay().factory(shop.getFactories().at(selection));
        break;
    case 's':
        selection = shop.getDisplay().getInt(std::string{"Factory"});
        if (selection >= shop.getFactories().size()) {
            shop.getDisplay().show(std::string{"Invalid factory\n"});
            selection = 0;
        }
        break;
    case 'm':
        moveDroids();
        break;
    case 'w':
        sellDroids();
        break;
    case 'a':
        upgradeDroids();
        break;
    case 'u':
        upgrade();
        break;
    case 'b':
        shop.setMenu(unique_ptr<Menu>{new MainMenu{shop}});
        break;
    case '?':
    default:
        shop.getDisplay().help(commands);
        break;
    }
}

void MainMenu::processCommand(const char cmd)
{
    switch (cmd) {
    case 'l':
        shop.getDisplay().factories(shop.getFactories());
        break;
    case '$':
        shop.getDisplay().status(shop);
        break;
    case 'f':
        shop.setMenu(unique_ptr<Menu>{new FactoryMenu{shop}});
        break;
    case 'n':
        shop.getTimeline().tick(shop);
        break;
    case 'b':
        shop.newFactory();
        break;
    case '?':
    default:
        shop.getDisplay().help(commands);
        break;
    }
}
#endif
