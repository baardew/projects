#include "Factory.hpp"
#include "Droid.hpp"

#include <iostream>

static unsigned maxId = 0;

Factory::Factory() :
    level{0},
    id{maxId++},
    readyIn{-1}
{

}

Factory& Factory::operator=(Factory &other)
{
    level = other.level;
    workers = other.workers;
    storage = other.storage;
    return *this;
}

long long Factory::daysToFinish()
{
    return readyIn;
}

long long Factory::workdayTick()
{
    long long cost = upkeepCost[level];
    readyIn = -1;

    for (auto d : workers) {
        cost += d->workdayTick();
        int daysLeft = d->daysToFinish();

        if (readyIn == -1 || daysLeft < readyIn)
            readyIn = daysLeft;

        if (daysLeft == 0 && storageCount() < storageCapacity()) {
            auto w = d->getWork();

            while (w->getLevel() < getLevel())
                w->upgradeLevel();

            storage.push_back(w);
        }
    }

    return cost;
}

int Factory::workerCapacity()
{
    return levelWorkerCapacity[level];
}

int Factory::storageCapacity()
{
    return levelStorageCapacity[level];
}

int Factory::workerCount()
{
    return workers.size();
}

int Factory::storageCount()
{
    return storage.size();
}

int Factory::getLevel()
{
    return level;
}

long long Factory::upgradeLevel()
{
    if (level == 4)
        return 0;

    return levelUpgrades[++level];
}

long long Factory::purchaseCost()
{
    return levelUpgrades[level];
}

unsigned Factory::getId()
{
    return id;
}

void Factory::addWorker(std::shared_ptr<Droid> droid)
{
    workers.push_back(droid);
}

shared_ptr<Droid> Factory::removeWorker(int level)
{
    for (auto it = workers.begin(); it != workers.end(); ++it) {
        if ((*it)->getLevel() == level) {
            auto d = (*it);
            workers.erase(it);
            return d;
        }
    }

    throw std::runtime_error("No workers");
}

void Factory::addStorage(std::shared_ptr<Droid> droid)
{
    storage.push_back(droid);
}

shared_ptr<Droid> Factory::removeStorage(int level)
{
    for (auto it = storage.begin(); it != storage.end(); ++it) {
        if ((*it)->getLevel() == level) {
            auto d = (*it);
            storage.erase(it);
            return d;
        }
    }

    throw std::runtime_error("No storage");
}

std::vector<shared_ptr<Droid>>& Factory::getWorkers()
{
    return workers;
}

std::vector<shared_ptr<Droid>>& Factory::getStorage()
{
    return storage;
}
