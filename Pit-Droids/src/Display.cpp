#include "Display.hpp"
#include "Shop.hpp"
#include "Factory.hpp"
#include "DisplayText.hpp"

#define PROGRESS_BAR_LENGTH 30

Display::Display()
{

}

Display::~Display()
{

}

Display::Display(const Display &other)
{
    UNUSED(other);
}

Display::Display(Display &other)
{
    UNUSED(other);
}

Display& Display::operator=(const Display& other)
{
    UNUSED(other);
    return *this;
}

Display& Display::operator=(Display& other)
{
    UNUSED(other);
    return *this;
}

int Display::getInt(const std::string &message)
{
    std::cout << message << ": ";

    std::string line;
    std::getline(std::cin, line);

    return std::stoi(line);
}

char Display::getChar(const std::string &message)
{
    std::cout << message << ": ";

    std::string line;
    std::getline(std::cin, line);

    if (line.length())
        return line.at(0);

    return '?';
}

void Display::title()
{
    std::cout << DisplayText::Title;
}

void Display::help(const std::string &commands)
{
    std::cout << commands;
}

void Display::show(const std::string &commands)
{
    std::cout << commands;
}

char Display::getCommand(Shop &shop)
{
    if (shop.getCash() < 0) {
        bankrupt(shop);
        return 'q';
    }

    return getChar(DisplayText::Prompt);
}

void Display::status(Shop &shop)
{
    std::cout
        << "  "
        << DisplayText::Color::Yellow
        << "$"
        << formatWithCommas(shop.getCash())
        << DisplayText::Color::Blue
        << "  Day "
        << DisplayText::Color::Blue
        << formatWithCommas(shop.getDay())
        << DisplayText::Color::None
        << std::endl
        << std::endl;

    for (auto f : shop.getFactories()) {
        std::cout
            << "  Factory: "
            << f.getId()
            << " ["
            << loadToColor((float)f.daysToFinish() / PROGRESS_BAR_LENGTH)
            << progressBar(f.daysToFinish() >= 0 ? f.daysToFinish() : PROGRESS_BAR_LENGTH)
            << DisplayText::Color::None
            << "]";

        if (f.daysToFinish() == 0) {
            std::cout
                << DisplayText::Color::Green
                << "  DONE!"
                << DisplayText::Color::None;
        }

        std::cout << std::endl;
    }

    std::cout << std::endl;
}

void Display::bankrupt(Shop &shop)
{
    std::cout << DisplayText::Bankrupt;
    this->status(shop);
    this->factories(shop.getFactories());
}

void Display::factories(std::vector<Factory> &factories)
{
    std::cout
        << "  Number of factories: "
        << DisplayText::Color::Yellow
        << factories.size()
        << DisplayText::Color::None
        << std::endl
        << std::endl;

    for (auto f : factories) {
        std::cout
            << levelToColor(f.getLevel())
            << "    /\\/\\/\\"
            << DisplayText::Color::None
            << "  Factory: "
            << DisplayText::Color::Blue
            << f.getId()
            << DisplayText::Color::None
            << std::endl
            << levelToColor(f.getLevel())
            << "    |    |  "
            << DisplayText::Color::None
            << "Workers: "
            << loadToColor((float)f.workerCount() / f.workerCapacity())
            << f.workerCount() << " / " << f.workerCapacity()
            << DisplayText::Color::None
            << "  Storage: "
            << loadToColor((float)f.storageCount() / f.storageCapacity())
            << f.storageCount() << " / " << f.storageCapacity()
            << DisplayText::Color::None
            << std::endl
            << std::endl
            ;
    }
}

void Display::factory(Factory &factory)
{
    std::cout
        << "  Factory: "
        << DisplayText::Color::Blue
        << factory.getId()
        << DisplayText::Color::None
        << std::endl
        << "  ["
        << loadToColor((float)factory.daysToFinish() / PROGRESS_BAR_LENGTH)
        << progressBar(factory.daysToFinish() >= 0 ? factory.daysToFinish() : PROGRESS_BAR_LENGTH)
        << DisplayText::Color::None
        << "]";

    if (factory.daysToFinish() == 0) {
        std::cout
            << DisplayText::Color::Green
            << "  DONE!"
            << DisplayText::Color::None;
    }

    std::cout << std::endl;

    size_t workDroidsByLevel[5] {0};
    size_t storageDroidsByLevel[5] {0};

    for (auto &d : factory.getWorkers())
        ++workDroidsByLevel[d->getLevel()];

    for (auto &d : factory.getStorage())
        ++storageDroidsByLevel[d->getLevel()];

    std::cout
        << std::endl
        << "Workers: "
        << loadToColor((float)factory.workerCount() / factory.workerCapacity())
        << factory.workerCount() << " / " << factory.workerCapacity()
        << DisplayText::Color::None
        << std::endl
        << "  Storage: "
        ;

    for (size_t i = 0 ; i < 5; i++) {
        std::cout
            << levelToColor(i)
            << "   o"
            << std::endl
            << "  /|\\"
            << std::endl
            << "  / \\"
            << DisplayText::Color::None
            << "  x"
            << workDroidsByLevel[i]
            << std::endl
            ;
    }

    std::cout
        << std::endl
        << "Storage: "
        << loadToColor((float)factory.storageCount() / factory.storageCapacity())
        << factory.storageCount() << " / " << factory.storageCapacity()
        << std::endl
        ;

    for (size_t i = 0 ; i < 5; i++) {
        std::cout
            << levelToColor(i)
            << "   o"
            << std::endl
            << "  /|\\"
            << std::endl
            << "  / \\"
            << DisplayText::Color::None
            << "  x"
            << storageDroidsByLevel[i]
            << std::endl
            ;
    }

    std::cout << std::endl;
}

std::string Display::formatWithCommas(long long value)
{
    bool belowZero = value < 0;
    size_t index = 0;
    std::string lines[8]; // Maximum number of seperation units

    if (value == 0)
        return "0";

    if (value < 0)
        value *= -1;

    while (value != 0) {
        int unit = value % 1000;

        if (value < 1000)
            lines[index] = std::to_string(value);
        else if (unit == 0)
            lines[index] = "000";
        else if (unit < 10)
            lines[index] = "00" + std::to_string(unit);
        else if (unit < 100)
            lines[index] = "0" + std::to_string(unit);
        else
            lines[index] = std::to_string(unit);

        lines[index] += index ? "," : "";

        value /= 1000;
        index++;
    }

    if (belowZero)
        lines[index++] = "-";

    std::string line;
    for (; index != 0; index--)
        line += lines[index - 1];

    return line;
}

std::string Display::loadToColor(float load)
{
    if (load <= 0.33)
        return DisplayText::Color::Green;

    if (load <= 0.67)
        return DisplayText::Color::Yellow;

    return DisplayText::Color::Red;
}

std::string Display::levelToColor(int level)
{
    switch (level) {
    case 0:
        return DisplayText::Color::None;
    case 1:
        return DisplayText::Color::Green;
    case 2:
        return DisplayText::Color::Blue;
    case 3:
        return DisplayText::Color::Yellow;
    case 4:
        return DisplayText::Color::Red;
    default:
        return DisplayText::Color::None;
    };
}

std::string Display::progressBar(long long days)
{
    std::string line;

    for (long long i = 0; i < PROGRESS_BAR_LENGTH; i++, days--)
        line += days > 0 ? " " : "*";

    common::reverse(line.begin(), line.end());

    return line;
}
