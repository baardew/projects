#include "Timeline.hpp"
#include "Shop.hpp"

void Timeline::tick(Shop &shop)
{
    bool workLeft = true;
    size_t workItems = 0;
    size_t totalItems = shop.getFactories().size();

    while (workLeft && workItems != totalItems) {
        shop.advanceDay();

        workItems = 0;
        totalItems = 0;

        for (auto &t : shop.getFactories()) {
            shop.addCash(t.workdayTick());

            ++totalItems;

            long long daysLeft = t.daysToFinish();
            if (daysLeft == 0)
                workLeft = false;

            if (daysLeft < 0)
                ++workItems;
        }
    }
}
