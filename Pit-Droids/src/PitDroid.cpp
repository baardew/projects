#include "PitDroid.hpp"

PitDroid::PitDroid()
{

}

PitDroid:: ~PitDroid()
{

}

PitDroid::PitDroid(const PitDroid &other) :
    Droid(other)
{
    UNUSED(other);
}

PitDroid::PitDroid(PitDroid &&other)
{
    UNUSED(other);
}

PitDroid& PitDroid::operator=(PitDroid &other)
{
    UNUSED(other);
    return *this;
}

PitDroid& PitDroid::operator=(PitDroid &&other)
{
    UNUSED(other);
    return *this;
}

long long PitDroid::workdayTick()
{
    if (readyIn < 0)
        readyIn = completionTime[level];

    if (!workDone)
        readyIn--;

    return workdayCost[level];
}

shared_ptr<Droid> PitDroid::getWork()
{
    readyIn = -1;
    return make_shared<PitDroid>();
}

long long PitDroid::sellValue()
{
    return sellValueList[level];
}

long long PitDroid::upgradeLevel()
{
    if (level == 4)
        return 0;

    return upgradeCost[level++];
}
