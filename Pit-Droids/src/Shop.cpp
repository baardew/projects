#include "Shop.hpp"
#include "PitDroid.hpp"

#define SHOP_COMMANDS_IMPL
#include "ShopCommands.cpp"

Shop::Shop() :
    cash{100000},
    day{0},
    menu{make_unique<MainMenu>(*this)}
{
    Factory factory;
    shared_ptr<Droid> droid = make_shared<PitDroid>();

    factory.addWorker(droid);
    factories.push_back(factory);
}

void Shop::newFactory()
{
    Factory factory;
    cash -= factory.purchaseCost();
    factories.push_back(factory);
}

int Shop::runBusiness()
{
    display.title();

    char cmd;
    while ((cmd = display.getCommand(*this)) != 'q')
        menu->processCommand(cmd);

    return EXIT_SUCCESS;
}

void Shop::advanceDay()
{
    day++;
}

void Shop::addCash(long long cash)
{
    this->cash += cash;
}

long long Shop::getCash()
{
    return cash;
}

long long Shop::getDay()
{
    return day;
}

std::vector<Factory>& Shop::getFactories()
{
    return factories;
}

Timeline& Shop::getTimeline()
{
    return time;
}

Display& Shop::getDisplay()
{
    return display;
}

void Shop::setMenu(unique_ptr<Menu> menu)
{
    this->menu = std::move(menu);
}
