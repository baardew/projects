#include "Droid.hpp"

#include <iostream>

Droid::Droid() :
    level{0},
    readyIn{-1},
    workDone{false}
{

}

Droid::~Droid()
{

}

Droid::Droid(const Droid &other)
{
    level = other.level;
}

Droid::Droid(Droid &&other)
{
    level = other.level;
}

Droid& Droid::operator=(Droid& other)
{
    level = other.level;
    return *this;
}

Droid& Droid::operator=(Droid&& other)
{
    level = other.level;
    return *this;
}

int Droid::getLevel()
{
    return level;
}

long long Droid::daysToFinish() {
    return readyIn;
}
