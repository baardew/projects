#ifndef FACTORY_H
#define FACTORY_H

#include "Common.hpp"
#include "WorkerTask.hpp"

#include <vector>

class Droid;

class Factory : WorkerTask
{

public:
    Factory();

    Factory& operator=(Factory &other);

    long long daysToFinish() override;
    long long workdayTick() override;

    int workerCapacity();
    int storageCapacity();
    int workerCount();
    int storageCount();
    int getLevel();
    long long upgradeLevel();
    long long purchaseCost();
    unsigned getId();
    std::vector<shared_ptr<Droid>>& getWorkers();
    std::vector<shared_ptr<Droid>>& getStorage();

    void addWorker(shared_ptr<Droid> droid);
    shared_ptr<Droid> removeWorker(int level);

    void addStorage(shared_ptr<Droid> droid);
    shared_ptr<Droid> removeStorage(int level);

private:
    int level;
    unsigned id;
    long long readyIn;
    const long long levelUpgrades[5] = {100000, 250000, 500000, 1000000, 10000000};
    const unsigned levelWorkerCapacity[5] = {25, 100, 250, 350, 500};
    const unsigned levelStorageCapacity[5] = {25, 250, 500, 1000, 2500};
    const long long upkeepCost[5] = {-250, -200, -150, -100, 0};

    std::vector<shared_ptr<Droid>> workers;
    std::vector<shared_ptr<Droid>> storage;
};

#endif
