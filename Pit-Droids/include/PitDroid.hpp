#ifndef PIT_DROID_H
#define PIT_DROID_H

#include "Common.hpp"
#include "Droid.hpp"

#include <iostream>

class PitDroid : public Droid
{
public:
    PitDroid();
    virtual ~PitDroid();
    PitDroid(const PitDroid &other);
    PitDroid(PitDroid &&other);
    PitDroid& operator=(PitDroid &other);
    PitDroid& operator=(PitDroid &&other);

    virtual long long workdayTick() override;
    virtual long long sellValue() override;
    virtual shared_ptr<Droid> getWork() override;
    virtual long long upgradeLevel() override;
    
private:
    const long long upgradeCost[4] = {250, 500, 1000, 2500};
    const long long workdayCost[5] = {-100, -95, -85, -55, -35};
    const long long completionTime[5] = {30, 25, 20, 15, 10};
    const long long sellValueList[5] = {-workdayCost[0] * completionTime[0] * 3,
                                        -workdayCost[1] * completionTime[1] * 5,
                                        -workdayCost[2] * completionTime[2] * 7,
                                        -workdayCost[3] * completionTime[3] * 9,
                                        -workdayCost[4] * completionTime[4] * 11};
};

#endif
