#ifndef DISPLAY_H
#define DISPLAY_H

#include "Common.hpp"

#include <string>
#include <vector>
#include <iostream>

class Shop;
class Factory;

class Display
{

public:
    Display();
    ~Display();
    Display(const Display &other);
    Display(Display &other);
    Display& operator=(const Display& other);
    Display& operator=(Display& other);

    void title();
    void help(const std::string &commands);
    void show(const std::string &commands);
    char getCommand(Shop &shop);
    void status(Shop &shop);
    void factories(std::vector<Factory> &factories);
    void factory(Factory &factory);
    int getInt(const std::string &message);
    char getChar(const std::string &message);

private:
    std::string formatWithCommas(long long value);
    std::string loadToColor(float load);
    std::string levelToColor(int level);
    std::string progressBar(long long days);

    void bankrupt(Shop &shop);
};

#endif
