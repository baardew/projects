#ifndef SHOP_H
#define SHOP_H

#include "Common.hpp"
#include "Display.hpp"
#include "Factory.hpp"
#include "Droid.hpp"
#include "Timeline.hpp"
#include "ShopCommands.hpp"

#include <cstdlib>

#include <vector>
#include <iostream>
#include <string>

class Shop
{

public:
    Shop();

    int runBusiness();

    void advanceDay();
    void addCash(long long cash);
    long long getCash();
    long long getDay();
    std::vector<Factory>& getFactories();
    Timeline& getTimeline();
    Display& getDisplay();
    void setMenu(unique_ptr<Menu> menu);
    void newFactory();

private:
    Display display;
    Timeline time;
    long long cash;
    long long day;
    std::vector<Factory> factories;

    unique_ptr<Menu> menu;

    char getCommand();
};

#endif
