#ifndef DROID_H
#define DROID_H

#include "Common.hpp"
#include "WorkerTask.hpp"

class Droid : WorkerTask
{

public:
    Droid();
    virtual ~Droid();
    Droid(const Droid &other);
    Droid(Droid &&other);
    Droid& operator=(Droid &other);
    Droid& operator=(Droid &&other);

    virtual long long daysToFinish() override;
    virtual long long workdayTick() = 0;
    virtual long long sellValue() = 0;
    virtual shared_ptr<Droid> getWork() = 0;
    virtual long long upgradeLevel() = 0;

    int getLevel();

protected:
    int level;
    int readyIn;
    bool workDone;
};

#endif
