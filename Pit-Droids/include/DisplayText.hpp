#ifndef DISPLAY_TEXT_H
#define DISPLAY_TEXT_H

#include <string>

namespace DisplayText
{
    namespace Color {
        static const std::string Red = "\033[0;31m";
        static const std::string Green = "\033[0;32m";
        static const std::string Blue = "\033[1;34m";
        static const std::string Yellow = "\033[0;33m";
        static const std::string None = "\033[0m";
    }

    static const std::string Title =
        Color::Red + "================== "
        + Color::Yellow + "PIT-DROIDS "
        + Color::Red + "==================\n"
        + Color::None + " Simple C++ program to learn and explore C++11.\n"
        "\n"
        " In simple terms, use the commands to build up\n"
        " a wast mass of pit-droids that you either sell\n"
        " or put to useful work. There is no end-game.\n"
        + Color::Red + "================================================\n\n\n"
        + Color::None
        ;

    static const std::string Bankrupt =
        Color::Red + "\n\nYou just got bankrupted.\nCash left:\n" + Color::None;

    static const std::string Prompt = "$> ";
}

#endif
