#ifndef COMMON_H
#define COMMON_H

#include <memory>

using std::shared_ptr;
using std::unique_ptr;
using std::make_unique;
using std::make_shared;

namespace common
{

    template <class BidirectionalIterator>
    void reverse(BidirectionalIterator first, BidirectionalIterator last)
    {
        while ((first != last) && (first != --last)) {
            std::iter_swap(first, last);
            ++first;
        }
    }

    template<class T>
    class PriorityItem
    {
    public:
        PriorityItem(T object, long long priority) : priority(priority), object(object) {

        }

        ~PriorityItem() {

        }

        PriorityItem(const PriorityItem &other) : priority(other.priority), object(other.object) {

        }

        PriorityItem(const PriorityItem &&other) : priority(other.priority), object(other.object) {

        }

        PriorityItem& operator=(PriorityItem &other) {
            priority = other.priority;
            object = other.object;
            return *this;
        }

        PriorityItem& operator=(PriorityItem &&other) {
            priority = other.priority;
            object = other.object;
            return *this;
        }

        bool operator<(const PriorityItem& rhs) const {
            return priority < rhs.priority;
        }

        T getObject() const {
            return object;
        }

    private:
        long long priority;
        T object;
    };
}
#define UNUSED(arg) ((void)arg)

#endif
