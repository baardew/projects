#ifndef SHOP_COMMANDS_H
#define SHOP_COMMANDS_H

#include <functional>

class Menu {
public:
    Menu(Shop &shop) : shop{shop} { }
    virtual ~Menu() = default;
    virtual void processCommand(const char cmd) = 0;

protected:
    Shop &shop;
};

#endif
