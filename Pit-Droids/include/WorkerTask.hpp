#ifndef WORKER_TASK_H
#define WORKER_TASK_H

class WorkerTask
{
public:
    virtual long long daysToFinish() = 0;
    virtual long long workdayTick() = 0;
};

#endif
