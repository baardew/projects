#ifndef TIMELINE_H
#define TIMELINE_H

#include "Common.hpp"
#include "WorkerTask.hpp"

#include <vector>

#include <climits>

class Shop;

class Timeline
{
public:
    void tick(Shop &shop);
};

#endif
