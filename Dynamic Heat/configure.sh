#!/bin/bash
qmake -project "QT += opengl" \
"OBJECTS_DIR = build" \
"DEFINES += CUDA" \
"MOC_DIR = moc" \
"QMAKE_CXXFLAGS += -fopenmp" \
"SYSTEM_NAME = unix" \
"SYSTEM_TYPE = 64" \
"INCLUDEPATH += include" \
"CUDA_DIR = /usr/local/cuda-6.5" \
"OTHER_FILES +=  cuda/world.cu" \
"CUDA_SOURCES += cuda/world.cu" \
"CUDA_SDK = /usr/local/cuda-6.5" \
"CUDA_DIR = /usr/local/cuda-6.5" \
"NVCC_OPTIONS = --use_fast_math" \
"INCLUDEPATH += \$\$CUDA_DIR/include" \
"INCLUDEPATH += include" \
"QMAKE_LIBDIR += \$\$CUDA_DIR/lib64" \
"QMAKE_LIBDIR += \$\$CUDA_DIR/lib64/lib" \
"CUDA_OBJECTS_DIR = build" \
"CUDA_LIBS = -lcuda -lcudart" \
"CUDA_INC = \$\$join(INCLUDEPATH,'\" -I\"','-I\"','\"')" \
"LIBS += \$\$CUDA_LIBS" \
"cuda.input = CUDA_SOURCES" \
"cuda.output = \$\$CUDA_OBJECTS_DIR/\${QMAKE_FILE_BASE}_cuda.o" \
"cuda.commands = nvcc \$\$NVCC_OPTIONS \$\$CUDA_INC \$\$NVCC_LIBS --machine \$\$SYSTEM_TYPE -c -o \${QMAKE_FILE_OUT} \${QMAKE_FILE_NAME}" \
"cuda.dependency_type = TYPE_C" \
"QMAKE_EXTRA_COMPILERS += cuda" \
"QMAKE_LFLAGS += -fopenmp"

qmake Dynamic\ Heat.pro
