#include "World.hpp"

void World::zoomIn()
{
    zoomOn = true;
}

void World::zoomOut()
{
    zoomOn = false;
}

void World::levelUp()
{
    if (zoomLvl + 1 < levels)
        zoomLvl++;
}

void World::levelDown()
{
    if (zoomLvl - 1 >= 0)
        zoomLvl--;
}

void World::mouseReleaseEvent(QMouseEvent *mouse)
{
    if (!zoomOn)
        return;

    tempX = mouse->x();
    tempY = mouse->y();
    tempZ = zoomLvl;
}
