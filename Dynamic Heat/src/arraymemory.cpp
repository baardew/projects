#include "arraymemory.hpp"

#include <stdio.h>
#include <assert.h>

static inline void free_on_failure(char *p1, char **p2, char ***p3);

void free_memory(void* memory, int dimensions)
{
    assert(1 <= dimensions && dimensions <= 3);
    assert(memory != NULL);

    void ***arr;
    void **ind;

    switch (dimensions) {
    case 3:
        arr = (void***)memory;
        free(arr[0][0]);
    case 2:
        ind = (void**)memory;
        free(ind[0]);
    case 1:
        free(memory);
        break;
    default:
        break;
    }
}

void* allocate_memory_1d(size_t bytesize, size_t width)
{
    assert(width > 0);

    return calloc(width, bytesize);
}

void** allocate_memory_2d(size_t bytesize, size_t width, size_t height)
{
    assert(width > 0);
    assert(height > 0);

    char **ptr = (char**)calloc(height, sizeof(char*));
    char *data = (char*)calloc(width * height, bytesize);

    if (ptr == NULL || data == NULL) {
        free_on_failure(data, ptr, NULL);
        return NULL;
    }

    size_t i;
    for (i = 0; i < height; i++)
        ptr[i] = &(data[i * width * bytesize]);

    return (void**)ptr;
}

void*** allocate_memory_3d(size_t bytesize, size_t width, size_t height, size_t depth)
{
    assert(width > 0);
    assert(height > 0);
    assert(depth > 0);

    char ***ptr = (char***)calloc(depth, sizeof(char**));
    char **ind = (char**)calloc(depth * height, sizeof(char*));
    char *data = (char*)calloc(width * depth * height, bytesize);

    if (ptr == NULL || ind == NULL || data == NULL) {
        free_on_failure(data, ind, ptr);
        return NULL;
    }

    size_t i, j;
    for (i = 0; i < depth; i++) {
        ptr[i] = &(ind[i * height]);

        for (j = 0; j < height; j++)
            ptr[i][j] = &(data[(j * width * bytesize) + (i * width * height * bytesize)]);
    }

    return (void***)ptr;
}

void array_print_int_1d(char *memory, size_t x)
{
    assert(memory != NULL);
    assert(x > 0);

    char first = 1;
    size_t i;

    printf("Array Dump:\n[");

    for (i = 0; i < x; i++) {
        if (first) {
            printf("%3d", ((int*)memory)[i]);
            first = 0;
        } else {
            printf(", %3d", ((int*)memory)[i]);
        }
    }

    printf("]\n");
}

void array_print_int_2d(char **memory, size_t x, size_t y)
{
    assert(memory != NULL);
    assert(x > 0);
    assert(y > 0);

    char first = 1;
    size_t i, j;

    int** array = (int**)memory;

    printf("Array Dump:\n[\n");

    for (i = 0; i < y; i++) {
        first = 1;
        printf("\t[");
        for (j = 0; j < x; j++) {
            if (first) {
                printf("%3d", array[i][j]);
                first = 0;
            } else {
                printf(", %3d", array[i][j]);
            }
        }
        printf("]\n");
    }

    printf("]\n");
}

void array_print_int_3d(char ***memory, size_t x, size_t y, size_t z)
{
    assert(memory != NULL);
    assert(x > 0);
    assert(y > 0);
    assert(z > 0);

    int*** array = (int***)memory;

    char first = 1;
    size_t i, j, k;

    printf("Array Dump:\n[\n");

    for (i = 0; i < z; i++) {
        printf("\t[\n");
        for (j = 0; j < y; j++) {
            first = 1;
            printf("\t\t[");
            for (k = 0; k < x; k++) {
                if (first) {
                    printf("%3d", array[i][j][k]);
                    first = 0;
                } else {
                    printf(", %3d", array[i][j][k]);
                }
            }
            printf("]\n");
        }
        printf("\t]\n");
    }

    printf("]\n");
}

static inline void free_on_failure(char *p1, char **p2, char ***p3)
{
    if (p1 != NULL)
        free(p1);
    if (p2 != NULL)
        free(p2);
    if (p3 != NULL)
        free(p3);
}
