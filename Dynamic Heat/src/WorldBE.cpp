#include "timetaker.hpp"
#include "World.hpp"
#include "KernelLoader.hpp"
#include "arraymemory.hpp"
#include "world.cuh"

#include <omp.h>
#include <xmmintrin.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include <iostream>
#include <string>
#include <cmath>

#include <QString>
#include <QEvent>
#include <QCoreApplication>

#define MICROSECONDS_IN_A_SECOND 1000000

World::World(int width, int height, int levels, int hotspots) :
    width(width),
    height(height),
    levels(levels),
    hotspots(hotspots),
    ready(false),
    zoomLvl(0),
    zoomOn(false),
    tempX(width/2),
    tempY(height/2),
    tempZ(levels-1)
{
#ifdef CUDA
    cudaInitialize(width, height, levels);
#endif

    guiMutex = new QMutex;
    guiLock  = new QWaitCondition;

    if (width < 1500 && height < 900) {
        setMinimumSize(width, height);
        setMaximumSize(width, height);
    }

#ifdef CUDA
    tmp  = (float***)allocate_memory_3d(sizeof(float), (size_t)width, (size_t)height, (size_t)levels);
    data = (float***)allocate_memory_3d(sizeof(float), (size_t)width, (size_t)height, (size_t)levels);
#else
    tmp  = (float***)malloc(levels * sizeof(float**));
    data = (float***)malloc(levels * sizeof(float**));
    for (int l = 0; l < levels; l++) {
        data[l] = D2calloc(width, height);
        tmp[l]  = D2calloc(width, height);
    }
#endif
    
    heater = (HeatSpot*)malloc(hotspots * sizeof(struct HeatSpot));
    for (int h = 0; h < hotspots; h++) {
        heater[h].x = rand() % width;
        heater[h].y = rand() % height;
    }

    heatAnimate();

    glfunction.initializeGLFunctions(context());
    resizeGL(width, height);
    updateGL();
    heatShader();
}

World::~World()
{
    clearShader();
#ifdef CUDA
    free_memory((void*)tmp, 3);
    free_memory((void*)data, 3);

    cudaTearDown();
#else
    for (int i = 0; i < levels; i++) {
        free(data[i]);
        free(tmp[i]);
    }
    free(data);
    free(tmp);
    free(heater);
#endif
    delete guiMutex;
    delete guiLock;
}

float** World::D2calloc(int width, int height)
{
    float *mem = (float*)calloc(width * height, sizeof(float));
    float **ptr = (float**)calloc(height, sizeof(float*));

    for (int i = 0; i < height; i++)
        ptr[i] = &(mem[i * width]);

    return ptr;
}

void World::heatAnimate()
{
    for (int i = 0; i < hotspots; i++) {
        int dir = rand() % 4;
        int vel = rand() % 3;
        int x = heater[i].x;
        int y = heater[i].y;

        switch (dir) {
        case 0:
            if (x + vel < width)
                x += vel;
            break;
        case 1:
            if (x - vel >= 0)
                x -= vel;
            break;
        case 2:
            if (y + vel < height)
                y += vel;
            break;
        case 3:
            if (y - vel >= 0)
                y -= vel;
            break;
        default:
            break;
        }

        data[0][y][x] = 1.0f;
        if (y + 1 < height)
            data[0][y+1][x] = 1.0f;
        if (y - 1 >= 0)
            data[0][y-1][x] = 1.0f;
        if (x + 1 < width)
            data[0][y][x+1] = 1.0f;
        if (x - 1 >= 0)
            data[0][y][x-1] = 1.0f;

        heater[i].x = x;
        heater[i].y = y;
    }
}

void World::run()
{
    struct timeval startTime, stopTime;
    bool dotemp = false, istemp = false;

    long int total_sec = 0, total_usec = 0;

    // iterate for 5 hours worth @ 60fps
    for (int iter = 0; iter < 1080000; iter++) {
        if (iter > 0) {
            long int sec, usec;
            timetaker_time(&startTime, &stopTime, &sec, &usec);
            total_sec += sec;
            total_usec += usec;


            if (total_usec >= MICROSECONDS_IN_A_SECOND) {
                total_sec += total_usec / MICROSECONDS_IN_A_SECOND;
                total_usec = total_usec % MICROSECONDS_IN_A_SECOND;
            }

            int vTime = iter / 60; // Virtual, simulated tim
            int pTime = (int)total_sec; // Time spend processing
            float mpps = ((((float)width * height * iter) / 1000000) * levels) / (total_sec + (total_usec / 1000000.0f));
            int fps = (int)(iter/(total_sec + (total_usec / 1000000.0f)));
            float speed = (float)vTime / (float)pTime;

            if (dotemp && !istemp) {
                fprintf(stderr, "\n >>>>> %d - %02d:%02d:%02d\n", iter, (vTime / 60) / 60, vTime / 60, vTime % 60);
                istemp = true;
            } else if (!dotemp && istemp) {
                fprintf(stderr, "\n <<<<< %d - %02d:%02d:%02d\n", iter, (vTime / 60) / 60, vTime / 60, vTime % 60);
                istemp = false;
            }

            fprintf(stderr,
                    "\rZoom: %d, Iter: %d/%d, vTime: %02d:%02d:%02d, pTime: %02d:%02d:%02d, Speed: %.2fx, %.2fMP/s, %dFPS, Temp: %f",
                    zoomLvl, iter, 1080000, (vTime / 60) / 60, vTime / 60, vTime % 60, (pTime / 60) / 60, pTime / 60, pTime % 60, speed, mpps, fps, 1600 * data[tempZ][tempY][tempX]);
        }

        timetaker_start(&startTime);

        dotemp = false;

#ifdef CUDA
        cuda_run(data, tmp, width, height, levels);
#else
#ifdef SSE
        __m128 levelfac = _mm_set_ps1(0.1272f);

        int l, y, x;
        __m128 val, xval;
#ifdef OMP // ^0
#pragma omp parallel for private (y, x, val, xval)
#endif // OMP ^0
        for (l = 0; l < levels; l++) {
#ifdef OMP // OMP ^1
            if (l > 0)
                levelfac = _mm_set_ps1(0.1262f);
#endif // OMP ^1
            for (y = 0; y < height; y++) {
                for (x = 0; x < width; x += 4) {
                    val = _mm_load_ps(&data[l][y][x]);
                    val = _mm_mul_ps(val, _mm_set_ps1(0.5099f));
                    if (y + 1 < height)
                        val = _mm_add_ps(_mm_mul_ps(_mm_load_ps(&data[l][y + 1][x]), levelfac), val);
                    if (y - 1 >= 0)
                        val = _mm_add_ps(_mm_mul_ps(_mm_load_ps(&data[l][y - 1][x]), levelfac), val);

                    if (x + 4 < width) {
                        xval = _mm_loadu_ps(&data[l][y][x+1]);
                    } else {
                        xval = _mm_set_ps(x + 4 < width ? data[l][y][x+4] : 0.0f,
                                          x + 3 < width ? data[l][y][x+3] : 0.0f,
                                          x + 2 < width ? data[l][y][x+2] : 0.0f,
                                          x + 1 < width ? data[l][y][x+1] : 0.0f);
                    }
                    val = _mm_add_ps(_mm_mul_ps(xval, levelfac), val);

                    if (x - 1 >= 0) {
                        xval = _mm_loadu_ps(&data[l][y][x-1]);
                    } else {
                        xval = _mm_set_ps(x + 2 >= 0 ? data[l][y][x+2] : 0.0f,
                                          x + 1 >= 0 ? data[l][y][x+1] : 0.0f,
                                          x  >= 0 ? data[l][y][x] : 0.0f,
                                          x - 1 >= 0 ? data[l][y][x-1] : 0.0f);
                    }
                    val = _mm_add_ps(_mm_mul_ps(xval, levelfac), val);

                    if (l + 1 < levels)
                        val = _mm_add_ps(_mm_mul_ps(_mm_load_ps(&data[l + 1][y][x]), _mm_set_ps1(5.0f/2800.0f)), val);
                    if (l + 1 == levels)
                        val = _mm_add_ps(_mm_mul_ps(_mm_load_ps(&data[l - 1][y][x]), _mm_set_ps1(5.0f/2800.0f)), val);
                    if (l - 1 >= 0)
                        val = _mm_add_ps(_mm_mul_ps(_mm_load_ps(&data[l - 1][y][x]), _mm_set_ps1(5.0f/667.0f)), val);


                    val = _mm_mul_ps(_mm_set_ps1(0.97722f), val);

                    val = _mm_max_ps(val, _mm_setzero_ps());
                    val = _mm_min_ps(val, _mm_set_ps1(1.0f));

                    _mm_store_ps(&tmp[l][y][x], val);

                    if (l == levels - 1 && (tmp[l][y][x] == 1.0f || tmp[l][y][x+1] == 1.0f || tmp[l][y][x+2] == 1.0f || tmp[l][y][x+3] == 1.0f))
                        dotemp = true;
                }
            }
#ifndef OMP // OMP ^2
            levelfac = _mm_set_ps1(0.1262f);
#endif // OMP ^2
        }
#else // not SSE
        float levelfac = 0.1272f;
        for (int l = 0; l < levels; l++) {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    float val = data[l][y][x] * 0.5099f;

                    if (y + 1 < height)
                        val += data[l][y+1][x] * levelfac;
                    if (y - 1 >= 0)
                        val += data[l][y-1][x] * levelfac;
                    if (x + 1 < width)
                        val += data[l][y][x+1] * levelfac;
                    if (x - 1 >= 0)
                        val += data[l][y][x-1] * levelfac;
                    if (l + 1 < levels)
                        val += data[l+1][y][x] * (5.0f / 2800.0f);
                    if (l + 1 == levels)
                        val += data[l-1][y][x] * (5.0f / 2800.0f);
                    if (l - 1 >= 0)
                        val += data[l-1][y][x] * (5.0f / 667.0f);

                    val *= 0.97722f;

                    if (val < 0.0f)
                        val = 0.0f;
                    if (val > 1.0f)
                        val = 1.0f;

                    tmp[l][y][x] = val;

                    if (l == levels - 1 && val == 1.0f)
                        dotemp = true;
                }
            }
            levelfac = 0.1262f;
        }

#endif // SSE
#endif // CUDA
        timetaker_stop(&stopTime);

        float ***_tmp = tmp;
        tmp = data;
        data = _tmp;
        
        heatAnimate();

        QCoreApplication::removePostedEvents(this);
        QCoreApplication::postEvent(this, new QEvent(QEvent::Paint));
    }

    fprintf(stdout, "\n");
    wait();
}

void World::initializeGL()
{
    glShadeModel(GL_FLAT);
    glEnable(GL_TEXTURE_2D);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    checkGLError("InitializeGL");

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
    checkGLError("Render settings");
}

void World::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
}

void World::checkGLError(const char *msg)
{
    int err = glGetError();
    if (err) {
        fprintf(stderr, "OpenGL Error 0x%x: %s.\n", err, msg);
        std::exit(err);
    }
}

void World::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
    checkGLError("paintGL settings");

    if (!ready)
        return;

    float d = (float)height / levels;
    float o = width / 4.0f;

    if (zoomOn) {
        for (int j = 0; j < height; j++)
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, j, width, 1, GL_RED, GL_FLOAT, data[zoomLvl][j]);

        glBegin(GL_QUADS);
        glTexCoord2f(0, 0);       glVertex2f(0, 0);
        glTexCoord2f(1.0f, 0);    glVertex2f(width, 0);
        glTexCoord2f(1.0f, 1.0f); glVertex2f(width, height);
        glTexCoord2f(0, 1.0f);    glVertex2f(0, height);
        glEnd();

        checkGLError("paintGL paint ZOOM");
    } else {
        for (int i = 0; i < levels; i++) {
            for (int j = 0; j < height; j++)
                glTexSubImage2D(GL_TEXTURE_2D, 0, 0, j, width, 1, GL_RED, GL_FLOAT, data[i][j]);

            glBegin(GL_QUADS);
            glTexCoord2f(0, 0);       glVertex2f(o + 15        , d * (levels - 1 - i) + 10);
            glTexCoord2f(1.0f, 0);    glVertex2f(width - o + 15, d * (levels - 1 - i) + 10);
            glTexCoord2f(1.0f, 1.0f); glVertex2f(width - 15    , height - d * i - 10);
            glTexCoord2f(0, 1.0f);    glVertex2f(15            , height - d * i - 10);
            glEnd();

            checkGLError("paintGL paint");
        }
    }

    guiLock->wakeOne();
}

void World::configureTexture(size_t idx)
{
    glBindTexture(GL_TEXTURE_2D, screenTexture[idx]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
}

void World::heatShader()
{
    screenTextureCount = 1;
    glGenTextures(screenTextureCount, screenTexture);

    configureTexture(0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0,
                 GL_RED, GL_FLOAT, NULL);
    checkGLError("Terrain shader: terrain");

    bool src_c = shaderProgram.addShaderFromSourceCode(
        QGLShader::Fragment,
        KernelLoader::readKernel("opengl/heat.frg").c_str()
        );
    if (!src_c)
        fprintf(stderr, "OpenGL Error: Terrain shader compilation failed.\n");

    shaderProgram.bind();
    ready = true;
}

void World::clearShader() {
    glDeleteTextures(screenTextureCount, screenTexture);
    shaderProgram.release();
    shaderProgram.removeAllShaders();
}
