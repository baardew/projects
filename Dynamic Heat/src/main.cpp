#include "main.hpp"

#include <QApplication>
#include <QGridLayout>
#include <QThread>
#include <QPushButton>

#include <iostream>

#include <stdio.h>
#include <stdlib.h>

Prototype::Prototype(int w, int h, int l, int p)
{
    world = new World(w, h, l, p);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(world, 0, 0, 1, 2);

    QPushButton *up = new QPushButton("UP");
    QPushButton *down = new QPushButton("DOWN");
    QPushButton *zoomIn = new QPushButton("Zoom +");
    QPushButton *zoomOut = new QPushButton("Zoom -");
    layout->addWidget(up, 1, 0);
    layout->addWidget(down, 2, 0);
    layout->addWidget(zoomIn, 1, 1);
    layout->addWidget(zoomOut, 2, 1);

    connect(up, SIGNAL(released()), world, SLOT(levelUp()));
    connect(down, SIGNAL(released()), world, SLOT(levelDown()));
    connect(zoomIn, SIGNAL(released()), world, SLOT(zoomIn()));
    connect(zoomOut, SIGNAL(released()), world, SLOT(zoomOut()));
    
    setLayout(layout);
}

void Prototype::run()
{
    ThreadExecutioner *t = new ThreadExecutioner(this);
    t->start();
}

World* Prototype::getWorld()
{
    return world;
}

void Prototype::updateGUI(GUICommand cmd)
{
    switch (cmd) {
    case SHOW:
        show();
        break;
    case CLOSE:
        close();
        break;
    default:
        break;
    }
}

ThreadExecutioner::ThreadExecutioner(Prototype *prototype) :
    prototype(prototype)
{
    connect(this, SIGNAL(updateGUI(GUICommand)), prototype, SLOT(updateGUI(GUICommand)));
}

ThreadExecutioner::~ThreadExecutioner()
{
    
}

void ThreadExecutioner::run()
{
    emit updateGUI(SHOW);
    prototype->getWorld()->run();
    emit updateGUI(CLOSE);
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    qRegisterMetaType<GUICommand>("GUICommand");

    if (argc != 5) {
        printf("Missing resolution.\n");
        return EXIT_FAILURE;
    }

    int w = atoi(argv[1]);
    int h = atoi(argv[2]);
    int l = atoi(argv[3]);
    int p = atoi(argv[4]);

    Prototype mainWin(w, h, l, p);
#ifdef NOGUI
    mainWin.getWorld()->run();
    return EXIT_SUCCESS;
#else
    mainWin.run();
    return app.exec();
#endif
}
