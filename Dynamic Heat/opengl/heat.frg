uniform sampler2D tex_heat;

void main()
{
    vec4 heat = texture2D(tex_heat, gl_TexCoord[0].xy);
    if (heat.r >= 0.97)
        gl_FragColor = vec4(1.0, 1.0, 1.0, 0.0);
    else
        gl_FragColor = vec4(heat.r, 0.0, 1.0 - heat.r, 0.0);
}
