#ifndef WORLD_H
#define WORLD_H

#define GL_GLEXT_PROTOTYPES
#include <QGLWidget>
#include <QGLShader>
#include <QGLShaderProgram>
#include <QGLFunctions>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QMouseEvent>

class World : public QGLWidget
{
    Q_OBJECT;

private:
    struct HeatSpot {
        int x;
        int y;
    };

public:
    World(int width, int height, int levels, int hotspots);
    ~World();

public:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void run();

private:
    // Common
    int width, height, levels, hotspots;
    bool ready;
    float ***tmp;
    float ***data;
    QMutex *guiMutex;
    HeatSpot *heater;
    QWaitCondition *guiLock;

    float** D2calloc(int width, int height);
    void heatShader();
    void heatAnimate();

    // OpenGL
    int screenTextureCount;
    GLuint screenTexture[1];
    QGLFunctions glfunction;
    QGLShaderProgram shaderProgram;

    void checkGLError(const char*);
    void configureTexture(size_t idx);
    void clearShader();

    //GUI
    int zoomLvl;
    bool zoomOn;
    int tempX, tempY, tempZ;

public slots:
    void zoomIn();
    void zoomOut();
    void levelUp();
    void levelDown();
    void mouseReleaseEvent(QMouseEvent *mouse);
};

#endif
