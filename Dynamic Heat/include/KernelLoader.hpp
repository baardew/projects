#ifndef KERNEL_LOADER_H
#define KERNEL_LOADER_H

#include <string>

class KernelLoader
{

public:
    static std::string readKernel(const char* filename);
};

#endif
