#ifndef TIMETAKER_H
#define TIMETAKER_H

#include <sys/time.h>
#include <stdlib.h>

/**
 * @file
 *
 * @brief A set of operations to perform timetaking of a task.
 *
 * @note The measurement uses the C time function, which means that it
 * is not completely accurate.
 */
#ifdef _cplusplus
extern "C"
{
#endif

/**
 * @brief Starts the clock for a specified holder.
 *
 * The clock is started by setting the current time value to it.
 *
 * @param clock The clock to start.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_start(struct timeval *clock);

/**
 * @brief Stops the clock for a specified holder.
 *
 * The clock is stopped by setting the current time value to it.
 *
 * @param clock The clock to start.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_stop(struct timeval *clock);

/**
 * @brief Retrieves the actual time between two clocks.
 *
 * @param from The _start clock.
 * @param to The _stop clock.
 * @param sec The time part for seconds.
 * @param usec The time part for microseconds.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_time(struct timeval *from, struct timeval *to, long int *sec, long int *usec);

#ifdef _cplusplus
}
#endif

#endif /* TIMETAKER_H */
