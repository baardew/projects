#include "World.hpp"
#include "GUICommand.hpp"


#include <QWidget>
#include <QCheckBox>

class Prototype : public QWidget
{
    Q_OBJECT;
    
public:
    Prototype(int w, int h, int l, int p);
    void run();
    World* getWorld();

public slots:    
    void updateGUI(GUICommand cmd);
    
private:
    World *world;
};

class ThreadExecutioner : public QThread
{
    Q_OBJECT
    
public:
    ThreadExecutioner(Prototype *prototype);
    ~ThreadExecutioner();

    void run();

private:
    Prototype *prototype;

signals:
    void updateGUI(GUICommand cmd);
};
