#ifndef WORLD_CUDA_H
#define WORLD_CUDA_H

#include <stdio.h>

#include <cuda.h>

#define CUDA_ERR_VAR cudaError_t _cuda_err
#define CUDA_CHECK(_cuda)                                               \
    _cuda_err = _cuda;                                                  \
    if (_cuda_err != cudaSuccess) {                                     \
        fprintf(stderr, "CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
        exit(EXIT_FAILURE);                                             \
    }

#define CUDA_CHECK_KERNEL()                                             \
    CUDA_CHECK(cudaDeviceSynchronize());                                \
    _cuda_err = cudaGetLastError();                                     \
    if (_cuda_err != cudaSuccess) {                                     \
        fprintf(stderr, "CUDA KERNEL error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
        exit(EXIT_FAILURE);                                             \
    }

extern "C" {
    void cuda_run(float ***data, float ***result, int width, int height, int levels);
    float*** cuda3Dmalloc(int width, int height, int levels);
    void cudaInitialize(int width, int height, int levels);
    void cudaTearDown();
}

#endif
