#include "world.cuh"

#include <stdio.h>

#define TPB 32

__device__
int XYZ(int x, int y, int z, int width, int height)
{
    return (z * width * height) + (y * width) + (x);
}

float *d_tmp;
float *d_data;

__global__
void compute_dynamic_heat(float *data, float *result, int width, int height, int levels)
{
    int x = blockIdx.x * TPB + threadIdx.x;
    int y = blockIdx.y * TPB  + threadIdx.y;
    int z = blockIdx.z;
    
    if (x >= width || y >= height)
        return;
    
    float val = data[XYZ(x, y, z, width, height)] * 0.5099f;
    float levelfac = z == 0 ? 0.1272f : 0.1262f;
    if (y + 1 < height)
        val += data[XYZ(x, y+1, z, width, height)] * levelfac;
    if (y > 0)
        val += data[XYZ(x, y-1, z, width, height)] * levelfac;
    if (x + 1 < width)
        val += data[XYZ(x+1, y, z, width, height)] * levelfac;
    if (x > 0)
        val += data[XYZ(x-1, y, z, width, height)] * levelfac;
    if (z + 1 < levels)
        val += data[XYZ(x, y, z+1, width, height)] * (5.0f / 2800.0f);
    if (z + 1 == levels)
        val += data[XYZ(x, y, z-1, width, height)] * (5.0f / 2800.0f);
    if (z > 0)
        val += data[XYZ(x, y, z-1, width, height)] * (5.0f / 667.0f);
    
    val *= 0.97722f;
    
    if (val < 0.0f)
        val = 0.0f;
    if (val > 1.0f)
        val = 1.0f;
    
    result[XYZ(x, y, z, width, height)] = val;
}

__host__
void cuda_run(float ***data, float ***result, int width, int height, int levels)
{    
    CUDA_ERR_VAR;

    dim3 gridDim(width / TPB, height / TPB, levels);
    dim3 blockDim(TPB, TPB);

    CUDA_CHECK(cudaMemcpy(d_data, data[0][0], width * height * levels * sizeof(float), cudaMemcpyHostToDevice));
    
    compute_dynamic_heat<<<gridDim, blockDim>>>(d_data, d_tmp, width, height, levels); CUDA_CHECK_KERNEL();
    CUDA_CHECK_KERNEL();

    CUDA_CHECK(cudaMemcpy(result[0][0], d_tmp, width * height * levels * sizeof(float), cudaMemcpyDeviceToHost));
}


__host__
void cudaInitialize(int width, int height, int levels)
{
    CUDA_ERR_VAR;
    CUDA_CHECK(cudaMalloc(&d_tmp, width * height * levels * sizeof(float)));
    CUDA_CHECK(cudaMalloc(&d_data, width * height * levels * sizeof(float)));
}

__host__
void cudaTearDown()
{
    cudaFree(d_tmp);
    cudaFree(d_data);
}
