package no.wintherproduksjoner.synthservice.db.engine;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.db.tool.ItemCollection;

import no.wintherproduksjoner.synthservice.db.storage.Item;

import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;

import java.lang.Iterable;

public class ItemStorage implements ItemCollection {

    private HashMap<String, ArrayList<Item>> data;

    public ItemStorage(int initialsize) {
        data = new HashMap<String, ArrayList<Item>>(initialsize);
    }

    public int addItemCollection(Iterable<? extends Item> collection) {
        enter();
        int adds = 0;

        for (Item i: collection) {
            if (addItem(i))
                adds++;
        }

        leave(adds);
        return adds;
    }

    public boolean addItem(Item i) {
        if (data.get(i.getName()) == null)
            data.put(i.getName(), new ArrayList<Item>());

        Item dataItem = findItem(i);
        if (dataItem == null) {
            boolean ret = data.get(i.getName()).add(i);
            leave(ret);
            return ret;
        }

        dataItem.setCount(dataItem.getCount() + i.getCount());
        leave(true);
        return true;
    }

    public boolean removeItem(Item i) {
        Item dataItem = findItem(i);
        if (dataItem == null) {
            message("dataItem is null.");
            leave(false);
            return false;
        }

        boolean ret = data.get(i.getName()).remove(dataItem);
        leave(ret);
        return ret;
    }

    public Item[] getItems(String name) {
        enter(name);

        ArrayList<Item> itemLine = data.get(name);
        if (itemLine == null) {
            message("item not found.");
            leave(0);
            return new Item[0];
        }

        Item[] arrayItems = new Item[itemLine.size()];
        itemLine.toArray(arrayItems);
        leave(arrayItems.length);
        return arrayItems;
    }

    public Item getItem(String name, double price, boolean ordered) {
        enter(name, price, ordered);
        Item search = new Item(name, 0, price, ordered, false);
        Item item = findItem(search);
        leave(item);
        return item;
    }

    public Item[] getAllItems() {
        enter();
        ArrayList<Item> allItems = new ArrayList<Item>();

        for (ArrayList<Item> itemLine: data.values())
            allItems.addAll(itemLine);

        Item[] arrayItems = new Item[allItems.size()];
        allItems.toArray(arrayItems);

        Item.setCompareMethod(Item.CompareMethod.COMPARE_ORDERED);
        Arrays.sort(arrayItems);

        leave(arrayItems.length);
        return arrayItems;
    }

    private Item findItem(Item i) {
        enter(i);
        ArrayList<Item> list = data.get(i.getName());
        if (list == null) {
            message("list is null.");
            leave((Object)null);
            return null;
        }

        Item.setCompareMethod(Item.CompareMethod.COMPARE_PRICE);
        for (Item o: list) {
            if (i.compareTo(o) == 0 && i.getOrdered() == o.getOrdered()) {
                leave(o);
                return o;
            }
        }

        leave((Object)null);
        return null;
    }
}
