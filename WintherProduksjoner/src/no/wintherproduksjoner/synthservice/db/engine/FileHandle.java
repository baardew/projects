package no.wintherproduksjoner.synthservice.db.engine;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;

import java.io.File;
import java.io.IOException;
import java.io.EOFException;
import java.io.Serializable;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.ObjectInputStream;

import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.ObjectOutputStream;

import java.util.ArrayList;

public class FileHandle<T> {

    private final String filename;
    private static String DBDIR = "";

    private ObjectInputStream in = null;
    private ObjectOutputStream out = null;
    private int size;
    private int index;

    public FileHandle(String filename, Monitor monitor, int initialSize) {
        this.filename = filename != null ? filename : "";
        size = initialSize;
        index = 0;
        in = null;
        out = null;
    }

    public String getFilename() {
        return DBDIR + filename;
    }

    public static void setHomeDirectory(String dir) {
        DBDIR = dir;
    }

    public boolean write(ArrayList<T> list, Monitor monitor) {
        enter(list.size());
        monitor.setMessage("Writing file " + filename);

        if (list.size() == 0) {
            // Must explicitly set the size to 0, the for loop does not do that
            try {
                if (out == null)
                    out = new ObjectOutputStream(new FileOutputStream(getFilename().concat(".tmp")));

                out.writeObject(new Integer(0));
            } catch (Exception e) {
                e.printStackTrace();
                leave(false);
                return false;
            }
        }

        size = list.size();

        for (T obj: list) {
            if (!write(obj, monitor)) {
                leave(false);
                return false;
            }
        }

        leave(true);
        return true;
    }

    public boolean write(T obj, Monitor monitor) {
        enter(obj);
        try {
            if (out == null) {
                monitor.setMessage("Writing file " + filename);
                out = new ObjectOutputStream(new FileOutputStream(getFilename().concat(".tmp")));
                out.writeObject(new Integer(size));
            }

            out.writeObject(obj);
        } catch (Exception e) {
            e.printStackTrace();
            leave(false);
            return false;
        }

        leave(true);
        return true;
    }

    public ArrayList<T> readToArrayList(Monitor monitor) {
        enter();
        monitor.setMessage("Reading file " + filename);

        File f = new File(getFilename());
        if (!f.exists()) {
            message("File does not exist.");
            leave((Object)null);
            return null;
        }

        ArrayList<T> list = new ArrayList<T>();

        T obj = read(monitor);
        while (obj != null) {
            list.add(obj);
            obj = read(monitor);
        }

        leave(list.size() == 0 ? (Object)null : list.size());
        return list.size() == 0 ? null : list;
    }

    //Supress a casting warning when reading the ArrayList object
    @SuppressWarnings("unchecked")
    public T read(Monitor monitor) {
        enter();
        T obj = null;

        // EOF tracker, to avoid exception handling for EOF
        if (index >= size && in != null) {
            message("EOF tracker");
            leave((Object)null);
            return null;
        }

        try {
            getSize(monitor);
            if (size == 0) {
                message("EOF");
                leave((Object)null);
                return null; //EOF
            }

            obj = (T)in.readObject();
            index++;
        } catch(Exception e) {
            e.printStackTrace();
            leave((Object)null);
            return null;
        }

        leave(obj);
        return obj;
    }

    public boolean closeFailedWrite() {
        enter();
        if (out != null) {
            try {
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
                leave(false);
                return false;
            }

            leave(true);
            return true;
        }

        leave(false);
        return false;
    }

    @SuppressWarnings("unchecked")
    public int getSize(Monitor monitor) {
        enter();
        if (in == null) {
            monitor.setMessage("Reading file " + filename);

            try {
                File f = new File(getFilename());
                if (!f.exists()) {
                    leave(0);
                    return 0;
                }

                in = new ObjectInputStream(new FileInputStream(getFilename()));
                size = (Integer)in.readObject();
            } catch(Exception e) {
                e.printStackTrace();
                leave(-1);
                return -1;
            }
        }

        leave(size);
        return size;
    }

    public boolean close() {
        enter();
        if (in != null) {
            try {
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
                enter(false);
                return false;
            }
            in = null;
            index = 0;
        }

        if (out != null) {
            try {
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
                leave(false);
                return false;
            }

            File original = new File(getFilename());
            File updated = new File(getFilename().concat(".tmp"));
            original.delete();
            updated.renameTo(new File(getFilename()));

            out = null;
        }

        leave(true);
        return true;
    }

    public static String getHomeDirectory() {
        String home;
        try{
            home = System.getProperty("user.home");
        } catch (Exception e) {
            e.printStackTrace();
            home = new String("");
        }

        if (home.length() > 1)
            home = home.concat("/WintherProduksjoner Documents/");
        else
            home = home.concat("WintherProduksjoner Documents/");

        return home;
    }
}
