package no.wintherproduksjoner.synthservice.db.engine;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.db.engine.ClientServer;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Status;
import no.wintherproduksjoner.synthservice.db.storage.Item;
import no.wintherproduksjoner.synthservice.db.storage.RepairListItem;

import no.wintherproduksjoner.synthservice.db.engine.FileHandle;
import no.wintherproduksjoner.synthservice.db.engine.ItemStorage;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;
import no.wintherproduksjoner.synthservice.db.tool.SynchronizeStatistic;
import no.wintherproduksjoner.synthservice.db.tool.ItemCollection;
import no.wintherproduksjoner.synthservice.db.tool.AccountSetting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

public class Core {

    private ArrayList<Synthesizer> register = null;
    private FileHandle<Synthesizer> file_register;

    private ItemStorage storage;
    private FileHandle<Item> file_storage;

    private ClientServer server;

    private SynchronizeStatistic synchronizeStatistic;

    public Core(Monitor monitor, AccountSetting setting) {
        monitor.start();
        synchronizeStatistic = null;
        file_register = new FileHandle<Synthesizer>("register.wpdb", monitor, 0);
        file_storage = new FileHandle<Item>("storage.wpdb", monitor, 0);

        server = new ClientServer(setting);

        if ((register = file_register.readToArrayList(monitor)) == null)
            register = new ArrayList<Synthesizer>();

        int size = file_storage.getSize(monitor);
        storage = new ItemStorage(size);
        for (int i = 0; i < size; i++) {
            Item item = file_storage.read(monitor);
            storage.addItem(item);
        }

        monitor.setMessage("Database loaded");
        monitor.stop();
    }

    public ItemCollection getStorage() {
        return storage;
    }

    public synchronized int getRegisterSize() {
        enter();
        leave(register.size());
        return register.size();
    }

    public SynchronizeStatistic getSynchronizationStatistics() {
        return synchronizeStatistic;
    }

    public synchronized Synthesizer[] getAllSynthesizers() {
        enter();
        Synthesizer[] synths = new Synthesizer[getRegisterSize()];
        register.toArray(synths);
        leave(synths.length);
        return synths;
    }

    public synchronized boolean save(Monitor monitor) {
        enter();
        monitor.start();
        boolean ret = write(monitor);
        monitor.stop();
        leave(ret);
        return ret;
    }

    public synchronized void acceptSynthesizer() {
        enter();
        for (Synthesizer s: register)
            s.decreaseQueueNo();
        leave();
    }

    public synchronized boolean declineSynthesizer(Synthesizer synth) {
        enter();
        int queueNo = Integer.MAX_VALUE;
        int q = synth.getQueueNo();

        if (!synth.decline()) {
            leave(false);
            return false;
        }

        int current = 0;
        for (Synthesizer s: register)
            if (s.getQueueNo() > q)
                s.decreaseQueueNo();

        leave(true);
        return true;
    }

    public synchronized boolean cancelSynthesizer(Synthesizer synth) {
        enter();
        boolean ret = synth.cancel();
        leave(ret);
        return ret;
    }

    public synchronized int getArchiveSize(Monitor monitor) {
        enter();
        monitor.start();
        FileHandle<Synthesizer> file =
            new FileHandle<Synthesizer>("archive.wpdb", monitor, 0);

        int size = file.getSize(monitor);
        monitor.stop();
        leave(size);
        return size;
    }

    public synchronized Synthesizer[] readFromArchive(Monitor monitor, int start, int end) {
        enter(start, end);
        monitor.start();
        FileHandle<Synthesizer> file =
            new FileHandle<Synthesizer>("archive.wpdb", monitor, 0);

        int size = file.getSize(monitor);
        if (size < 0)
            return archiveReadFailure(file, monitor);

        size = Math.max(size, 0);
        end = Math.min(end, size);
        start = Math.max(start, 0);
        if (start > end)
            start = end;

        if (end == 0 || size == 0) {
            monitor.setMessage("The archive is empty");
            leave(0);
            return new Synthesizer[0];
        }

        ArrayList<Synthesizer> archive = new ArrayList<Synthesizer>(end - start);

        int idx;
        Synthesizer s;

        monitor.setMessage("Searching into archive");
        for (idx = 0; idx < start; idx++) {
            s = file.read(monitor);
            if (s == null) {
                Synthesizer[] ret = archiveReadFailure(file, monitor);
                leave(0);
                return ret;
            }
        }

        monitor.setMessage("Fetching from archive");
        for (; idx < end; idx++) {
            s = file.read(monitor);
            if (s == null) {
                Synthesizer[] ret =archiveReadFailure(file, monitor);
                leave(0);
                return ret;
            }
            archive.add(s);
        }

        monitor.setMessage("Extracted from archive");
        Synthesizer synths[] = new Synthesizer[archive.size()];
        monitor.stop();
        Synthesizer[] ret = archive.toArray(synths);
        leave(ret.length);
        return ret;
    }

    public synchronized boolean writeToArchive(Monitor monitor, Synthesizer synth) {
        enter(synth);
        monitor.start();

        int[] to_server = new int[1];
        to_server[0] = synth.getReferenceNo();
        if (!server.connect(monitor)) {
            monitor.setMessage("Failed to archive Synthesizer at server");
            monitor.stop();
            leave(false);
            return false;
        }

        if (!server.archiveSynthesizers(to_server, monitor)) {
            monitor.setMessage("Failed to archive Synthesizer at server");
            monitor.stop();
            leave(false);
            return false;
        }

        server.disconnect(monitor);

        synth.setStatus(Status.Archived);
        Synthesizer.setCompareMethod(Synthesizer.CompareMethod.COMPARE_MODIFIED);

        FileHandle<Synthesizer> file_r = new FileHandle<Synthesizer>("archive.wpdb", monitor, 0);
        int size = file_r.getSize(monitor);
        FileHandle<Synthesizer> file_w = new FileHandle<Synthesizer>("archive.wpdb", monitor, size + 1);

        int idx, progress = 0;

        if (size < 0) {
            boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
            leave(ret);
            return ret;
        }

        if (size == 0) {
            if (!file_w.write(synth, monitor)) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("size 0 w_write failed.");
                leave(ret);
                return ret;
            }

            if (!file_r.close()) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("size 0 r_close failed.");
                leave(ret);
                return ret;
            }

            if (!file_w.close()) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("size 0 w_close failed.");
                leave(ret);
                return ret;
            }

            if ((idx = register.indexOf(synth)) == -1) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("size 0 register indexOf failed.");
                leave(ret);
                return ret;
            }

            monitor.setMessage("Archived Synthesizer");
            monitor.stop();
            leave(true);
            return true;
        }

        Synthesizer s = null;
        monitor.setMessage("Search for archive slot");
        for (idx = 0; idx < size; idx++) {
            s = file_r.read(monitor);
            if (s == null) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("search s is null.");
                leave(ret);
                return ret;
            }

            if (synth.compareTo(s) > 0)
                break;

            if (!file_w.write(s, monitor)) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("search w_write failed.");
                leave(ret);
                return ret;
            }
        }

        monitor.setMessage("Archiving Synthesizer");
        if (!file_w.write(synth, monitor)) {
            boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
            message("archive w_write failed.");
            leave(ret);
            return ret;
        }

        monitor.setMessage("Finishing archive");
        if (idx < size) {
            if (!file_w.write(s, monitor)) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("finishing w_write failed.");
                leave(ret);
                return ret;
            }
        }

        for (idx++; idx < size; idx++) {
            s = file_r.read(monitor);
            if (s == null) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("copy s is null.");
                leave(ret);
                return ret;
            }

            if (!file_w.write(s, monitor)) {
                boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
                message("w_write failed.");
                leave(ret);
                return ret;
            }
        }


        if (!file_w.write(synth, monitor)) {
            boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
            message("w_write failed.");
            leave(ret);
            return ret;
        }

        if (!file_r.close()) {
            boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
            message(" r_close failed.");
            leave(ret);
            return ret;
        }

        if (!file_w.close()) {
            boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
            message("w_close failed.");
            leave(ret);
            return ret;
        }

        if ((idx = register.indexOf(synth)) == -1) {
            boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
            message("size 0 register indexOf failed.");
            leave(ret);
            return ret;
        }

        if (!write(monitor)) {
            boolean ret = writeArchiveFailure(file_r, file_w, monitor, synth);
            message("wrote failed.");
            leave(ret);
            return ret;
        }

        monitor.setMessage("Archived Synthesizer");
        monitor.stop();
        leave(true);
        return true;
    }

    public synchronized boolean synchronize(Monitor monitor) {
        enter();
        monitor.start();
        SynchronizeStatistic stat = new SynchronizeStatistic();

        if (!server.connect(monitor)) {
            monitor.setMessage("Synchronization failed: connection");
            monitor.stop();
            leave(false);
            return false;
        }

        Synthesizer[] newSynth = server.downloadNew(monitor);

        if (newSynth == null) {
            monitor.setMessage("Synchronization failed: check for new");
            monitor.stop();
            leave(false);
            return false;
        }

        if (newSynth.length != 0) {
            stat.news = newSynth.length;

            for (Synthesizer s: newSynth)
                register.add(s);

            fixQueueNumbers();

            if (!server.registerNewDownload(newSynth, monitor)) {
                monitor.setMessage("Synchronization failed: register new");
                monitor.stop();
                leave(false);
                return false;
            }
        }

        Synthesizer[] oldSynth = register.toArray(new Synthesizer[getRegisterSize()]);

        if ((stat.updates = server.uploadUpdates(oldSynth, monitor)) < 0) {
            monitor.setMessage("Synchronization failed: upload updates");
            monitor.stop();
            leave(false);
            return false;
        }

        if (!write(monitor)) {
            monitor.setMessage("Synchronization failed: save changes");
            monitor.stop();
            leave(false);
            return false;
        }

        server.disconnect(monitor);
        synchronizeStatistic = stat;
        monitor.setMessage("Synchronization finished");
        monitor.stop();
        leave(true);
        return true;
    }

    public synchronized boolean systemClean(Monitor monitor) {
        enter();
        monitor.start();

        if (!write(monitor)) {
            monitor.setMessage("Failed to Save before System Cleaning");
            monitor.stop();
            leave(false);
            return false;
        }

        if (!synchronize(monitor)) {
            monitor.setMessage("Failed to Synchronize before System Cleaning");
            monitor.stop();
            leave(false);
            return false;
        }

        int deleted = 0;
        Synthesizer synth[] = getAllSynthesizers();
        ArrayList<Synthesizer> removed = new ArrayList<Synthesizer>();
        monitor.setMessage("Removing unused instances");

        int idx = 0;
        for (Synthesizer s: synth) {
            if (s.getStatus() == Status.Declined || s.getStatus() == Status.Archived) {
                removed.add(s);
                if (!register.remove(s)) {
                    monitor.setMessage("Removing archived failed");
                    for (Synthesizer z: removed)
                        register.add(z);
                }
            }
        }


        if (!write(monitor)) {
            monitor.setMessage("Synchronization completed but System Cleaning failed");
            monitor.stop();
            leave(false);
            return false;
        }

        if (!server.connect(monitor)) {
            monitor.setMessage("Synchronization completed but System Cleaning failed");
            monitor.stop();
            leave(false);
            return false;
        }

        if (!server.cleanServer(monitor)) {
            monitor.setMessage("Synchronization completed but System Cleaning failed");
            monitor.stop();
            leave(false);
            return false;
        }

        server.disconnect(monitor);
        synchronizeStatistic.deleted = deleted;
        monitor.setMessage("System succsessfully cleaned");
        monitor.stop();
        leave(true);
        return true;
    }

    public synchronized RepairListItem[] downloadRepairList(Monitor monitor) {
        enter();
        monitor.start();

        if (!server.connect(monitor)) {
            monitor.setMessage("Failed to download Repair List");
            monitor.stop();
            leave((Object)null);
            return null;
        }

        RepairListItem[] items = server.downloadRepairList(monitor);

        server.disconnect(monitor);
        monitor.stop();

        leave(items.length);
        return items;
    }

    public synchronized boolean updateRepairList(RepairListItem[] oldItem, RepairListItem[] newItem, Monitor monitor) {
        enter(oldItem.length, newItem.length);
        monitor.start();

        if (!server.connect(monitor)) {
            monitor.setMessage("Synchronization failed");
            monitor.stop();
            leave(false);
            return false;
        }

        boolean ret = true;
        for (int i = 0; i < oldItem.length; i++) {
            ret = server.updateRepairList(oldItem[i], newItem[i], monitor);

            if (ret = false)
                break;
        }

        server.disconnect(monitor);
        monitor.stop();

        leave(ret);
        return ret;
    }

    private Synthesizer[] archiveReadFailure(FileHandle<Synthesizer> file, Monitor monitor) {
        file.close();
        monitor.setMessage("Failed to read from archive");
        try {
            throw new Exception("Failed to read from archive");
        } catch (Exception e) {
            e.printStackTrace();
        }
        monitor.stop();
        return new Synthesizer[0];
    }

    private boolean writeArchiveFailure(FileHandle<Synthesizer> file_r, FileHandle<Synthesizer> file_w, Monitor monitor, Synthesizer synth) {
        file_w.closeFailedWrite(); // Close writer
        file_r.close(); // Close reader
        synth.setStatus(Status.ReturnedToOwner);
        monitor.setMessage("Failed to archive Synthesizer");
        try {
            throw new Exception("Failed to archive Synthesizer");
        } catch (Exception e) {
            e.printStackTrace();
        }
        monitor.stop();
        return false;
    }

    private void fixQueueNumbers() {
        enter();
        Synthesizer[] sorted = getAllSynthesizers();

        Arrays.sort(sorted, new Comparator<Synthesizer>() {
                public int compare(Synthesizer m1, Synthesizer m2) {
                    return m1.getQueueNo() - m2.getQueueNo();
                }
            });

        int cur = 0;
        for (Synthesizer s: sorted) {
            if (s.getStatus() != Status.InQueue)
                continue;

            while (s.getQueueNo() - cur > 1)
                s.decreaseQueueNo();
            cur++;
        }
        leave();
    }

    private boolean write(Monitor monitor) {
        enter();
        monitor.start();
        ArrayList<Item> storageArray = new ArrayList<Item>();
        for (Item i: storage.getAllItems())
            storageArray.add(i);

        if (!file_storage.write(storageArray, monitor)) {
            monitor.stop();
            message("storage write failed.");
            leave(false);
            return false;
        }

        if (!file_register.write(register, monitor)) {
            monitor.stop();
            message("register write failed.");
            leave(false);
            return false;
        }

        boolean ret = file_register.close() && file_storage.close();
        monitor.setMessage("Database saved");
        monitor.stop();
        leave(ret);
        return ret;
    }
}
