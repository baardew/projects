package no.wintherproduksjoner.synthservice.db.engine;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Status;
import no.wintherproduksjoner.synthservice.db.storage.RepairListItem;

import no.wintherproduksjoner.synthservice.db.tool.CoreUtil;
import no.wintherproduksjoner.synthservice.db.tool.Monitor;
import no.wintherproduksjoner.synthservice.db.tool.AccountSetting;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Arrays;
import java.util.Comparator;

public class ClientServer {

    private AccountSetting setting;
    private Connection server;

    public ClientServer(AccountSetting setting) {
        this.setting = setting;
        server = null;
    }

    public synchronized boolean connect(Monitor monitor) {
        enter();
        monitor.setMessage("Connecting to the Client Server");

        // Connection request happens in a seperate thread
        // in order to have a timeout function (not implemented in driver)
        DatabaseConnector dc = new DatabaseConnector(this);
        dc.start();

        synchronized (this) {
            try {
                if (!dc.haveConnection())
                    wait(5000);
            } catch (InterruptedException e) {
                // Task finished before timeout
            }
        }

        server = dc.getConnection();

        if (server != null) {
            monitor.setMessage("Connection established");
            leave(true);
            return true;
        }

        monitor.setMessage("Could not connect to Client Server");
        leave(false);
        return false;
    }

    public synchronized void disconnect(Monitor monitor) {
        enter();
        monitor.setMessage("Disconnecting from the Client Server");

        if (server != null) {
            try {
                server.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            server = null;
        }

        monitor.setMessage("Disconnected from the Client Server");
        leave();
    }

    public synchronized Synthesizer[] downloadNew(Monitor monitor) {
        enter();
        monitor.setMessage("Checking for new Synthesizers");

        Statement query = null;
        ResultSet result;
        Synthesizer synth[] = new Synthesizer[0];

        try {
            query = server.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            result = query.executeQuery("SELECT COUNT(referenceno) as refnos FROM synthrepairform WHERE newinstance IS TRUE");
            if (!result.next()) {
                monitor.setMessage("Failed to download new Synthesizers");
                leave((Object)null);
                return null;
            }

            monitor.setMessage("Downloading new Synthesizers");

            synth = new Synthesizer[result.getInt("refnos")];
            result = query.executeQuery("SELECT * FROM synthrepairform NATURAL JOIN clientcontact WHERE newinstance IS TRUE ORDER BY queueno");

            for (int i = 0; i < synth.length; i++) {
                if (!result.next()) {
                    monitor.setMessage("Failed to download new Synthesizers");
                    leave((Object)null);
                    return null;
                }

                synth[i] = new Synthesizer(result.getInt("referenceno"),
                                           result.getInt("queueno"),
                                           result.getString("modified").substring(0, 19),
                                           result.getString("firstname"),
                                           result.getString("lastname"),
                                           result.getString("firm"),
                                           result.getString("address"),
                                           result.getInt("postcode"),
                                           result.getString("postname"),
                                           result.getInt("phone"),
                                           result.getString("email"),
                                           result.getString("manufacturer"),
                                           result.getString("model"),
                                           result.getString("serialno"),
                                           result.getString("errordescription"));
            }

        } catch (SQLException e) {
            try {
                query.close();
            } catch (SQLException r) {
                r.printStackTrace();
                monitor.setMessage("Failed to download new Synthesizers");
                leave((Object)null);
                return null;
            }
            e.printStackTrace();
            monitor.setMessage("Failed to download new Synthesizers");
            leave((Object)null);
            return null;

        } finally {
            try {
                query.close();
            } catch (SQLException e) {
                e.printStackTrace();
                monitor.setMessage("Failed to download new Synthesizers");
                leave((Object)null);
                return null;
            }
        }

        leave(synth.length);
        return synth;
    }

    public synchronized boolean registerNewDownload(Synthesizer[] synths, Monitor monitor) {
        enter();
        monitor.setMessage("Registering new downloads");

        Statement query = null;
        int idx = 0;

        try {
            query = server.createStatement();
            query.executeUpdate("BEGIN");
            for (Synthesizer s: synths) {
                query.executeUpdate("UPDATE synthrepairform SET newinstance = FALSE WHERE referenceno = " + s.getReferenceNo());
                query.executeUpdate("DELETE FROM clientcontact WHERE referenceno = " + s.getReferenceNo());
            }
            query.executeUpdate("COMMIT");

        } catch (SQLException e) {
            try {
                query.close();
                query.executeUpdate("ROLLBACK");
                monitor.setMessage("Failed to register new downloads");
            } catch (SQLException r) {
                r.printStackTrace();
                leave(false);
                return false;
            }
            e.printStackTrace();
            monitor.setMessage("Failed to register new downloads");
            return false;

        } finally {
            try {
                query.close();
            } catch (SQLException e) {
                e.printStackTrace();
                monitor.setMessage("Failed to register new downloads");
                leave(false);
                return false;
            }
        }

        leave(false);
        return true;
    }

    public synchronized int uploadUpdates(Synthesizer[] synths, Monitor monitor) {
        Statement query = null;
        Statement update = null;
        PreparedStatement statement = null;
        ResultSet result;
        int updates = 0;

        enter(synths.length);
        if (synths.length == 0) {
            monitor.setMessage("No Synthesizers to update");
            leave(0);
            return 0;
        }

        monitor.setMessage("Sorting for upload check");

        Arrays.sort(synths, new Comparator<Synthesizer>() {
                public int compare(Synthesizer s1, Synthesizer s2) {
                    return s1.getReferenceNo() - s2.getReferenceNo();
                }
            });

        monitor.setMessage("Comparing Synthesizer for upload");

        try {
            query = server.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            update = server.createStatement();
            update.executeUpdate("BEGIN");
            result = query.executeQuery("SELECT referenceno, modified FROM synthrepairform WHERE archived IS FALSE AND newinstance IS FALSE ORDER BY referenceno");
            if (!result.next()) {
                leave(-1);
                return -1;
            }

            monitor.setMessage("Updating " + synths.length + " synths");
            int idx = 0;
            for (Synthesizer s: synths) {
                int refNo = s.getReferenceNo();
                if (refNo < result.getInt("referenceno"))
                    continue;

                while (refNo > result.getInt("referenceno")) {
                    if (!result.next()) {
                        leave(-2);
                        return -2;
                    }
                }

                if (refNo != result.getInt("referenceno"))
                    continue;

                if (CoreUtil.stringToDate(s.getModified()).compareTo(CoreUtil.stringToDate(result.getString("modified").substring(0, 19))) > 0) {
                    monitor.setMessage("Uploading Synthesizer updates");

                    statement = server.prepareStatement("UPDATE synthrepairform SET queueno = ?, modified = ?"
                                                   + ", manufacturer = ?, model = ?"
                                                   + ", serialno = ?, status = ?"
                                                   + ", feedback = ? WHERE referenceno = ?");
                    statement.setInt(1, s.getQueueNo());
                    statement.setString(2, s.getModified());
                    statement.setString(3, s.getManufacturer());
                    statement.setString(4, s.getModel());
                    statement.setString(5, s.getSerialNo());
                    statement.setString(6, s.getStatus().toString());
                    statement.setString(7, s.getFeedback());
                    statement.setInt(8, refNo);
                    statement.executeUpdate();
                    statement.close();

                    updates++;
                }
            }

            update.executeUpdate("COMMIT");

        } catch (SQLException e) {
            try {
                update.executeUpdate("ROLLBACK");
                monitor.setMessage("Failed to upload updates");
                update.close();
                query.close();
            } catch (SQLException r) {
                r.printStackTrace();
                monitor.setMessage("Failed to upload updates");
                leave(-3);
                return -3;
            }
            e.printStackTrace();
            monitor.setMessage("Failed to upload updates");
            leave(-4);
            return -4;

        } finally {
            try {
                update.close();
                query.close();
            } catch (SQLException e) {
                e.printStackTrace();
                monitor.setMessage("Failed to upload updates");
                leave(-5);
                return -5;
            }
        }

        leave(updates);
        return updates;
    }

    public synchronized boolean archiveSynthesizers(int[] synth, Monitor monitor) {
        enter(synth.length);
        monitor.setMessage("Archiving Synthesizers at the Client Server");

        Statement query = null;

        try {
            query = server.createStatement();
            query.executeUpdate("BEGIN");

            for (int i = 0; i < synth.length; i++) {
                query.executeUpdate("UPDATE synthrepairform SET status = '" + Status.Archived.toString()
                                    + "', archived = TRUE WHERE referenceno = " + synth[i]);
            }
            query.executeUpdate("COMMIT");

        } catch (SQLException e) {
            try {
                query.executeUpdate("ROLLBACK");
                query.close();
            } catch (SQLException r) {
                r.printStackTrace();
                monitor.setMessage("Failed to archive Synthesizers at the Client Server");
                leave(false);
                return false;
            }
            e.printStackTrace();
            monitor.setMessage("Failed to archive Synthesizers at the Client Server");
            leave(false);
            return false;

        } finally {
            try {
                query.close();
            } catch (SQLException e) {
                e.printStackTrace();
                monitor.setMessage("Failed to archive Synthesizers at the Client Server");
                leave(false);
                return false;
            }
        }

        leave(true);
        return true;
    }

    public synchronized boolean cleanServer(Monitor monitor) {
        enter();
        monitor.setMessage("Cleaning the Client Server");

        Statement query = null;

        try {
            query = server.createStatement();
            query.executeUpdate("BEGIN");
            query.executeUpdate("INSERT INTO repairlist (manufacturer, model, repairs) "
                                + "SELECT manufacturer, model, 1 "
                                + "FROM synthrepairform "
                                + "WHERE archived = TRUE "
                                + "ON DUPLICATE KEY UPDATE repairs = repairs + 1");

            query.executeUpdate("DELETE FROM synthrepairform WHERE status = '" + Status.Declined.toString() + "' OR archived = TRUE");
            query.executeUpdate("COMMIT");

        } catch (SQLException e) {
            try {
                query.executeUpdate("ROLLBACK");
                query.close();
            } catch (SQLException r) {
                r.printStackTrace();
                monitor.setMessage("Failed to clean the Client Server");
                leave(false);
                return false;
            }
            monitor.setMessage("Failed to clean the Client Server");
            e.printStackTrace();
            leave(false);
            return false;

        } finally {
            try {
                query.close();
            } catch (SQLException e) {
                e.printStackTrace();
                monitor.setMessage("Failed to clean the Client Server");
                leave(false);
                return false;
            }
        }

        monitor.setMessage("Cleaned the Client Server");
        leave(true);
        return true;
    }

    public synchronized RepairListItem[] downloadRepairList(Monitor monitor) {
        enter();
        monitor.setMessage("Retreiving the Repair List");

        Statement query = null;
        ResultSet result;
        RepairListItem list[];

        try {
            query = server.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            result = query.executeQuery("SELECT COUNT(*) AS cnt FROM repairlist");

            if (!result.next()) {
                monitor.setMessage("Failed to retrieve the Repair List");
                leave(0);
                return new RepairListItem[0];
            }

            list = new RepairListItem[result.getInt("cnt")];

            result = query.executeQuery("SELECT * FROM repairlist");
            for (int i = 0; i < list.length; i++) {
                if (!result.next()) {
                    monitor.setMessage("Failed to retrieve the Repair List");
                    leave(0);
                    return new RepairListItem[0];
                }

                list[i] = new RepairListItem(result.getString("manufacturer"), result.getString("model"), result.getInt("repairs"));
            }

        } catch (SQLException e) {
            try {
                query.close();
            } catch (SQLException r) {
                r.printStackTrace();
                monitor.setMessage("Failed to retrieve the Repair List");
                leave(0);
                return new RepairListItem[0];
            }
            e.printStackTrace();
            monitor.setMessage("Failed to retrieve the Repair List");
            leave(0);
            return new RepairListItem[0];

        } finally {
            try {
                query.close();
            } catch (SQLException e) {
                e.printStackTrace();
                monitor.setMessage("Failed to retrieve the Repair List");
                leave(0);
                return new RepairListItem[0];
            }
        }

        leave(list.length);
        return list;
    }

    public synchronized boolean updateRepairList(RepairListItem oldItem, RepairListItem newItem, Monitor monitor) {
        enter(oldItem, newItem);
        monitor.setMessage("Updating the Repair List");

        Statement query = null;
        PreparedStatement statement = null;

        try {
            query = server.createStatement();
            query.executeUpdate("BEGIN");

            statement = server.prepareStatement("DELETE FROM repairlist WHERE manufacturer = ? AND model = ?");
            statement.setString(1, oldItem.getManufacturer());
            statement.setString(2, oldItem.getModel());
            statement.executeUpdate();
            statement.close();

            statement = server.prepareStatement("INSERT INTO repairlist VALUES (?, ?, ?) "
                                           + "ON DUPLICATE KEY UPDATE repairs = repairs + 1");
            statement.setString(1, newItem.getManufacturer());
            statement.setString(2, newItem.getModel());
            statement.setInt(3, newItem.getRepairs());
            statement.executeUpdate();
            statement.close();

            query.executeUpdate("COMMIT");

        } catch (SQLException e) {
            try {
                query.executeUpdate("ROLLBACK");
                query.close();
            } catch (SQLException r) {
                r.printStackTrace();
                monitor.setMessage("Failed to update the Repair List");
                leave(false);
                return false;
            }
            e.printStackTrace();
            monitor.setMessage("Failed to update the Repair List");
            leave(false);
            return false;

        } finally {
            try {
                query.close();
            } catch (SQLException e) {
                e.printStackTrace();
                monitor.setMessage("Failed to update the Repair List");
                leave(false);
                return false;
            }
        }

        monitor.setMessage("Updated the Repair List");
        leave(true);
        return true;
    }

    private class DatabaseConnector extends Thread {
        private boolean timedOut;
        private Connection connection;
        private final Object owner;

        public DatabaseConnector(Object owner) {
            this.owner = owner;
            connection = null;
            timedOut = false;
        }

        public void run() {
            try {
                Connection c = DriverManager.getConnection(setting.getURL(), setting.getUsername(), setting.getPassword());
                setConnection(c);

                if (isTimedOut()) {
                    c.close();
                    return;
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        private synchronized boolean isTimedOut() {
            return timedOut;
        }

        private synchronized void setConnection(Connection connection) {
            this.connection = connection;

            synchronized (owner) {
                owner.notify();
            }
        }

        public synchronized Connection getConnection() {
            if (connection == null)
                timedOut = true;

            return connection;
        }

        public synchronized boolean haveConnection() {
            return connection != null;
        }
    }
}
