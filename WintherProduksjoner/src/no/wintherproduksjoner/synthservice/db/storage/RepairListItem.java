package no.wintherproduksjoner.synthservice.db.storage;

public class RepairListItem {

    private String manufacturer;
    private String model;
    private int repairs;

    public RepairListItem(String manufacturer, String model, int repairs) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.repairs = repairs;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setRepairs(int repairs) {
        this.repairs = repairs;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public int getRepairs() {
        return repairs;
    }
}
