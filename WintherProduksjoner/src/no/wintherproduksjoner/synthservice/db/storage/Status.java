package no.wintherproduksjoner.synthservice.db.storage;

public enum Status {
    InQueue("In Queue"),
    ReadyForDelivery("Ready for Delivery"),
    ReceivedForRepair("Received for Repair"),
    RepairingTroubleshooting("Repairing: Troubleshooting"),
    RepairingWaitingForParts("Repairing: Waiting for Parts"),
    RepairingInProgress("Repairing: In Progress"),
    ReadyForPickup("Finished: Ready for Pickup"),
    ReturnedToOwner("Returned to Owner"),
    Declined("Declined"),
    Cancelled("Cancelled"),
    Archived("Archived");

    final String text;

    Status(String text) {
        this.text = text;
    }

    public String toString() {
        return text;
    }

    public boolean isArchivable() {
        return this == ReturnedToOwner;
    }

    public static Status fromString(String status) {
        if (status.equals("In Queue"))
            return InQueue;
        if (status.equals("Ready for Delivery"))
            return ReadyForDelivery;
        if (status.equals("Received for Repair"))
            return ReceivedForRepair;
        if (status.equals("Repairing: Troubleshooting"))
            return RepairingTroubleshooting;
        if (status.equals("Repairing: Waiting for Parts"))
            return RepairingWaitingForParts;
        if (status.equals("Repairing: In Progress"))
            return RepairingInProgress;
        if (status.equals("Finished: Ready for Pickup"))
            return ReadyForPickup;
        if (status.equals("Returned to Owner"))
            return ReturnedToOwner;
        if (status.equals("Declined"))
            return Declined;
        if (status.equals("Cancelled"))
            return Cancelled;
        if (status.equals("Archived"))
            return Archived;

        return null;
    }

    public int getOrderValue() {
        switch (this) {
        case ReadyForPickup:
            return 0;
        case ReadyForDelivery:
            return 1;
        case ReceivedForRepair:
            return 2;
        case RepairingTroubleshooting:
            return 3;
        case RepairingWaitingForParts:
            return 4;
        case RepairingInProgress:
            return 5;
        case InQueue:
            return 6;
        case ReturnedToOwner:
            return 7;
        default:
            return Integer.MIN_VALUE;
        }
    }

}
