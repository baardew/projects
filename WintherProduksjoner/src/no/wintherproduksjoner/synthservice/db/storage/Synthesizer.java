package no.wintherproduksjoner.synthservice.db.storage;

import no.wintherproduksjoner.synthservice.db.tool.ItemCollection;

import no.wintherproduksjoner.synthservice.db.storage.Status;
import no.wintherproduksjoner.synthservice.db.storage.Item;

import no.wintherproduksjoner.synthservice.lib.AlphanumComparator;

import java.io.Serializable;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.ArrayList;

public class Synthesizer implements Comparable<Synthesizer>, Serializable, ItemCollection {
    public static final long serialVersionUID = 284930001L;

    public enum CompareMethod{COMPARE_OWNER, COMPARE_STATUS, COMPARE_MODIFIED};

    private static CompareMethod compareMethod;

    private final int referenceNo;

    private int    queueNo;
    private Status status;
    private String modified;
    private String feedback;
    private String note;
    private String manufacturer;
    private String model;
    private String serialNo;
    private String errorDescription;

    private String firstName;
    private String lastName;
    private String firm;
    private String address;
    private int    postCode;
    private String postName;
    private int    phone;
    private String email;

    private ArrayList<Item> items;

    public Synthesizer(int    referenceNo,
                       int    queueNo,
                       String modified,
                       String firstName,
                       String lastName,
                       String firm,
                       String address,
                       int    postCode,
                       String postName,
                       int    phone,
                       String email,
                       String manufacturer,
                       String model,
                       String serialNo,
                       String errorDescription)
        {
            this.referenceNo      = referenceNo;
            this.queueNo          = queueNo;
            this.status           = Status.InQueue;
            this.modified         = modified;
            this.feedback         = new String();
            this.note             = new String();
            this.manufacturer     = manufacturer;
            this.model            = model;
            this.serialNo         = serialNo;
            this.errorDescription = errorDescription;

            this.firstName = firstName;
            this.lastName  = lastName;
            this.firm      = firm;
            this.address   = address;
            this.postCode  = postCode;
            this.postName  = postName;
            this.phone     = phone;
            this.email     = email;

            items = new ArrayList<Item>();
        }

    public static void setCompareMethod(CompareMethod method) {
        compareMethod = method;
    }

    public int compareTo(Synthesizer other) {
        switch (compareMethod) {
        case COMPARE_STATUS:
            int stat = status.getOrderValue() - other.getStatus().getOrderValue();
            if (stat == 0)
                return new AlphanumComparator().compare(createStatusString(), other.createStatusString());
            return stat;
        case COMPARE_OWNER:
            return new AlphanumComparator().compare(createOwnerString(), other.createOwnerString());
        case COMPARE_MODIFIED:
            return new AlphanumComparator().compare(createModifiedString(), other.createModifiedString());
        default:
            return 0;
        }
    }

    public void decreaseQueueNo() {
        if (queueNo > 0) {
            --queueNo;

            if (queueNo == 0)
                setStatus(Status.ReadyForDelivery);

            updateModified();
        }
    }

    public void updateModified() {
        SimpleDateFormat dateParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        modified = dateParse.format(date);
    }

    public String createOwnerString() {
        return getFirstName() + " " + getLastName() + " [" + getFirm() + "] | "
            + getModel() + " " + getManufacturer() + " [" + getSerialNo() + "]";
    }

    public String createStatusString() {
        String str = status.toString();

        if (status == Status.InQueue)
            str += " (" + getQueueNo() + ")";

        str +=  " | " + getModel() + " " + getManufacturer() + " [" + getSerialNo() + "]";

        return str;
    }

    public String createModifiedString() {
        return getModified() + " | " + getModel() + " " + getManufacturer() + " [" + getSerialNo() + "]";
    }

    public String toString() {
        return Integer.toString(referenceNo);
    }
    
    public Item[] getItems(String name) {
        throw new UnsupportedOperationException("Cannot get item from Synthesizer by name.");
    }

    public Item getItem(String name, double price, boolean ordered) {
        throw new UnsupportedOperationException("Cannot get item from Synthesizer by name and price.");
    }

    public Item getItem(int index) {
        if (index < 0 || index >= getItemCount()) {
            return null;
        }

        return items.get(index);
    }

    public int getItemCount() {
        return items.size();
    }

    public Item[] getAllItems() {
        return items.toArray(new Item[getItemCount()]);
    }

    public boolean addItem(Item i) {
        return items.add(i);
    }

    public boolean removeItem(Item i) {
        return items.remove(i);
    }

    public boolean decline() {
        if (getStatus() == Status.InQueue) {
            queueNo = 0;
            setStatus(Status.Declined);
            updateModified();
            return true;
        }

        return false;
    }

    public boolean cancel() {
        setStatus(Status.Cancelled);
        updateModified();
        return true;
    }

    public int getReferenceNo() {
        return referenceNo;
    }

    public int getQueueNo() {
        return queueNo;
    }

    public Status getStatus() {
        return status;
    }

    public String getModified() {
        return modified;
    }

    public String getFeedback() {
        return feedback;
    }

    public String getNote() {
        return note;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirm() {
        return firm;
    }

    public String getAddress() {
        return address;
    }

    public int getPostCode() {
        return postCode;
    }

    public String getPostName() {
        return postName;
    }

    public int getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
