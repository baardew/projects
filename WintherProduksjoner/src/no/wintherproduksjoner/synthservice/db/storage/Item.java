package no.wintherproduksjoner.synthservice.db.storage;

import java.io.Serializable;

public class Item implements Comparable<Item>, Serializable {
    public static final long serialVersionUID = 284930000L;

    public enum CompareMethod {COMPARE_NAME, COMPARE_COUNT, COMPARE_PRICE, COMPARE_ORDERED};

    private static CompareMethod compareMethod = CompareMethod.COMPARE_NAME;

    private String name;
    private double count;
    private double price;
    private boolean ordered;
    private boolean fromStorage;

    public Item() {
        name = new String("");
        count = 0.0;
        price = 0.0;
        ordered = false;
        fromStorage = false;
    }

    public Item(String name) {
        this.name = name;
        count = 0.0;
        price = 0.0;
        ordered = false;
    }

    public Item(String name, double count, double price) {
        this.name = name;
        this.count = count;
        this.price = price;
        ordered = false;
        fromStorage = false;
    }

    public Item(String name, double count, double price, boolean ordered, boolean fromStorage) {
        this.name = name;
        this.count = count;
        this.price = price;
        this.ordered = ordered;
        this.fromStorage = fromStorage;
    }

    public static void setCompareMethod(CompareMethod method) {
        compareMethod = method;
    }

    public int compareTo(Item other) {
        switch (compareMethod) {
        case COMPARE_NAME:
            return this.getName().compareTo(other.getName());
        case COMPARE_ORDERED:
            return compareToOrdered(other);
        case COMPARE_COUNT:
            if (this.getCount() > other.getCount())
                return 1;
            else if (this.getCount() < other.getCount())
                return -1;
            break;
        case COMPARE_PRICE:
            if (this.getPrice() > other.getPrice())
                return 1;
            else if (this.getPrice() < other.getPrice())
                return -1;
            break;
        default:
            return 0;
        }
        return 0;
    }

    private int compareToOrdered(Item other) {
        int name = this.getName().compareTo(other.getName());
        int price = 0;
        int ordered = 0;

        if (this.getPrice() > other.getPrice())
            price = 1;
        else if (this.getPrice() < other.getPrice())
            price = -1;

        if (!this.getOrdered() && other.getOrdered())
            ordered = 1;
        else if (this.getOrdered() && !other.getOrdered())
            ordered = -1;

        if (name == 0) {
            if (ordered == 0)
                return price;

            return ordered;
        } else {
            return name;
        }
    }

    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public double getCount() {
        return count;
    }

    public double getPrice() {
        return price;
    }

    public boolean getOrdered() {
        return ordered;
    }

    public boolean getFromStorage() {
        return fromStorage;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }

    public void setFromStorage(boolean fromStorage) {
        this.fromStorage = fromStorage;
    }
}
