package no.wintherproduksjoner.synthservice.db.tool;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.db.storage.Item;
import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;

import no.wintherproduksjoner.synthservice.db.tool.ItemCollection;

import no.wintherproduksjoner.synthservice.lib.SimilarStringComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ItemBroker {

    private ItemCollection collection;

    public ItemBroker(ItemCollection collection) {
        this.collection = collection;
    }

    public Item[] add(String name, double count, double price, boolean ordered, boolean fromStorage) {
        enter(name);
        Item item[] = new Item[0];

        if (!fromStorage) {
            item = new Item[1];
            item[0] = new Item(name, count, price, ordered, fromStorage);
            return item;
        }

        Item store = collection.getItem(name, price, ordered);
        if (store == null) {
            store = new Item(name, 0, price, ordered, fromStorage);
            collection.addItem(store);
        }

        double quantity = store.getCount();
        store.setCount(Math.max(quantity - count, 0));

        if (quantity >= count || quantity == 0.0 || !store.getOrdered()) {
            item = new Item[1];
            item[0] = new Item(name, count, store.getPrice(), store.getOrdered(), true);
        } else {
            item = new Item[2];
            item[0] = new Item(name, quantity, store.getPrice(), true, true);
            item[1] = new Item(name, count - quantity, store.getPrice(), false, false);
        }

        leave(item[0]);
        return item;
    }

    public boolean remove(Item item) {
        enter(item);
        if (item.getFromStorage()) {
            Item store = collection.getItem(item.getName(), item.getPrice(), item.getOrdered());

            if (store == null) {
                store = new Item(item.getName(), 0, item.getPrice(), item.getOrdered(), false);
                collection.addItem(store);
            }

            store.setCount(store.getCount() + item.getCount());
            store.setOrdered(store.getOrdered() && item.getOrdered());
            item.setCount(0);
            leave(true);
            return true;
        }

        leave(false);
        return false;
    }

    public Item[] search(String key) {
        enter(key);
        StringBuilder value = new StringBuilder();
        Item items[] = getAllItems();
        String prev = "";

        for (int i = 0; i < items.length; i++) {
            if (!items[i].getName().equals(prev)) {
                prev = items[i].getName();
                value.append(items[i].getName());
                value.append(" ");
            }
        }

        String selection[] = SimilarStringComparator.compareSimilarString(key, value.toString());

        ArrayList<Item> results = new ArrayList<Item>();
        for (String s: selection) {
            Item[] itemSet = collection.getItems(s);
            for (Item i: itemSet)
                results.add(i);
        }

        final String k = key;
        Collections.sort(results, new Comparator<Item>() {
                public int compare(Item i1, Item i2) {
                    String s1 = i1.getName().toLowerCase();
                    String s2 = i2.getName().toLowerCase();
                    if (s1.compareTo(k) < 0) {
                        if (s2.compareTo(k) < 0)
                            return s1.compareTo(s2);
                        else if (s2.compareTo(k) > 0)
                            return -1;
                        else
                            return 1;
                    } else if (s1.compareTo(k) > 0) {
                        if (s2.compareTo(k) < 0)
                            return 1;
                        else if (s2.compareTo(k) > 0)
                            return s1.compareTo(s2);
                        else
                            return 1;
                    } else {
                        if (s2.compareTo(k) < 0)
                            return -1;
                        else if (s2.compareTo(k) > 0)
                            return -1;
                        else
                            return 0;
                    }
                }

            });
        items = new Item[results.size()];
        leave(items.length);
        return results.toArray(items);
    }

    public Item[] getAllItems() {
        enter();
        leave(collection.getAllItems().length);
        return collection.getAllItems();
    }
}
