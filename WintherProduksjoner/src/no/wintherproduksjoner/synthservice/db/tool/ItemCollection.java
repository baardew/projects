package no.wintherproduksjoner.synthservice.db.tool;

import no.wintherproduksjoner.synthservice.db.storage.Item;

public interface ItemCollection {
    public boolean addItem(Item i);
    public boolean removeItem(Item i);
    public Item getItem(String name, double price, boolean ordered);
    public Item[] getItems(String name);
    public Item[] getAllItems();
}