package no.wintherproduksjoner.synthservice.db.tool;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import java.io.Serializable;

import java.io.File;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.ObjectInputStream;

import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.ObjectOutputStream;


public class AccountSetting implements Serializable {
    public static final long serialVersionUID = 284930002L;

    private final String filename = "account_setting.wpdb";

    private String password;
    private String username;
    private String database;
    private String serverAddress;
    private String port;

    public AccountSetting() {
        password = "";
        username = "";
        database = "";
        serverAddress = "";
        port = "";
    }

    public void setAccount(String password, String username, String database, String serverAddress, String port) {
        enter();
        this.password = password;
        this.username = username;
        this.database = database;
        this.serverAddress = serverAddress;
        this.port = port;
        leave();
    }

    public boolean read() {
        enter();
        try {
            File f = new File(filename);
            if (!f.exists())
                return false;

            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
            AccountSetting readData = (AccountSetting)in.readObject();
            this.setAccount(readData.getPassword(),
                            readData.getUsername(),
                            readData.getDatabase(),
                            readData.getServerAddress(),
                            readData.getPort());
        } catch (Exception e) {
            e.printStackTrace();
            leave(false);
            return false;
        }

        leave(true);
        return true;
    }

    public boolean save() {
        enter();
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
            out.writeObject(this);
        } catch(Exception e) {
            e.printStackTrace();
            leave(false);
            return false;
        }
        leave(true);
        return true;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getDatabase() {
        return database;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public String getPort() {
        return port;
    }

    public String getURL() {
        return "jdbc:mysql://" + serverAddress + ":" + port + "/" + database;
    }
}
