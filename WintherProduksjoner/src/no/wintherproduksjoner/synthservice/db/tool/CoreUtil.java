package no.wintherproduksjoner.synthservice.db.tool;

import java.text.SimpleDateFormat;
import java.text.ParseException;

import java.util.Date;

public abstract class CoreUtil {

    public static Date stringToDate(String source) {
        final SimpleDateFormat dateParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return dateParse.parse(source);
        } catch (ParseException e) {
            return null;
        }
    }

}
