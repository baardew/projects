package no.wintherproduksjoner.synthservice.db.tool;

public interface Monitor {
    public void setMessage(String message);
    public void start();
    public void stop();
}
