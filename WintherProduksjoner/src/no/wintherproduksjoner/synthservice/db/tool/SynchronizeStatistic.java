package no.wintherproduksjoner.synthservice.db.tool;

public class SynchronizeStatistic {
    public int news;
    public int updates;
    public int deleted;

    public SynchronizeStatistic() {
        news = updates = deleted = 0;
    }
}
