package no.wintherproduksjoner.synthservice;

import no.wintherproduksjoner.synthservice.window.tool.CrashDialog;
import no.wintherproduksjoner.synthservice.window.tool.TemplateFrame;

import no.wintherproduksjoner.synthservice.window.desktop.MainWindow;

import no.wintherproduksjoner.synthservice.db.tool.AccountSetting;

import no.wintherproduksjoner.synthservice.db.engine.FileHandle;

import no.wintherproduksjoner.synthservice.lib.systemlogging.EventQueueLogger;
import no.wintherproduksjoner.synthservice.lib.systemlogging.ProgramLogger;

import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.EventQueue;

import java.io.PrintStream;
import java.io.IOException;
import java.io.File;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class SynthService {

    private final static String version = "v. 2.3.1";

    public static void main(String[] args) {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // Do not change set, use defaults
        }

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        TemplateFrame.setScreenSize((int)screen.getWidth(), (int)screen.getHeight());

        AccountSetting setting = new AccountSetting();
        if (!setting.read())
            setting.setAccount("", "", "", "", "");

        if ((args.length > 0 && args[0].equals("-test"))) {
            TemplateFrame.setLogging(false);

        } else {
            new File(FileHandle.getHomeDirectory()).mkdir();

            FileHandle.setHomeDirectory(FileHandle.getHomeDirectory());
            ProgramLogger.initialize(FileHandle.getHomeDirectory() + "synthservice.log");

            TemplateFrame.setLogging(true);
            enableLogging(FileHandle.getHomeDirectory());
        }

        new MainWindow(setting, version);
    }

    /**
     * Maps System.out and System.err to a logging file
     *
     * @source https://blogs.oracle.com/nickstephen/entry/java_redirecting_system_out_and
     */
    public static void enableLogging(String logDirectory) {
        // Enable logging of exceptions in events
        EventQueue queue = Toolkit.getDefaultToolkit().getSystemEventQueue();
        queue.push(new EventQueueLogger());

        // Rebind stdout/stderr
        System.setOut(new PrintStream(new ProgramLogger(), true));
        System.setErr(new PrintStream(new ProgramLogger(), true));


        // Make all exceptions go to logger/screen
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread t, Throwable e) {
                    System.err.print("Exception in thread \"" + t.getName() + "\":");
                    e.printStackTrace();
                    CrashDialog.execute();
                }
            });
    }
}
