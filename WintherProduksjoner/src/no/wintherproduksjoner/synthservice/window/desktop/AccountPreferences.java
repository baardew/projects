package no.wintherproduksjoner.synthservice.window.desktop;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.window.tool.TemplateFrame;

import no.wintherproduksjoner.synthservice.db.tool.AccountSetting;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

public class AccountPreferences extends TemplateFrame {
    private final static long serialVersionUID = 184930012L;

    private JButton btn_save, btn_close;
    private JTextField txt_user, txt_password, txt_server, txt_database, txt_port;

    private AccountSetting account;

    public AccountPreferences() {
        super("Account Preferences");
        account = new AccountSetting();

        txt_user = new JTextField();
        txt_password = new JPasswordField();
        txt_database = new JTextField();
        txt_server= new JTextField();
        txt_port = new JTextField();


        if (account.read()) {
            txt_user.setText(account.getUsername());
            txt_password.setText(account.getPassword());
            txt_database.setText(account.getDatabase());
            txt_server.setText(account.getServerAddress());
            txt_port.setText(account.getPort());
        }

        setLayout(new GridLayout(0, 2));

        add(new JLabel("    Username:"));
        add(txt_user);

        add(new JLabel("    Password:"));
        add(txt_password);

        add(new JLabel("    Database:"));
        add(txt_database);

        add(new JLabel("    Server:"));
        add(txt_server);

        add(new JLabel("    Port:"));
        add(txt_port);


        add(new JLabel("    Relaunch program to use"));
        add(new JLabel("the new settings"));

        btn_save = new JButton("Save");
        btn_save.addActionListener(this);
        add(btn_save);

        btn_close = new JButton("Close");
        btn_close.addActionListener(this);
        add(btn_close);

        setPreferredSize(new Dimension(400, 200));
        setVisible();
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == btn_close) {
            dispose();
        }

        if (event.getSource() == btn_save) {
            AccountSetting newSetting = new AccountSetting();
            newSetting.setAccount(txt_password.getText(), txt_user.getText(), txt_database.getText(), txt_server.getText(), txt_port.getText());

            if (newSetting.save())
                saveNotification(btn_save);
        }
    }
}
