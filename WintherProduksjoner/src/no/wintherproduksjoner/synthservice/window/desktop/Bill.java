package no.wintherproduksjoner.synthservice.window.desktop;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.window.tool.TemplateFrame;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Item;

import java.io.FileOutputStream;
import java.io.File;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.UnsupportedEncodingException;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.text.StyleConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.JFileChooser;

import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.Font;

public class Bill extends TemplateFrame {
    private final static long serialVersionUID = 184930004L;

    private JButton btn_save, btn_cancel;
    private JTextPane text;
    private Synthesizer synthesizer;

    public Bill(Synthesizer synthesizer) {
        super("Billing for ".concat(synthesizer.createOwnerString()));
        this.synthesizer = synthesizer;

        setResizable(true);
        setPreferredSize(new Dimension(500, 400));

        btn_save = new JButton("Save");
        btn_save.addActionListener(this);
        btn_cancel = new JButton("Close");
        btn_cancel.addActionListener(this);


        text = new JTextPane();
        text.setEditable(false);
        text.setFont(new Font("Monospaced", Font.PLAIN, 12));

        SimpleAttributeSet attrb = new SimpleAttributeSet();
        StyleConstants.setAlignment(attrb, StyleConstants.ALIGN_RIGHT);
        text.setParagraphAttributes(attrb, true);

        text.setText(createBilling());

        JScrollPane panel = new JScrollPane(text);

        JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        buttons.add(btn_cancel);
        buttons.add(btn_save);


        setLayout(new BorderLayout());
        add(buttons, BorderLayout.SOUTH);
        add(panel, BorderLayout.CENTER);

        setVisible();
    }

    public void actionPerformed(ActionEvent event) {
        enter();
        if (event.getSource() == btn_cancel) {
            message("Cancel");
            dispose();
            leave();
            return;
        }

        saveExpensesToFile();
        leave();
    }

    private String createBilling() {
        enter();
        StringBuffer bill = new StringBuffer();
        double total = 0.00;

        for (Item i: synthesizer.getAllItems()) {
            bill.append(String.format("%s %s stk %s à %s\n",
                                      i.getName(),
                                      doubleToString(i.getCount()),
                                      doubleToString(i.getPrice()),
                                      doubleToString(i.getPrice() * i.getCount())
                                      ));
            total += i.getPrice() * i.getCount();
        }


        bill.append(String.format("------------------\nTOTALT: %s", doubleToString(total)));

        // Convert to norwegian comma for money
        leave();
        return bill.toString().replace('.', ',');
    }

    // Works as String.format("%7.2f", d);
    private String doubleToString(double d) {
        String whitespace;
        if (d < 10.0)
            whitespace = new String("      ");
        else if (d < 100.0)
            whitespace = new String("     ");
        else if (d < 1000.0)
            whitespace = new String("    ");
        else if (d < 10000.0)
            whitespace = new String("   ");
        else if (d < 100000.0)
            whitespace = new String("  ");
        else if (d < 1000000.0)
            whitespace = new String(" ");
        else
            whitespace = new String();

        return whitespace.concat(String.format("%.2f", d));
    }

    private void saveExpensesToFile() {
        enter();
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(System.getProperty("user.home")));

        int retval = fc.showSaveDialog(null);
        if (retval != JFileChooser.APPROVE_OPTION)
            return;

        File file = fc.getSelectedFile();

        // Create new writer
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter( new FileOutputStream( file ), "UTF-8" ) );
        } catch(UnsupportedEncodingException e) {
            System.err.println("Unsupported encoding.");
            e.printStackTrace();
            leave();
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            leave();
            return;
        }

        try {
            bw.write(createBilling());
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        leave();
    }
}
