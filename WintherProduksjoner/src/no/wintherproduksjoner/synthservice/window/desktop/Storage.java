package no.wintherproduksjoner.synthservice.window.desktop;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.window.tool.TemplateFrame;
import no.wintherproduksjoner.synthservice.window.tool.ItemTable;
import no.wintherproduksjoner.synthservice.window.tool.WindowHub;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JFrame;

public class Storage extends TemplateFrame {

    private final static long serialVersionUID = 184930008L;

    private ItemTable table;
    private JButton btn_new, btn_search, btn_reset, btn_delete, btn_clean, btn_save;

    public Storage() {
        super("Storage");

        setLayout(new BorderLayout());

        table = new ItemTable(WindowHub.getCore().getStorage());
        add(table, BorderLayout.CENTER);

        JPanel buttons = new JPanel(new GridLayout(1, 0, 10, 10));

        btn_new = new JButton("New");
        btn_new.addActionListener(this);
        buttons.add(btn_new);

        btn_search = new JButton("Search");
        btn_search.addActionListener(this);
        buttons.add(btn_search);

        btn_reset = new JButton("Reset Search");
        btn_reset.addActionListener(this);
        buttons.add(btn_reset);

        btn_delete = new JButton("Delete");
        btn_delete.addActionListener(this);
        buttons.add(btn_delete);

        btn_clean = new JButton("Clean");
        btn_clean.addActionListener(this);
        buttons.add(btn_clean);

        btn_save = new JButton("Save");
        btn_save.addActionListener(this);
        buttons.add(btn_save);

        add(buttons, BorderLayout.SOUTH);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    table.abort();
                    dispose();
                }
            });

        setVisible();
    }

    public void actionPerformed(ActionEvent event) {
        enter();
        if (event.getSource() == btn_save) {
            message("Save");
            table.commit();
            WindowHub.setChangesMade();
            saveNotification(btn_save);
            leave();
            return;
        }

        if (event.getSource() == btn_new) {
            message("New");
            newItem();
            leave();
            return;
        }

        if (event.getSource() == btn_delete) {
            message("Delete");
            table.remove(false);
            leave();
            return;
        }

        if (event.getSource() == btn_search) {
            message("Search");
            search();
            leave();
            return;
        }

        if (event.getSource() == btn_reset) {
            message("Reset");
            table.resetSearch();
            leave();
            return;
        }

        if (event.getSource() == btn_clean) {
            message("Clean");
            clean();
            leave();
            return;
        }
    }

    private void search() {
        enter();
        String itemname = JOptionPane.showInputDialog(null,
                                                      "Item Name:",
                                                      "Item search",
                                                      JOptionPane.PLAIN_MESSAGE
                                                      );

        if (itemname == null) {
            message("Itemname not defined");
            leave();
            return;
        }

        table.search(itemname);
        leave();
    }

    private void clean() {
        enter();
        start();
        setMessage("Cleaning storage");
        table.clean();
        stop();
        leave();
    }

    private void newItem() {
        enter();
        JTextField txt_name  = new JTextField();
        JTextField txt_count = new JTextField("0.0");
        JTextField txt_price = new JTextField("0.0");
        JCheckBox  box_order = new JCheckBox();
        box_order.setSelected(true);

        JPanel panel = new JPanel(new GridLayout(4, 0, 0, 0));
        panel.add(new JLabel("Name:"));    panel.add(txt_name);
        panel.add(new JLabel("Count:"));   panel.add(txt_count);
        panel.add(new JLabel("Price:"));   panel.add(txt_price);
        panel.add(new JLabel("Ordered:")); panel.add(box_order);

        Object[] options = { "Create", "Cancel" };
        int select = JOptionPane.showOptionDialog(null,
                                                  panel,
                                                  "New Expense Item",
                                                  JOptionPane.DEFAULT_OPTION,
                                                  JOptionPane.PLAIN_MESSAGE,
                                                  null,
                                                  options,
                                                  options[0]
                                                  );

        if (select != 0) {
            message("No selection");
            leave();
            return;
        }

        double count = 0.0;
        try {
            count = Double.parseDouble(txt_count.getText());
        } catch (NumberFormatException e) {}

        double price = 0.0;
        try {
            price = Double.parseDouble(txt_price.getText());
        } catch (NumberFormatException e) {}

        table.add(txt_name.getText(), count, price, box_order.isSelected(), false);
        leave();
    }
}
