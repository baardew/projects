package no.wintherproduksjoner.synthservice.window.desktop;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.lib.AlphanumComparator;

import no.wintherproduksjoner.synthservice.window.tool.TemplateFrame;
import no.wintherproduksjoner.synthservice.window.tool.WindowHub;
import no.wintherproduksjoner.synthservice.window.tool.IntegerComparator;
import no.wintherproduksjoner.synthservice.window.tool.ForcedListSelectionModel;

import no.wintherproduksjoner.synthservice.db.storage.RepairListItem;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;

import java.util.ArrayList;

import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JCheckBox;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class RepairList extends TemplateFrame {

    private final static long serialVersionUID = 184930009L;

    private enum Header {
        Manufacturer("Manufacturer", 0),
        Model("Model", 1),
        Repairs("Repairs", 2);

        private final String name;
        private final int column;

        Header(String name, int column) {
            this.name = name;
            this.column = column;
        }

        public static String columnToString(int column) {
            switch (column) {
            case 0:
                return Header.Manufacturer.toString();
            case 1:
                return Header.Model.toString();
            case 2:
                return Header.Repairs.toString();
            default:
                return null;
            }
        }

        public String toString() {
            return name;
        }

        public int column() {
            return column;
        }

        public static int length() {
            return 3; // The largest column index
        }
    }

    private JTable table;
    private JPanel tableArea;
    private JButton btn_update;
    private RepairListItem[] items;
    private RepairTableModel model;

    public RepairList() {
        super("Repair List");
        items = null;
        model = null;

        setLayout(new BorderLayout());

        JPanel controls = new JPanel(new GridLayout(0, 1, 5, 5));

        btn_update = new JButton("Update");
        btn_update.addActionListener(this);

        controls.add(btn_update);
        controls.add(createProgressFooter());
        add(controls, BorderLayout.SOUTH);

        // Create the table
        updateTable();

        setResizable(true);
        setVisible();

        new Thread() {
            public void run() {
                downloadRegister();
            }
        }.start();
    }

    public void actionPerformed(ActionEvent event) {
        enter();
        if (event.getSource() != btn_update) {
            message("Not update");
            leave();
            return;
        }

        final Monitor m = this;
        new Thread() {
            public void run() {
                model.commit(m);
                downloadRegister();
            }
        }.start();
        leave();
    }

    private int getRow() {
        int row = table.getSelectedRow();
        if (row < 0 || row >= table.getRowCount())
            return -1;

        return row = table.convertRowIndexToModel(row);
    }

    private RepairListItem get() {
        int row = getRow();
        return ((RepairTableModel)table.getModel()).getValue(row);
    }

    private void downloadRegister() {
        enter();
        items = WindowHub.getCore().downloadRepairList(this);

        if (items == null) {
            JOptionPane.showMessageDialog(null,
                                          "Could not connect to remote server",
                                          "Repair List",
                                          JOptionPane.ERROR_MESSAGE);
            message("Could not connect");
            dispose();
            leave();
            return;
        }

        updateTable();
        leave();
    }

    private void updateTable() {
        if (tableArea != null) {
            remove(table);
            remove(tableArea);
        }

        table = new JTable((model = new RepairTableModel(items)));
        table.setSelectionModel(new ForcedListSelectionModel());
        table.setAutoCreateRowSorter(true);

        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
        sorter.setComparator(0, new AlphanumComparator());
        sorter.setComparator(1, new AlphanumComparator());
        sorter.setComparator(2, new IntegerComparator());
        table.setRowSorter(sorter);
        table.getTableHeader().setReorderingAllowed(false);

        table.getRowSorter().toggleSortOrder(Header.Manufacturer.column());

        tableArea = new JPanel(new GridLayout(1, 1));
        tableArea.add(new JScrollPane(table));

        add(tableArea, BorderLayout.CENTER);
        tableArea.revalidate();
        tableArea.repaint();
    }

    private class RepairTableModel extends DefaultTableModel {
        private final static long serialVersionUID = 184930010L;

        private RepairListItem[] original;
        private RepairListItem[] working;
        private ArrayList<Integer> updates;

        public RepairTableModel(RepairListItem[] original) {
            this.original = original;

            if (original == null) {
                working = new RepairListItem[0];
            } else {
                working = new RepairListItem[original.length];
                for (int i = 0; i < original.length; i++)
                    working[i] = new RepairListItem(original[i].getManufacturer(),
                                                    original[i].getModel(),
                                                    original[i].getRepairs());
            }

            updates = new ArrayList<Integer>();
        }

        public boolean isCellEditable(int row, int column){
            return true;
        }

        public int getRowCount() {
            return working != null ? working.length : 0;
        }

        public int getColumnCount() {
            return Header.length();
        }

        public Object getValueAt(int row, int column) {
            if (column == Header.Manufacturer.column())
                return working[row].getManufacturer();
            if (column == Header.Model.column())
                return working[row].getModel();
            if (column == Header.Repairs.column())
                return working[row].getRepairs();

            return null;
        }

        public void setValueAt(Object value, int row, int column) {
            Integer i = -1;

            for (Integer u: updates) {
                if (u.intValue() == row) {
                    i = u;
                    break;
                }
            }

            if (i == -1)
                updates.add(new Integer(row));

            RepairListItem n = working[row];

            if (column == Header.Manufacturer.column())
                n.setManufacturer((String)value);
            if (column == Header.Model.column())
                n.setModel((String)value);
            if (column == Header.Repairs.column())
                n.setRepairs(((Integer)value).intValue());
        }


        public String getColumnName(int column) {
            return Header.columnToString(column);
        }

        public RepairListItem getValue(int row) {
            return working[row];
        }

        public void commit(Monitor monitor) {
            enter();
            RepairListItem[] olds = new RepairListItem[updates.size()];
            RepairListItem[] news = new RepairListItem[updates.size()];

            for (int i = 0; i < updates.size(); i++) {
                olds[i] = original[updates.get(i)];
                news[i] = working[updates.get(i)];
            }

            WindowHub.getCore().updateRepairList(olds, news, monitor);
            leave();
        }
    }
}
