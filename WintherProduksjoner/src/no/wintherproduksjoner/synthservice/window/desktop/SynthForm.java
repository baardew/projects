package no.wintherproduksjoner.synthservice.window.desktop;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.window.tool.TemplateFrame;
import no.wintherproduksjoner.synthservice.window.tool.ItemTable;
import no.wintherproduksjoner.synthservice.window.tool.WindowHub;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Status;
import no.wintherproduksjoner.synthservice.db.storage.Item;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

public class SynthForm extends TemplateFrame {

    public static final long serialVersionUID = 184930005L;

    private Synthesizer synthesizer;

    private Information info;
    private JTextPane errorDescription, notes, feedback;
    private ItemTable items;
    private JButton btn_accept, btn_decline, btn_cancel, btn_save, btn_new, btn_del;
    private JPanel buttons;

    public SynthForm(Synthesizer synthesizer) {
        super(synthesizer.createOwnerString());
        this.synthesizer = synthesizer;

        JPanel leftSide = new JPanel(new BorderLayout(10, 10));
        leftSide.setBorder(new EmptyBorder(10, 10, 10, 10));
        info = new Information(synthesizer);
        leftSide.add(info, BorderLayout.NORTH);

        buttons = new JPanel(new GridLayout(1, 0, 10, 10));

        if (synthesizer.getStatus() != Status.Archived
            && synthesizer.getStatus() != Status.Declined
            && synthesizer.getStatus() != Status.Cancelled) {
            btn_save = new JButton("Save");
            btn_save.addActionListener(this);
            buttons.add(btn_save);

            if (synthesizer.getStatus() == Status.InQueue) {
                btn_decline = new JButton("Decline");
                btn_decline.setForeground(Color.RED);
                btn_decline.addActionListener(this);
                buttons.add(btn_decline);
            } else {
                btn_cancel = new JButton("Cancel");
                btn_cancel.setForeground(Color.RED);
                btn_cancel.addActionListener(this);
                buttons.add(btn_cancel);
            }

            if (synthesizer.getStatus() == Status.InQueue
                && synthesizer.getQueueNo() == 1) {
                btn_accept = new JButton("Accept");
                btn_accept.setForeground(Color.GREEN.darker());
                btn_accept.addActionListener(this);
                buttons.add(btn_accept);
            }
        }

        leftSide.add(buttons, BorderLayout.SOUTH);

        JPanel rightSide = new JPanel(new GridLayout(0, 1, 15, 15));
        rightSide.setBorder(new EmptyBorder(10, 10, 10, 10));

        errorDescription = new JTextPane();
        JScrollPane scroll_errorDescription = new JScrollPane(errorDescription);
        JPanel panel_errorDescription = new JPanel(new BorderLayout());
        errorDescription.setText(synthesizer.getErrorDescription());
        errorDescription.setEditable(false);
        panel_errorDescription.add(new JLabel("Error Description:"), BorderLayout.NORTH);
        panel_errorDescription.add(scroll_errorDescription, BorderLayout.CENTER);

        leftSide.add(panel_errorDescription, BorderLayout.CENTER);

        notes = new JTextPane();
        JScrollPane scroll_notes = new JScrollPane(notes);
        JPanel panel_notes = new JPanel(new BorderLayout());
        notes.setText(synthesizer.getNote());
        panel_notes.add(new JLabel("Note:"), BorderLayout.NORTH);
        panel_notes.add(scroll_notes, BorderLayout.CENTER);
        rightSide.add(panel_notes);

        feedback = new JTextPane();
        JScrollPane scroll_feedback = new JScrollPane(feedback);
        JPanel panel_feedback = new JPanel(new BorderLayout());
        feedback.setText(synthesizer.getFeedback());
        panel_feedback.add(new JLabel("Feedback:"), BorderLayout.NORTH);
        panel_feedback.add(scroll_feedback, BorderLayout.CENTER);
        rightSide.add(panel_feedback);

        items = new ItemTable(synthesizer);
        JPanel itemControl = new JPanel(new GridLayout(1, 0));
        btn_new = new JButton("New Item");
        btn_new.addActionListener(this);
        itemControl.add(btn_new);
        btn_del = new JButton("Delete Item");
        btn_del.addActionListener(this);
        itemControl.add(btn_del);
        JPanel panel_items = new JPanel(new BorderLayout());
        panel_items.add(new JLabel("Expenses:"), BorderLayout.NORTH);
        panel_items.add(items, BorderLayout.CENTER);
        panel_items.add(itemControl, BorderLayout.SOUTH);

        rightSide.add(panel_items);

        setLayout(new GridLayout(1, 2, 10, 10));

        add(leftSide);
        add(rightSide);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    items.abort();
                    dispose();
                }
            });

        setPreferredSize(new Dimension(850, 650));
        setVisible();
    }

    public void actionPerformed(ActionEvent event) {
        enter();
        if (event.getSource() == btn_save) {
            message("Save");
            if (feedback.getText().length() >= 500) {
                JOptionPane.showMessageDialog(null,
                                              "Feedback text too long.",
                                              "Feedbacl Text Error",
                                              JOptionPane.ERROR_MESSAGE);
            }

            synthesizer.setNote(notes.getText());
            synthesizer.setFeedback(feedback.getText());

            info.save();
            items.commit();

            synthesizer.updateModified();

            WindowHub.setChangesMade();
            saveNotification(btn_save);
            leave();
            return;
        }

        if (event.getSource() == btn_new) {
            message("New");
            new ItemAdder(this, items);
            leave();
            return;
        }

        if (event.getSource() == btn_del) {
            message("Delete");
            int loc = JOptionPane.showOptionDialog(null,
                                                   "Put Item into Storage?",
                                                   "Delete Expense",
                                                   JOptionPane.YES_NO_CANCEL_OPTION,
                                                   JOptionPane.INFORMATION_MESSAGE,
                                                   null,
                                                   null,
                                                   null);

            if (loc == JOptionPane.CANCEL_OPTION) {
                message("Not removed");
                leave();
                return;
            }

            items.remove((loc == JOptionPane.YES_OPTION));
            leave();
            return;
        }

        if (event.getSource() == btn_accept) {
            message("Accept");
            accept();
            leave();
            return;
        }

        if (event.getSource() == btn_decline) {
            message("Decline");
            decline();
            synthesizer.updateModified();
            dispose();
            WindowHub.updateStatistics();
            leave();
            return;
        }

        if (event.getSource() == btn_cancel) {
            message("Cancel");
            cancel();
            synthesizer.updateModified();
            dispose();
            WindowHub.updateStatistics();
            leave();
            return;
        }
    }

    private void accept() {
        enter();
        int confirm = JOptionPane.showOptionDialog(null,
                                                   "Are you sure you want to Accept this synthesizer?",
                                                   "Accept",
                                                   JOptionPane.YES_NO_OPTION,
                                                   JOptionPane.QUESTION_MESSAGE,
                                                   null,
                                                   null,
                                                   null);
        if (confirm != 0) {
            message("Not confirmed");
            leave();
            return;
        }

        WindowHub.getCore().acceptSynthesizer();
        synthesizer.updateModified();
        WindowHub.setChangesMade();

        buttons.remove(btn_accept);
        buttons.remove(btn_decline);

        btn_cancel = new JButton("Cancel");
        btn_cancel.setForeground(Color.RED);
        btn_cancel.addActionListener(this);
        buttons.add(btn_cancel);

        info.enableStatus();

        buttons.revalidate();
        buttons.repaint();
        leave();
    }

    private void decline() {
        enter();
        int confirm = JOptionPane.showOptionDialog(null,
                                                   "Are you sure you want to Decline this synthesizer?",
                                                   "Decline",
                                                   JOptionPane.YES_NO_OPTION,
                                                   JOptionPane.QUESTION_MESSAGE,
                                                   null,
                                                   null,
                                                   null);
        if (confirm != 0) {
            message("Not confirmed");
            leave();
            return;
        }

        WindowHub.getCore().declineSynthesizer(synthesizer);
        WindowHub.setChangesMade();
        leave();
    }

    private void cancel() {
        enter();
        int confirm = JOptionPane.showOptionDialog(null,
                                                   "Are you sure you want to Cancel this synthesizer?",
                                                   "Decline",
                                                   JOptionPane.YES_NO_OPTION,
                                                   JOptionPane.QUESTION_MESSAGE,
                                                   null,
                                                   null,
                                                   null);
        if (confirm != 0) {
            message("Not confirmed");
            leave();
            return;
        }

        WindowHub.getCore().cancelSynthesizer(synthesizer);
        WindowHub.setChangesMade();
        leave();
    }

    private class ItemAdder extends TemplateFrame {

        public static final long serialVersionUID = 184930011L;

        private ItemTable items, storage;
        private JTextField txt_name, txt_count, txt_price, txt_quant;
        private JCheckBox box_order;
        private JButton btn_cancel_1, btn_cancel_2, btn_custom, btn_storage, btn_search, btn_clear;

        public ItemAdder(SynthForm parent, ItemTable items) {
            super("New Expense");
            this.items = items;

            btn_cancel_1  = new JButton("Cancel");
            btn_cancel_1.addActionListener(this);
            btn_cancel_2  = new JButton("Cancel");
            btn_cancel_2.addActionListener(this);
            btn_custom  = new JButton("Create");
            btn_custom.addActionListener(this);
            btn_storage = new JButton("Select");
            btn_storage.addActionListener(this);
            btn_search  = new JButton("Search");
            btn_search.addActionListener(this);
            btn_clear   = new JButton("Clear Search");
            btn_clear.addActionListener(this);

            txt_name  = new JTextField();
            txt_count = new JTextField("0.0");
            txt_price = new JTextField("0.0");
            txt_quant = new JTextField("0.0");
            box_order = new JCheckBox();
            box_order.setSelected(true);

            JPanel custom = new JPanel(new BorderLayout());
            JPanel customize = new JPanel(new GridLayout(5, 0, 0, 0));
            customize.setBorder(new EmptyBorder(20, 20, 20, 20));
            customize.add(new JLabel("Name:"));    customize.add(txt_name);
            customize.add(new JLabel("Count:"));   customize.add(txt_count);
            customize.add(new JLabel("Price:"));   customize.add(txt_price);
            customize.add(new JLabel("Ordered:")); customize.add(box_order);
            JPanel options = new JPanel(new GridLayout(1, 0));
            options.add(btn_custom);
            options.add(btn_cancel_1);
            custom.add(customize, BorderLayout.NORTH);
            custom.add(options, BorderLayout.SOUTH);

            JPanel quantity = new JPanel(new GridLayout(1, 0));
            quantity.add(new JLabel("Count:", JLabel.RIGHT));
            quantity.add(txt_quant);
            JPanel buttons = new JPanel(new GridLayout(1, 0));
            buttons.add(btn_storage);
            buttons.add(btn_search);
            buttons.add(btn_clear);
            buttons.add(btn_cancel_2);

            JPanel controls = new JPanel(new GridLayout(0, 1));
            controls.add(quantity);
            controls.add(buttons);

            storage = new ItemTable(WindowHub.getCore().getStorage());
            JPanel store = new JPanel(new BorderLayout());
            store.add(storage, BorderLayout.CENTER);
            store.add(controls, BorderLayout.SOUTH);

            JTabbedPane tabs = new JTabbedPane();
            tabs.add("From Storage", store);
            tabs.add("New", custom);

            add(tabs);
            setVisible();
        }

        public void actionPerformed(ActionEvent event) {
            enter();
            if (event.getSource() == btn_cancel_1 || event.getSource() == btn_cancel_2) {
                message("Cancel");
                dispose();
                leave();
                return;
            }

            if (event.getSource() == btn_search) {
                message("Search");
                String itemname = JOptionPane.showInputDialog(null,
                                                              "Item Name:",
                                                              "Item search",
                                                              JOptionPane.PLAIN_MESSAGE
                                                              );

                if (itemname == null) {
                    message("No itemname");
                    leave();
                    return;
                }

                storage.search(itemname);
                leave();
                return;
            }

            if (event.getSource() == btn_clear) {
                message("Clear");
                storage.resetSearch();
                leave();
                return;
            }

            if (event.getSource() == btn_storage) {
                message("Storage");
                String name = storage.getSelectedName();
                boolean ordered = storage.getSelectedOrdered();
                double price = storage.getSelectedPrice();
                double count = 0.0;
                try {
                    count = Double.parseDouble(txt_quant.getText());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if (count < 0.0)
                    count *= -1;

                items.add(name, count, price, ordered, true);

                dispose();
                leave();
                return;
            }

            if (event.getSource() == btn_custom) {
                message("Custom");
                double count = 0.0;
                try {
                    count = Double.parseDouble(txt_count.getText());
                } catch (NumberFormatException e) {}

                double price = 0.0;
                try {
                    price = Double.parseDouble(txt_price.getText());
                } catch (NumberFormatException e) {}

                items.add(txt_name.getText(), count, price, box_order.isSelected(), false);

                dispose();
                leave();
                return;
            }

        }

    }

    private class Information extends JPanel {
        private final static long serialVersionUID = 184930006L;

        private JTextField txt_firstname, txt_lastname, txt_firm, txt_address,
            txt_postcode, txt_postname, txt_phone, txt_mail, txt_manufacturer, txt_model, txt_serialno;

        private Synthesizer synth;
        private JLabel lbl_status;
        private JPanel fields;
        private JComboBox box_status;

        public Information(Synthesizer synth) {
            this.synth = synth;
            box_status = null;

            boolean enableChanges = synth.getStatus() != Status.InQueue
                && synth.getStatus() != Status.Declined
                && synth.getStatus() != Status.Archived;

            txt_firstname    = new JTextField(synth.getFirstName());
            txt_lastname     = new JTextField(synth.getLastName());
            txt_firm         = new JTextField(synth.getFirm());
            txt_address      = new JTextField(synth.getAddress());
            txt_postcode     = new JTextField(synth.getPostCode() != -1 ? String.format("%04d", synth.getPostCode()) : "");
            txt_postname     = new JTextField(synth.getPostName());
            txt_phone        = new JTextField(synth.getPhone() != -1 ? String.format("%8d", synth.getPhone()) : "");
            txt_mail         = new JTextField(synth.getEmail());
            txt_manufacturer = new JTextField(synth.getManufacturer());
            txt_model        = new JTextField(synth.getModel());
            txt_serialno     = new JTextField(synth.getSerialNo());
            JTextField REFNO = new JTextField(String.format("%d", synth.getReferenceNo()));
            REFNO.setEditable(false);

            JPanel labels = new JPanel(new GridLayout(0, 1, 5, 5));
            fields = new JPanel(new GridLayout(0, 1, 5, 5));
            labels.add(new JLabel("          ReferenceNo:  ", JLabel.LEFT));
            fields.add(REFNO);

            labels.add(new JLabel());
            fields.add(new JLabel());

            labels.add(new JLabel("          Status:  "), JLabel.LEFT);
            if (enableChanges) {
                fields.add((box_status = createStatusField()));
            } else {
                if (synth.getStatus() == Status.InQueue) {
                    lbl_status = new JLabel("Queue No: " + synth.getQueueNo());
                    fields.add(lbl_status);
                } else {
                    lbl_status = new JLabel(synth.getStatus().toString());
                    fields.add(lbl_status);
                }
            }
            labels.add(new JLabel("          Manufacturer:  ", JLabel.LEFT));
            fields.add(txt_manufacturer);
            labels.add(new JLabel("          Model:  ", JLabel.LEFT));
            fields.add(txt_model);
            labels.add(new JLabel("          Serial No.:  ", JLabel.LEFT));
            fields.add(txt_serialno);

            labels.add(new JLabel());
            fields.add(new JLabel());

            labels.add(new JLabel("          First Name:  ", JLabel.LEFT));
            fields.add(txt_firstname);
            labels.add(new JLabel("          Last Name:  ", JLabel.LEFT));
            fields.add(txt_lastname);
            labels.add(new JLabel("          Firm:  ", JLabel.LEFT));
            fields.add(txt_firm);
            labels.add(new JLabel("          Address:  ", JLabel.LEFT));
            fields.add(txt_address);
            labels.add(new JLabel("          PostCode:  ", JLabel.LEFT));
            fields.add(txt_postcode);
            labels.add(new JLabel("          PostName:  ", JLabel.LEFT));
            fields.add(txt_postname);
            labels.add(new JLabel("          Phone:  ", JLabel.LEFT));
            fields.add(txt_phone);
            labels.add(new JLabel("          E-mail:  ", JLabel.LEFT));
            fields.add(txt_mail);

            setLayout(new BorderLayout());
            add(labels, BorderLayout.WEST);
            add(fields, BorderLayout.CENTER);
            setPreferredSize(new Dimension(400, 440));
        }

        public void enableStatus() {
            enter();
            fields.remove(lbl_status);
            fields.add((box_status = createStatusField()), 2);

            fields.revalidate();
            fields.repaint();
            leave();
        }

        public void save() {
            enter();
            synth.setFirstName(txt_firstname.getText());
            synth.setLastName(txt_lastname.getText());
            synth.setFirm(txt_firm.getText());
            synth.setAddress(txt_address.getText());
            synth.setPostName(txt_postname.getText());
            synth.setEmail(txt_mail.getText());
            synth.setManufacturer(txt_manufacturer.getText());
            synth.setModel(txt_model.getText());
            synth.setSerialNo(txt_serialno.getText());

            if (box_status != null) {
                String stat = (String)box_status.getSelectedItem();

                if (stat != null && stat.length() != 0)
                    synthesizer.setStatus(Status.fromString(stat));
            }

            String indata = txt_postcode.getText();
            if (indata.length() > 0) {
                int val = -1;
                try {
                    val = Integer.parseInt(indata);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null,
                                                  "Invalid Post Code number.",
                                                  "Contact Information Error",
                                                  JOptionPane.ERROR_MESSAGE);
                    val = -1;
                } finally {
                    synth.setPostCode(val);
                }
            }

            indata = txt_phone.getText();
            if (indata.length() > 0) {
                int val = -1;
                try {
                    val = Integer.parseInt(indata);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null,
                                                  "Invalid Phone number.",
                                                  "Contact Information Error",
                                                  JOptionPane.ERROR_MESSAGE);
                    val = -1;
                } finally {
                    synth.setPhone(val);
                }
            }

            WindowHub.updateStatistics();
            leave();
        }

        private JComboBox createStatusField() {
            String[] statuses = {
                Status.ReadyForDelivery.toString(),
                Status.ReceivedForRepair.toString(),
                Status.RepairingTroubleshooting.toString(),
                Status.RepairingWaitingForParts.toString(),
                Status.RepairingInProgress.toString(),
                Status.ReadyForPickup.toString(),
                Status.ReturnedToOwner.toString(),
                Status.Cancelled.toString(),
            };

            int idx;
            String selected = synth.getStatus().toString();
            for (idx = 0; idx < statuses.length; idx++)
                if (selected.equals(statuses[idx]))
                    break;

            if (idx >= statuses.length)
                throw new RuntimeException("Could not find status " + selected + " in statuses list. Index " + idx + " out of bounds");

            JComboBox status = new JComboBox(statuses);
            status.setSelectedIndex(idx);

            return status;
        }
    }
}
