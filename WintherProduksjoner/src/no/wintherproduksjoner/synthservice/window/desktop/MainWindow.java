package no.wintherproduksjoner.synthservice.window.desktop;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.window.desktop.Register;
import no.wintherproduksjoner.synthservice.window.desktop.Storage;
import no.wintherproduksjoner.synthservice.window.desktop.RepairList;
import no.wintherproduksjoner.synthservice.window.desktop.AccountPreferences;

import no.wintherproduksjoner.synthservice.window.tool.TemplateFrame;
import no.wintherproduksjoner.synthservice.window.tool.WindowHub;
import no.wintherproduksjoner.synthservice.window.tool.InformationReader;

import no.wintherproduksjoner.synthservice.db.engine.Core;
import no.wintherproduksjoner.synthservice.db.tool.AccountSetting;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

public class MainWindow extends TemplateFrame {

    private final static long serialVersionUID = 184930000L;

    private JButton btn_register, btn_storage, btn_archive, btn_version, btn_synch, btn_save, btn_repair, btn_clean, btn_quit, btn_account;
    private String version;

    public MainWindow(AccountSetting setting, String version) {
        super("Synth Service");
        this.version = version;

        setPreferredSize(new Dimension(600, 650));

        loadScreen();
        setVisible();

        Core core = new Core(this, setting);

        remove(progressLabel);
        remove(progressBar);

        new WindowHub(core);

        initialize();
        updateNotification();
    }

    public void actionPerformed(ActionEvent event) {
        enter();
        final Monitor m = this;
        
        if (event.getSource() == btn_save) {
            message("Save");
            new Thread() {
                public void run() {
                    if (WindowHub.save(m))
                        WindowHub.clearChangesMade();
                }
            }.start();
            leave();
            return;
        }

        if (event.getSource() == btn_synch) {
            message("Sync");
            new Thread() {
                public void run() {
                    if (!WindowHub.save(m))
                        return;

                    WindowHub.clearChangesMade();
                    WindowHub.getCore().synchronize(m);
                    WindowHub.updateStatistics();
                }
            }.start();
            leave();
            return;
        }

        if (event.getSource() == btn_register) {
            message("Register");
            new Register(Register.Mode.Register);
            leave();
            return;
        }

        if (event.getSource() == btn_storage) {
            enter("Storage");
            new Storage();
            leave();
            return;
        }

        if (event.getSource() == btn_archive) {
            message("Archive");
            new Register(Register.Mode.Archive);
            leave();
            return;
        }

        if (event.getSource() == btn_repair) {
            message("Repair");
            new RepairList();
            leave();
            return;
        }

        if (event.getSource() == btn_quit) {
            message("Quit");
            if (WindowHub.hasChangesMade()) {
                int confirm = JOptionPane.showOptionDialog(null,
                                                           "Are You sure You want to exit?\nAny changes will be lost.",
                                                           "Exit Confirmation",
                                                           JOptionPane.YES_NO_OPTION,
                                                           JOptionPane.WARNING_MESSAGE,
                                                           null,
                                                           null,
                                                           null);
                if (confirm != 0) {
                    message("Not confirmed");
                    leave();
                    return;
                }
            }

            System.exit(0);
            leave();
        }

        if (event.getSource() == btn_clean) {
            message("Clean");
            new Thread() {
                public void run() {
                    WindowHub.getCore().systemClean(m);
                }
            }.start();
            leave();
            return;
        }

        if (event.getSource() == btn_account) {
            message("Account");
            new AccountPreferences();
            leave();
            return;
        }

        if (event.getSource() == btn_version) {
            message("Version");
            displayVersion();
            leave();
            return;
        }
    }

    private void loadScreen() {
        GridBagConstraints constraint = new GridBagConstraints();
        constraint.fill = GridBagConstraints.HORIZONTAL;
        constraint.anchor = GridBagConstraints.FIRST_LINE_START;
        constraint.ipady = 10;
        constraint.ipadx = 300;
        constraint.gridy = 0;

        setLayout(new GridBagLayout());

        add(progressBar, constraint);

        constraint.gridy = 1;
        add(progressLabel, constraint);
    }

    private void initialize() {
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    if (WindowHub.hasChangesMade()) {
                        int confirm = JOptionPane.showOptionDialog(null,
                                                                   "Are You sure You want to exit?\nAny changes will be lost.",
                                                                   "Exit Confirmation",
                                                                   JOptionPane.YES_NO_OPTION,
                                                                   JOptionPane.WARNING_MESSAGE,
                                                                   null,
                                                                   null,
                                                                   null);
                        if (confirm != 0)
                            return;
                    }

                    System.exit(0);
                }
            });

        setLayout(new BorderLayout());

        JPanel statistics = new JPanel(new GridLayout(0, 1, 5, 5));
        statistics.add(WindowHub.getStatisticsPanel());

        JPanel buttons = new JPanel(new GridLayout(0, 1, 5, 5));

        btn_register = new JButton("Register");
        btn_register.addActionListener(this);
        buttons.add(btn_register);

        btn_storage = new JButton("Storage");
        btn_storage.addActionListener(this);
        buttons.add(btn_storage);

        btn_archive = new JButton("Archive");
        btn_archive.addActionListener(this);
        buttons.add(btn_archive);

        btn_repair = new JButton("Repaired List");
        btn_repair.addActionListener(this);
        buttons.add(btn_repair);

        btn_save = new JButton("Save");
        btn_save.addActionListener(this);
        buttons.add(btn_save);

        btn_synch = new JButton("<html>Save &amp;<br />Synchronize</html>"); // Save &\nSynchronize
        btn_synch.addActionListener(this);
        buttons.add(btn_synch);

        btn_clean = new JButton("System Clean");
        btn_clean.addActionListener(this);
        buttons.add(btn_clean);

        btn_account = new JButton("Preferences");
        btn_account.addActionListener(this);
        buttons.add(btn_account);

        btn_version = new JButton("About");
        btn_version.addActionListener(this);
        buttons.add(btn_version);

        btn_quit = new JButton("Quit");
        btn_quit.addActionListener(this);
        buttons.add(btn_quit);

        JPanel center = new JPanel(new BorderLayout(10, 10));
        center.setBorder(new EmptyBorder(10, 20, 10, 20));
        center.add(buttons, BorderLayout.CENTER);
        center.add(statistics, BorderLayout.EAST);

        add(center, BorderLayout.CENTER);
        add(createProgressFooter(), BorderLayout.SOUTH);
        center.revalidate();
        center.repaint();
    }

    private void updateNotification() {
        InformationReader file = new InformationReader("VersionUpdate");
        if (!file.exists())
            return;

        String text = file.readFile();
        if (text == null)
            return;

        JOptionPane.showMessageDialog(null,
                                      text,
                                      "Version Update",
                                      JOptionPane.PLAIN_MESSAGE
                                      );

        file.delete();
    }

    private void displayVersion() {
        JPanel textarea = new JPanel(new GridLayout(0, 1, 5, 5));

        textarea.add(new JLabel("WintherProduksjoner Synth Service", JLabel.CENTER));
        textarea.add(new JLabel(version, JLabel.CENTER));
        textarea.add(new JLabel(" "));
        textarea.add(new JLabel("Developer: Bård Eirik Winther"));
        textarea.add(new JLabel("GUI Designer: Aksel Ludvigsen"));

        JOptionPane.showMessageDialog(null,
                                      textarea,
                                      "Version",
                                      JOptionPane.INFORMATION_MESSAGE
                                      );
    }
}
