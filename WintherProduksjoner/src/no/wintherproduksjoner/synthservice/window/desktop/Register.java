package no.wintherproduksjoner.synthservice.window.desktop;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer.*;

import no.wintherproduksjoner.synthservice.lib.AlphanumComparator;

import no.wintherproduksjoner.synthservice.window.desktop.Bill;
import no.wintherproduksjoner.synthservice.window.desktop.SynthForm;

import no.wintherproduksjoner.synthservice.window.tool.TemplateFrame;
import no.wintherproduksjoner.synthservice.window.tool.StatusComparator;
import no.wintherproduksjoner.synthservice.window.tool.DateComparator;
import no.wintherproduksjoner.synthservice.window.tool.WindowHub;
import no.wintherproduksjoner.synthservice.window.tool.ForcedListSelectionModel;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Status;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;

import java.util.ArrayList;

import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JScrollPane;
import javax.swing.JCheckBox;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class Register extends TemplateFrame implements ListSelectionListener {

    private final static long serialVersionUID = 184930001L;

    private enum Header {
        Model("Model", 0),
        Manufacturer("Manufacturer", 1),
        SerialNo("SerialNo", 2),
        Status("Status", 3),
        Owner("Owner", 4),
        Modified("Last Modified", 5);

        private final String name;
        private final int column;

        Header(String name, int column) {
            this.name = name;
            this.column = column;
        }

        public static String columnToString(int column) {
            switch (column) {
            case 0:
                return Header.Model.toString();
            case 1:
                return Header.Manufacturer.toString();
            case 2:
                return Header.SerialNo.toString();
            case 3:
                return Header.Status.toString();
            case 4:
                return Header.Owner.toString();
            case 5:
                return Header.Modified.toString();
            default:
                return null;
            }
        }

        public String toString() {
            return name;
        }

        public int column() {
            return column;
        }

        public static int length() {
            return 6; // The largest column index
        }
    }

    public enum Mode{ Register, Archive };
    private Mode mode;

    private int archive_idx;
    private final int ARCHIVE_STEP = 50;

    private JTable table;
    private JPanel tableArea;
    private JButton btn_open, btn_archive, btn_bill, btn_next, btn_prev;
    private JCheckBox box_work, box_queue, box_deliver, box_decline, box_pick, box_owner;

    public Register(Mode mode) {
        super("Register");
        this.mode = mode;
        archive_idx = 0;

        setLayout(new BorderLayout());

        JPanel buttons = new JPanel(new GridLayout(1, 3));
        btn_open = new JButton("Open");
        btn_open.addActionListener(this);
        btn_open.setEnabled(false);
        buttons.add(btn_open);

        if (mode == Mode.Register) {
            btn_archive = new JButton("Put in Archive");
            btn_archive.addActionListener(this);
            btn_archive.setEnabled(false);
            btn_bill = new JButton("Create Bill");
            btn_bill.addActionListener(this);
            btn_bill.setEnabled(false);

            buttons.add(btn_archive);
            buttons.add(btn_bill);
        } else if (mode == Mode.Archive) {
            btn_next = new JButton("Next");
            btn_next.addActionListener(this);
            btn_prev = new JButton("Previous");
            btn_prev.addActionListener(this);
            btn_prev.setEnabled(false);

            if (ARCHIVE_STEP >= WindowHub.getCore().getArchiveSize(this))
                btn_next.setEnabled(false);

            buttons.add(btn_prev);
            buttons.add(btn_next);
        }

        JPanel controls = new JPanel(new GridLayout(1, 3));

        if (mode == Mode.Register) {
            JPanel filterProgress = new JPanel(new BorderLayout());
            JPanel filters = new JPanel(new GridLayout(3, 3));
            filters.add(new JLabel("     Show only:"));
            filters.add(new JLabel(""));
            filters.add(new JLabel(""));
            box_work = new JCheckBox("Work in Progress", true);
            box_work.addActionListener(this);
            filters.add(box_work);
            box_deliver = new JCheckBox("Ready for Delivery");
            box_deliver.addActionListener(this);
            filters.add(box_deliver);
            box_pick = new JCheckBox("Ready for Pickup");
            box_pick.addActionListener(this);
            filters.add(box_pick);
            box_queue = new JCheckBox("In Queue");
            box_queue.addActionListener(this);
            filters.add(box_queue);
            box_decline = new JCheckBox("Declined/Cancelled");
            box_decline.addActionListener(this);
            filters.add(box_decline);
            box_owner = new JCheckBox("Returned to Owner");
            box_owner.addActionListener(this);
            filters.add(box_owner);

            filterProgress.add(filters, BorderLayout.CENTER);
            filterProgress.add(createProgressFooter(), BorderLayout.SOUTH);
            controls.add(filterProgress);

        } else if (mode == Mode.Archive) {
            controls.add(createProgressFooter());
        }

        controls.add(buttons);
        add(controls, BorderLayout.SOUTH);

        // Create the table
        updateTable();

        setPreferredSize(new Dimension(1000, 650));
        setResizable(true);
        setVisible();
    }

    public void actionPerformed(ActionEvent event) {
        enter();
        if (event.getSource() == btn_open) {
            message("Open");
            Synthesizer s = get();
            dispose();
            new SynthForm(s);
            leave();
            return;
        }

        if (event.getSource() == btn_bill) {
            message("Bill");
            Synthesizer s = get();
            dispose();
            new Bill(s);
            leave();
            return;
        }

        if (event.getSource() == box_work
            || event.getSource() == box_queue
            || event.getSource() == box_deliver
            || event.getSource() == box_decline
            || event.getSource() == box_pick
            || event.getSource() == box_owner) {
            message("Filter changed");
            btn_bill.setEnabled(false);
            btn_archive.setEnabled(false);
            btn_open.setEnabled(false);
            updateTable();
            leave();
            return;
        }

        if (event.getSource() == btn_archive) {
            message("Archive");
            int confirm = JOptionPane.showOptionDialog(null,
                                                       "Are you sure you want to archive?\nA save will be performed.",
                                                       "Save Confirmation",
                                                       JOptionPane.YES_NO_OPTION,
                                                       JOptionPane.WARNING_MESSAGE,
                                                       null,
                                                       null,
                                                       null);
            if (confirm != 0) {
                message("Not confirmed");
                leave();
                return;
            }

            table.setVisible(false);
            table.setEnabled(false);
            btn_open.setEnabled(false);
            btn_archive.setEnabled(false);

            new Thread() {
                public void run() {
                    archive();
                }
            }.start();
            leave();
            return;
        }

        if (event.getSource() == btn_next) {
            message("Next");
            archive_idx += ARCHIVE_STEP;
            btn_prev.setEnabled(true);

            if ((archive_idx + ARCHIVE_STEP) >= WindowHub.getCore().getArchiveSize(this))
                btn_next.setEnabled(false);

            new Thread() {
                public void run() {
                    updateTable();
                }
            }.start();
            leave();
            return;
        }

        if (event.getSource() == btn_prev) {
            message("Prev");
            archive_idx = Math.max(archive_idx - ARCHIVE_STEP, 0);

            if (archive_idx == 0)
                btn_prev.setEnabled(false);

            btn_next.setEnabled(true);

            new Thread() {
                public void run() {
                    updateTable();
                }
            }.start();
            leave();
            return;
        }
    }

    public void valueChanged(ListSelectionEvent e) {
        btn_open.setEnabled(true);

        if (mode == Mode.Register) {
            Status s = get().getStatus();
            btn_bill.setEnabled(s == Status.ReadyForPickup);
            btn_archive.setEnabled(s == Status.ReturnedToOwner);
        }
    }

    private void archive() {
        enter();
        start();

        // Archive
        if (!WindowHub.getCore().writeToArchive(this, get())) {
            JOptionPane.showMessageDialog(null,
                                          "Failed to write to archive.",
                                          "Archive Failure",
                                          JOptionPane.ERROR_MESSAGE);
            message("Failed to archive");
            leave();
            return;
        }

        if (!WindowHub.save(this)) {
            JOptionPane.showMessageDialog(null,
                                          "Failed to save register.",
                                          "Archive Failure",
                                          JOptionPane.ERROR_MESSAGE);
            message("Failed to save regiser");
            leave();
            return;
        }

        JOptionPane.showMessageDialog(null,
                                      "Synthesizer added to archive.",
                                      "Archive Success",
                                      JOptionPane.INFORMATION_MESSAGE);

        WindowHub.clearChangesMade();
        stop();
        leave();
    }

    private int getRow() {
        int row = table.getSelectedRow();
        if (row < 0 || row >= table.getRowCount())
            return -1;

        return row = table.convertRowIndexToModel(row);
    }

    private Synthesizer get() {
        int row = getRow();
        return ((RegisterTableModel)table.getModel()).getValue(row);
    }

    private void updateTable() {
        if (tableArea != null) {
            remove(table);
            remove(tableArea);
        }

        if (mode == Mode.Register)
            table = new JTable(new RegisterTableModel(createFilteredRegister()));
        else if (mode == Mode.Archive)
            table = new JTable(new RegisterTableModel(readFromArchive()));
        table.setSelectionModel(new ForcedListSelectionModel());
        table.setAutoCreateRowSorter(true);
        table.getSelectionModel().addListSelectionListener(this);

        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
        sorter.setComparator(0, new AlphanumComparator());
        sorter.setComparator(1, new AlphanumComparator());
        sorter.setComparator(2, new AlphanumComparator());
        sorter.setComparator(3, new StatusComparator());
        sorter.setComparator(4, new AlphanumComparator());
        sorter.setComparator(5, new DateComparator());
        table.setRowSorter(sorter);
        table.getTableHeader().setReorderingAllowed(false);

        if (mode == Mode.Register)
            table.getRowSorter().toggleSortOrder(Header.Status.column());
        else if (mode == Mode.Archive)
            table.getRowSorter().toggleSortOrder(Header.Modified.column());

        tableArea = new JPanel(new GridLayout(1, 1));
        tableArea.add(new JScrollPane(table));

        add(tableArea, BorderLayout.CENTER);
        tableArea.revalidate();
        tableArea.repaint();
    }

    private boolean isListable(Synthesizer synth) {
        Status s = synth.getStatus();
        boolean include = false;

        // Special format to enable quick reading
        if (box_work.isSelected())
            include |=
                s == Status.ReceivedForRepair ||
                s == Status.RepairingTroubleshooting ||
                s == Status.RepairingWaitingForParts ||
                s == Status.RepairingInProgress ;
        if (box_queue.isSelected())
            include |=
                s == Status.InQueue;
        if (box_deliver.isSelected())
            include |=
                s == Status.ReadyForDelivery ;
        if (box_decline.isSelected())
            include |=
                s == Status.Declined ||
                s == Status.Cancelled ;
        if (box_pick.isSelected())
            include |=
                s == Status.ReadyForPickup ;
        if (box_owner.isSelected())
            include |=
                s == Status.ReturnedToOwner ;

        return include;
    }

    private Synthesizer[] createFilteredRegister() {
        Synthesizer[] list = WindowHub.getCore().getAllSynthesizers();
        ArrayList<Synthesizer> data = new ArrayList<Synthesizer>(list.length);

        for (Synthesizer s: list)
            if (isListable(s))
                data.add(s);

        Synthesizer[] ret = new Synthesizer[data.size()];

        return data.toArray(ret);
    }

    private Synthesizer[] readFromArchive() {
        enter();
        Synthesizer[] synths = WindowHub.getCore().readFromArchive(this, archive_idx, archive_idx + ARCHIVE_STEP);
        stop();
        leave(synths.length);
        return synths;
    }

    private class RegisterTableModel extends DefaultTableModel {
        private final static long serialVersionUID = 184930002L;

        private Synthesizer[] register;

        public RegisterTableModel(Synthesizer[] register) {
            this.register = register;
        }

        public boolean isCellEditable(int row, int column){
            return false;
        }

        public int getRowCount() {
            return register != null ? register.length : 0;
        }

        public int getColumnCount() {
            return Header.length();
        }

        public Object getValueAt(int row, int column) {
            Synthesizer s = register[row];
            if (column == Header.Model.column()) {
                return s.getModel();
            } if (column == Header.Manufacturer.column()) {
                return s.getManufacturer();
            } if (column == Header.SerialNo.column()) {
                return s.getSerialNo();
            } if (column == Header.Status.column()) {
                if (s.getStatus() == Status.InQueue)
                    return Status.InQueue.toString() + " " + s.getQueueNo();
                else
                    return s.getStatus().toString();
            } if (column == Header.Owner.column()) {
                return s.getFirstName() + " " + s.getLastName() + " [" + s.getFirm() + "]";
            } if (column == Header.Modified.column()) {
                return s.getModified();
            }

            return null;
        }

        public String getColumnName(int column) {
            return Header.columnToString(column);
        }

        public Synthesizer getValue(int row) {
            return register[row];
        }
    }
}
