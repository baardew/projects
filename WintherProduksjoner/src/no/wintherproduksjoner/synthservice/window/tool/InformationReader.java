package no.wintherproduksjoner.synthservice.window.tool;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class InformationReader {

    private final String filename;
    private static String INFODIR = "";
    
    public InformationReader(String filename) {
	this.filename = filename;
    }

    public boolean exists() {
	File file = new File(INFODIR + filename);
	return file.exists();
    }

    public String readFile() {
	if (!exists())
	    return null;

	StringBuilder all = new StringBuilder();
	String line;
	boolean firstLine = true;
	
	try {
	    
	    BufferedReader in = new BufferedReader(new FileReader(INFODIR + filename));

	    while ((line = in.readLine()) != null) {
		if (!firstLine)
		    all.append("\n");
		else
		    firstLine = false;
		
		all.append(line);
	    }
	
	    in.close();
	} catch (IOException e) {
	    System.err.println(e);
	    return null;
	}
	return all.toString();
    }

    public void delete() {
	File file = new File(INFODIR + filename);
	file.delete();
    }

    public static void setInformationDirectory(String dir) {
	INFODIR = dir;
    }
}