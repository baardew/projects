package no.wintherproduksjoner.synthservice.window.tool;

import java.util.Comparator;

public class IntegerComparator implements Comparator<Integer> {

    public IntegerComparator() {}

    public int compare(Integer d1, Integer d2) {
	return d1.compareTo(d2);
    }

    public boolean equals(Integer d1, Integer d2) {
	return compare(d1, d2) == 0;
    }
}