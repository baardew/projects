package no.wintherproduksjoner.synthservice.window.tool;

import java.util.Comparator;
import java.util.Date;

import no.wintherproduksjoner.synthservice.db.tool.CoreUtil;

public class DateComparator implements Comparator<String> {

    public DateComparator() {}

    public int compare(String d1, String d2) {
	Date a = CoreUtil.stringToDate(d1);
	Date b = CoreUtil.stringToDate(d2);

	return a.compareTo(b);
    }

    public boolean equals(String d1, String d2) {
	return compare(d1, d2) == 0;
    }
}