package no.wintherproduksjoner.synthservice.window.tool;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;

public class ForcedListSelectionModel extends DefaultListSelectionModel {
    private final static long serialVersionUID = 184930003L;

    public ForcedListSelectionModel () {
	setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void clearSelection() {}
    public void removeSelectionInterval(int index0, int index1) {}
}
