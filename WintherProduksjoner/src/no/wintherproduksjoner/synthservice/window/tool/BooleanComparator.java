package no.wintherproduksjoner.synthservice.window.tool;

import java.util.Comparator;

public class BooleanComparator implements Comparator<Boolean> {

    public BooleanComparator() {}

    public int compare(Boolean d1, Boolean d2) {
	return d1.compareTo(d2);
    }

    public boolean equals(Boolean d1, Boolean d2) {
	return compare(d1, d2) == 0;
    }
}