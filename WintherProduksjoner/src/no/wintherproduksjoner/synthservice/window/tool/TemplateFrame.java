package no.wintherproduksjoner.synthservice.window.tool;

import no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;

import javax.swing.JFrame;

import javax.swing.UIManager;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JProgressBar;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.util.logging.Logger;

public abstract class TemplateFrame extends JFrame implements ActionListener, Monitor {

    protected static int screenWidth, screenHeight, offsetWidth, offsetHeight;
    protected static boolean enableLogging;

    protected JProgressBar progressBar;
    protected JLabel progressLabel;

    public TemplateFrame(String title) {
        super(title);

        progressLabel = new JLabel("No recent operation.");
        progressBar = new JProgressBar();
        progressBar.setIndeterminate(false);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
    }

    public abstract void actionPerformed(ActionEvent event);

    public static void setScreenSize(int width, int height) {
        screenWidth = width;
        screenHeight = height;
    }

    public static void setScreenOffset(int width, int height) {
        offsetWidth = width;
        offsetHeight = height;
    }

    public static void setLogging(boolean enable) {
        enableLogging = enable;
    }

    public void setVisible() {
        setMinimumSize(new Dimension(160, 90));
        pack();
        setLocation(screenWidth/2-getWidth()/2 + offsetWidth, screenHeight/2-getHeight()/2 + offsetHeight);
        setVisible(true);
    }

    protected void saveNotification(final JButton button) {
        button.setText("done");
        button.setForeground(Color.GREEN.darker());

        new Thread() {
            public void run() {
                try{
                    Thread.sleep(600);
                } catch (InterruptedException e) {}

                button.setText("save");
                button.setForeground(Color.BLACK);
            }
        }.start();
    }

    protected JPanel createProgressFooter() {
        JPanel lowerBar = new JPanel();
        lowerBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        lowerBar.add(progressBar);
        lowerBar.add(progressLabel);
        return lowerBar;
    }

    // Monitor
    public void setMessage(String message) {
        progressLabel.setText(message);

        if (enableLogging)
            FunctionTracer.message(message);
    }

    // Monitor
    public void start() {
        progressBar.setIndeterminate(true);
    }

    // Monitor
    public void stop() {
        progressBar.setIndeterminate(false);
        progressBar.setValue(progressBar.getMaximum());
    }
}
