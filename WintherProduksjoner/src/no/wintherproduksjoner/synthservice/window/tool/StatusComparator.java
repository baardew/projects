package no.wintherproduksjoner.synthservice.window.tool;

import no.wintherproduksjoner.synthservice.db.storage.Status;

import java.util.Comparator;

public class StatusComparator implements Comparator<String> {

    public StatusComparator() {}

    public int compare(String s1, String s2) {
	Status a, b;

	if (s1.contains(Status.InQueue.toString()))
	    a = Status.InQueue;
	else
	    a = Status.fromString(s1);

	if (s2.contains(Status.InQueue.toString()))
	    b = Status.InQueue;
	else
	    b = Status.fromString(s2);

	if (a == Status.InQueue && b == Status.InQueue) {
	    String sa = s1.replace(Status.InQueue.toString().concat(" "), "");
	    String sb = s2.replace(Status.InQueue.toString().concat(" "), "");;

	    return Integer.parseInt(sa) - Integer.parseInt(sb);
	}

	return a.getOrderValue() - b.getOrderValue();
    }

    public boolean equals(String s1, String s2) {
	return compare(s1, s2) == 0;
    }
}