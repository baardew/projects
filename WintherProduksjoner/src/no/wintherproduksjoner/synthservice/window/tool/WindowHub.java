package no.wintherproduksjoner.synthservice.window.tool;

import no.wintherproduksjoner.synthservice.db.engine.Core;

import no.wintherproduksjoner.synthservice.db.storage.Status;
import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;
import no.wintherproduksjoner.synthservice.db.tool.SynchronizeStatistic;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import java.awt.GridLayout;

public class WindowHub {

    private static Core core;
    private static boolean changesMade;

    private static JPanel statistics;
    private static JLabel news, updates, deleted;
    private static JLabel queue, delivery, received, trouble, wait, progress, pickup, returned, declined;

    // The class is static, but requires initialization.
    public WindowHub(Core c) {
	core = c;
	changesMade = false;

	news     = new JLabel("<MISSING>", JLabel.RIGHT);
	updates  = new JLabel("<MISSING>", JLabel.RIGHT);
	deleted  = new JLabel("<MISSING>", JLabel.RIGHT);
	queue    = new JLabel("<MISSING>", JLabel.RIGHT);
	delivery = new JLabel("<MISSING>", JLabel.RIGHT);
	received = new JLabel("<MISSING>", JLabel.RIGHT);
	trouble  = new JLabel("<MISSING>", JLabel.RIGHT);
	wait     = new JLabel("<MISSING>", JLabel.RIGHT);
	progress = new JLabel("<MISSING>", JLabel.RIGHT);
	pickup   = new JLabel("<MISSING>", JLabel.RIGHT);
	returned = new JLabel("<MISSING>", JLabel.RIGHT);
	declined = new JLabel("<MISSING>", JLabel.RIGHT);

	statistics = new JPanel(new GridLayout(0, 2, 5, 5));

	statistics.add(new JLabel("New"));
	statistics.add(news);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel("Updated"));
	statistics.add(updates);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel("Deleted"));
	statistics.add(deleted);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));

	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));

	statistics.add(new JLabel(Status.InQueue.toString()));
	statistics.add(queue);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel(Status.ReadyForDelivery.toString()));
	statistics.add(delivery);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel(Status.ReceivedForRepair.toString()));
	statistics.add(received);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel(Status.RepairingTroubleshooting.toString()));
	statistics.add(trouble);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel(Status.RepairingWaitingForParts.toString()));
	statistics.add(wait);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel(Status.RepairingInProgress.toString()));
	statistics.add(progress);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel(Status.ReadyForPickup.toString()));
	statistics.add(pickup);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel(Status.ReturnedToOwner.toString()));
	statistics.add(returned);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JLabel(Status.Declined.toString()));
	statistics.add(declined);
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));
	statistics.add(new JSeparator(SwingConstants.HORIZONTAL));

	updateStatistics();
    }

    public static Core getCore() {
	return core;
    }

    public static void setChangesMade() {
	changesMade = true;
    }

    public static boolean hasChangesMade() {
	return changesMade;
    }

    public static void clearChangesMade() {
	changesMade = false;
    }

    public static boolean save(Monitor monitor) {
	return core.save(monitor);
    }

    public static JPanel getStatisticsPanel() {
	return statistics;
    }

    public static void updateStatistics() {
	SynchronizeStatistic sync = core.getSynchronizationStatistics();

	if (sync != null) {
	    news.setText(new Integer(sync.news).toString());
	    updates.setText(new Integer(sync.updates).toString());
	    deleted.setText(new Integer(sync.deleted).toString());
	} else {
	    news.setText("0");
	    updates.setText("0");
	    deleted.setText("0");
	}

	int[] count = new int[9];
	for (Synthesizer s: core.getAllSynthesizers()) {
	    switch(s.getStatus()) {
	    case InQueue:
		count[0]++;
		break;
	    case ReadyForDelivery:
		count[1]++;
		break;
	    case ReceivedForRepair:
		count[2]++;
		break;
	    case RepairingTroubleshooting:
		count[3]++;
		break;
	    case RepairingWaitingForParts:
		count[4]++;
		break;
	    case RepairingInProgress:
		count[5]++;
		break;
	    case ReadyForPickup:
		count[6]++;
		break;
	    case ReturnedToOwner:
		count[7]++;
		break;
	    case Declined:
		count[8]++;
		break;
	    default:
		break;
	    }
	}

	   queue.setText(new Integer(count[0]).toString());
	delivery.setText(new Integer(count[1]).toString());
	received.setText(new Integer(count[2]).toString());
	 trouble.setText(new Integer(count[3]).toString());
	    wait.setText(new Integer(count[4]).toString());
	progress.setText(new Integer(count[5]).toString());
	  pickup.setText(new Integer(count[6]).toString());
	returned.setText(new Integer(count[7]).toString());
	declined.setText(new Integer(count[8]).toString());

	statistics.revalidate();
	statistics.repaint();
    }
}