package no.wintherproduksjoner.synthservice.window.tool;

import java.util.Comparator;

public class DoubleComparator implements Comparator<Double> {

    public DoubleComparator() {}

    public int compare(Double d1, Double d2) {
	return d1.compareTo(d2);
    }

    public boolean equals(Double d1, Double d2) {
	return compare(d1, d2) == 0;
    }
}