package no.wintherproduksjoner.synthservice.window.tool;

import no.wintherproduksjoner.synthservice.lib.AlphanumComparator;

import no.wintherproduksjoner.synthservice.window.tool.WindowHub;
import no.wintherproduksjoner.synthservice.window.tool.BooleanComparator;
import no.wintherproduksjoner.synthservice.window.tool.DoubleComparator;
import no.wintherproduksjoner.synthservice.window.tool.ForcedListSelectionModel;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Item;

import no.wintherproduksjoner.synthservice.db.tool.ItemCollection;
import no.wintherproduksjoner.synthservice.db.tool.ItemBroker;

import no.wintherproduksjoner.synthservice.lib.SimilarStringComparator;

import java.util.ArrayList;
import java.util.Comparator;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

public class ItemTable extends JPanel {

    private final static long serialVersionUID = 184930007L;

    private enum Header {
	Name("Name", 0, String.class),
	    Count("Count", 1, Double.class),
	    Price("Price", 2, Double.class),
	    Ordered("Ordered", 3, Boolean.class);

	private final String name;
	private final int column;
	private final Class<?> klass;

	Header(String name, int column, Class<?> klass) {
	    this.name = name;
	    this.column = column;
	    this.klass = klass;
	}

	public static String columnToString(int column) {
	    switch (column) {
	    case 0:
		return Header.Name.toString();
	    case 1:
		return Header.Count.toString();
	    case 2:
		return Header.Price.toString();
	    case 3:
		return Header.Ordered.toString();
	    default:
		return null;
	    }
	}

	public String toString() {
	    return name;
	}

	public int column() {
	    return column;
	}

	public Class<?> type() {
	    return klass;
	}

	public static int length() {
	    return 4; // The largest column
	}
    }

    private JTable table;
    private JPanel tableArea;
    private ItemTableModel tableModel;
    private ItemBroker broker;
    private String key;

    public ItemTable(ItemCollection items) {
	broker = new ItemBroker(WindowHub.getCore().getStorage());
	key = null;

	setLayout(new BorderLayout());

	// Create the table
	tableModel = new ItemTableModel(items);
	updateTable();
    }
    
    public void add(String name, double count, double price, boolean ordered, boolean fromStorage) {
	Item[] items = broker.add(name, count, price, ordered, fromStorage);

	tableModel.add(items);

	updateTable();
    }

    public void remove(boolean toStorage) {
	int row = getRow();
	if (row == -1)
	    return;

	tableModel.remove(row, toStorage);
	updateTable();
    }

    public void search(String key) {
	this.key = key;
	tableModel.search(key);
	updateTable();
    }

    public void resetSearch() {
	key = null;
	tableModel.resetSearch();
	updateTable();
    }

    public void clean() {
	tableModel.clean();
	updateTable();
    }

    public void commit() {
	tableModel.commit();
    }

    public void abort() {
	tableModel.abort();
    }

    public String getSelectedName() {
	return (String)tableModel.getValueAt(getRow(), Header.Name.column());
    }

    public Double getSelectedPrice() {
	return (Double)tableModel.getValueAt(getRow(), Header.Price.column());
    }

    public Boolean getSelectedOrdered() {
	return (Boolean)tableModel.getValueAt(getRow(), Header.Ordered.column());
    }


    private int getRow() {
	int row = table.getSelectedRow();
	if (row < 0 || row >= table.getRowCount())
	    return -1;

	return table.convertRowIndexToModel(row);
    }

    private void updateTable() {
	if (tableArea != null) {
	    remove(table);
	    remove(tableArea);
	}

	table = new JTable(tableModel);
	table.setSelectionModel(new ForcedListSelectionModel());
	table.setAutoCreateRowSorter(true);

	TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
	if (key == null) {
	    sorter.setComparator(0, new AlphanumComparator());
	} else {
	    final String k = key.toLowerCase();
	    sorter.setComparator(0, new Comparator<String>() {
			public int compare(String s1, String s2) {
			    s1 = s1.toLowerCase();
			    s2 = s2.toLowerCase();
			    if (s1.compareTo(k) < 0) {
				if (s2.compareTo(k) < 0) {
				    return s1.compareTo(s2);
				} else if (s2.compareTo(k) > 0) {
				    return -1;
				} else {
				    return 1;
				}
			    } else if (s1.compareTo(k) > 0) {
				if (s2.compareTo(k) < 0) {
				    return 1;
				} else if (s2.compareTo(k) > 0) {
				    return s1.compareTo(s2);
				} else {
				    return 1;
				}
			    } else {
				if (s2.compareTo(k) < 0) {
				    return -1;
				} else if (s2.compareTo(k) > 0) {
				    return -1;
				} else {
				    return 0;
				}
			    }
			}

		    });
	}
	sorter.setComparator(1, new DoubleComparator());
	sorter.setComparator(2, new DoubleComparator());
	sorter.setComparator(3, new BooleanComparator());
	table.setRowSorter(sorter);
	table.getTableHeader().setReorderingAllowed(false);

	table.getRowSorter().toggleSortOrder(Header.Name.column());

	tableArea = new JPanel(new GridLayout(1, 1));
	tableArea.add(new JScrollPane(table));

	add(tableArea, BorderLayout.CENTER);
	tableArea.revalidate();
	tableArea.repaint();
    }

    private class ItemTableModel extends DefaultTableModel {
	private final static long serialVersionUID = 184930008L;

	private ItemCollection collection;
	private ArrayList<TableRowItem> items;
	private ArrayList<TableRowItem> trash;
	private ArrayList<DisplayItem> display;

	public ItemTableModel(ItemCollection collection) {
	    this.collection = collection;

	    Item[] original = collection.getAllItems();
	    trash = new ArrayList<TableRowItem>();
	    items = new ArrayList<TableRowItem>(original.length);
	    display = new ArrayList<DisplayItem>(original.length);

	    for (Item i: original) {
		TableRowItem it = new TableRowItem(collection, broker, i, false);
		items.add(it);
		display.add(new DisplayItem(it));
	    }
	}

	public void setValueAt(Object value, int row, int column) {
	    Item work = getItem(row);
	    if (column == Header.Name.column())
		work.setName((String)value);
	    if (column == Header.Count.column())
		work.setCount(((Double)value).doubleValue());
	    if (column == Header.Price.column())
		work.setPrice(((Double)value).doubleValue());
	    if (column == Header.Ordered.column())
		work.setOrdered(((Boolean)value).booleanValue());
	}

	public Class<?> getColumnClass(int column) {
	    if (column == Header.Name.column())
		return Header.Name.type();
	    if (column == Header.Count.column())
		return Header.Count.type();
	    if (column == Header.Price.column())
		return Header.Price.type();
	    if (column == Header.Ordered.column())
		return Header.Ordered.type();

	    return Object.class;
	}

	public int getRowCount() {
	    return display != null ? display.size() : 0;
	}

	public int getColumnCount() {
	    return Header.length();
	}

	public Object getValueAt(int row, int column) {
	    Item i = getItem(row);
	    if (column == Header.Name.column())
		return i.getName();
	    if (column == Header.Count.column())
		return i.getCount();
	    if (column == Header.Price.column())
		return i.getPrice();
	    if (column == Header.Ordered.column())
		return i.getOrdered();

	    return null;
	}

	public String getColumnName(int column) {
	    return Header.columnToString(column);
	}

	public Item getItem(int row) {
	    return display.get(row).getData().getWork();
	}

	public void add(Item[] item) {
	    for (Item i: item) {
		TableRowItem it = new TableRowItem(collection, broker, i, true);
		items.add(it);
		display.add(new DisplayItem(it));
	    }
	}

	public void remove(int row, boolean toStorage) {
	    DisplayItem dsp = display.remove(row);
	    if (dsp == null)
		return;

	    TableRowItem i = dsp.getData();
	    items.remove(i);
	    i.setRemoved();
	    i.getWork().setFromStorage(toStorage);
	    trash.add(i);
	}

	public void search(String key) {
	    display = new ArrayList<DisplayItem>(items.size());

	    String prev = "";
	    StringBuilder value = new StringBuilder();
	    for (TableRowItem i: items) {
		if (!i.getWork().getName().equals(prev)) {
		    prev = i.getWork().getName();
		    value.append(i.getWork().getName().replace(" ", "_"));
		    value.append(" ");
		}
	    }

	    String selection[] = SimilarStringComparator.compareSimilarString(key, value.toString());

	    for (TableRowItem i: items)
		for (String s: selection)
		    if (i.getWork().getName().equals(s.replace("_", " ")))
			display.add(new DisplayItem(i));
	}

	public void resetSearch() {
	    display = new ArrayList<DisplayItem>(items.size());
	    for (TableRowItem i: items)
		display.add(new DisplayItem(i));
	}

	public void clean() {
	    // Cannot remove while iterating, so use cache instead
	    ArrayList<TableRowItem> delete_cache = new ArrayList<TableRowItem>();

	    // First remove empty ones
	    for (TableRowItem i: items)
		if (i.getWork().getCount() == 0.0)
		    delete_cache.add(i);

	    for (TableRowItem i: delete_cache) {
		items.remove(i);
		i.setRemoved();
		trash.add(i);
	    }

	    resetSearch();
	}

	public void commit() {
	    for (TableRowItem i: items)
		i.commit();

	    for (TableRowItem i: trash)
		i.commit();
	}

	public void abort() {
	    for (TableRowItem i: items)
		i.abort();

	    for (TableRowItem i: trash)
		i.abort();
	}
    }

    private class TableRowItem {

	private boolean added, removed;
	private Item original;
	private Item work;
	private ItemCollection collection;
	private ItemBroker broker;

	public TableRowItem(ItemCollection collection, ItemBroker broker, Item original, boolean added) {
	    this.broker = broker;
	    this.added = added;
	    this.collection = collection;
	    this.original = original;
	    this.removed = false;

	    work = new Item(original.getName(),
			    original.getCount(),
			    original.getPrice(),
			    original.getOrdered(),
			    original.getFromStorage());
	}

	public Item getWork() {
	    return work;
	}

	public void commit() {
	    if (removed) {
		original.setFromStorage(work.getFromStorage());
		broker.remove(original);
		collection.removeItem(original);
		return;
	    }

	    if (added) {
		original = new Item(work.getName(),
				    work.getCount(),
				    work.getPrice(),
				    work.getOrdered(),
				    work.getFromStorage());
		collection.addItem(original);
		added = false;
		return;
	    }

	    original.setName(work.getName());
	    original.setCount(work.getCount());
	    original.setPrice(work.getPrice());
	    original.setOrdered(work.getOrdered());
	}

	public void abort() {
	    if (added) {
		original.setFromStorage(work.getFromStorage());
		broker.remove(original);
		added = false;
	    }
	}

	public void setRemoved() {
	    removed = true;
	}
    }

    private class DisplayItem {
	private TableRowItem data;

	public DisplayItem(TableRowItem data) {
	    this.data = data;
	}

	public TableRowItem getData() {
	    return data;
	}
    }
}