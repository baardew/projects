package no.wintherproduksjoner.synthservice.window.tool;

import javax.swing.JOptionPane;

public class CrashDialog {

    private static final String errorMessage =
        "The program got an exception while performing an operation."
        + "\n\n\n"
        + "The cause of this can be found in the file 'synthservice.log'\n"
        + "located inside the 'WintherProduksjoner Documents' folder\n"
        + "that can be found in your home directory. Report this issue by\n"
        + "attaching it with an e-mail and send it to dev@wintherproduksjoner.no.";

    private static final String errorHeader = "Synth Service has Crashed";

    public static void execute() {
        JOptionPane.showMessageDialog(null,
                                      errorMessage,
                                      errorHeader,
                                      JOptionPane.ERROR_MESSAGE);

        System.exit(1);
    }
}
