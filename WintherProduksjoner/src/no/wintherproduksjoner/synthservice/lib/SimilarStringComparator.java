package no.wintherproduksjoner.synthservice.lib;

import java.util.ArrayList;

/**
 * A string comparison class that compares a string against a key.
 *
 * Implements a standard edit distance comparison algorithm.
 * The mismatch tolerance is depending on the length og the word.
 *
 * @author baardew
 * @version 2.1
 * @date 12th FEB 2014
 */
public class SimilarStringComparator {

    /**
     * A new comparator.
     *
     * @param key The search key.
     * @param value The search stack.
     * @return A list of words that are similar. Order is the same as they are matched in the value string.
     */
    public static String[] compareSimilarString(String key, String value) {
	if (key == null || value == null || key.length() < 1 || value.length() < 1)
	    return new String[0];

	String[] values = value.split("[\\s]+");
	key = key.toLowerCase();
	ArrayList<String> results = new ArrayList<String>();

	for (String s: values) {
	    String word = s.toLowerCase();

	    if (word.contains(key) || key.contains(word)) {
		results.add(s);
	    } else if (isSimilar(key, word)) {
		results.add(s);
	    } else {
		String[] split = word.split("[_]+");
		for (String p: split) {
		    if (isSimilar(key, p)) {
			results.add(s);
			break;
		    }
		}
	    }
	}

	String[] ret = new String[results.size()];
	return results.toArray(ret);
    }

    /**
     * Computes the edit distance between two words.
     *
     * @param key The string to match to.
     * @param value The string check for match.
     * @return TRUE if the strings are similar, false if not.
     */
    private static boolean isSimilar(String key, String value) {
	int faultTolerance;
	int m = key.length();
	int n = value.length();

	if (key.length() < 6)
	    faultTolerance = m / 2;
	else if (key.length() < 12)
	    faultTolerance = 4;
	else
	    faultTolerance = (int)(Math.ceil(m * 0.3));

	int table[][] = new int[m+1][n+1];

	for (int i = 0; i <= m; i++)
	    table[i][0] = i;
	for (int j = 0; j <= n; j++)
	    table[0][j] = j;

	for (int i = 1; i <= m; i++) {
	    for (int j = 1; j <= n; j++) {
		if (key.charAt(i-1) == value.charAt(j-1))
		    table[i][j] = table[i-1][j-1];
		else
		    table[i][j] = min(table[i-1][j-1] + 1, table[i-1][j] + 1, table[i][j-1] + 1);
	    }
	}

	return table[m][n] <= faultTolerance;
    }

    /**
     * Gets the minimum value from three int.
     *
     * @param a First int.
     * @oaram b Second int.
     * @param c Third int.
     * @return Smalles number among the three input numbers.
     */
    private static int min(int a, int b, int c) {
	int min = Math.min(a, b);
	min = Math.min(min, c);
	return min;
    }
}