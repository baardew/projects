package no.wintherproduksjoner.synthservice.lib.systemlogging;

import java.io.IOException;
import java.io.PrintStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

import java.util.Locale;

public class ProgramLogger extends OutputStream {

    private static PrintStream log = null;

    public static void initialize(String filename) {
        if (log != null)
            throw new RuntimeException("ProgramLogger already initialized");

        try {
            log = new PrintStream(new FileOutputStream(filename), true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void teardown() {
        log.flush();
        log.close();
        log = null;
    }

    public static synchronized void log(String string) {
        if (log != null)
            log.print(string);
    }

    /*
     * Override for FileOutputStream to make it synchronized and
     * correctly indented (and to make the static class usable
     * in situations where the class must be an instance.
     */
    public synchronized void close() throws IOException {
        if (log != null)
            log.close();
    }

    public synchronized void flush() throws IOException {
        if (log != null)
            log.flush();
    }

    public synchronized void write(byte[] b) throws IOException {
        if (log != null)
            log.write(b);
    }

    public synchronized void write(byte[] b, int off, int len) throws IOException {
        if (log != null)
            log.write(b, off, len);
    }

    public synchronized void write(int b) throws IOException {
        if (log != null)
            log.write(b);
    }
}
