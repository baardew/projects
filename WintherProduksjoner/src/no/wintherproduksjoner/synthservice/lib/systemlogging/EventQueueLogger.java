package no.wintherproduksjoner.synthservice.lib.systemlogging;

import no.wintherproduksjoner.synthservice.window.tool.CrashDialog;

import java.awt.EventQueue;
import java.awt.AWTEvent;
import javax.swing.JOptionPane;

public class EventQueueLogger extends EventQueue {
    protected void dispatchEvent(AWTEvent newEvent) {
        try {
            super.dispatchEvent(newEvent);
        } catch (Throwable t) {
            t.printStackTrace();
            CrashDialog.execute();
        }
    }
}
