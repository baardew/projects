package no.wintherproduksjoner.synthservice.lib.systemlogging;

import static no.wintherproduksjoner.synthservice.lib.systemlogging.ProgramLogger.log;

import java.util.ArrayList;

public class FunctionTracer {

    private static final String tab = "    ";
    private static final String filter = "no.wintherproduksjoner.synthservice.";

    private static boolean enabled = false;
    private static int indentationLevel = 0;

    public FunctionTracer() {
        throw new RuntimeException("Cannot initiate static class");
    }

    public static void enableLogging() {
        enabled = true;
    }

    public static void enter(Object... args) {
        log(getIndentation());
        log(getStackFunctionName());
        log("(");

        int argc = args.length;
        for (int i = 0; i < argc; i++) {
            log(formatArgument(args[i]));

            if ((i + 1) < argc)
                log(", ");
        }

        log(") {\n");
        indentationLevel++;
    }
    
    public static void leave(Object... args) {
        indentationLevel--;

        if (args.length > 1)
            throw new RuntimeException("Cannot return more than one argument");

        log(getIndentation());

        if (args.length == 1)
            log("} = " + formatArgument(args[0]) + "\n");
        else
            log("}\n");
    }

    public static void message(String message) {
        StringBuilder sb = new StringBuilder();
        String indent = getIndentation();
        String entry = message.replace("\n", "\n" + indent + " * ");

        sb.append(indent);
        sb.append("/*\n");
        sb.append(indent);
        sb.append(" * ");
        sb.append(entry);
        sb.append("\n");
        sb.append(indent);
        sb.append(" */\n");

        log(sb.toString());
    }

    private static String getIndentation() {
        String indent = new String();
        for (int i = 0; i < indentationLevel; i++)
            indent += tab;

        return indent;
    }

    private static String getStackFunctionName() {
        String trace = Thread.currentThread().getStackTrace()[3].toString();
        trace = trace.replace(filter, "");
        return trace.substring(0, trace.indexOf('('));
    }

    private static String formatArgument(Object arg) {
        String prefix = new String("");
        String postfix = new String("");

        if (arg == null)
            return "NULL";


        if (arg instanceof Double || arg instanceof Float || arg instanceof Integer)
            return arg.toString();


        if (arg instanceof Boolean)
            return arg.toString().toUpperCase();


        if (arg instanceof String)
            prefix = new String("\"");
        else if (arg instanceof Character)
            prefix = new String("'");
        else
            prefix = new String("<");


        if (arg instanceof String)
            postfix = new String("\"");
        else if (arg instanceof Character)
            postfix = new String("'");
        else
            postfix = new String(">");


        return prefix + arg.toString() + postfix;
    }
}
