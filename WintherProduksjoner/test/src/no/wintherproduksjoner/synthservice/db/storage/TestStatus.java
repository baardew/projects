package no.wintherproduksjoner.synthservice.db.storage;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestStatus {

    @Test
    public void from_string() {
        Status s;

        s = Status.fromString("In Queue");
        assertEquals(Status.InQueue, s);
        s = Status.fromString("Ready for Delivery");
        assertEquals(Status.ReadyForDelivery, s);
        s = Status.fromString("Received for Repair");
        assertEquals(Status.ReceivedForRepair, s);
        s = Status.fromString("Repairing: Troubleshooting");
        assertEquals(Status.RepairingTroubleshooting, s);
        s = Status.fromString("Repairing: Waiting for Parts");
        assertEquals(Status.RepairingWaitingForParts, s);
        s = Status.fromString("Repairing: In Progress");
        assertEquals(Status.RepairingInProgress, s);
        s = Status.fromString("Finished: Ready for Pickup");
        assertEquals(Status.ReadyForPickup, s);
        s = Status.fromString("Declined");
        assertEquals(Status.Declined, s);
        s = Status.fromString("Returned to Owner");
        assertEquals(Status.ReturnedToOwner, s);
        s = Status.fromString("Cancelled");
        assertEquals(Status.Cancelled, s);
        s = Status.fromString("Archived");
        assertEquals(Status.Archived, s);
    }

    @Test
    public void is_archivable() {
        Status s;

        s = Status.InQueue;
        assertFalse(s.isArchivable());
        s = Status.ReadyForDelivery;
        assertFalse(s.isArchivable());
        s = Status.ReceivedForRepair;
        assertFalse(s.isArchivable());
        s = Status.RepairingTroubleshooting;
        assertFalse(s.isArchivable());
        s = Status.RepairingWaitingForParts;
        assertFalse(s.isArchivable());
        s = Status.RepairingInProgress;
        assertFalse(s.isArchivable());
        s = Status.ReadyForPickup;
        assertFalse(s.isArchivable());
        s = Status.Declined;
        assertFalse(s.isArchivable());
        s = Status.Cancelled;
        assertFalse(s.isArchivable());
        s = Status.Archived;
        assertFalse(s.isArchivable());

        s = Status.ReturnedToOwner;
        assertTrue(s.isArchivable());
    }

    @Test
    public void get_order_value() {
        Status s;

        s = Status.ReadyForPickup;
        assertEquals(0, s.getOrderValue());
        s = Status.Declined;
        assertEquals(Integer.MIN_VALUE, s.getOrderValue());
    }
}
