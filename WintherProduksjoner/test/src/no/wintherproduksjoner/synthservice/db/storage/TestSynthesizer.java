package no.wintherproduksjoner.synthservice.db.storage;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Item;

import no.wintherproduksjoner.synthservice.db.tool.CoreUtil;

import testutils.Testutil;

import java.util.Date;
import java.util.Arrays;

import java.io.File;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestSynthesizer {

    private Synthesizer synth;

    @Before
    public void init() {
        synth = new Synthesizer(12345678, 2, "2000-11-22 01:50:15", "first", "second", "firm1", "address1", 1450, "postName",
                                55512378, "email1", "manufacturer", "model", "serial 1", "fault");
    }

    @After
    public void exit() {
        File f = new File("synthesizer.ser");
        if (f.exists())
            f.delete();
    }

    @Test
    public void serializable() {
        assertNull(Testutil.serialize("synthesizer.ser", synth));
    }

    @Test
    public void deserializable() {
        Testutil.serialize("synthesizer.ser", synth);
        Synthesizer s = (Synthesizer)Testutil.deserialize("synthesizer.ser");
        assertNotNull(s);
        assertEquals(synth.createOwnerString(), s.createOwnerString());
    }

    @Test
    public void decreaseQueueNo() {
        synth.decreaseQueueNo();
        assertEquals(1, synth.getQueueNo());
        synth.decreaseQueueNo();
        assertEquals(0, synth.getQueueNo());
        synth.decreaseQueueNo();
        assertEquals(0, synth.getQueueNo());
    }

    @Test
    public void change_status_on_queue_update() {
        synth.decreaseQueueNo();
        synth.decreaseQueueNo();
        assertEquals(Status.ReadyForDelivery, synth.getStatus());
    }

    @Test
    public void update_modified() throws Exception {
        synth.updateModified();
        Date minimum = CoreUtil.stringToDate(synth.getModified());
        Thread.sleep(1100);
        synth.updateModified();
        Date now = CoreUtil.stringToDate(synth.getModified());
        assertTrue(now.compareTo(minimum) > 0);
    }

    @Test
    public void decline() {
        synth.decline();
        assertEquals(0, synth.getQueueNo());
        assertEquals(Status.Declined, synth.getStatus());
    }

    @Test
    public void get_modified() {
        Date expected = CoreUtil.stringToDate("2000-11-22 01:50:15");
        Date actual = CoreUtil.stringToDate(synth.getModified());
        assertEquals(expected, actual);
    }

    @Test
    public void create_owner_string() {
        assertEquals("first second [firm1] | model manufacturer [serial 1]",
                     synth.createOwnerString());
    }

    @Test
    public void create_modified_string() {
        assertEquals("2000-11-22 01:50:15 | model manufacturer [serial 1]",
                     synth.createModifiedString());
    }

    @Test
    public void create_status_string() {
        assertEquals("In Queue (2) | model manufacturer [serial 1]",
                     synth.createStatusString());

        synth.decreaseQueueNo();
        synth.decreaseQueueNo();

        assertEquals("Ready for Delivery | model manufacturer [serial 1]",
                     synth.createStatusString());
    }

    @Test
    public void compare_to_status() {
        Synthesizer s1 = new Synthesizer(12345678, 1, "2000-11-22 01:50:15", "first", "second", "firm1", "address1", 1450, "postName",
                                         5557751, "email1", "manufacturer", "model", "serial 1", "fault");
        Synthesizer s2 = new Synthesizer(87654321, 2, "", "third", "fourth", "firm2", "address2", 1450, "postName",
                                         55512378, "email2", "manufacturer", "model", "serial 2", "fault");
        Synthesizer s3 = new Synthesizer(12348765, 3, "", "fifth", "sixth", "firm2", "address3", 1450, "postName",
                                         55512378, "email4", "manufacturer", "model X", "serial X", "fault");

        Synthesizer.setCompareMethod(Synthesizer.CompareMethod.COMPARE_STATUS);
        assertTrue(s1.compareTo(s3) < 0);
        assertTrue(s1.compareTo(s1) == 0);
        assertTrue(s3.compareTo(s2) > 0);
        s1.decreaseQueueNo();
        assertTrue(s1.compareTo(s2) < 0);
        s2.decreaseQueueNo();
        s2.decreaseQueueNo();
        assertTrue(s1.compareTo(s2) < 0);
    }

    @Test
    public void compare_to_modified() {
        Synthesizer s1 = new Synthesizer(12345678, 1, "2000-11-22 01:50:15", "first", "second", "firm1", "address1", 1450, "postName",
                                         5557751, "email1", "manufacturer", "model", "serial 1", "fault");
        Synthesizer s2 = new Synthesizer(87654321, 2, "2000-11-22 01:50:35", "third", "fourth", "firm2", "address2", 1450, "postName",
                                         55512378, "email2", "manufacturer", "model", "serial 2", "fault");

        Synthesizer.setCompareMethod(Synthesizer.CompareMethod.COMPARE_MODIFIED);

        assertTrue(s1.compareTo(s2) < 0);
        assertTrue(s2.compareTo(s2) == 0);
        assertTrue(s2.compareTo(s1) > 0);
    }

    @Test
    public void compare_to_owner() {
        Synthesizer s1 = new Synthesizer(12345678, 1, "2000-11-22 01:50:15", "first", "second", "firm1", "address1", 1450, "postName",
                                         5557751, "email1", "manufacturer", "model", "serial 1", "fault");
        Synthesizer s2 = new Synthesizer(87654321, 2, "", "third", "fourth", "firm2", "address2", 1450, "postName",
                                         55512378, "email2", "manufacturer", "model", "serial 2", "fault");
        Synthesizer s3 = new Synthesizer(12348765, 3, "", "fifth", "sixth", "firm2", "address3", 1450, "postName",
                                         55512378, "email4", "manufacturer", "model X", "serial X", "fault");

        Synthesizer.setCompareMethod(Synthesizer.CompareMethod.COMPARE_OWNER);

        assertTrue(s1.compareTo(s2) < 0);
        assertTrue(s3.compareTo(s2) < 0);
        assertTrue(s2.compareTo(s1) > 0);
        assertTrue(s2.compareTo(s2) == 0);
    }

    @Test
    public void add_item() {
        Item[] items = new Item[4];
        items[0] = new Item("IT-1", 1, 500);
        items[1] = new Item("AMK", 6, 46.7);
        items[2] = new Item("6h-3", 7, 6.3);
        items[3] = new Item("g6-rtt33", 1, 25.5);

        for (int i = 0; i < 4; i++)
            if (!synth.addItem(items[i]))
                fail("Failed at index " + i);

        assertEquals(4, synth.getItemCount());
    }

    @Test
    public void remove_item() {
        Item[] items = new Item[4];
        items[0] = new Item("IT-1", 1, 500);
        items[1] = new Item("AMK", 6, 46.7);
        items[2] = new Item("6h-3", 7, 6.3);
        items[3] = new Item("g6-rtt33", 1, 25.5);

        for (Item i : items)
            synth.addItem(i);

        assertTrue(synth.removeItem(items[3]));
    }

    @Test
    public void get_all_items() {
        Item[] items = new Item[4];
        items[0] = new Item("IT-1", 1, 500);
        items[1] = new Item("AMK", 6, 46.7);
        items[2] = new Item("6h-3", 7, 6.3);
        items[3] = new Item("g6-rtt33", 1, 25.5);

        for (Item i : items)
            synth.addItem(i);

        Item[] it = synth.getAllItems();
        Item.setCompareMethod(Item.CompareMethod.COMPARE_NAME);
        assertArrayEquals(items, it);
    }

    @Test
    public void serializable_with_items() {
        Item[] items = new Item[4];
        items[0] = new Item("IT-1", 1, 500);
        items[1] = new Item("AMK", 6, 46.7);
        items[2] = new Item("6h-3", 7, 6.3);
        items[3] = new Item("g6-rtt33", 1, 25.5);

        for (Item i : items)
            synth.addItem(i);

        assertNull(Testutil.serialize("synthesizer.ser", synth));
    }

    @Test
    public void deserializable_with_items() {
        Item[] items = new Item[4];
        items[0] = new Item("IT-1", 1, 500);
        items[1] = new Item("AMK", 6, 46.7);
        items[2] = new Item("6h-3", 7, 6.3);
        items[3] = new Item("g6-rtt33", 1, 25.5);

        for (Item i : items)
            synth.addItem(i);

        Testutil.serialize("synthesizer.ser", synth);
        Synthesizer s = (Synthesizer)Testutil.deserialize("synthesizer.ser");
        assertNotNull(s);
        Item[] it = s.getAllItems();

        String[] expected = new String[4];
        String[] actual = new String[4];

        for (int i = 0; i < 4; i++) {
            expected[i] = items[i].getName();
            actual[i] = it[i].getName();
        }

        Arrays.sort(expected);
        Arrays.sort(actual);

        assertArrayEquals(expected, actual);
    }
}
