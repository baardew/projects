package no.wintherproduksjoner.synthservice.db.storage;

import testutils.Testutil;

import java.util.ArrayList;
import java.util.Collections;

import java.io.File;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestItem {

    @After
    public void exit() {
        File f = new File("item.ser");
        if (f.exists())
            f.delete();
    }

    @Test
    public void constructors() {
        Item i = new Item();
        assertEquals("", i.getName());
        assertEquals(0, i.getCount(), 0);
        assertEquals(0, i.getPrice(), 0);
        assertFalse(i.getOrdered());

        i = new Item("Component");
        assertEquals("Component", i.getName());
        assertEquals(0, i.getCount(), 0);
        assertEquals(0, i.getPrice(), 0);
        assertFalse(i.getOrdered());

        i = new Item("Working Hours", 5.0, 499.99);
        assertEquals("Working Hours", i.getName());
        assertEquals(5.0, i.getCount(), 0);
        assertEquals(499.99, i.getPrice(), 0);
        assertFalse(i.getOrdered());
    }

    @Test
    public void serializable() {
        Item i = new Item("Working Hours", 5.0, 499.99);
        assertNull(Testutil.serialize("item.ser", i));
    }

    @Test
    public void deserializable() {
        Item i = new Item("Working Hours", 5.0, 499.99);
        assertNull(Testutil.serialize("item.ser", i));
        Item r = (Item)Testutil.deserialize("item.ser");

        assertEquals(i.getName(), r.getName());
        assertEquals(i.getCount(), r.getCount(), 0);
        assertEquals(i.getPrice(), r.getPrice(), 0);
        assertEquals(i.getOrdered(), r.getOrdered());
    }

    @Test
    public void compare_to_ordered() {
        ArrayList<Item> sortTest = new ArrayList<Item>(6);
        sortTest.add(new Item("Name 2", 0, 2, true, false));
        sortTest.add(new Item("Name 2", 0, 2, false, false));
        sortTest.add(new Item("Name 1", 0, 2, false, false));
        sortTest.add(new Item("Name 2", 0, 1, true, false));
        sortTest.add(new Item("Name 1", 0, 1, true, false));
        sortTest.add(new Item("Name 1", 0, 2, true, false));
        sortTest.add(new Item("Name 2", 0, 1, false, false));
        sortTest.add(new Item("Name 1", 0, 1, false, false));

        ArrayList<Item> sortCheck = new ArrayList<Item>(6);
        sortCheck.add(new Item("Name 1", 0, 1, true, false));
        sortCheck.add(new Item("Name 1", 0, 2, true, false));
        sortCheck.add(new Item("Name 1", 0, 1, false, false));
        sortCheck.add(new Item("Name 1", 0, 2, false, false));
        sortCheck.add(new Item("Name 2", 0, 1, true, false));
        sortCheck.add(new Item("Name 2", 0, 2, true, false));
        sortCheck.add(new Item("Name 2", 0, 1, false, false));
        sortCheck.add(new Item("Name 2", 0, 2, false, false));

        Item.setCompareMethod(Item.CompareMethod.COMPARE_ORDERED);
        Collections.sort(sortTest);

        for (int i = 0; i < 6; i++)
            if (sortCheck.get(i).compareTo(sortTest.get(i)) != 0)
                fail("Failed at index " + i);
    }
}
