package no.wintherproduksjoner.synthservice.db.engine;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;

import java.util.Comparator;

public class QueueNoComparator implements Comparator<Synthesizer> {
    public int compare(Synthesizer s1, Synthesizer s2) {
        return s1.getQueueNo() - s2.getQueueNo();
    }
}
