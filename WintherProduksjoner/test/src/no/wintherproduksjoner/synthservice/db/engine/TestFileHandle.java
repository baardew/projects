package no.wintherproduksjoner.synthservice.db.engine;

import no.wintherproduksjoner.synthservice.db.engine.FileHandle;
import no.wintherproduksjoner.synthservice.db.tool.Monitor;

import java.io.File;

import java.util.ArrayList;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

/**
 * Testing readToArray indirectly tests read directly, only a for loop in difference.
 * Hence, explicit read is not done.
 */

public class TestFileHandle implements Monitor {

    // Required by Monitor
    public void setMessage(String message){}
    public void start(){}
    public void stop(){}

    private void deleteFile(String name) {
        File f = new File("test/files/" + name);
        if (f.exists())
            f.delete();
    }

    @Before
    public void init() {
        FileHandle.setHomeDirectory("test/files/");
    }

    @After
    public void exit() {
        deleteFile("synthesizer.ser");
        deleteFile("nonexistantfile.404");
        deleteFile("content.ser");
        deleteFile("uft.ser");
    }

    @Test
    public void set_home_directory() {
        FileHandle<Integer> f = new FileHandle<Integer>(null, this, 0);
        assertEquals("test/files/", f.getFilename());

        f = new FileHandle<Integer>("synthesizer.ser", this, 0);
        assertEquals("test/files/synthesizer.ser", f.getFilename());
    }

    @Test
    public void read_to_array_list_nonexistent_file() {
        ArrayList<Integer> rl;
        FileHandle<Integer> f = new FileHandle<Integer>("nonexistantfile.404", this, 0);
        rl = f.readToArrayList(this);
        assertNull(rl);
    }

    @Test
    public void close_nonexistent_file() {
        FileHandle<Integer> f = new FileHandle<Integer>("nonexistantfile.404", this, 0);
        assertTrue(f.close());
    }

    @Test
    public void write_empty() {
        FileHandle<Integer> f = new FileHandle<Integer>("nonexistantfile.404", this, 0);
        ArrayList<Integer> wl = new ArrayList<Integer>(0);
        assertTrue(f.write(wl, this));
        assertTrue(f.close());
    }

    @Test
    public void read_to_array_list_empty() {
        FileHandle<Integer> f = new FileHandle<Integer>("content.ser", this, 0);
        ArrayList<Integer> wl = new ArrayList<Integer>(0);
        f.write(wl, this);
        ArrayList<Integer> rl = f.readToArrayList(this);
        assertNull(rl);
        assertTrue(f.close());
    }


    @Test
    public void write_items() {
        ArrayList<Integer> wl = new ArrayList<Integer>(4);
        wl.add(123);
        wl.add(234);
        wl.add(345);
        wl.add(456);

        FileHandle<Integer> f = new FileHandle<Integer>("content.ser", this, 0);
        assertTrue(f.write(wl, this));
        assertTrue(f.close());
    }


    @Test
    public void read_to_array_list_items() {
        ArrayList<Integer> wl = new ArrayList<Integer>(4);
        wl.add(123);
        wl.add(234);
        wl.add(345);
        wl.add(456);

        FileHandle<Integer> f = new FileHandle<Integer>("content.ser", this, 0);
        f.write(wl, this);
        f.close();

        FileHandle<Integer> fr = new FileHandle<Integer>("content.ser", this, 0);
        ArrayList<Integer> rl = fr.readToArrayList(this);
        assertNotNull(rl);

        Integer[] expected = new Integer[4];
        Integer[] actual = new Integer[4];

        wl.toArray(expected);
        rl.toArray(actual);

        assertEquals(wl, rl);
        assertTrue(fr.close());
    }

    @Test
    public void utf_8_support() {
        FileHandle<String> f = new FileHandle<String>("utf8.ser", this, 1);
        f.write("Bølge", this);
        f.close();

        FileHandle<String> fr = new FileHandle<String>("utf8.ser", this, 1);
        assertEquals("Bølge", fr.read(this));
        fr.close();
    }
}
