package no.wintherproduksjoner.synthservice.db.engine;

import no.wintherproduksjoner.synthservice.db.storage.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

import java.lang.Iterable;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestItemStorage {

    private ArrayList<Item> testItems;
    private ItemStorage storage;

    @Before
    public void init() {
        storage = new ItemStorage(4);

        testItems = new ArrayList<Item>(4);
        testItems.add(new Item("Reference", 12, 100, true, false));
        testItems.add(new Item("Simple", 5, 51, true, false));
        testItems.add(new Item("Missing", 48, 277, true, false));
        testItems.add(new Item("Create", 531, 5, true, false));
        Collections.sort(testItems);

        storage.addItemCollection(testItems);
    }

    @After
    public void exit() {
        testItems = null;
        storage = null;
    }

    @Test
    public void add_item_collection() {
        storage = new ItemStorage(4);
        assertEquals(4, storage.addItemCollection(testItems));
    }

    @Test
    public void add() {
        assertTrue(storage.addItem(new Item("Empty")));
    }

    @Test
    public void get_all_items_empty() {
        storage = new ItemStorage(4);
        assertEquals(0, storage.getAllItems().length);
    }

    @Test
    public void get_all_items() {
        Item[] items = storage.getAllItems();

        assertEquals(4, items.length);

        String[] expected = new String[4];
        String[] actual = new String[4];

        for (int i = 0; i < 4; i++) {
            expected[i] = testItems.get(i).getName();
            actual[i] = items[i].getName();
        }

        Arrays.sort(expected);
        Arrays.sort(actual);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void remove_item_not_found() {
        assertFalse(storage.removeItem(new Item("NotFound", 48, 277, true, false)));
    }

    @Test
    public void remove_item_found() {
        assertTrue(storage.removeItem(new Item("Missing", 48, 277, true, false)));
        assertEquals(3, storage.getAllItems().length);
    }

    @Test
    public void add_merge_instances() {
        storage = new ItemStorage(2);
        storage.addItem(new Item("Double", 10, 99));
        storage.addItem(new Item("Double", 10, 99));
        Item[] items = storage.getAllItems();
        assertEquals(1, items.length);
        assertEquals(20, items[0].getCount(), 0);
        assertEquals(99, items[0].getPrice(), 0);
    }

    @Test
    public void add_stack_instances() {
        storage = new ItemStorage(2);
        storage.addItem(new Item("Double", 10, 99));
        storage.addItem(new Item("Double", 10, 75));
        Item[] items = storage.getAllItems();
        assertEquals(2, items.length);
        assertEquals(10, items[1].getCount(), 0);
        assertEquals(99, items[1].getPrice(), 0);
        assertEquals(10, items[0].getCount(), 0);
        assertEquals(75, items[0].getPrice(), 0);
    }

    @Test
    public void add_divide_instances() {
        storage = new ItemStorage(2);
        storage.addItem(new Item("Triple", 10, 75));
        storage.addItem(new Item("Triple", 10, 99));
        storage.addItem(new Item("Triple", 10, 99, true, true));
        assertEquals(3, storage.getAllItems().length);
    }

    @Test
    public void get_items_not_found() {
        Item[] items = storage.getItems("NotFound");
        assertEquals(0, items.length);
    }

    @Test
    public void get_item_found() {
        Item[] items = storage.getItems("Reference");
        assertEquals(1, items.length);
    }

    @Test
    public void get_items_found() {
        storage.addItem(new Item("Triple", 10, 75));
        storage.addItem(new Item("Triple", 10, 99));
        storage.addItem(new Item("Triple", 10, 99, true, true));
        Item[] items = storage.getItems("Triple");
        assertEquals(3, items.length);
    }
}
