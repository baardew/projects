package no.wintherproduksjoner.synthservice.db.engine;

import no.wintherproduksjoner.synthservice.db.engine.Core;
import no.wintherproduksjoner.synthservice.db.engine.ClientServer;
import no.wintherproduksjoner.synthservice.db.engine.TestClientServer;
import no.wintherproduksjoner.synthservice.db.engine.FileHandle;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Status;
import no.wintherproduksjoner.synthservice.db.storage.Item;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;
import no.wintherproduksjoner.synthservice.db.tool.AccountSetting;

import testutils.Testutil;
import no.wintherproduksjoner.synthservice.db.engine.QueueNoComparator;

import java.util.ArrayList;
import java.util.Arrays;

import java.io.File;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.ResultSet;

import java.lang.reflect.Method;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

@SuppressWarnings("unchecked")
public class TestCore implements Monitor {

    private final String psw = "treveark_05+NOT";

    private ClientServer serv;
    private Core core;
    private Synthesizer testsynth[];
    private Item item[];

    private File f_reg;
    private File f_arch;
    private File f_stor;

    // Required by Monitor
    public void setMessage(String message){}
    public void start(){}
    public void stop(){}

    /* Copied from TestClientServer START */
    public void executeUpdate(String query) {
        Connection c = (Connection)Testutil.getPrivateField(serv, "server");
        Statement q;
        try {
            q = c.createStatement();
            q.executeUpdate(query);
            q.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet executeQuery(String query) {
        Connection c = (Connection)Testutil.getPrivateField(serv, "server");
        Statement q;
        try {
            q = c.createStatement();
            return q.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void insertTestSet_1() {
        executeUpdate("INSERT INTO synthrepairform VALUES (2076784497 , 1, CURTIME(), TRUE, FALSE, 'Petter Smart', 'S0+3', 'sgz', 'Missing the latest patch.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (2076784497, 'Donald', 'Duck', 'PetterSkrue AS', 'Veien 313', 1441, 'Andeby', 80055522, 'donald@andeby.co')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1203130041 , 2, CURTIME(), TRUE, FALSE, 'Bølge', 'nummer 5', '34r', 'Mangler fustasjeopphengsforkoblere.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1203130041, 'Lærsen', 'Bølge', 'Bøttekott', 'Næsset 25', 1444, 'Ørstad', 35362362, 'nordmann.laersen@naesset.no')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1480484070 , 3, CURTIME(), TRUE, FALSE, 'Petter Smart', 'PHP', 'klistrelapp ole', 'Ingen lyd på minijack.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1480484070, 'Ole', '', '', '', -1, '', 80055522, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1219444047 , 4, CURTIME(), TRUE, FALSE, 'Petter Smart', 'PHP', 'klistrelapp dole', 'A key does not work.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1219444047, 'Dole', '', '', '', -1, '', 80055522, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1316994994 , 5, CURTIME(), TRUE, FALSE, 'Petter Smart', 'PHP', 'klistrelapp doffen', 'Missing a capacitor.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1316994994, 'Doffen', '', '', '', -1, '', 80055522, '')");
    }

    public void insertTestSet_2() {
        executeUpdate("INSERT INTO synthrepairform VALUES (1828741486 , 6, CURTIME(), TRUE, FALSE, 'AD', 'Wood', '12', '', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1828741486, 'Mr. Meister', '', 'TreverkVirke', 'Osloveien 15, Filleveien', 13, 'Ørstad', -1, 'noe@borte.her')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1812321906 , 7, CURTIME(), TRUE, FALSE, 'Boat', 'ok', 'oy', '', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1812321906, '', '', 'WintherStormer', '', -1, '', -1, 'terje.unof@unof.no')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1052172376 , 8, CURTIME(), TRUE, FALSE, 'Bad People', '2345', '456', '<form id=\"clientform\" action=\"register\" method=\"POST\" >\n<label>First Name:</label> <input type=\"txt\" name=\"firstname\" size=\"50\" maxlength=\"50\" /><br />\n<label>Last Name:</label> <input type=\"txt\" name=\"lastname\" size=\"50\" maxlength=\"50\" /><br', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1052172376, 'CURDATE()', '<b>NAME</b>', '[spamtest]', 'ok, indeed XD!', 2, 'Gågata', 12345678, 'spammer@spammer.spam')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1784694841 , 9, CURTIME(), TRUE, FALSE, 'Møøg', 'nummer 5', '1945', 'A lot of screws have broken the PCB: needs to be fixed like now', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1784694841, '', 'Bølge', '', '', -1, '', 35362362, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1254819177 , 10, CURTIME(), TRUE, FALSE, 'Petter Smart', 'Modylar', 'asd', 'Missing:\n- control panel\n- keys\n- resistors\n- transistors', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1254819177, 'Fisherman', '', 'Beacon', '', -1, '', -1, 'somehting.soaj.arig@iaghaiergh.ca')");
    }

    public void insertTestSet_3() {
        executeUpdate("INSERT INTO synthrepairform VALUES (1280751016 , 11, CURTIME(), TRUE, FALSE, 'Petter Smart', 'S0+3', 'grønne lysdioder', 'Missing a huge part, but cannot tell what that is. In addition there are no power coming into the synth...', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1280751016, 'Nordmann', '', '', 'Trespasser no 1', 352, 'Skygger', 41467476, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1000734979 , 12, CURTIME(), TRUE, FALSE, 'AD', 'mvst', '456f', 'The main matrix sampler has too many values.\nThe metro bus has broken lanes.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1000734979, '', 'Anderson', '', '', -1, '', 57632985, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (2030981012 , 13, CURTIME(), TRUE, FALSE, 'OD', 'Wood', '456h', '', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (2030981012, '', '', 'Monolithic', '', -1, '', -1, 'kernel.apple@layer.org')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1018403300 , 14, CURTIME(), TRUE, FALSE, 'Moog', 'Modylar', 'h-332', 'Broken:\n- pitch\n- wave\n- filers (all of them)\n- mini-jack stereo out', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1018403300, '', '', 'Modular', '', -1, '', -1, 'a@a.b')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1419839937 , 15, CURTIME(), TRUE, FALSE, 'Moog', 'Modylar', 'h-333', 'None of the outputs gives a signal.\nMust replace the main capacitors.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1419839937, 'Old', 'Myklesvendsen', '', '', -1, '', 42252555, '')");
    }
    /* Copied from TestClientServer END */


    @Before
    public void init() {
        FileHandle.setHomeDirectory("test/files/");

        testsynth = new Synthesizer[7];
        testsynth[0] = new Synthesizer(12345678, 1, "", "first", "second", "firm1", "address1", 1450, "postName",
                                       55512378, "email1", "manufacturer", "model", "serial 1", "fault");
        testsynth[1] = new Synthesizer(84002759, 2, "", "third", "fourth", "firm1", "address1", 1450, "postName",
                                       55512378, "email1", "manufacturer", "model", "serial 2", "fault");
        testsynth[2] = new Synthesizer(95038266, 3, "", "fifth", "sixth", "1_firm", "1_address", 1664, "namePost",
                                       55588712, "1_email@not", "facturman", "lodema", "784g-ff", "segm");
        testsynth[3] = new Synthesizer(85098832, 4, "", "seventh", "eight", "_F_", "_A_", 8593, "_NP_",
                                       55599671, "_@_", "_M_", "_M_", "_S_", "_F_");
        testsynth[4] = new Synthesizer(11849744, 5, "", "ninth", "tenth", "F__F", "A__A", 7357, "NP_NP",
                                       55599671, "at", "man", "mod", "ser", "err");
        testsynth[5] = new Synthesizer(15675221, 15, "", "duo", "quda", "tatata", "tunnel", 4458, "place",
                                       55578545, "sub", "common", "tone", "63", "code");
        testsynth[6] = new Synthesizer(25987531, 10, "", "penta", "hex", "under", "hollow", 1597, "green",
                                       55519985, "underline", "vertical", "bell", "i5-f", "pipe");

        f_reg = new File("test/files/register.wpdb");
        f_arch = new File("test/files/archive.wpdb");
        f_stor = new File("test/files/storage.wpdb");

        AccountSetting setting = new AccountSetting();
        setting.setAccount(psw, "localmachine", "WintherProduksjoner", "127.0.0.1", "3306");

        serv = new ClientServer(setting);
        if (!serv.connect(this))
            throw new RuntimeException("Unable to connect to test server");

        core = new Core(this, setting);

        ArrayList<Synthesizer> register = (ArrayList<Synthesizer>)Testutil.getPrivateField(core, "register");
        for (Synthesizer s : testsynth)
            register.add(s);

        item = new Item[4];
        item[0] = new Item("123", 129.99, 4);
        item[1] = new Item("ABC", 50.0, 0);
        item[2] = new Item("Banana", 3, 55);
        item[3] = new Item("Potato", 400, 3.5);

        for (Item i: item)
            core.getStorage().addItem(i);
    }

    public void verifyQueue(Synthesizer[] synth) {
        Arrays.sort(synth, new QueueNoComparator());

        int q = 1;
        for (int i = 0; i < synth.length; i++)
            if (synth[i].getStatus() == Status.InQueue)
                assertEquals(q++, synth[i].getQueueNo());
    }

    @After
    public void exit() {
        f_reg.delete();
        f_arch.delete();
        f_stor.delete();

        executeUpdate("DELETE FROM clientcontact");
        executeUpdate("DELETE FROM synthrepairform");
        executeUpdate("DELETE FROM repairlist");

        serv.disconnect(this);

        serv = null;
        testsynth = null;
        item = null;
        f_reg = null;
        f_arch = null;
        f_stor = null;
        core = null;
    }

    @Test
    public void save() {
        assertTrue(core.save(this));
    }

    @Test
    public void load_from_file() {
        assertTrue(core.save(this));
        core = null;

        AccountSetting setting = new AccountSetting();
        setting.setAccount(psw, "localmachine", "WintherProduksjoner", "127.0.0.1", "3306");

        core = new Core(this, setting);
        assertEquals(7, core.getRegisterSize());
        assertEquals(4, core.getStorage().getAllItems().length);
    }

    @Test
    public void get_all_synthesizers() {
        Synthesizer[] synth = core.getAllSynthesizers();
        assertArrayEquals(testsynth, synth);
    }

    @Test
    public void fix_queue_numbers() {
        Method fixQueueNumbers = Testutil.getPrivateMethod(core, "fixQueueNumbers");
        Testutil.invokePrivateMethod(core, fixQueueNumbers);
        Synthesizer[] synth = core.getAllSynthesizers();
        Arrays.sort(synth, new QueueNoComparator());

        int[] expected = {1, 2, 3, 4, 5, 6, 7};
        int[] actual = new int[7];

        for (int i = 0; i < 7; i++)
            actual[i] = synth[i].getQueueNo();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void accept_synthesizer() {
        Method fixQueueNumbers = Testutil.getPrivateMethod(core, "fixQueueNumbers");
        Testutil.invokePrivateMethod(core, fixQueueNumbers);

        core.acceptSynthesizer();
        Synthesizer[] synth = core.getAllSynthesizers();
        Arrays.sort(synth, new QueueNoComparator());

        assertEquals(Status.ReadyForDelivery, synth[0].getStatus());
        verifyQueue(synth);
    }

    @Test
    public void decline_synthesizer_first_in_queue() {
        Method fixQueueNumbers = Testutil.getPrivateMethod(core, "fixQueueNumbers");
        Testutil.invokePrivateMethod(core, fixQueueNumbers);

        Synthesizer[] synth = core.getAllSynthesizers();
        Arrays.sort(synth, new QueueNoComparator());
        Synthesizer reject = synth[0];
        core.declineSynthesizer(synth[0]);

        assertEquals(Status.Declined, reject.getStatus());
        verifyQueue(synth);
    }

    @Test
    public void declineSynthesizer_middle_in_queue() {
        Method fixQueueNumbers = Testutil.getPrivateMethod(core, "fixQueueNumbers");
        Testutil.invokePrivateMethod(core, fixQueueNumbers);

        Synthesizer[] synth = core.getAllSynthesizers();
        Synthesizer reject = synth[5];
        Arrays.sort(synth, new QueueNoComparator());
        core.declineSynthesizer(reject);

        assertEquals(Status.Declined, reject.getStatus());
        verifyQueue(synth);
    }

    @Test
    public void read_from_archive_empty() {
        Synthesizer[] arc = core.readFromArchive(this, 0, 5);
        assertEquals(0, arc.length);
    }

    @Test
    public void get_archive_size_empty() {
        assertEquals(0, core.getArchiveSize(this));
    }

    @Test
    public void write_to_archive_verify_size() {
        assertTrue(core.writeToArchive(this, testsynth[0]));
        assertEquals(1, core.getArchiveSize(this));
        assertTrue(core.writeToArchive(this, testsynth[4]));
        assertEquals(2, core.getArchiveSize(this));
        assertTrue(core.writeToArchive(this, testsynth[6]));
        assertEquals(3, core.getArchiveSize(this));
    }

    @Test
    public void read_from_archive_start() throws Exception {
        for (Synthesizer s : testsynth) {
            Thread.sleep(1100);
            s.updateModified();
            core.writeToArchive(this, s);
        }

        Synthesizer[] arc = core.readFromArchive(this, 0, 2);
        assertEquals(2, arc.length);
        assertEquals(testsynth[6].createOwnerString(), arc[0].createOwnerString());
        assertEquals(testsynth[5].createOwnerString(), arc[1].createOwnerString());
    }

    @Test
    public void read_from_archive_end() throws Exception {
        for (Synthesizer s : testsynth) {
            Thread.sleep(1100);
            s.updateModified();
            core.writeToArchive(this, s);
        }

        assertEquals(7, core.getArchiveSize(this));
        Synthesizer[] arc = core.readFromArchive(this, 5, 7);
        assertEquals(2, arc.length);
        assertEquals(testsynth[1].createOwnerString(), arc[0].createOwnerString());
        assertEquals(testsynth[0].createOwnerString(), arc[1].createOwnerString());
    }

    @Test
    public void synchronize_half() {
        insertTestSet_1();
        insertTestSet_2();
        assertTrue(core.synchronize(this));
    }

    @Test
    public void synchronize_full() {
        insertTestSet_1();
        insertTestSet_2();
        assertTrue(core.synchronize(this));
        insertTestSet_3();
        assertTrue(core.synchronize(this));
    }

    @Test
    public void system_clean() {
        insertTestSet_1();
        insertTestSet_2();
        insertTestSet_3();
        core.synchronize(this);
        assertTrue(core.systemClean(this));
    }
}
