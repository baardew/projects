package no.wintherproduksjoner.synthservice.db.engine;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Status;
import no.wintherproduksjoner.synthservice.db.storage.RepairListItem;

import no.wintherproduksjoner.synthservice.db.engine.ClientServer;
import no.wintherproduksjoner.synthservice.db.tool.Monitor;
import no.wintherproduksjoner.synthservice.db.tool.CoreUtil;
import no.wintherproduksjoner.synthservice.db.tool.AccountSetting;

import testutils.Testutil;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.ResultSet;

import java.util.Date;

import java.io.File;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestClientServer implements Monitor {

    private final String psw = "treveark_05+NOT";
    private ClientServer serv;

    // Required by monitor
    public void setMessage(String message){}
    public void start(){}
    public void stop(){}

    public void executeUpdate(String query) {
        Connection c = (Connection)Testutil.getPrivateField(serv, "server");
        Statement q;
        try {
            q = c.createStatement();
            q.executeUpdate(query);
            q.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet executeQuery(String query) {
        Connection c = (Connection)Testutil.getPrivateField(serv, "server");
        Statement q;
        try {
            q = c.createStatement();
            return q.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void insertTestSet_1() {
        executeUpdate("INSERT INTO synthrepairform VALUES (2076784497 , 1, CURTIME(), TRUE, FALSE, 'Petter Smart', 'S0+3', 'sgz', 'Missing the latest patch.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (2076784497, 'Donald', 'Duck', 'PetterSkrue AS', 'Veien 313', 1441, 'Andeby', 80055522, 'donald@andeby.co')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1203130041 , 2, CURTIME(), TRUE, FALSE, 'Bølge', 'nummer 5', '34r', 'Mangler fustasjeopphengsforkoblere.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1203130041, 'Lærsen', 'Bølge', 'Bøttekott', 'Næsset 25', 1444, 'Ørstad', 35362362, 'nordmann.laersen@naesset.no')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1480484070 , 3, CURTIME(), TRUE, FALSE, 'Petter Smart', 'PHP', 'klistrelapp ole', 'Ingen lyd på minijack.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1480484070, 'Ole', '', '', '', -1, '', 80055522, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1219444047 , 4, CURTIME(), TRUE, FALSE, 'Petter Smart', 'PHP', 'klistrelapp dole', 'A key does not work.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1219444047, 'Dole', '', '', '', -1, '', 80055522, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1316994994 , 5, CURTIME(), TRUE, FALSE, 'Petter Smart', 'PHP', 'klistrelapp doffen', 'Missing a capacitor.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1316994994, 'Doffen', '', '', '', -1, '', 80055522, '')");
    }

    public void insertTestSet_2() {
        executeUpdate("INSERT INTO synthrepairform VALUES (1828741486 , 6, CURTIME(), TRUE, FALSE, 'AD', 'Wood', '12', '', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1828741486, 'Mr. Meister', '', 'TreverkVirke', 'Osloveien 15, Filleveien', 13, 'Ørstad', -1, 'noe@borte.her')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1812321906 , 7, CURTIME(), TRUE, FALSE, 'Boat', 'ok', 'oy', '', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1812321906, '', '', 'WintherStormer', '', -1, '', -1, 'terje.unof@unof.no')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1052172376 , 8, CURTIME(), TRUE, FALSE, 'Bad People', '2345', '456', '<form id=\"clientform\" action=\"register\" method=\"POST\" >\n<label>First Name:</label> <input type=\"txt\" name=\"firstname\" size=\"50\" maxlength=\"50\" /><br />\n<label>Last Name:</label> <input type=\"txt\" name=\"lastname\" size=\"50\" maxlength=\"50\" /><br', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1052172376, 'CURDATE()', '<b>NAME</b>', '[spamtest]', 'ok, indeed XD!', 2, 'Gågata', 12345678, 'spammer@spammer.spam')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1784694841 , 9, CURTIME(), TRUE, FALSE, 'Møøg', 'nummer 5', '1945', 'A lot of screws have broken the PCB: needs to be fixed like now', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1784694841, '', 'Bølge', '', '', -1, '', 35362362, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1254819177 , 10, CURTIME(), TRUE, FALSE, 'Petter Smart', 'Modylar', 'asd', 'Missing:\n- control panel\n- keys\n- resistors\n- transistors', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1254819177, 'Fisherman', '', 'Beacon', '', -1, '', -1, 'somehting.soaj.arig@iaghaiergh.ca')");
    }

    public void insertTestSet_3() {
        executeUpdate("INSERT INTO synthrepairform VALUES (1280751016 , 11, CURTIME(), TRUE, FALSE, 'Petter Smart', 'S0+3', 'grønne lysdioder', 'Missing a huge part, but cannot tell what that is. In addition there are no power coming into the synth...', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1280751016, 'Nordmann', '', '', 'Trespasser no 1', 352, 'Skygger', 41467476, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1000734979 , 12, CURTIME(), TRUE, FALSE, 'AD', 'mvst', '456f', 'The main matrix sampler has too many values.\nThe metro bus has broken lanes.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1000734979, '', 'Anderson', '', '', -1, '', 57632985, '')");

        executeUpdate("INSERT INTO synthrepairform VALUES (2030981012 , 13, CURTIME(), TRUE, FALSE, 'OD', 'Wood', '456h', '', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (2030981012, '', '', 'Monolithic', '', -1, '', -1, 'kernel.apple@layer.org')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1018403300 , 14, CURTIME(), TRUE, FALSE, 'Moog', 'Modylar', 'h-332', 'Broken:\n- pitch\n- wave\n- filers (all of them)\n- mini-jack stereo out', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1018403300, '', '', 'Modular', '', -1, '', -1, 'a@a.b')");

        executeUpdate("INSERT INTO synthrepairform VALUES (1419839937 , 15, CURTIME(), TRUE, FALSE, 'Moog', 'Modylar', 'h-333', 'None of the outputs gives a signal.\nMust replace the main capacitors.', 'In Queue', '')");
        executeUpdate("INSERT INTO clientcontact VALUES (1419839937, 'Old', 'Myklesvendsen', '', '', -1, '', 42252555, '')");
    }

    private boolean modifiedMatches(Synthesizer synth, String sql) {
        Date expected =  CoreUtil.stringToDate(synth.getModified());
        Date actual = CoreUtil.stringToDate(sql);

        if (expected.equals(actual))
            return true;

        // If we hit a second boundary difference, ensure we have that covered as well
        expected.setTime(expected.getTime() + 1000);
        if (expected.equals(actual))
            return true;

        // If we hit a second boundary difference, ensure we have that covered as well
        // If we are really unfortunate, it might be 2 seconds
        expected.setTime(expected.getTime() + 1000);
        if (expected.equals(actual))
            return true;

        assertEquals(expected.getTime(), actual.getTime());
        return false;
    }

    private void testUpdate(Synthesizer[] synth) throws Exception {
        ResultSet set;
        int ret;

        // Timestamp operates on seconds,
        // so need to wait that long before changes are recorded
        Thread.sleep(1100);

        synth[1].updateModified();
        synth[3].updateModified();

        Thread.sleep(1100);

        ret = serv.uploadUpdates(synth, this);
        assertEquals(2, ret);

        set = executeQuery("SELECT * FROM synthrepairform WHERE referenceno = " + synth[1].getReferenceNo());
        set.next();
        assertTrue(modifiedMatches(synth[1], set.getString("modified").substring(0, 19)));

        set = executeQuery("SELECT * FROM synthrepairform WHERE referenceno = " + synth[3].getReferenceNo());
        set.next();
        assertTrue(modifiedMatches(synth[3], set.getString("modified").substring(0, 19)));
    }

    @Before
    public void init() {
        File f_reg = new File("test/files/register.wpdb");
        if (f_reg.exists())
            f_reg.delete();
        File f_arch = new File("test/files/archive.wpdb");
        if (f_arch.exists())
            f_arch.delete();
        File f_stor = new File("test/files/storage.wpdb");
        if (f_stor.exists())
            f_stor.delete();

        AccountSetting setting = new AccountSetting();
        setting.setAccount(psw, "localmachine", "WintherProduksjoner", "127.0.0.1", "3306");
        serv = new ClientServer(setting);
        if (!serv.connect(this))
            throw new RuntimeException("Unable to connect to test server");
    }

    @After
    public void exit() {
        executeUpdate("DELETE FROM clientcontact");
        executeUpdate("DELETE FROM synthrepairform");
        executeUpdate("DELETE FROM repairlist");
        serv.disconnect(this);
        serv = null;
    }

    @Test
    public void download_new() {
        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        assertEquals(5, synth.length);
    }

    @Test
    public void download_new_reference_no_check() {
        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);

        int[] expected = {2076784497, 1203130041, 1480484070, 1219444047, 1316994994};
        int[] actual = new int[5];

        for (int i = 0; i < synth.length; i++)
            actual[i] = synth[i].getReferenceNo();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void register_mew_download_cleanup_verification() throws SQLException {
        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        ResultSet set = executeQuery("SELECT COUNT(*) AS cnt FROM synthrepairform WHERE newinstance IS TRUE");
        set.next();
        assertEquals(0, set.getInt("cnt"));

        set = executeQuery("SELECT COUNT(*) AS cnt FROM clientcontact");
        set.next();
        assertEquals(0, set.getInt("cnt"));
    }

    @Test
    public void upload_updates_none() {
        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        assertEquals(0, serv.uploadUpdates(synth, this));
    }

    @Test
    public void upload_updates_two_updates_cleanly() throws Exception {
        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        testUpdate(synth);
    }

    @Test
    public void upload_updates_inorder_local() throws Exception {
        Synthesizer[] synth_fake = new Synthesizer[6];

        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        Thread.sleep(1100);
        testUpdate(synth);

        System.arraycopy(synth, 0, synth_fake, 0, 5);
        synth_fake[5] = new Synthesizer(1501010010, 1, "", "first", "second", "firm1", "address1", 1450, "postName",
                                        55512378, "email1", "manufacturer", "model", "serial 1", "fault");

        testUpdate(synth_fake);
    }

    @Test
    public void upload_updates_inorder_server() throws Exception {
        insertTestSet_2();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        testUpdate(synth);
    }

    @Test
    public void upload_updates_inorder_both() throws Exception {
        insertTestSet_1();
        insertTestSet_2();
        Synthesizer[] synth_fake = new Synthesizer[6];
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        System.arraycopy(synth, 0, synth_fake, 0, 5);
        synth_fake[5] = new Synthesizer(1501010010, 1, "", "first", "second", "firm1", "address1", 1450, "postName",
                                        55512378, "email1", "manufacturer", "model", "serial 1", "fault");

        testUpdate(synth);
        testUpdate(synth_fake);
    }

    @Test
    public void download_new_remaining() {
        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        insertTestSet_2();
        synth = serv.downloadNew(this);
        assertEquals(5, synth.length);
    }

    @Test
    public void decline_is_updated() throws Exception {
        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        int refno = synth[2].getReferenceNo();
        Thread.sleep(1100);

        assertTrue(synth[2].decline());

        // Upload updates sorts synth, so it cannot be used again after this point
        serv.uploadUpdates(synth, this);

        ResultSet set = executeQuery("SELECT * FROM synthrepairform WHERE referenceno = " + refno);
        set.next();
        assertTrue(modifiedMatches(synth[2], set.getString("modified").substring(0, 19)));
        assertEquals(Status.Declined.toString(), set.getString("status"));
    }

    @Test
    public void archive_synthesizers() throws SQLException {
        int[] archive = {1480484070, 1219444047, 1316994994};

        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        assertTrue(serv.archiveSynthesizers(archive, this));

        ResultSet set = executeQuery("SELECT COUNT(*) as count FROM synthrepairform WHERE archived = TRUE");
        set.next();
        assertEquals(3, set.getInt("count"));
    }

    @Test
    public void clean_server() throws SQLException {
        int[] archive = {1480484070, 1219444047, 1316994994};

        insertTestSet_1();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        assertTrue(serv.archiveSynthesizers(archive, this));

        assertTrue(serv.cleanServer(this));
        ResultSet set = executeQuery("SELECT * FROM repairlist");
        set.next();
        assertEquals(3, set.getInt("repairs"));
    }

    @Test
    public void archive_synthesizers_additional() throws SQLException {
        int[] archive0 = {1480484070, 1219444047, 1316994994};
        int[] archive1 = {1254819177, 1018403300, 1419839937};

        insertTestSet_1();
        insertTestSet_2();
        insertTestSet_3();
        Synthesizer[] synth = serv.downloadNew(this);
        serv.registerNewDownload(synth, this);

        serv.archiveSynthesizers(archive0, this);
        serv.archiveSynthesizers(archive1, this);

        assertTrue(serv.cleanServer(this));
        ResultSet set = executeQuery("SELECT COUNT(*) AS cnt FROM repairlist");
        set.next();
        assertEquals(3, set.getInt("cnt"));
    }

    @Test
    public void download_repair_list() {
        int[] archive = {1480484070, 1219444047, 1316994994, 1254819177, 1018403300, 1419839937};

        insertTestSet_1();
        insertTestSet_2();
        insertTestSet_3();
        Synthesizer[] synth = serv.downloadNew(this);

        serv.registerNewDownload(synth, this);
        serv.archiveSynthesizers(archive, this);
        serv.cleanServer(this);

        RepairListItem[] repair = serv.downloadRepairList(this);
        assertEquals(3, repair.length);
    }

    @Test
    public void update_repair_list() throws SQLException {
        int[] archive = {1480484070, 1219444047, 1316994994, 1254819177, 1018403300, 1419839937};

        insertTestSet_1();
        insertTestSet_2();
        insertTestSet_3();
        Synthesizer[] synth = serv.downloadNew(this);

        serv.registerNewDownload(synth, this);
        serv.archiveSynthesizers(archive, this);
        serv.cleanServer(this);

        RepairListItem[] repair = serv.downloadRepairList(this);
        RepairListItem ni = new RepairListItem("Moog", repair[1].getModel(), repair[1].getRepairs());
        assertTrue(serv.updateRepairList(repair[1], ni, this));

        ResultSet set = executeQuery("SELECT COUNT(*) AS cnt FROM repairlist");
        set.next();
        assertEquals(2, set.getInt("cnt"));

        set = executeQuery("SELECT * FROM repairlist WHERE manufacturer = 'Moog'");
        set.next();
        assertEquals(3, set.getInt("repairs"));
    }
}
