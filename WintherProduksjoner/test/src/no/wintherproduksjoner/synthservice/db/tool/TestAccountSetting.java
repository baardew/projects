package no.wintherproduksjoner.synthservice.db.tool;

import testutils.Testutil;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestAccountSetting {

    private final String fileName = "accountsetting.ser";

    private AccountSetting acc;

    @Before
    public void init() {
        acc = new AccountSetting();
        acc.setAccount("pass", "use", "db", "serv", "port");
    }

    @After
    public void exit() {
        acc = null;
        Testutil.removeFile(fileName);
    }

    @Test
    public void getters_and_setter() {
        assertEquals("pass", acc.getPassword());
        assertEquals("use", acc.getUsername());
        assertEquals("db", acc.getDatabase());
        assertEquals("serv", acc.getServerAddress());
        assertEquals("port", acc.getPort());
    }

    @Test
    public void serializable() {
        String serres = Testutil.serialize(fileName, acc);
        if (serres != null)
            fail(serres);
    }

    @Test
    public void deserializable() {
        String serres = Testutil.serialize(fileName, acc);

        acc = null;
        acc = (AccountSetting)Testutil.deserialize(fileName);
        assertNotNull(acc);
    }

    @Test
    public void correctly_deserialized() {
        String serres = Testutil.serialize(fileName, acc);
        acc = null;
        acc = (AccountSetting)Testutil.deserialize(fileName);

        getters_and_setter();
    }

    @Test
    public void save_succeeds() {
        assertTrue(acc.save());
    }

    @Test
    public void read_succeeds() {
        acc.save();
        acc = new AccountSetting();
        assertTrue(acc.read());
    }

    @Test
    public void save_and_read_works_correctly() {
        acc.save();
        acc = new AccountSetting();
        acc.read();

        getters_and_setter();
    }

    @Test
    public void getURL_correctly_created() {
        assertEquals("jdbc:mysql://serv:port/db", acc.getURL());
    }
}
