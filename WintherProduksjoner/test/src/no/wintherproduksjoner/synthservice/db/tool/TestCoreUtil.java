package no.wintherproduksjoner.synthservice.db.tool;

import java.util.Date;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestCoreUtil {

    @Test
    public void stringToDate() {
        Date d1 = new Date(0L);
        Date d2 = CoreUtil.stringToDate("1970-01-01 01:00:00"); // +1 hour due to timezone

        assertEquals(d1.toString(), d2.toString());
    }
}
