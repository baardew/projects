package no.wintherproduksjoner.synthservice.db.tool;

import no.wintherproduksjoner.synthservice.db.tool.ItemBroker;
import no.wintherproduksjoner.synthservice.db.tool.Monitor;
import no.wintherproduksjoner.synthservice.db.tool.AccountSetting;

import no.wintherproduksjoner.synthservice.db.engine.Core;

import no.wintherproduksjoner.synthservice.db.storage.Synthesizer;
import no.wintherproduksjoner.synthservice.db.storage.Item;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestItemBroker implements Monitor {

    private Core core;
    private ItemBroker ib;

    // Required by Monitor
    public void setMessage(String message){}
    public void start(){}
    public void stop(){}

    @Before
    public void init() {
        core = new Core(this, new AccountSetting());
        ib = new ItemBroker(core.getStorage());
    }

    @After
    public void exit() {
        core = null;
        ib = null;
    }

    @Test
    public void add_regular() {
        Item stuff[] = ib.add("Simple", 1, 5.5, true, false);
        assertEquals(1, stuff.length);
    }

    @Test
    public void add_reference() {
        core.getStorage().addItem(new Item("Reference", 12, 100, true, false));
        Item stuff[] = ib.add("Reference", 5, 100, true, true);

        assertEquals(1, stuff.length);
        assertEquals(100, stuff[0].getPrice(), 0);

        Item store = core.getStorage().getItem("Reference", 100, true);
        assertEquals(7, store.getCount(), 0);
    }

    @Test
    public void add_new_reference() {
        Item stuff[] = ib.add("Create", 3, 59.99, true, true);
        assertEquals(1, stuff.length);

        Item store = core.getStorage().getItem("Create", 59.99, true);
        assertEquals(0, store.getCount(), 0);
    }

    @Test
    public void add_reference_split() {
        core.getStorage().addItem(new Item("Reference", 7, 100, true, false));
        Item stuff[] = ib.add("Reference", 16, 100, true, true);
        Item store = core.getStorage().getItem("Reference", 100, true);
        assertEquals(2, stuff.length);

    }

    @Test
    public void add_reference_split_dependency() {
        core.getStorage().addItem(new Item("Reference", 7, 100, true, false));
        Item stuff[] = ib.add("Reference", 16, 100, true, true);
        Item store = core.getStorage().getItem("Reference", 100, true);

        assertTrue(stuff[0].getFromStorage());
        assertFalse(stuff[1].getFromStorage());
        assertEquals(7, stuff[0].getCount(), 0);
        assertEquals(9, stuff[1].getCount(), 0);
        assertEquals(0, store.getCount(), 0);
    }

    @Test
    public void remove_regular() {
        core.getStorage().addItem(new Item("Reference", 7, 100, true, false));
        Item stuff[] = ib.add("Reference", 16, 100, true, true);

        assertFalse(ib.remove(stuff[1]));
    }

    @Test
    public void remove_reference() {
        core.getStorage().addItem(new Item("Reference", 7, 100, true, false));
        Item stuff[] = ib.add("Reference", 16, 100, true, true);

        assertTrue(ib.remove(stuff[0]));
    }

    @Test
    public void remove_missing_in_storage() {
        Item store = new Item("Empty", 0, 0, true, true);
        assertTrue(ib.remove(store));
        assertNotNull(core.getStorage().getItem("Empty", 0, true));
    }

    @Test
    public void search_nothing() {
        Item stuff[] = ib.search("nothing");
        assertEquals(0, stuff.length);
    }

    @Test
    public void search_regular() {
        core.getStorage().addItem(new Item("Simple", 66, 13));
        Item stuff[] = ib.search("Simple");
        assertEquals(1, stuff.length);
        assertEquals("Simple", stuff[0].getName());
    }

    @Test
    public void search_list() {
        core.getStorage().addItem(new Item("Simple", 66, 13));
        core.getStorage().addItem(new Item("Sjmple", 31, 44));

        Item stuff[] = ib.search("Simple");
        assertEquals(2, stuff.length);
    }

    @Test
    public void search_doubleled_items() {
        core.getStorage().addItem(new Item("Simple", 31, 44));
        core.getStorage().addItem(new Item("Simple", 13, 66));
        Item stuff[] = ib.search("Simple");
        assertEquals(2, stuff.length);
    }

    @Test
    public void remove_order_merge() {
        core.getStorage().addItem(new Item("OrderMerge", 1, 150, true, true));
        core.getStorage().addItem(new Item("OrderMerge", 1, 100, true, true));
        core.getStorage().addItem(new Item("OrderMerge", 1, 100, false, true));

        ib.remove(new Item("OrderMerge", 1, 150, true, true));
        Item store = core.getStorage().getItem("OrderMerge", 150, true);
        assertEquals(2, store.getCount(), 0);

        ib.remove(new Item("OrderMerge", 1, 100, true, true));
        store = core.getStorage().getItem("OrderMerge", 100, true);
        assertEquals(2, store.getCount(), 0);

        ib.remove(new Item("OrderMerge", 1, 100, false, true));
        store = core.getStorage().getItem("OrderMerge", 100, false);
        assertEquals(2, store.getCount(), 0);
    }
}
