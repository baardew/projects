package no.wintherproduksjoner.synthservice.window.tool;

import no.wintherproduksjoner.synthservice.db.tool.Monitor;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import java.io.File;
import java.io.PrintWriter;

public class TestInformationReader implements Monitor {

    /* Required by Monitor */
    public void setMessage(String message){}
    public void start(){}
    public void stop(){}

    private final String simpleContent = "UPDATE!";
    private final String UTFContent = "æøå";
    private final String newlineContent = "UPDATE!\nYour version have been updated.";

    private InformationReader reader;
    private File file;

    @Before
    public void init() {
        InformationReader.setInformationDirectory("test/files/");
        reader = new InformationReader("informationreader_testfile");
        file = new File("test/files/informationreader_testfile");
    }

    private void createTestFile(int content) throws Throwable {
        PrintWriter out = new PrintWriter("test/files/informationreader_testfile");

        switch (content) {
        case 0:
            out.write(simpleContent);
            break;
        case 1:
            out.write(UTFContent);
            break;
        case 2:
            out.write(newlineContent);
            break;
        default:
            break;
        }

        out.close();
    }

    @After
    public void exit() {
        if (file != null)
            file.delete();

        reader = null;
    }

    @Test
    public void no_file_exists() {
        file.delete();
        file = null;
        assertFalse(reader.exists());
    }

    @Test
    public void exists_have_file() throws Throwable {
        createTestFile(0);
        assertTrue(reader.exists());
    }

    @Test
    public void read_file_no_file() {
        file.delete();
        file = null;
        String value = reader.readFile();
        assertNull(value);
    }

    @Test
    public void read_file_correct_content() throws Throwable {
        createTestFile(0);
        String value = reader.readFile();
        assertEquals(simpleContent, value);
    }

    @Test
    public void read_file_correct_utf_content() throws Throwable {
        createTestFile(1);
        String value = reader.readFile();
        assertEquals(UTFContent, value);
    }

    @Test
    public void read_file_correct_advanved_content() throws Throwable {
        createTestFile(2);
        String value = reader.readFile();
        assertEquals(newlineContent, value);
    }

    @Test
    public void delete() throws Throwable {
        createTestFile(0);
        reader.delete();
        assertFalse(reader.exists());
        assertFalse(file.exists());
        file = null;
    }
}
