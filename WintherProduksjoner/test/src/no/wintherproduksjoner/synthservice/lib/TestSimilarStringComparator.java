package no.wintherproduksjoner.synthservice.lib;

import no.wintherproduksjoner.synthservice.lib.SimilarStringComparator;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestSimilarStringComparator {

    private String[] compare(String key, String value) {
        return SimilarStringComparator.compareSimilarString(key, value);
    }

    @Test
    public void case_insensitive() {
        String[] expected = {"case"};
        String[] result = compare("CASE", "case");
        assertArrayEquals(expected, result);
    }

    @Test
    public void equals_single_word() {
        String[] expected = {"potato"};
        String[] result = compare("potato", "potato");
        assertArrayEquals(expected, result);
    }

    @Test
    public void similar_single_word_first_letter() {
        String[] expected = {"eotato"};
        String[] result = compare("potato", "eotato");
        assertArrayEquals(expected, result);
    }

    @Test
    public void similar_single_word_middle_letter() {
        String[] expected = {"potito"};
        String[] result = compare("potato", "potito");
        assertArrayEquals(expected, result);
    }

    @Test
    public void similar_single_word_last_letter() {
        String[] expected = {"potata"};
        String[] result = compare("potato", "potata");
        assertArrayEquals(expected, result);
    }

    @Test
    public void unequal_single_word() {
        String[] result = compare("potato", "banana");
        assertEquals(0, result.length);
    }

    @Test
    public void similar_length_1() {
        String[] result = compare("1", "2");
        assertEquals(0, result.length);
    }

    @Test
    public void similar_length_2() {
        String[] expected = {"tt"};
        String[] result = compare("to", "tt");
        assertArrayEquals(expected, result);
    }

    @Test
    public void similar_length_3() {
        String[] expected = {"tt"};
        String[] result = compare("to", "tt");
        assertArrayEquals(expected, result);
    }

    @Test
    public void similar_length_4() {
        String[] expected = {"tri"};
        String[] result = compare("tre", "tri");
        assertArrayEquals(expected, result);
    }

    @Test
    public void similar_length_5() {
        String[] expected = {"cajes"};
        String[] result = compare("cakes", "cajes");
        assertArrayEquals(expected, result);
    }

    @Test
    public void similar_length_10() {
        String[] expected = {"kompotired"};
        String[] result = compare("computerid", "kompotired");
        assertArrayEquals(expected, result);
    }

    @Test
    public void similar_length_15() {
        String[] expected = {"dhtbaasesirvess"};
        String[] result = compare("databaseservers", "dhtbaasesirvess");
        assertArrayEquals(expected, result);
    }

    @Test
    public void short_key_long_value_end() {
        String[] expected = {"multivaluekey"};
        String[] result = compare("key", "multivaluekey");
        assertArrayEquals(expected, result);
    }

    @Test
    public void short_key_long_value_start() {
        String[] expected = {"key"};
        String[] result = compare("keychains", "key");
        assertArrayEquals(expected, result);
    }

    @Test
    public void long_key_short_value_end() {
        String[] expected = {"key"};
        String[] result = compare("multivaluekey", "key");
        assertArrayEquals(expected, result);
    }

    @Test
    public void many_values_correct() {
        String[] expected = {"orkh", "multifork", "or"};
        String[] result = compare("fork", "orkh fish multifork\nbanana potato\testimate or ffoorrkk");
        assertArrayEquals(expected, result);
    }
}
