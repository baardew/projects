package no.wintherproduksjoner.synthservice.lib.systemlogging;

import no.wintherproduksjoner.synthservice.lib.systemlogging.ProgramLogger;

import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.IOException;
import java.io.BufferedReader;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestProgramLogger {

    private final String snapFile = "test/files/programlogger.log";

    private String readSnapFile() throws IOException {
        File f = new File(snapFile);
        if (!f.exists())
            return snapFile + " not found";

        String line;
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(f));
        while ((line = br.readLine()) != null)
            sb.append(line);

        return sb.toString();
    }

    @Before
    public void init() throws IOException {
        ProgramLogger.initialize(snapFile);
    }

    @After
    public void exit() {
        ProgramLogger.teardown();

        File f = new File(snapFile);
        if (f.exists())
            f.delete();
    }

    @Test
    public void initializedFile_noEvents_emptyFile() throws IOException {
        assertEquals("", readSnapFile());
    }

    @Test
    public void stdout_to_file() throws IOException {
        String testString = "This standard output should be in a file";
        PrintStream oldOut = System.out;
        System.setOut(new PrintStream(new ProgramLogger(), true));
        System.out.println(testString);
        System.setOut(oldOut);
        assertEquals(testString, readSnapFile());
    }

    @Test
    public void stderr_to_file() throws IOException {
        String testString = "This error output should be in a file";
        PrintStream oldErr = System.err;
        System.setErr(new PrintStream(new ProgramLogger(), true));
        System.err.println(testString);
        System.setErr(oldErr);
        assertEquals(testString, readSnapFile());
    }

    @Test
    public void log_to_file() throws IOException {
        String testString = "This test output should be in a file";
        ProgramLogger.log(testString);
        assertEquals(testString, readSnapFile());
    }
}
