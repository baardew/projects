package no.wintherproduksjoner.synthservice.lib.systemlogging;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestFunctionTracerHelper {

    private String s;

    public TestFunctionTracerHelper() {
        // Required by testing framework
    }

    public void setString(String s) {
        this.s = s;
    }

    public String toString() {
        return s;
    }

    @Test
    public void verify() {
        TestFunctionTracerHelper h = new TestFunctionTracerHelper();
        h.setString("8");
        assertEquals("8", h.toString());
    }
}
