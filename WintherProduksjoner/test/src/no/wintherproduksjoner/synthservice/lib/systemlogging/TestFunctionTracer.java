package no.wintherproduksjoner.synthservice.lib.systemlogging;

import no.wintherproduksjoner.synthservice.lib.systemlogging.FunctionTracer;
import no.wintherproduksjoner.synthservice.lib.systemlogging.ProgramLogger;
import no.wintherproduksjoner.synthservice.lib.systemlogging.TestFunctionTracerHelper;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class TestFunctionTracer {

    private String snapFile;
    private String snapDir = "test/files/";

    private String readSnapFile() throws IOException {
        File f = new File(snapDir + snapFile + ".log");
        if (!f.exists())
            return snapDir + snapFile + ".log not found";

        String line;
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(f));
        while ((line = br.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }

        return sb.toString();
    }

    public void init(String file) throws IOException {
        snapFile = file;
        File f = new File(snapDir + snapFile + ".log");
        if (f.exists())
            f.delete();

        ProgramLogger.initialize(snapDir + snapFile + ".log");
    }

    @After
    public void exit() {
        ProgramLogger.teardown();
    }

    @Test
    public void no_entry_empty_file() throws IOException {
        init("no_entry_empty_file");
        assertEquals("", readSnapFile());
    }

    @Test
    public void single_entry_logged() throws IOException {
        init("single_entry_logged");

        FunctionTracer.enter();
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.single_entry_logged() {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void sequential_entry_logged() throws IOException {
        init("sequential_entry_logged");

        FunctionTracer.enter();
        FunctionTracer.leave();
        FunctionTracer.enter();
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.sequential_entry_logged() {\n" +
                     "}\n" +
                     "lib.systemlogging.TestFunctionTracer.sequential_entry_logged() {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void stacked_entry_logged() throws IOException {
        init("stacked_entry_logged");

        FunctionTracer.enter();
        FunctionTracer.enter();
        FunctionTracer.leave();
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.stacked_entry_logged() {\n" +
                     "    lib.systemlogging.TestFunctionTracer.stacked_entry_logged() {\n" +
                     "    }\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void children_entry_logged() throws IOException {
        init("children_entry_logged");

        FunctionTracer.enter();
        FunctionTracer.enter();
        FunctionTracer.leave();
        FunctionTracer.enter();
        FunctionTracer.leave();
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.children_entry_logged() {\n" +
                     "    lib.systemlogging.TestFunctionTracer.children_entry_logged() {\n" +
                     "    }\n" +
                     "    lib.systemlogging.TestFunctionTracer.children_entry_logged() {\n" +
                     "    }\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void deep_entry_logged() throws IOException {
        init("deep_entry_logged");

        FunctionTracer.enter();
        FunctionTracer.enter();
        FunctionTracer.enter();
        FunctionTracer.enter();
        FunctionTracer.enter();

        FunctionTracer.leave();
        FunctionTracer.leave();
        FunctionTracer.leave();
        FunctionTracer.leave();
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.deep_entry_logged() {\n" +
                     "    lib.systemlogging.TestFunctionTracer.deep_entry_logged() {\n" +
                     "        lib.systemlogging.TestFunctionTracer.deep_entry_logged() {\n" +
                     "            lib.systemlogging.TestFunctionTracer.deep_entry_logged() {\n" +
                     "                lib.systemlogging.TestFunctionTracer.deep_entry_logged() {\n" +
                     "                }\n" +
                     "            }\n" +
                     "        }\n" +
                     "    }\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void simpleMessage_entry_logged() throws IOException {
        init("simpleMessage_entry_logged");

        FunctionTracer.message("Simple one-line message.");

        assertEquals("/*\n" +
                     " * Simple one-line message.\n" +
                     " */\n",
                     readSnapFile());
    }

    @Test
    public void indentationMessage_entry_logged() throws IOException {
        init("indentationMessage_entry_logged");

        FunctionTracer.enter();
        FunctionTracer.message("Indentation one-line message.");
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.indentationMessage_entry_logged() {\n" +
                     "    /*\n" +
                     "     * Indentation one-line message.\n" +
                     "     */\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void multilineMessage_entry_logged() throws IOException {
        init("multilineMessage_entry_logged");

        FunctionTracer.message("Multiline one-line messages,\nspanning multiple lines,\nmany times.");

        assertEquals("/*\n" +
                     " * Multiline one-line messages,\n" +
                     " * spanning multiple lines,\n" +
                     " * many times.\n" +
                     " */\n",
                     readSnapFile());
    }

    @Test
    public void complexMessage_entry_logged() throws IOException {
        init("complexMessage_entry_logged");

        FunctionTracer.enter();
        FunctionTracer.message("Complex multi-line message,\nspanning multiple lines.");
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.complexMessage_entry_logged() {\n" +
                     "    /*\n" +
                     "     * Complex multi-line message,\n" +
                     "     * spanning multiple lines.\n" +
                     "     */\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_intArgument_logged() throws IOException {
        init("enter_intArgument_logged");
        FunctionTracer.enter(758);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_intArgument_logged(758) {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_trueArgument_logged() throws IOException {
        init("enter_trueArgument_logged");
        FunctionTracer.enter(true);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_trueArgument_logged(TRUE) {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_falseArgument_logged() throws IOException {
        init("enter_falseArgument_logged");
        FunctionTracer.enter(false);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_falseArgument_logged(FALSE) {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_intArguments_logged() throws IOException {
        init("enter_intArguments_logged");
        FunctionTracer.enter(758, 745, 735);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_intArguments_logged(758, 745, 735) {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_stringArgument_logged() throws IOException {
        init("enter_stringArgument_logged");
        FunctionTracer.enter("758");
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_stringArgument_logged(\"758\") {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_charArgument_logged() throws IOException {
        init("enter_charArgument_logged");
        FunctionTracer.enter('7');
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_charArgument_logged('7') {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_floatArgument_logged() throws IOException {
        init("enter_floatArgument_logged");
        FunctionTracer.enter(7.7);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_floatArgument_logged(7.7) {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_nullArgument_logged() throws IOException {
        init("enter_nullArgument_logged");

        String nullobj = null;

        FunctionTracer.enter(nullobj);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_nullArgument_logged(NULL) {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_objectArgument_logged() throws IOException {
        init("enter_objectArgument_logged");

        TestFunctionTracerHelper th = new TestFunctionTracerHelper();
        th.setString("example class");

        FunctionTracer.enter(th);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_objectArgument_logged(<example class>) {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void enter_mixedArgument_logged() throws IOException {
        init("enter_mixedArgument_logged");

        TestFunctionTracerHelper th = new TestFunctionTracerHelper();
        th.setString("7");

        FunctionTracer.enter(7, 7.7, '7', "7", th, true, false, null);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.enter_mixedArgument_logged(7, 7.7, '7', \"7\", <7>, TRUE, FALSE, NULL) {\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void leave_intArgument_logged() throws IOException {
        init("leave_intArgument_logged");
        FunctionTracer.enter();
        FunctionTracer.leave(758);

        assertEquals("lib.systemlogging.TestFunctionTracer.leave_intArgument_logged() {\n" +
                     "} = 758\n",
                     readSnapFile());
    }

    @Test
    public void leave_intArguments_throwsException() throws IOException {
        init("enter_intArgument_logged");
        FunctionTracer.enter();
        try {
            FunctionTracer.leave(758, 768);
        } catch (RuntimeException e) {
            return; // PASS
        }

        fail("Did not throw exception");
    }

    @Test
    public void leaveDouble_intArgument_logged() throws IOException {
        init("leaveDouble_intArgument_logged");
        FunctionTracer.enter();
        FunctionTracer.enter();
        FunctionTracer.leave(758);
        FunctionTracer.leave();

        assertEquals("lib.systemlogging.TestFunctionTracer.leaveDouble_intArgument_logged() {\n" +
                     "    lib.systemlogging.TestFunctionTracer.leaveDouble_intArgument_logged() {\n" +
                     "    } = 758\n" +
                     "}\n",
                     readSnapFile());
    }

    @Test
    public void leave_trueArgument_logged() throws IOException {
        init("leave_trueArgument_logged");
        FunctionTracer.enter();
        FunctionTracer.leave(true);

        assertEquals("lib.systemlogging.TestFunctionTracer.leave_trueArgument_logged() {\n" +
                     "} = TRUE\n",
                     readSnapFile());
    }

    @Test
    public void leave_falseArgument_logged() throws IOException {
        init("leave_falseArgument_logged");
        FunctionTracer.enter();
        FunctionTracer.leave(false);

        assertEquals("lib.systemlogging.TestFunctionTracer.leave_falseArgument_logged() {\n" +
                     "} = FALSE\n",
                     readSnapFile());
    }

    @Test
    public void leave_stringArgument_logged() throws IOException {
        init("leave_stringArgument_logged");
        FunctionTracer.enter();
        FunctionTracer.leave("758");

        assertEquals("lib.systemlogging.TestFunctionTracer.leave_stringArgument_logged() {\n" +
                     "} = \"758\"\n",
                     readSnapFile());
    }

    @Test
    public void leave_charArgument_logged() throws IOException {
        init("leave_charArgument_logged");
        FunctionTracer.enter();
        FunctionTracer.leave('7');

        assertEquals("lib.systemlogging.TestFunctionTracer.leave_charArgument_logged() {\n" +
                     "} = '7'\n",
                     readSnapFile());
    }

    @Test
    public void leave_floatArgument_logged() throws IOException {
        init("leave_floatArgument_logged");
        FunctionTracer.enter();
        FunctionTracer.leave(7.7);

        assertEquals("lib.systemlogging.TestFunctionTracer.leave_floatArgument_logged() {\n" +
                     "} = 7.7\n",
                     readSnapFile());
    }

    @Test
    public void leave_nullArgument_logged() throws IOException {
        init("leave_nullArgument_logged");

        String nullobj = null;

        FunctionTracer.enter();
        FunctionTracer.leave(nullobj);

        assertEquals("lib.systemlogging.TestFunctionTracer.leave_nullArgument_logged() {\n" +
                     "} = NULL\n",
                     readSnapFile());
    }


    @Test
    public void leave_objectArgument_logged() throws IOException {
        init("leave_objectArgument_logged");

        TestFunctionTracerHelper th = new TestFunctionTracerHelper();
        th.setString("example class");

        FunctionTracer.enter();
        FunctionTracer.leave(th);

        assertEquals("lib.systemlogging.TestFunctionTracer.leave_objectArgument_logged() {\n" +
                     "} = <example class>\n",
                     readSnapFile());
    }
}
