/* 
 * Sets up the server MySQL database for WintherProduksjoner.
 * Creates a new database, selects that.
 * Then creates the tables required and links them.
 * Check WIntherProduksjoner_databaselayout.pdf for more detail.
 *
 * Only requied to run once upon setup.
 * Use 'USE' to select the databse if it is not.
 *  
 * All CHECK constraints must be performed by program(s).
 *
 * Additional views are created to ensure security
 * so that a user cannot connect Keys with data
 * or obtain person, as well as to hide data when not required.
 * It also doubles as pre-quieries to simplify the PHP pages.
 *
 * Run in a terminal with the command> \. setup.sql
 */

/*
 * VALUES ARE FOR TESTING!
 *
 * Create and select WintherProduksjoner database.
 *
CREATE DATABASE WintherProduksjoner;
USE WintherProduksjoner;
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('roMIC#99@nokLITT');
SET PASSWORD FOR 'root'@'127.0.0.1' = PASSWORD('roMIC#99@nokLITT');
SET PASSWORD FOR 'root'@'::1' = PASSWORD('roMIC#99@nokLITT');
DROP USER ''@'localhost';
 */

/*
 * Synthesizer iformation, status and repair form.
 */
CREATE TABLE synthrepairform
(
	referenceno int,
	queueno int,
	modified timestamp,
	newinstance boolean,
	archived boolean,

	manufacturer varchar(50),
	model varchar(50),
	serialno varchar(50),
	errordescription varchar(500),

	status varchar(30),
	feedback varchar(500),

	PRIMARY KEY (referenceno),
	INDEX(serialno)
);

/*
 * Client contact information.
 */
CREATE TABLE clientcontact
(
	referenceno int,
	
	firstname varchar(50),
	lastname varchar(50),
	firm varchar(50),
	address varchar(50),
	postcode int,
	postname varchar(50),
	phone int,
	email varchar(100),

	PRIMARY KEY (referenceno),
	FOREIGN KEY (referenceno) REFERENCES synthrepairform(referenceno)
);

/*
 * Table AllRepairs.
 *
 * Contains the list off all repaired synths.
 * To update this form correctly, use the
 * http://stackoverflow.com/questions/2171544/mysql-insert-on-duplicate-key
 * command.
 */
CREATE TABLE repairlist
(
	manufacturer varchar(50),
	model varchar(50),
	repairs int,
	
	PRIMARY KEY (manufacturer, model)
);

/*
 * A view for all the repairs ever performed so far.
 * Since subqueries cannot be used inside views, it must
 * be split so that allrepairs_set contains the subquery.
 */
CREATE VIEW allrepairs_sub AS
SELECT manufacturer, model, COUNT(*) AS repairs
FROM synthrepairform
WHERE status = 'Returned to Owner' OR status = 'Archived'
GROUP BY manufacturer, model
UNION
SELECT * FROM repairlist;

CREATE VIEW allrepairs AS
SELECT manufacturer, model, SUM(repairs) AS repairs
FROM allrepairs_sub
GROUP BY manufacturer, model;


/*
 * VALUES ARE FOR TESTING!
 *
 * Client user for PHP/webpage side.
 *
CREATE USER 'client'@'localhost' IDENTIFIED BY 'ysEr2_?_FIXi7@#';
GRANT INSERT ON clientcontact TO 'client'@'localhost';
GRANT INSERT, SELECT ON synthrepairform TO 'client'@'localhost';
GRANT SELECT ON allrepairs TO 'client'@'localhost';
 */

/*
 * VALUES ARE FOR TESTING!
 *
 * Owner user for Java side.
 *
CREATE USER 'localmachine'@'%' IDENTIFIED BY 'treveark_05+NOT';
GRANT INSERT, DELETE, SELECT, UPDATE ON synthrepairform TO 'localmachine'@'%';
GRANT INSERT, DELETE, SELECT, UPDATE ON clientcontact TO 'localmachine'@'%';
GRANT INSERT, DELETE, SELECT, UPDATE ON repairlist TO 'localmachine'@'%';
 */
