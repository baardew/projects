# What is this? #

This is my repo for projects, experiments and other code pices that I work on in my spare time.

I have also contributed to video4linux in my summer internship 2013. The repo can be found here: http://git.linuxtv.org/v4l-utils.git, or my contribution directly here: http://git.linuxtv.org/cgit.cgi/v4l-utils.git/log/?qt=author&q=winther

### License ###

Everything is under public domain, except for WintherProduksjoner and EquestrianStartList. WintherProduksjoner is under a GPL and its licence can be found in their respective folders.

### Projects Summary ###

* WintherProduksjoner: Client Database sytem with complete GUI for wintherproduksjoner. Official site is http://wintherproduksjoner.no/
* EquestrianStartList: Application for setting up and managing eqeustrian competitions and classes
* Pit-Droids: Simple C++ program to learn and explore C++11; Build up a wast mass of pit-droids that you either sell or put to useful work.
* Timetaker C library: C/C++ code using gettimeofday. Can be used for simple time-taking.
* GameOfLife: The Game of Life implementation with GUI in Java.
* Dynamic Heat: A project to explore aspects of Qt and optimizations. It is a program to simulate heat dissipation that look good (not physically correct).
* Project: A project to look into CUDA programming.
* Array Memory C Library: Small library to create 1D to 3D arrays using a single contiguous memory.
* DistributionService: Selfconfiguring network abstraction library in C++ for Master/Slave applications with its own serializer and system information discovery.
* Field-of-View: Algorithm for getting a line of sight in a 2D world.
