The Game Of Life comes with all the source code, and any part can be changed.
However only javadoc and UML diagrams are the only source of support, and are recommended to consult.
The javadoc can be accesses with the javadoc.sh, found in the root directory in the GameOfLife folder after installation.

IMPORTANT:
Changes may cause the program to malfunction.

The simplest changes would be to the Unit and Rules (see their respected readmes).
For thoose who wants to create or modify the User Interface, read the dedicated readme.
