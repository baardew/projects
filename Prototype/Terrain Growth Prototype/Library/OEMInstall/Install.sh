echo The Game Of Life
echo Installing...

echo Compiling source files...
javac GameOfLife/GameOfLife.java
javac GameOfLife/Simulator/Unit.java
javac GameOfLife/Simulator/Generation.java
javac GameOfLife/Simulator/Rules.java
javac GameOfLife/Simulator/Core.java
javac GameOfLife/ControlSystem/UserInterface.java
javac GameOfLife/ControlSystem/GUIControlSystem.java
javac GameOfLife/ControlSystem/TextPrint.java
javac GameOfLife/ControlSystem/TXTControlSystem.java

echo Creating launch files...
mv Library/OEMInstall/LI.sh LinuxUI.sh
mv Library/OEMInstall/MI.sh MacUI.sh

echo Cleaning...
rm GameOfLife/GameOfLife.java
rm GameOfLife/Simulator/Unit.java
rm GameOfLife/Simulator/Generation.java
rm GameOfLife/Simulator/Rules.java
rm GameOfLife/Simulator/Core.java
rm GameOfLife/ControlSystem/UserInterface.java
rm GameOfLife/ControlSystem/GUIControlSystem.java
rm GameOfLife/ControlSystem/TextPrint.java
rm GameOfLife/ControlSystem/TXTControlSystem.java
rm Install.sh
rmdir Library/OEMInstall

echo Install complete.