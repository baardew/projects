THE GAME OF LIFE README

written pretty much by baardew@ifi@uio.
Developed as INF1000 and INF1010 practice at University of Oslo.

v. 1.0

IMPORTANT NOTICE:
This version includes a Graphilcal and a Text based user interface. They both have all the features.
The GUI included here is only verified to work with Linux OS and the Mac OS, however, Linux is the recommended OS.
The system is currently untested with Window OS.
There are some instability when using a large Map with a low-end computer system.

System reqirements:
minimum
	- Java SE 6 (java v.1.6) (Works with Java 5 or greater, but not recommended) (type "java -version" in the Terminal/shell window to see your version)
	- Linux OS or Mac OS with one monitor conntected (two or more monitors not supported)
recommended
	- 20MB free space for The Game Of Life application
	- 1GB or more free space on the hard disk drive for saved history
	- Two or more cores
	- 1.25 GHz or faster processor
	- 1GB or more RAM
	- Terminal/shell resolution of 101x47 for standard map size when using Text based UI
	- 1280*1024 resolution or higher when using Graphical based UI

Installing:
	- See the Manual.pdf

Using the applcation:
	- See the Manual.pdf

Uninstalling:
	Drag the folder GameOfLife to the trash bin.
