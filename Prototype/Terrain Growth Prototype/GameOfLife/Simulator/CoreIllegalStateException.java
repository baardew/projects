package GameOfLife.Simulator;

/**
 *Custom exception for controlling Control System operation depending on the Core`s current status.
 *@author baardew
 *@version 1.0
 */

public class CoreIllegalStateException extends Exception{
    /**
     *Displays the error message.
     *@param error The error message from the caused error.
     */
    public CoreIllegalStateException(String error){
	super(error);
    }
}