package GameOfLife.Simulator;

import java.awt.Color;

/**
 *Game Of Life Unit. This is the objects beeing simulated.
 *@author baardew
 *@version 1.0
 */
public enum Unit{
    A('X',1, new Color(120, 70, 0)), B('H',2, Color.DARK_GRAY), C('.',0, Color.BLACK), D(' ', 2, Color.GREEN);
    final char unit;
    final int point;
    final Color color;

    Unit(char unit, int point, Color color){
	this.unit = unit;
	this.point = point;
	this.color = color;
    }

    /**
     *Get thr char representation.
     *@return The char for this Unit.
     */
    public char getUnit(){
	return unit;
    }

    /**
     *Get the point value for this Unit.
     *@return The point value of this Unit.
     */
    public int getPoint(){
	return point;
    }
    
    /**
     *Get the color representation.
     *@return The Color for this Unit.
     */
    public Color getColor(){
	return color;
    }
}
