package GameOfLife.Simulator;

import GameOfLife.Simulator.Unit;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

/**
 *Class containing the simulation rules. Made as a Runnable to be excecuted.
 *@author baardew
 *@version 1.0
 */
public class Rules implements Runnable{
    private final int x, y;
    private Unit[][] newMap;
    private Unit[][] refMap;
    private Generation generation, b2;
    private CountDownLatch lock;
    
    private int countIn, countOut, countA, countB, countC, total;
    private Unit[][] oldMap;
    
    /**
     *Construct a new Rules instance for one Unit on a specified map.
     *@param x The X coordinate for the specified Unit.
     *@param y The Y coordinate for the specified Unit.
     *@param generation The Generation for which the Unit resides in.
     *@param newMap The map the new Unit for the specified position is set.
     *@param lock The synchronization lock to be used.
     */
    public Rules(int x, int y, Generation generation, Generation b2, Unit[][] newMap, CountDownLatch lock){
	this.x = x;
	this.y = y;
	this.lock = lock;
	this.generation = generation;
	this.b2 = b2;
	this.newMap = newMap;
    }
    
    /**
     *Calculates the new Unit with given specifications.
     */
    public void run(){
	oldMap = generation.getMap();
	refMap = generation.getMap();
	countIn = countNeighboursInner();
	countOut = countNeighboursOuter();
	countA = generation.getCount(Unit.A);
	countB = generation.getCount(Unit.B);
	countC = generation.getCount(Unit.C);
	total = generation.totalUnits();
	oldMap = generation.getMap();
	
	newMap[x][y] = optimized_newUnit();

	lock.countDown();
    }

    private Unit optimized_newUnit() {
	boolean A_small = hasA_small();
	boolean B_small = hasB_small();
	
	if (A_small && B_small) {
		if (countNeighboursOuter_A() >= countNeighboursOuter_B()*2)
		    return Unit.A;
		else if (hasFar())
		    return Unit.B;
	    }

	if (!A_small && B_small)
	    return Unit.B;
	
	if (A_small || (hasA_big() && !(B_small || hasB_big())))
	    return Unit.A;
	
	return Unit.C;
    }

    private Unit newUnit() {	
	if (oldMap[x][y] == Unit.A) {	    
	    if (hasD_small())
		return Unit.B;
	    else
		return Unit.A;
	  
	} else if (oldMap[x][y] == Unit.B) {
	    if (countIn == (Unit.B.getPoint() * 3 * 3 - Unit.B.getPoint()))
		return Unit.D;
	    else if (!hasA_small() && (x == 0 || x == Generation.getSIZEX()-1 || y == 0 || y == Generation.getSIZEY()-1))
		return Unit.D;
	    else if (hasB_big())
		return Unit.B;
	    else
		return Unit.C;
	    
	} else if (oldMap[x][y] == Unit.D) {
	    if (hasA_small())
		return Unit.B;
	    return Unit.D;
	    
	} else {
	    if (hasD_small())
		return Unit.B;
	    
	    if (hasA_small() && hasB_small()) {
		if (countNeighboursOuter_A() >= countNeighboursOuter_B()*2 && refMap[x][y] != Unit.C)
		    return Unit.A;
		else if (hasFar())
		    return Unit.B;
	    }

	    if (!hasA_small() && hasB_small())
		return Unit.B;

	    if (hasA_small() || hasA_big() && !(hasB_small() || hasB_big()))
		return Unit.A;
	    
	    return Unit.C;
	}
    }

    private boolean hasA_big(){
	boolean has = false;
	for(int checkY = y-2; checkY <= y+2; checkY++){
	    for(int checkX = x-2; checkX <= x+2; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
		    has |= oldMap[checkX][checkY].getPoint() == 1;
		}
	    }
	}
	return has;
    }

    private boolean hasA_small(){
	boolean has = false;
	for(int checkY = y-1; checkY <= y+1; checkY++){
	    for(int checkX = x-1; checkX <= x+1; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
		    has |= oldMap[checkX][checkY].getPoint() == 1;
		}
	    }
	}
	return has;
    }

    private boolean hasD_small(){
	boolean has = false;
	for(int checkY = y-1; checkY <= y+1; checkY++){
	    for(int checkX = x-1; checkX <= x+1; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
		    has |= oldMap[checkX][checkY] == Unit.D;;
		}
	    }
	}
	return has;
    }
    
    private boolean hasB_big(){
	boolean has = false;
	for(int checkY = y-2; checkY <= y+2; checkY++){
	    for(int checkX = x-2; checkX <= x+2; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
		    has |= oldMap[checkX][checkY].getPoint() == 2;
		}
	    }
	}
	return has;
    }
    
    private boolean hasB_small(){
	boolean has = false;
	for(int checkY = y-1; checkY <= y+1; checkY++){
	    for(int checkX = x-1; checkX <= x+1; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
		    has |= oldMap[checkX][checkY].getPoint() == 2;
		}
	    }
	}
	return has;
    }


    
    /**
     *Counts the neighbours around the specified with a radius of 1.
     *@return The total sum of all surrounding Units in points.
     */
    private int countNeighboursInner(){
	int count = 0;
	for(int checkY = y-1; checkY <= y+1; checkY++){
	    for(int checkX = x-1; checkX <= x+1; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
		    count += oldMap[checkX][checkY].getPoint();
		}
	    }
	}
	return count;
    }


    private int countNeighboursOuter(){
	int count = 0;
	for(int checkY = y-2; checkY <= y+2; checkY++){
	    for(int checkX = x-2; checkX <= x+2; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
			count += oldMap[checkX][checkY].getPoint();
		}
	    }
	}
	if(count-countIn < 0)
	    return 0;
	return count-countIn;
    }
    

    private int countNeighboursOuter_A(){
	int count = 0;
	for(int checkY = y-2; checkY <= y+2; checkY++){
	    for(int checkX = x-2; checkX <= x+2; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
		    if (oldMap[checkX][checkY].getPoint() == 1)
			count += 1;
		}
	    }
	}
	if(count-countIn < 0)
	    return 0;
	return count-countIn;
    }

    private boolean hasFar(){
	int count = 0;
	for(int checkY = y-5; checkY <= y+5; checkY++){
	    for(int checkX = x-5; checkX <= x+5; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()
		   && (checkX == x-5 || checkX == x+5) && (checkY == y-5 || checkY == y+5)){
		    if (oldMap[checkX][checkY].getPoint() == 2)
			count += 1;
		}
	    }
	}
	if(count-countIn < 0)
	    return false;
	return count-countIn > 0;
      }

    private int countNeighboursOuter_B(){
	int count = 0;
	for(int checkY = y-2; checkY <= y+2; checkY++){
	    for(int checkX = x-2; checkX <= x+2; checkX++){
		if(!(checkX == x && checkY == y) && checkX > -1 && checkX < Generation.getSIZEX()
		   && checkY > -1 && checkY < Generation.getSIZEY()){
		    if (oldMap[checkX][checkY].getPoint() == 2)
			count += 1;
		}
	    }
	}
	if(count-countIn < 0)
	    return 0;
	return count-countIn;
    }
}
