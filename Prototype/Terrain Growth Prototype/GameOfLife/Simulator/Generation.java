package GameOfLife.Simulator;

import GameOfLife.Simulator.Unit;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *Hold data for each generation. Responsible for map management.
 *@author baardew
 *@version 1.0
 */
public class Generation{
    private static int SIZEX = 1000, SIZEY = 1680, SPAWNRATIO = 48000;
    private int countA = 0, countB = 0, countC = 0, generation = 0;
    private Unit [][] map;

    /**
     *Creates a new map with random Units.
     */
    public Generation(){
	map = new Unit[SIZEX][SIZEY];
	Random rand = new Random();
	for(int x = 0; x < SIZEX; x++){
	    for(int y = 0; y < SIZEY; y++){
		int bool = rand.nextInt(SPAWNRATIO + 20);
		if(bool < 10){
		    map[x][y] = Unit.A;
		    countA++;
		}
		else if(bool < 50){
		    map[x][y] = Unit.B;
		    countB++;
		}
		else{
		    map[x][y] = Unit.C;
		    countC++;
		}
	    }
	}
    }

    /**
     *Creates a new generation based on a Data file.
     *@param filename The name of the Data file.
     *@throws IOexception If threre is a read exception.
     *@throws FileNotFoundException If the Data file cannot be found
     *@throws NumberFormatException If the Data file has incorrect format.
     *@throws ArrayIndexOutOfBoundsException If the Data file has incorrect format.
     */
    public Generation(String filename) throws IOException, FileNotFoundException, NumberFormatException, ArrayIndexOutOfBoundsException{
	File file = new File(filename);
	if(!file.exists())
	    throw new FileNotFoundException();
	if(!filename.endsWith(".gol"))
            throw new IOException();
 
	Scanner reader = new Scanner(file);

	String[] sizes = reader.nextLine().split("-");
	SIZEX = Integer.parseInt(sizes[0]);
	SIZEY = Integer.parseInt(sizes[1]);
	map = new Unit[SIZEX][SIZEY];
	for(int x = 0; x < SIZEX; x++){
	    String input = reader.nextLine();
	    for(int y = 0; y < SIZEY; y++){
		char bool = input.charAt(y);
		if(bool == '0'){
		    map[x][y] = Unit.A;
		    countA++;
		}
		else if(bool == '1'){
		    map[x][y] = Unit.B;
		    countB++;
		}
		else{
		    map[x][y] = Unit.C;
		    countC++;
		}
	    }
	}
    }
    
    /**
     *Generate a new generation based on a map.
     *@param usemap The map to be used in this Generation. Must match with SIZEX and SIZEY defined map size.
     *@param generation The index number of this Generation. If it is the first generation, it must me set to -1;
     *@exception ArrayIndexOutOfBoundsException If usemap does not correspond with defined sizes.
     */
    public Generation(Unit [][] usemap, int generation){
	this.generation = ++generation;
	this.map = usemap;

	for(int x = 0; x < SIZEX; x++){
	    for(int y = 0; y < SIZEY; y++){
		if(map[x][y] == Unit.A)
		    countA++;
		else if(map[x][y] == Unit.B)
		    countB++;
		else if (map[x][y] == Unit.C)
		    countC++;
	    }
	}	
    }

    /**
     *Sets the map`s dimensions. Changes if value is greater than 0, else it keeps its original value.
     *@param newSIZEX The new size for X-axis.
     *@param newSIZEY The new size for Y-axis.
     */
    public static void setSizeXY(int newSIZEX, int newSIZEY){
	if(newSIZEX > 0)
	    SIZEX = newSIZEX;
	if(newSIZEY > 0)
	    SIZEY = newSIZEY;
    }

    /**
     *Sets the Spawn Ratio. Must be greater than 5 to be changed.
     *@param newSpawnRatio The new spawn ratio.
     */
    public static void setSPAWNRATIO(int newSpawnRatio){
	if(newSpawnRatio > 5)
	    SPAWNRATIO = newSpawnRatio;
    }

    /**
     *Gets the Spawn Ratio.
     *@return The Spawn Ratio.
     */
    public static int getSPAWNRATIO(){
	return SPAWNRATIO;
    }

    /**
     *Gets the map containing all of the Units.
     *@return The map.
     */
    public Unit[][] getMap(){
	return map;
    }

    /**
     *Gets the count of a specified Unit in the map.
     *@param unit The unit to get the number of.
     *@return The number of specified Units in map.
     */
    public int getCount(Unit unit){
	if(unit == Unit.A)
	    return countA;
	else if (unit == Unit.B)
	    return countB;
	else
	    return countC;
    }

    /**
     *Gets the X-axis size of the map.
     *@return The size of X.
     */
    public static int getSIZEX(){
	return SIZEX;
    }

    /**
     *Gets the Y-axis size of the map.
     *@return The size of Y.
     */
    public static int getSIZEY(){
	return SIZEY;
    }
    
    /**
     *Gets the Generation`s generation number.
     *@return The generation number.
     */
    public int getGeneration(){
	return generation;
    }

    /**
     *Gets the total number of Units on the map.
     *@return The total number of Units.
     */
    public static int totalUnits(){
	return SIZEX * SIZEY;
    }
}
