package GameOfLife.Simulator;

import GameOfLife.ControlSystem.UserInterface;
import GameOfLife.ControlSystem.TextPrint;
import GameOfLife.Simulator.Unit;
import GameOfLife.Simulator.Generation;
import GameOfLife.Simulator.Rules;
import GameOfLife.Simulator.CoreIllegalStateException;

import java.util.ArrayList;
import java.io.File;
import java.io.PrintStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.CountDownLatch;
import java.util.Calendar;
import java.util.concurrent.RejectedExecutionException;

/**
 *The simulator class that runs the simulation. Keeps records of all Generations, and sends this info to the Control System as it works or by request.
 *@author baardew
 *@version 1.0
 */
public class Core extends Thread{
    private final UserInterface UI;
    private ExecutorService generationPoolProcessor;
    private String threadName;

    private static volatile int HOLDTIME = 100;
    private static volatile boolean PMAP = true, PSTATISTICS = true, PGENERATIONSTATISTICS = true, skipPrint = false,
	LONGSIMULATION = true, game = false, solid = false, CUSTOMMODE = false;

    private int roundNo, totalGenerations, minGeneration, maxGeneration;

    private ArrayList<Generation> history;
    private Generation simulationBasis;

    private boolean equalStatus; //For use with difference maps
    private volatile boolean CONTINUE;

    /**
     *Creates a new Core simulator with specified User Interface. Starts with a random first Generation.
     *@param UI The Control System to use.
     */
    public Core(UserInterface UI){
	this.UI = UI;
	roundNo = 0;
	totalGenerations = 0;
	minGeneration = 0;
	maxGeneration = 0;
	simulationBasis = null;
	CONTINUE = true;
	setCUSTOMMODE(false);
	setThreadName("main default simulation ");
	UI.printSimulationStatus("Starting new thread: " + threadName + " " + roundNo);
	newSimulation();
	generationPoolProcessor = Executors.newFixedThreadPool(1 /*Runtime.getRuntime().availableProcessors()*/);
    }
    
    /**
     *Creates a new Core simulator with Specified User Interface and the first Generation.
     *@param UI The Control System to use.
     *@param g0 The Generation to start with
     */
    public Core(UserInterface UI, Generation g0){
	this.UI = UI;
	roundNo = 0;
	totalGenerations = 0;
	minGeneration = 0;
	maxGeneration = 0;
	simulationBasis = g0;
	CONTINUE = true;
	setThreadName("main custom simulation ");
	UI.printSimulationStatus("Starting new thread: " + threadName + " " + roundNo);
	history = new ArrayList<Generation>();
	history.add(g0);
	history.add(g0);
	generationPoolProcessor = Executors.newFixedThreadPool(1 /*Runtime.getRuntime().availableProcessors()*/);
	setCUSTOMMODE(true);
    }

    /**
     *Sets the simulation`s name.
     */
    private void setThreadName(String name){
	threadName = name;
    }

    /**
     *Starts a new random simulation. It removes any previous ones, but continue to keep track of all statistics within this Core object.
     */
    private void newSimulation(){
	++roundNo;
	Thread.currentThread().setName(threadName + " " + roundNo);
	history = new ArrayList<Generation>();
	Generation g0;
	if(CUSTOMMODE)
	    g0 = simulationBasis;
	else
	    g0 = new Generation();
	history.add(g0);
	history.add(g0);
    }
    
    /**
     *Main Simulation Run Loop.
     */
    public void run(){
	Thread.currentThread().setName(threadName + " " + roundNo);
	if(CUSTOMMODE)
	    ++roundNo;
	boolean first = true;
	startGame();
	
	if(!isSkipPrint())
	    printToUI();	
	
	while(isGame()){
	    solid = false;
	    Thread.currentThread().setName(threadName + " " + roundNo);
	    
	    if(!first){
		newSimulation();
		if(!isSkipPrint())
		    printToUI();
	    }
	    UI.printSimulationStatus("Running...");
	    
	    while(!solid && isGame()){
		try{
		    nextGeneration();
		}catch(OutOfMemoryError e){
		    UI.printSimulationStatus("Simulation out of memory.");
		    outOfMemoryEventException();
		    UI.printSimulationStatus("Running...");
		}
		findStabilized();
		
		if(!isSkipPrint()){
		    printToUI();
		}

		if(!CONTINUE)
		    holdThread();
	    }	    
	    
	    int generationLength = historyLength();
	    totalGenerations += generationLength;
	    if(minGeneration == 0){
		minGeneration = generationLength;
	    }if(generationLength < minGeneration){
		minGeneration = generationLength;
	    }if(generationLength > maxGeneration){
		maxGeneration = generationLength;
	    }

	    if(!isSkipPrint())
		printToUI();

	    if(LONGSIMULATION && isGame()){
		try{
		    Thread.sleep(HOLDTIME*10);
		}catch(Exception e){}
 	    }
	    
	    if(first)
		first = false;
	    
	    if(CUSTOMMODE && !LONGSIMULATION){
		setCUSTOMMODE(false);
		endGame();
		generationPoolProcessor.shutdown();
		UI.printSimulationStatus("Custom simulation finished.");
	    }
	    else if(!LONGSIMULATION){
		endGame();
		generationPoolProcessor.shutdown();
		UI.printSimulationStatus("Simulation finished.");
	    }

	    if(!CONTINUE)
		holdThread();
	}
    }

    /**
     *Ends the current simulation in progress. Delay on one Generation.
     *@throws CoreIllegalStateException If the core has no running simulation.
     */
    public void end() throws CoreIllegalStateException{
	if(!isGame() && CONTINUE)
	    throw new CoreIllegalStateException("No simulation to end.");
	UI.printSimulationStatus("Waiting for simulation to end...");
	CONTINUE = false;
	if(!CONTINUE)
	    continueSimulation();
	endGame();
	generationPoolProcessor.shutdownNow();
	try{
	    join();
	}catch(Exception e){
	    UI.printSimulationStatus("Synchronized termination failed.");
	}
	UI.printSimulationStatus("Simulation ended.");
    }

    /**
     *Holds the current thread. Released with continueCurrent() or end().
     */
    private synchronized void holdThread(){
	while(!CONTINUE){
	    try{
		wait();
	    }catch(InterruptedException e){
		UI.printSimulationStatus("Simulation interruption.");

	    }
	}
    }


    /**
     *Finds out whether the simulation has stabilized or not. If it is, then it will be marked as Sabilized. When stabilization has occured, one more generation might be generated.
     */
    private void findStabilized(){
	Generation g1 = history.get(historyLength()-1), g0;
	if(g1.getGeneration() > 0){
	    g0 = history.get(g1.getGeneration()-1);
	    if(g1.getCount(Unit.B) == g0.getCount(Unit.B))
		solid = differenceMap();
	}
    }

    /**
     *Find Generation repeatedness in a ten Generations back.
     *@return true If any simulations is exactly like the current one.
     *@return false If no previous simulation is exactly like the current one.
     */
    private boolean differenceMap(){
	int genBack = 1;
	equalStatus = false;
	CountDownLatch lock = new CountDownLatch(genBack);
	Generation gn = history.get(historyLength()-1);
	for(int i = 1; i <= genBack; i++){
	    Comparer c = new Comparer(gn, i, lock);
	    c.run();
	}
	try{
	    lock.await();
	}catch(InterruptedException e){
	    UI.printSimulationStatus("Simulation comparing problem has occured.");
	}
	return equalStatus;
    }

    private synchronized void addEqualStatus(boolean status){
	equalStatus = equalStatus || status;
    }

    private class Comparer implements Runnable{
	private Generation gn;
	private int generationBack;
	private CountDownLatch lock;

	/**
	 *Compares two generations for equality.
	 *@param gn The current generation.
	 *@param generationBack The number of generations back to compare current Generation with.
	 */
	 public Comparer(Generation gn, int generationBack, CountDownLatch lock){
	    this.gn = gn;
	    this.generationBack = generationBack;
	    this.lock = lock;
	}
	
	/**
	 *Difference map computation method.
	 */
	public void run(){
	    addEqualStatus(compareDifferenceMap());
	    lock.countDown();
	}
	
	/**
	 *Compares the current Generation with one earlier in history, according to specified Generation.
	 *@return true If current generation is excactly like the history Generation.
	 *@return false If the current Generation is not excactly like the history Generation.
	 */
	private boolean compareDifferenceMap() {
	    boolean equalStatus = true;
	    if(historyLength() <= generationBack)
		return false;
	    else{
		int sizeX = Generation.getSIZEX(), sizeY = Generation.getSIZEY();
		Generation g0 = history.get(historyLength()-(generationBack+1));
		Unit [][] currentMap = gn.getMap(), historyMap = g0.getMap();
		for(int x = 0; x < sizeX && equalStatus; x++){
		    for(int y = 0; y < sizeY && equalStatus; y++){
			if(currentMap[x][y] != historyMap[x][y])
			    equalStatus = false;
		    }
		}
		return equalStatus;
	    } 
	}
    }


    /**
     *Creates the next set for a new Generation Computation.
     *@exception RejectedExcecutionException If the processing pool had an execution error.
     */
    private void nextGeneration(){
	int sizex = Generation.getSIZEX(), sizey = Generation.getSIZEY();
	Generation g1 = history.get(historyLength()-2);
	Generation g0 = history.get(historyLength()-1);
	Unit[][] newMap = new Unit[sizex][sizey];
	Unit[][] oldMap = g0.getMap();
	CountDownLatch coreLock = new CountDownLatch(g0.getCount(Unit.C));
	
	for(int x = 0; x < sizex; x++){
	    for(int y = 0; y < sizey; y++){
		if (oldMap[x][y] == Unit.C) {
			Rules r = new Rules(x, y, g0, g1, newMap, coreLock);
			r.run();
		} else {
		    newMap[x][y] = oldMap[x][y];
		}
	    }
	}

	try{
	    coreLock.await();
	}catch(InterruptedException e){
	    UI.printSimulationStatus("Simulation Computation problem has occured.");
	}
	
	Generation r1 = new Generation(newMap, g0.getGeneration());
	history.add(r1);
    }
    
    /**
     *Writes a generation to a file. Writes either one specified generation if generation param is given, else every generation will be written. 
     *@param fileName The name of the file to be written to.
     *@param generation Optional. Write only the specified generation.
     *@throws CoreIllegalStateException If the core is running.
     */
    public void writeToFile(String fileName, int ... generation) throws CoreIllegalStateException{
	if(isGame())
	    throw new CoreIllegalStateException("Simulation is running.");
	PrintStream output;
	String pathName = "Saved History";
	
	UI.printWriteFileStatus("Writing...");
	File filePath = new File(pathName);
	if(!filePath.exists())
	    (new File(pathName)).mkdir();
	
	if(generation.length == 0){
	    try{
		output = new PrintStream(new File(pathName, fileName + ".txt"));
		int total = historyLength();
		for(int i = 0; i < total; i++){
		    output.print("\n\n\n");
		    output.print(printToFileGenerator(i));
		    output.flush();
		    UI.printWriteFileProgress((((double)(i+1)/total)*100));
		}
		output.close();
		UI.printWriteFileStatus("All generations have been saved.");
	    }catch (Exception e){
		UI.printWriteFileStatus("Error during write to file.");
	    }
	}else{	    
	    try{
		output = new PrintStream(new File(pathName, fileName));
		output.print(printToFileGenerator(generation[0]));
		output.close();
		output.flush();
		UI.printWriteFileProgress(100.0);
		UI.printWriteFileStatus("Generation has been saved.");
	    }catch (Exception e){
		UI.printWriteFileStatus("Error during write to file.");
	    }
	}
    }
    
    /**
     *Generates the format to print to file. Uses the TextPrint from Control System to format, and only prints whats specified by the preferences.
     *@param generationIndex The index in history to generate a print format from.
     *@return String containing the printout fully formated.
     *@see GameOfLife.ControlSystem.TextPrint
     */
    private String printToFileGenerator(int generationIndex){
	String generatedString = new String();
	Generation generation = history.get(generationIndex);
	TextPrint generator = new TextPrint();
	int total = Generation.totalUnits();
	if(PMAP){
	    generatedString += generator.generateMap(generation.getMap());
	}
	if(PSTATISTICS){
	    generatedString += generator.generateGeneral(generationIndex, total, solid);
	    generatedString += generator.generateGenerationStatistics(roundNo, totalGenerations, minGeneration, maxGeneration);
	    if(PGENERATIONSTATISTICS){
		Generation prevgeneration;
		if(generation.getGeneration() < 1)
		    prevgeneration = generation;
		else
		    prevgeneration = history.get(generation.getGeneration()-1);
		
		generatedString += generator.generateStatistics(Unit.A, total, generation.getCount(Unit.A), prevgeneration.getCount(Unit.A));
		generatedString += generator.generateStatistics(Unit.B, total, generation.getCount(Unit.B), prevgeneration.getCount(Unit.B));
		generatedString += generator.generateStatistics(Unit.C, total, generation.getCount(Unit.C), prevgeneration.getCount(Unit.C));			
	    }
	}
	return generatedString;
    }

    /**
     *Inner batch command to display the next Generations` statistics and map. Only prints whats specified by the preferences. Prints the newest Generation in history if no generation has specifically been requested.
     *@param generation The Generation to be printed to the Control System.
     */
    public void printToUI(int ... generation){
	int generationSelection;
	if(generation.length > 0)
	    generationSelection = generation[0];
	else
	    generationSelection = historyLength()-1;

	if(PMAP){
	    generateAndPrintMap(generationSelection);
	}
	if(PSTATISTICS){
	    generateAndPrintProgression(generationSelection);
	    generateAndPrintStatistics(generationSelection);
	    if(PGENERATIONSTATISTICS)
		generateAndPrintGenerationStatistics();
	    
	}
    }

    /**
     *Gets and sends the map to the UI to be displayed.
     *@param generation The Generation to be displayed.
     */
    private void generateAndPrintMap(int generation){
	Generation g1 = history.get(generation);
	UI.printMap(g1.getMap());
    }

    /**
     *Gets and sends Unit-specific counts for current Generation to be displayed.
     *@param generation The Generation to be displayed.
     */
    private void generateAndPrintStatistics(int generation){
	Generation g1 = history.get(generation), g0;
	int total = Generation.totalUnits();
	if(g1.getGeneration() < 1)
	    g0 = g1;
	else
	    g0 = history.get(g1.getGeneration()-1);

	UI.printStatistics(Unit.A, total, g1.getCount(Unit.A), g0.getCount(Unit.A));
	UI.printStatistics(Unit.B, total, g1.getCount(Unit.B), g0.getCount(Unit.B));
	UI.printStatistics(Unit.C, total, g1.getCount(Unit.C), g0.getCount(Unit.C));
    }

    /**
     *Gets and sends general Generation progression to be displayed.
     *@param generation The Generation to be displayed.
     */
    private void generateAndPrintProgression(int generation){
	Generation gn = history.get(generation);
	UI.printGeneral(gn.getGeneration(), (Generation.getSIZEX()*Generation.getSIZEY()), solid);
    }

    /**
     *Gets and sends general simulation statistics to be displayed.
     */
    private void generateAndPrintGenerationStatistics(){
	UI.printGenerationStatistics(roundNo, totalGenerations, minGeneration, maxGeneration);
    }

    /**
     *Generates and write Graph-ready files for Unit count for all generations.
     *@param fileName The name of the folder to put the Unit count files.
     *@throws CoreIllegalStateException If the core is running.
     */
    public void generateGraphFiles(String fileName) throws CoreIllegalStateException{
	if(isGame())
	    throw new CoreIllegalStateException("Simulation is running.");
	PrintStream outputA, outputB, outputC;
	String pathName = "Graph Files";
	
	UI.printWriteFileStatus("Writing...");
	File outerPath = new File(pathName);
	if(!outerPath.exists()){
	    outerPath.mkdir();
	}

	File innerPath = new File(pathName, fileName);
	if(!innerPath.exists()){
	    innerPath.mkdir();
	}

	try{
	    outputA = new PrintStream(new File(innerPath, "Unit_A.txt"));
	    outputB = new PrintStream(new File(innerPath, "Unit_B.txt"));
	    outputC = new PrintStream(new File(innerPath, "Unit_C.txt"));
	    int total = historyLength();
		for(int i = 0; i < total; i++){
		    Generation g = history.get(i);
		    outputA.println(g.getCount(Unit.A));
		    outputB.println(g.getCount(Unit.B));
		    outputC.println(g.getCount(Unit.C));
		    outputA.flush();
		    outputB.flush();
		    outputC.flush();
		    UI.printWriteFileProgress((((double)(i+1)/total)*100));
		}
		outputA.close();
		outputB.close();
		outputC.close();
		UI.printWriteFileStatus("All files have been generated.");
	}catch (Exception e){
	    UI.printWriteFileStatus("Error during write to file.");
	}
    }

    /**
     *Creates and writes a Generation Data file.
     *@param filename The name of the file to be written to.
     *@param generation The Generation to make to a Data file.
     *@throws CoreIllegalStateException If the core is running.
     */
    public void writeToData(String filename, int generation) throws CoreIllegalStateException{
	if(isGame())
	    throw new CoreIllegalStateException("Simulation is running.");
	Unit[][] map = history.get(generation).getMap();
	PrintStream output;
	String pathName = "Data Files";
	
	UI.printWriteFileStatus("Writing...");
	File filePath = new File(pathName);
	if(!filePath.exists())
	    new File(pathName).mkdir();
	try{
	    output = new PrintStream(new File(pathName, filename + ".gol"));
	    output.println(Generation.getSIZEX() + "-" + Generation.getSIZEY());
	    String tmp = null;
	    for(int x = 0; x < Generation.getSIZEX(); x++){
		tmp = new String();
		for(int y = 0; y < Generation.getSIZEY(); y++){
		    if(map[x][y] == Unit.A)
			tmp += "0";
		    else if(map[x][y] == Unit.B)
			tmp += "1";
		    else
			tmp += "#";
		}
		output.println(tmp);
	    }

	    output.flush();
	    output.close();
	    UI.printWriteFileProgress(100.0);
	    UI.printWriteFileStatus("Datafile has been saved.");
	}catch(Exception e){
	    UI.printWriteFileStatus("Error during write to file.");
	}
    }

    /**
     *Handles the event when the next generation process runs out of memory. It will save all current progression in full format with statistics, and continue with the last seconds last one.
     */
    private void outOfMemoryEventException(){
	UI.printSimulationStatus("Ending simulation....");
	try{
	    end();
	}catch(CoreIllegalStateException e){}
	/* Sketch of an out of memory occurance. NOT OPERATIONAL, as memory nedded to execute this, is already used up
	try{
	    sleep(5000);
	}catch(InterruptedException e){}
	if(!isGame())
	    return;

	UI.printSimulationStatus("Handling Out of Memory Error.");
	String dateStamp = Calendar.YEAR + "-" + Calendar.MONTH + "-" + Calendar.DAY_OF_MONTH + "-" + Calendar.HOUR_OF_DAY + "-" + Calendar.MINUTE;

	Generation gn = history.get(historyLength()-2);
	boolean tmpPMAP = PMAP, tmpPSTATISTICS = PSTATISTICS, tmpPGENERATIONSTATISTICS = PGENERATIONSTATISTICS;
	PMAP = true;
	PSTATISTICS = true;
	PGENERATIONSTATISTICS = true;


	try{
	    writeToFile(dateStamp);
	    generateGraphFiles(dateStamp);
	}catch(CoreIllegalStateException e){
	    UI.printSimulationStatus(e.getMessage());
	}
	history = new ArrayList<Generation>();
	history.add(gn);

	PMAP = tmpPMAP;
	PSTATISTICS = tmpPSTATISTICS;
	PGENERATIONSTATISTICS = tmpPGENERATIONSTATISTICS;*/
    }
    
    /**
     *Gets the total number of Generations in the simulation.
     *@return The number of Generations.
     */
    public int historyLength(){
	return history.size();
    }

    /**
     *Sets the value for whether the simulator shall continue to print new info when a new Generation has been computed.
     *@param skip Whether to skip printout. Set it to true to skip.
     */
    public static synchronized void setSkipPrint(boolean skip){
	skipPrint = skip;
    }

    /**
     *Gets the value for whether the simulator shall continue to print new info when a new Generation has been computed.
     *@return true if the printout is beeing skipped.
     *@param false if the printout is beeing processed.
     */
    private synchronized boolean isSkipPrint(){
	return skipPrint;
    }

    /**
     *Sets the indicator for the status for this simulation. Determens the state CUSTOMMODE or default.
     *@param mode The mode to set the simulation to. Set it to true if the simulation is from a file or custom map.
     */
    public static void setCUSTOMMODE(boolean mode){
	CUSTOMMODE = mode;
    }

    /**
     *Sets the simulation run indication to running.
     */
    private synchronized void startGame(){
	game = true;
	UI.printSimulationStatus("Running...");
    }

    /**
     *Sets the simulation run indication to stopped.
     */
    private void endGame(){
	if(!CONTINUE){
	    try{
		continueSimulation();
	    }catch(CoreIllegalStateException e){
		UI.printSimulationStatus(e.getMessage());
	    }
	}
	game = false;
	UI.printSimulationStatus("Stopped.");
    }

    /**
     *Re-initiates a paused simulation run.
     *@throws CoreIllegalStateException If there are no paused simulations.
     */
    public synchronized void continueSimulation() throws CoreIllegalStateException{
	if(!isGame() && CONTINUE)
	    throw new CoreIllegalStateException("No simulation paused.");
	CONTINUE = true;
	startGame();
	notify();
    }

    /**
     *Makes the simulation pause in its progression. continueSimulation or end must be called in order to end simulation.
     *@throws CoreIllegalStateException If there are no simulation running.
     */
    public void hold() throws CoreIllegalStateException{
	if(!isGame() && !CONTINUE)
	    throw new CoreIllegalStateException("No simulation paused.");
	endGame();
	CONTINUE = false;
    }

    /**
     *Gets whether the simulation is computing or not.
     *@return true if a simulation is running.
     *@return false if no simulation is running.
     */
    public static synchronized boolean isGame(){
	return game;
    }

    /**
     *Sets the stabilized attribute for the simulation.
     *@param setSolid The state to set the stabilized to. Set it to true to have the simulation Stabilized.
     */
    public static void setSolid(boolean setSolid){
	solid = setSolid;
    }

    /**
     *Sets the next Generation display and computation delay time. Must be greater than one to be changed, else default value of ten will be used.
     *@param holdTime The delay time set to one tenth of a second. 
     */
    public static void setHOLDTIME(int holdTime){
	if(holdTime < 1)
	    holdTime = 10;
	HOLDTIME = holdTime*100;
    }

    /**
     *Returns the current hold time in one tenth of a second.
     *@return The current hold time.
     */
    public static int getHOLDTIME(){
	return HOLDTIME/100;
    }

    /**
     *Sets the status to print the Generation statistics or not.
     *@param genStat Whether to print or not. Set it to true to print.
     */
    public static void setPGENERATIONSTATISTICS(boolean genStat){
	PGENERATIONSTATISTICS = genStat;
    }

    /**
     *Gets the current status to print Generation statistics.
     *@return true if Generation statistics is beeing printed.
     *@return false if Generation statistics print is beeing skipped.
     */
    public static boolean getPGENERATIONSTATISTICS(){
	return PGENERATIONSTATISTICS;
    }
    
    /**
     *Sets the status to compute several runs or not.
     *@param longSimulation Whether to compute more than one simulation or not. Set it to true to compute many.
     */
    public static void setLONGSIMULATION(boolean longSimulation){
	LONGSIMULATION = longSimulation;
    }

   /**
     *Gets the current status to compute several runs.
     *@return true if long simulation is beeing performed.
     *@return false if long simulation is disabled.
     */
    public static boolean getLONGSIMULATION(){
	return LONGSIMULATION;
    }

    /**
     *Sets the status to print the map  or not.
     *@param pMap  Whether to print or not. Set it to true to print.
     */
    public static void setPMAP(boolean pMap){
	PMAP = pMap;
    }

   /**
     *Gets the current status to print the map.
     *@return true if map is beeing printed.
     *@return false if map statistics print is beeing skipped.
     */
    public static boolean getPMAP(){
	return PMAP;
    }

   /**
    *Sets the status to print the statistics sets or not.
     *@param pStatistics Whether to print or not. Set it to true to print.
     */
    public static void setPSTATISTICS(boolean pStatistics){
	PSTATISTICS = pStatistics;
    }

   /**
     *Gets the current status to print statistics.
     *@return true if statistics is beeing printed.
     *@return false if statistics print is beeing skipped.
     */
    public static boolean getPSTATISTICS(){
	return PSTATISTICS;
    }

    /**
     *Gets the simulation paused status.
     *@return true if the simulation is paused, false if not.
     */
    public synchronized boolean isPaused(){
	return !CONTINUE;
    }
}
