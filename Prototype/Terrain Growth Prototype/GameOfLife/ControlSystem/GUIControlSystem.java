package GameOfLife.ControlSystem;

import GameOfLife.ControlSystem.UserInterface;
import GameOfLife.Simulator.Core;
import GameOfLife.Simulator.Generation;
import GameOfLife.Simulator.Unit;
import GameOfLife.Simulator.CoreIllegalStateException;

import java.util.ArrayList;
import java.util.Calendar;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup; 
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.JFileChooser;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.filechooser.FileFilter;


/**
 *A graphical User Interface and Control System to be used with Core.
 *@author baardew
 *@version 1.0
 */
public class GUIControlSystem implements UserInterface{
    private Core simulator;
    private final int screenWidth, screenHeight;
    private MainMenu mainMenu;
    private FileMenu fileMenu;
    private WindowMenu windowMenu;
    private WriteInformation writeFrame;
    private StatisticsInformation statisticsFrame;
    private MapInformation mapFrame;
    private GeneralInformation generalFrame;
    private GenerationInformation generationFrame;
    private JLabel simulationStatus;
    private GUIControlSystem master; //Self for access from individual sub-windows


    /**
     *Colors to use with rendering frames.
     */
    private final Color BACK = Color.DARK_GRAY, FORE = Color.WHITE, INDI = Color.GREEN.darker();
    private final boolean macMode;


    /**
     *Initializes a new GUI Control System.
     *@param macMode If the Look And Feel shall correspond with Mac OS (true for mac mode).
     */
    public GUIControlSystem(boolean macMode){
	StartScreen splashScreen = new StartScreen();
	master = this;
	this.macMode = macMode;

	setLookAndFeel();
	if(macMode){
	    UIManager.put("Button.foreground", BACK);
	    UIManager.put("ProgressBar.selectionBackground", BACK);
	    UIManager.put("ProgressBar.selectionForeground", BACK);
	}
	
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	screenWidth = (int)screen.getWidth();
	screenHeight = (int)screen.getHeight();

	JFrame menus = createMenus();
	statisticsFrame = new StatisticsInformation();
	mapFrame = new MapInformation();
	generalFrame = new GeneralInformation();
	generationFrame = new GenerationInformation();
	splashScreen.remove();
	setRunningMode(false);
	displayAllJFrames(statisticsFrame, mapFrame, generalFrame, generationFrame, menus);
    }

    /**
     *Sets the look and fell for all dependant classes
     */
    private void setLookAndFeel(){
	setButtonColor();
	setRadioButtonColor();
	setMenuItemColor();
	setPanelColor();
	setLabelColor();
	setTextFieldColor();
	setOptionPaneColor();
	setProgressBarColor();
    }
    
    /**
     *Closes a given window and enables it in the window menu.
     *@param window The window to be closed.
     */
    public void dispose(JFrame window){
	window.dispose();
	windowMenu.enable(window);
	window = null;
    }

    /**
     *Splash Screen.
     */
    private class StartScreen extends JWindow{
	private Image image;
	private long time;
	private long delay = 3000; //millis

	/**
	 *Creates and displays a new start screen.
	 */
	public StartScreen(){
	    Calendar c = Calendar.getInstance();
	    time = c.getTimeInMillis();
	    setImage();
	    setSize(800, 480);
	    setLocation(150, 150); //Middle screen does not work: runtime fault
	    repaint();
	    setVisible(true);
	}
	
	/**
	 *Sets the image to be used.
	 */
	private void setImage(){
	    try{
		//Made to be cross-platform compatible, hence many files
		image = ImageIO.read(new File(new File(new File("Library"), "Images"), "StartScreen.jpg"));
	    }catch(IOException e){
		dispose();
	    }
	}

	/**
	 *Draws an image to the screen.
	 */
	public void paint(Graphics g){
	    g.drawImage(image, 0, 0, null);
	}

	/**
	 *Removes the start screen. It takes at least 2 seconds before it is removed.
	 */
	public void remove(){
	    dispose();
	}
    }

    /**
     *Sets the displaying maps visibility status.
     *@param visible Set to true to display map, false to hide it.
     */
    protected void setMapVisible(boolean visible){
	mapFrame.setVisible(visible);
    }

    /**
     *Sets the enabled status for the preferences window.
     *@param enabled Whether it should be avaible (true) or unavailbe (false).
     */
    protected void setPreferencesEnabled(boolean enabled){
	mainMenu.setPreferencesEnabled(enabled);
    }

    /**
     *Displays all given JFrames.
     *@param window All the JFrames to be set to visible.
     */
    private void displayAllJFrames(JFrame ... windows){
	for(JFrame f: windows)
	    f.setVisible(true);
    }
    
    protected void setFileMenuEnabled(boolean enabled){
	fileMenu.setEnabled(enabled);
    }

    /**
     *Sets the menus status to running mode or idle mode.
     *@param running Whether the simulation is running or not. Set to true if it is running, false if not.
     */
    protected void setRunningMode(boolean running){
	mainMenu.setRunningMode(running);
	fileMenu.setRunningMode(running);
    }

    /**
     *Creates the menus for the menu bar and simulation status.
     *@return The frame containing the menu bar and simulation status field.
     */
    private JFrame createMenus(){
	JFrame menu = new JFrame();
	menu.setTitle("THE GAME OF LIFE");
	menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	menu.setResizable(false);
	
	JMenuBar menubar = new JMenuBar();
	menubar.setBackground(BACK);
	mainMenu = new MainMenu();
	fileMenu = new FileMenu();
	windowMenu = new WindowMenu();

	menubar.add(mainMenu);
	menubar.add(fileMenu);
	menubar.add(windowMenu);
	menu.setJMenuBar(menubar);

	FlowLayout flow = new FlowLayout();
	flow.setAlignment(FlowLayout.LEFT);
	menu.setLayout(flow);
	menu.add(new JLabel("    Simulation status:"));
	simulationStatus = new JLabel("    n/a    ");
	simulationStatus.setForeground(INDI);
	menu.add(simulationStatus);

	if(macMode)
	    menu.setPreferredSize(new Dimension(400, 70));
	else
	    menu.setPreferredSize(new Dimension(400, 50));
	menu.pack();
	menu.setLocation(0, 0);
	return menu;
    }

    /**
     *Sets the progress bar colors. Dependant on BACK and INDI sets.
     */
    private void setProgressBarColor(){
	UIManager.put("ProgressBar.background", BACK);
	UIManager.put("ProgressBar.foreground", INDI);
	UIManager.put("ProgressBar.selectionBackground", FORE);
	UIManager.put("ProgressBar.selectionForeground", FORE);

    }

    /**
     *Sets the button colors. Dependant on BACK and FORE sets.
     */
    private void setButtonColor(){
	ArrayList<Object> list = new ArrayList<Object>();
	list.add(new Float(0.3F));
	list.add(new Float(0));
	list.add(BACK);
	list.add(BACK.brighter().brighter());
	list.add(BACK.darker().darker());
	UIManager.put("Button.gradient", list);
	UIManager.put("Button.foreground", FORE);
    }

    /**
     *Sets the radio button colors. Dependant on BACK and FORE sets.
     */
    private void setRadioButtonColor(){
	UIManager.put("RadioButton.background", BACK);
	UIManager.put("RadioButton.foreground", FORE);
    }

    /**
     *Sets the menu and menu item colors. Dependant on BACK and FORE sets.
     */
    private void setMenuItemColor(){
	UIManager.put("MenuItem.background", BACK);
	UIManager.put("MenuItem.foreground", FORE);
	UIManager.put("Menu.background", BACK);
	UIManager.put("Menu.foreground", FORE);
    }

    /**
     *Sets the panel background color. Dependant on BACK.
     */
    private void setPanelColor(){
	UIManager.put("Panel.background", BACK);
    }

    /**
     *Sets the label colors. Dependant on BACK and FORE sets.
     */
    private void setLabelColor(){
	UIManager.put("Label.background", BACK);
	UIManager.put("Label.foreground", FORE);
    }

    /**
     *Sets the text field colors. Dependant on BACK and FORE sets.
     */
    private void setTextFieldColor(){
	UIManager.put("TextField.background", BACK.brighter().brighter());
	UIManager.put("TextField.foreground", FORE);
    }

    /**
     *Sets the pop-up window colors. Dependant on BACK and FORE sets.
     */
    private void setOptionPaneColor(){
	UIManager.put("OptionPane.background", BACK);
	UIManager.put("OptionPane.foreground", FORE);
	UIManager.put("OptionPane.messageForeground", FORE);
    }

    /**
     *Groups a selection of radio buttons. Adds them to a JPanel with either horizontal or vertical alignment.
     *@param vertical Whether the buttons should be vertical aligned (true for vertical) or not.
     *@param buttons The buttons to be grouped together.
     *@return A panel containing the radio buttons grouped together.
     */
    protected JPanel groupRadioButtons(boolean vertical, JRadioButton ... buttons){
	int items = buttons.length;
	JPanel panel = new JPanel();
	ButtonGroup group = new ButtonGroup();
	panel.setLayout(new GridLayout(vertical ? items : 0, !vertical ? items : 0));
	for(JRadioButton r: buttons){
	    panel.add(r);
	    group.add(r);
	}
	return panel;
    }

    /**
     *Preferences window.
     */
    private class Preferences extends JFrameWithClose implements ActionListener{
	private JLabel map, stat, longsim, genstat, lbl_delay;
	private JRadioButton mapT, mapF, statT, statF, longsimT, longsimF, genstatT, genstatF;
	private JTextField txt_delay;
	private JButton cancel, save;
	private boolean mapB, statB, longsimB, genstatB;
	private int delay;

	/**
	 *Creates a new instance of the preference window and displays it. Only 1 at a time.
	 */
	public Preferences(){
	    super("Preferences");
	    master.setPreferencesEnabled(false);
	    GridLayout grid = new GridLayout(6, 2, 5, 5);
	    setLayout(grid);

	    addWindowListener(this);
	    setResizable(false);
	    
	    mapB = Core.getPMAP();
	    statB = Core.getPSTATISTICS();
	    longsimB = Core.getLONGSIMULATION();
	    genstatB = Core.getPGENERATIONSTATISTICS();
	    delay = Core.getHOLDTIME();


	    map = new JLabel("    Update Map:");
	    stat = new JLabel("    Update statistics:");
	    longsim = new JLabel("    Long simulation:");
	    genstat = new JLabel("    Update Generation statistics:");
	    lbl_delay = new JLabel("    Next Generation delay time:");

	    txt_delay = new JTextField(String.format("%d", delay), 10);

	    mapT = new JRadioButton("Yes", mapB);
	    mapF = new JRadioButton("No", !mapB);
	    statT = new JRadioButton("Yes", statB);
	    statF = new JRadioButton("No", !statB);
	    longsimT = new JRadioButton("Yes", longsimB);
	    longsimF = new JRadioButton("No", !longsimB);
	    genstatT = new JRadioButton("Yes", genstatB);
	    genstatF = new JRadioButton("No", !genstatB);

	    save = new JButton("Save");
	    save.addActionListener(this);
	    cancel = new JButton("Cancel");
	    cancel.addActionListener(this);

	    add(map);
	    add(groupRadioButtons(false, mapT, mapF));
	    add(stat);
	    add(groupRadioButtons(false, statT, statF));
	    add(longsim);
	    add(groupRadioButtons(false, longsimT, longsimF));
	    add(genstat);
	    add(groupRadioButtons(false, genstatT, genstatF));
	    add(lbl_delay);
	    add(txt_delay);
	    add(cancel);
	    add(save);

	    pack();
	    setLocation(0, (screenHeight-getHeight())/2);
	    setVisible(true);
	}
	
	/**
	 *Button event manager. Either cancel and discard changes or save them.
	 *@param The button event.
	 *@trows NumberFormatException if the next generation delay number is incorrect.
	 */
	public void actionPerformed(ActionEvent event){
	    if(event.getSource() == cancel)
		dispose();
	    else if(event.getSource() == save){
		mapB = mapT.isSelected();
		statB = statT.isSelected();
		longsimB = longsimT.isSelected();
		genstatB = genstatT.isSelected();

		try{
		    delay = Integer.parseInt(txt_delay.getText());
		    if(delay < 1) throw new NumberFormatException();
		}catch(NumberFormatException e){
		    JOptionPane.showMessageDialog(null, "Illegal value set. Using default value." , "Next generation delay", JOptionPane.ERROR_MESSAGE);
		    delay = Core.getHOLDTIME();
		    txt_delay.setText(String.format("%d", delay));
		    return;
		}
		
		Core.setPMAP(mapB);
		Core.setPSTATISTICS(statB);
		Core.setLONGSIMULATION(longsimB);
		Core.setPGENERATIONSTATISTICS(genstatB);
		Core.setHOLDTIME(delay);
		JOptionPane.showMessageDialog(null, "Settings saved.", "Save", JOptionPane.INFORMATION_MESSAGE);
		dispose();
	    }
	}

	//@override
	public void windowClosed(WindowEvent event){
	    windowClosing(event);
	}

	//@override
	public void windowClosing(WindowEvent event){
	    master.setPreferencesEnabled(true);
	}
    }

    /**
     *Setup window.
     */
    private class Setup extends JFrameWithClose implements ActionListener{
	private JButton save, cancel;
	private JTextField txt_x, txt_y, txt_spawn;
	private JLabel lbl_x, lbl_y, lbl_spawn;
	private int sizex, sizey, spawnratio;
	private SimulationSetup simSetup;

	/**
	 *Creates and displays a new instance of a setup window. Only 1 at a time.
	 *@param simSetup The simulation setups master window.
	 */
	public Setup(SimulationSetup simSetup){
	    super("Setup");
	    this.simSetup = simSetup;
	    setResizable(false);
	    simSetup.setVisible(false);
	    GridLayout grid = new GridLayout(4, 2, 5, 5);
	    setLayout(grid);
	    addWindowListener(this);

	    sizey = Generation.getSIZEX();
	    sizex = Generation.getSIZEY();
	    spawnratio = Generation.getSPAWNRATIO();

	    lbl_x = new JLabel("    Map size X:");
	    lbl_y = new JLabel("    Map size Y:");
	    lbl_spawn = new JLabel("    Spawn ratio:");

	    txt_x = new JTextField(String.format("%d", sizex), 10);
	    txt_y = new JTextField(String.format("%d", sizey), 10);
	    txt_spawn = new JTextField(String.format("%d", spawnratio), 10);

	    save = new JButton("Save");
	    save.addActionListener(this);
	    cancel = new JButton("Cancel");
	    cancel.addActionListener(this);

	    add(lbl_x);
	    add(txt_x);
	    add(lbl_y);
	    add(txt_y);
	    add(lbl_spawn);
	    add(txt_spawn);
	    add(cancel);
	    add(save);

	    pack();
	    setLocation(0, (screenHeight-getHeight())/2);
	    setVisible(true);
	}

	/**
	 *Button event manager. Either cancel and discard changes or save them.
	 *@param event The button event.
	 *@throws NumberFormatException If any value is incorrect.
	 */
	public void actionPerformed(ActionEvent event){
	    if(event.getSource() == cancel){
		simSetup.setVisible(true);
		dispose();
	    }else if(event.getSource() == save){
		try{
		    sizey = Integer.parseInt(txt_x.getText());
		    if(sizey < 1) throw new NumberFormatException();
		}catch(NumberFormatException e){
		    sizey = Generation.getSIZEY();
		    txt_x.setText(String.format("%d", sizey));
		    JOptionPane.showMessageDialog(null, "Illegal value set. Using default value.", "Size X", JOptionPane.ERROR_MESSAGE);
		    return;
		}

		try{
		    sizex = Integer.parseInt(txt_y.getText());
		    if(sizex < 1) throw new NumberFormatException();
		}catch(NumberFormatException e){
		    sizex = Generation.getSIZEX();
		    txt_y.setText(String.format("%d", sizex));
		    JOptionPane.showMessageDialog(null, "Illegal value set. Using default value.", "Size Y", JOptionPane.ERROR_MESSAGE);
		    return;
		}

		try{
		    spawnratio = Integer.parseInt(txt_spawn.getText());
		    if(spawnratio < 5) throw new NumberFormatException();
		}catch(NumberFormatException e){
		    spawnratio = Generation.getSPAWNRATIO();
		    txt_spawn.setText(String.format("%d", spawnratio));
		    JOptionPane.showMessageDialog(null, "Illegal value set. Using default value.", "Spawn ratio", JOptionPane.ERROR_MESSAGE);
		    return;
		}

		Generation.setSPAWNRATIO(spawnratio);
		Generation.setSizeXY(sizex, sizey);

		if(mapFrame != null)
		    mapFrame.setNewSize();

		JOptionPane.showMessageDialog(null, "Settings saved.", "Save", JOptionPane.INFORMATION_MESSAGE);
		simSetup.setVisible(true);
		dispose();
	    }
	}

	//@override
	public void windowClosing(WindowEvent event){
	    simSetup.setVisible(true);
	    dispose();
	}
    }

    /**
     *Simulation setup window.
     */
    private class SimulationSetup extends JFrameWithClose implements ActionListener{
	private JButton setup, cancel, use;
	private JRadioButton random, custom, file;
	private boolean started;

	/**
	 *Creates and displays a new simulation setup window. Only 1 at a time. Contains three modes: Default (random), Custom and from File.
	 */
	public SimulationSetup(){
	    super("New simulation setup");
	    setResizable(false);
	    addWindowListener(this);
	    mainMenu.setSimulationSetupEnabled(false);
	    started = false;

	    GridLayout grid = new GridLayout(2, 2, 10, 10);
	    setLayout(grid);
	    
	    setup = new JButton("Setup");
	    setup.addActionListener(this);	    
	    cancel = new JButton("Cancel");
	    cancel.addActionListener(this);
	    use = new JButton("Use");
	    use.addActionListener(this);

	    random = new JRadioButton("Default", true);
	    custom = new JRadioButton("Custom", false);
	    file = new JRadioButton("From file", false);

	    add(groupRadioButtons(true, random, custom, file));
	    add(setup);
	    add(cancel);
	    add(use);

	    setPreferredSize(new Dimension(270, 130));
	    setLocation(0, (screenHeight-getHeight())/2);
	    pack();
	    setVisible(true);
	}

	/**
	 *Button event manager. Either cancel, access or use any of the three simulation setups.
	 *@param event The button event.
	 */
	public void actionPerformed(ActionEvent event){
	    if(event.getSource() == cancel){
		dispose();
	    }else if(event.getSource() == setup){
		new Setup(this);
	    }else if(event.getSource() == use){
		if(random.isSelected()){
		    simulator = new Core(master);
		}else if(custom.isSelected()){
		    new CustomSimulation();
		    started = true;
		    dispose();
		    return;
		}else if(file.isSelected()){
		    String file = fileMap();
		    if(file == null){
			return;
		    }
		    try{
			simulator = new Core(master, new Generation(file));
		    }catch(Exception e){
			JOptionPane.showMessageDialog(null, "The file has incorrect format." , "Simulation from file", JOptionPane.ERROR_MESSAGE);
			return;
		    }
		    if(mapFrame != null)
			mapFrame.setNewSize();
		}

		master.setRunningMode(true);
		simulator.start();
		started = true;
		dispose();
	    }
	}
	 
	//@Overrride
	public void windowClosed(WindowEvent event){
	    windowClosing(event);
	}

	//@Overrride
	public void windowClosing(WindowEvent event){
	    mainMenu.setSimulationSetupEnabled(!started);
	}

	/**
	 *Gets the file to be used as a basis.
	 *@return The path name for the file.
	 */
	private String fileMap(){
	    JFileChooser filefinder = new JFileChooser();
	    filefinder.addChoosableFileFilter(new FileFilter(){
		    
		    //@override
		    public String getDescription() {
			return "*.gol";
		    }
		    
		    //@override
		    public boolean accept(File f) {
			return f.getName().endsWith(".gol");
		    }
		});
	    int selection = filefinder.showOpenDialog(null);
	    if(selection == JFileChooser.APPROVE_OPTION){
		return filefinder.getSelectedFile().toString();
	    }else{
		return null;
	    }
	}
    }

    /**
     *Window menu in the menu bar. For re-opening closed windows.
     */
    private class WindowMenu extends JMenu implements ActionListener{
	private JMenuItem statistics, map, general, generation;

	/**
	 *Creates a new instance of the window menu.
	 */
	public WindowMenu(){
	    super("Window");
	    
	    statistics = new JMenuItem("Statistics");
	    statistics.addActionListener(this);
	    statistics.setEnabled(false);
	    map = new JMenuItem("Map");
	    map.addActionListener(this);
	    map.setEnabled(false);
	    general = new JMenuItem("General");
	    general.addActionListener(this);
	    general.setEnabled(false);
	    generation = new JMenuItem("Generation");
	    generation.addActionListener(this);
	    generation.setEnabled(false);

	    add(statistics);
	    add(map);
	    add(general);
	    add(generation);
	}

	/**
	 *Re-enables a specified window.
	 *@param window The window that has been closed. To be set as enabled to open.
	 */
	public void enable(JFrame window){
	    if(window == statisticsFrame)
		statistics.setEnabled(true);
	    else if(window == mapFrame)
		map.setEnabled(true);
	    else if(window == generalFrame)
		general.setEnabled(true);
	    else if(window == generationFrame)
		generation.setEnabled(true);
	}

	/**
	 *Button event manager. Displays the selected window from the menu list.
	 *@param event The button event.
	 */
	public void actionPerformed(ActionEvent event){
	    if(event.getSource() == statistics){
		statistics.setEnabled(false);
		statisticsFrame = new StatisticsInformation();
		statisticsFrame.setVisible(true);
	    }else if(event.getSource() == map){
		map.setEnabled(false);
		mapFrame = new MapInformation();
		mapFrame.setVisible(true);
	    }else if(event.getSource() == general){
		general.setEnabled(false);
		generalFrame = new GeneralInformation();
		generalFrame.setVisible(true);
	    }else if(event.getSource() == generation){
		generation.setEnabled(false);
		generationFrame = new GenerationInformation();
		generationFrame.setVisible(true);
	    }

	    if(simulator != null)
	    	simulator.printToUI();
	}
    }

    /**
     *The file menu for saving Generation history.
     */
    private class FileMenu extends JMenu implements ActionListener{
	private JMenuItem allGenerations, specifyGeneration, dataFile, filesForGraph;

	/**
	 *Creates a new instance of the file menu.
	 */
	public FileMenu(){
	    super("Write to File");

	    allGenerations = new JMenuItem("All generations");
	    allGenerations.addActionListener(this);
	    specifyGeneration = new JMenuItem("Specify generation");
	    specifyGeneration.addActionListener(this);
	    dataFile = new JMenuItem("Data file");
	    dataFile.addActionListener(this);
	    filesForGraph = new JMenuItem("Files for graphs");
	    filesForGraph.addActionListener(this);

	    add(allGenerations);
	    add(specifyGeneration);
	    add(dataFile);
	    add(filesForGraph);
	}

	/**
	 *Sets the menus mode to enabled or disabled depending on a simulation running or not.
	 *@param running The status of the current simulation. If it is running, the menu is not accessible.
	 */
	public void setRunningMode(boolean running){
	    if(simulator != null){
		setEnabled(!running || simulator.isPaused());
	    }else{
		setEnabled(false);
	    }
	}

	/**
	 *Button event manager. Either starts a new instance of writing all generations, one generation, a data file or graph files.
	 *@param event The button event.
	 */
	public void actionPerformed(ActionEvent event){
	    if(event.getSource() == allGenerations){
		new FileSaver(FileSaver.ALL);
	    }else if(event.getSource() == specifyGeneration){
		new FileSaver(FileSaver.GENERATION);
	    }else if(event.getSource() == dataFile){
		new FileSaver(FileSaver.DATA);
	    }else if(event.getSource() == filesForGraph){
		new FileSaver(FileSaver.GRAPH);
	    }
	}
    }
    
    /**
     *Window for setting parameters when using the write file menu.
     */
    private class FileSaver extends JFrameWithClose implements ActionListener{
	public static final int ALL = 0, GENERATION = 1, DATA = 2, GRAPH = 3;
	private int type;
	private JButton cancel, save;
	private JTextField txt_filename, txt_generation;
	private JLabel lbl_filename, lbl_generation;

	/**
	 *Creates a new instance of the file writer options, dependant of the type to be used.
	 *@param type The window type to be used.
	 */
	public FileSaver(int type){
	    super("Save");
	    this.type = type;
	    master.setFileMenuEnabled(false);
	    addWindowListener(this);

	    lbl_filename = new JLabel("    File name:");
	    lbl_generation = new JLabel("    Generation:");
	    txt_filename = new JTextField();
	    txt_generation = null;
	    cancel = new JButton("Cancel");
	    cancel.addActionListener(this);
	    save = new JButton("Save");
	    save.addActionListener(this);

	    if(type == ALL || type == GRAPH){
		fileMenu();
	    }else{
		fileAndGenerationMenu();
	    }
	    
	    pack();
	    setLocation(0, (screenHeight-getHeight())/2);
	    setResizable(false);
	    setVisible(true);
	}

	/**
	 *Creates a simple menu with only name.
	 */
	private void fileMenu(){
	    GridLayout grid = new GridLayout(2, 2, 5, 5);
	    setLayout(grid);
	    add(lbl_filename);
	    add(txt_filename);
	    add(cancel);
	    add(save);
	    setPreferredSize(new Dimension(500, 100));
	}

	/**
	 *Creates a simple menu with name and generation selection.
	 */
	private void fileAndGenerationMenu(){
	    txt_generation = new JTextField();
	    GridLayout grid = new GridLayout(3, 2, 5, 5);
	    setLayout(grid);
	    add(lbl_filename);
	    add(txt_filename);
	    add(lbl_generation);
	    add(txt_generation);
	    add(cancel);
	    add(save);
	    setPreferredSize(new Dimension(500, 130));
	}

	/**
	 *Button event manager. Either cancel the write or proceed.
	 *@param event The button event.
	 *@exeption NoFileName If there is no file name.
	 *@throws NumberFormatException If the specified generation is outside history span.
	 */
	public void actionPerformed(ActionEvent event){
	    if(event.getSource() == cancel){
		dispose();
	    }else if(event.getSource() == save){
		final String filename = txt_filename.getText();
		
		if(filename == null || filename.length() < 1){
		    JOptionPane.showMessageDialog(null, "No file name specified.", "File name", JOptionPane.ERROR_MESSAGE);
		    return;
		}

		if(type == ALL){
		    dispose();
		    writeFrame = new WriteInformation(filename);
		    new Thread(new Runnable(){
			    public void run(){
				try{
				    simulator.writeToFile(filename);
				}catch(CoreIllegalStateException e){
				    simulationStatus.setText(e.getMessage());
				    return;
				}
			    }
			}).start();

		}else if(type == GRAPH){
		    dispose();
		    writeFrame = new WriteInformation(filename);
		    new Thread(new Runnable(){
			    public void run(){
				try{
				    simulator.generateGraphFiles(filename);
				}catch(CoreIllegalStateException e){
				    simulationStatus.setText(e.getMessage());
				    return;
				}
			    }
			}).start();
		}else{
		    int gen = -1;
		    try{
			gen = Integer.parseInt(txt_generation.getText());
			if(gen < 0 || gen >= simulator.historyLength()) throw new NumberFormatException();
		    }catch(NumberFormatException e){
			JOptionPane.showMessageDialog(null, "Generation outside history span.", "Generation", JOptionPane.ERROR_MESSAGE);
			return;
		    }

		    dispose();
		    writeFrame = new WriteInformation(filename);

		    final int usegen = gen;

		    if(type == GENERATION)
			new Thread(new Runnable(){
				public void run(){
				    try{
					simulator.writeToFile(filename, usegen);
				    }catch(CoreIllegalStateException e){
					simulationStatus.setText(e.getMessage());
					return;
				    }
				}
			    }).start();
		    else if(type == DATA)
			new Thread(new Runnable(){
				public void run(){
				    try{
					simulator.writeToData(filename, usegen);
				    }catch(CoreIllegalStateException e){
					simulationStatus.setText(e.getMessage());
					return;
				    }
				}
			    }).start();
		}
	    }
	}
	
	//@overrride
	public void windowClosed(WindowEvent event){
	    windowClosing(event);
	}
	
	//@override
	public void windowClosing(WindowEvent event){
	    master.setFileMenuEnabled(true);
	}
    }

    /**
     *The main menu.
     */
    private class MainMenu extends JMenu implements ActionListener{
	private JMenuItem findGeneration, newSimulation, preferences, continuecurrent, pausecurrent, end, quit;

	/**
	 *Creates a new main menu.
	 */
	public MainMenu(){
	    super("Main Menu");

	    findGeneration = new JMenuItem("Find generation");
	    findGeneration.addActionListener(this);

	    
	    newSimulation = new JMenuItem("New simulation setup");
	    newSimulation.addActionListener(this);

	    
	    preferences = new JMenuItem("preferences");
	    preferences.addActionListener(this);

	    continuecurrent = new JMenuItem("Continue current");
	    continuecurrent.addActionListener(this);

	    pausecurrent = new JMenuItem("Pause current");
	    pausecurrent.addActionListener(this);
  
	    end = new JMenuItem("End current");
	    end.addActionListener(this);

	    
	    quit = new JMenuItem("Quit");
	    quit.addActionListener(this);

	    add(findGeneration);
	    add(newSimulation);
	    add(continuecurrent);
	    add(pausecurrent);
	    add(preferences);
	    add(end);
	    add(quit);
	}

	/**
	 *Button event manager. Either quit the application, end current simulation, starts a new simulation, display preferences pane or find generation window.
	 *@param event The button event.
	 */
	public void actionPerformed(ActionEvent event){
	    if(event.getSource() == quit){
		if(simulator != null && Core.isGame()){
		    try{
			simulator.end();
		    }catch(CoreIllegalStateException e){
			simulationStatus.setText("No simulation to end.");
		    }
		}
		System.exit(0);
	    }else if(event.getSource() == end){
		try{
		    simulator.end();
		    master.setRunningMode(false);
		}catch(CoreIllegalStateException e){
		    simulationStatus.setText(e.getMessage());
		    return;
		}
		master.setRunningMode(false);
		printSimulationStatus("Idle");
	    }else if(event.getSource() == newSimulation){
		new SimulationSetup();
	    }else if(event.getSource() == preferences){
		new Preferences();
	    }else if(event.getSource() == findGeneration){
		if(simulator == null){
		    JOptionPane.showMessageDialog(null, "No simulation stored.", "No Simulator", JOptionPane.INFORMATION_MESSAGE);
		}else{
		    new GenerationFinder();
		}
	    }else if(event.getSource() == continuecurrent){
		try{
		    simulator.continueSimulation();
		    master.setRunningMode(true);
		}catch(CoreIllegalStateException e){
		    simulationStatus.setText(e.getMessage());
		}
	    }else if(event.getSource() == pausecurrent){
		try{
		    simulator.hold();
		    master.setRunningMode(false);
		}catch(CoreIllegalStateException e){
		    simulationStatus.setText(e.getMessage());
		}
	    }
	}
	
	/**
	 *Sets the accessibility for menu items. Depending on the status of the simulation.
	 *@param running Whether the simulation is running or not.
	 */
	public void setRunningMode(boolean running){
	    if(simulator == null){
		newSimulation.setEnabled(true);
		end.setEnabled(false);
		continuecurrent.setEnabled(false);
		pausecurrent.setEnabled(false);
	    }else{
		newSimulation.setEnabled(!running && !simulator.isPaused());
		end.setEnabled(running || simulator.isPaused());
		continuecurrent.setEnabled(!running && simulator.isPaused());
		pausecurrent.setEnabled(running && !simulator.isPaused());
	    }
	}

	/**
	 *Sets the find generation enabled status.
	 @param enabled Whether the menu item is enabled or not (true if enabled).
	 */
	public void setFindGenerationEnabled(boolean enabled){
	    findGeneration.setEnabled(enabled);
	}

	/**
	 *Sets the new simulation setup enabled status.
	 *@param enabled Whtether the menu item is enabled or not (true if enabled).
	 */
	public void setSimulationSetupEnabled(boolean enabled){
	    newSimulation.setEnabled(enabled);
	}

	/**
	 *Sets the preferences enabled status.
	 *@param enabled Whtether the menu item is enabled or not (true if enabled).
	 */
	public void setPreferencesEnabled(boolean enabled){
	    preferences.setEnabled(enabled);
	}
    }

    /**
     *Generation finder window.
     */
    private class GenerationFinder extends JFrameWithClose implements ActionListener{
	private JLabel lbl_generation, txt_current;
	private JTextField txt_generation;
	private JButton see, done, lbl_current;

	/**
	 *Creates and displays a new instance of a find generation window.
	 */
	public GenerationFinder(){
	    super("Find generation");
	    mainMenu.setFindGenerationEnabled(false);
	    simulator.setSkipPrint(true);
	    
	    setResizable(false);
	    setLayout(new GridLayout(3, 2, 5, 5));
	    addWindowListener(this);

	    lbl_current = new JButton("Generations:");
	    lbl_current.addActionListener(this);
	    txt_current = new JLabel("0 - " + (simulator.historyLength()-1));
	    lbl_generation = new JLabel("    Generation:");
	    txt_generation = new JTextField();
	    see = new JButton("Display");
	    see.addActionListener(this);
	    done = new JButton("Done");
	    done.addActionListener(this);

	    add(lbl_generation);
	    add(txt_generation);
	    add(lbl_current);
	    add(txt_current);
	    add(done);
	    add(see);

	    pack();
	    setVisible(true);
	}

	/**
	 *Button event manager. Displays the specified generation to all visible windows.
	 *@param event The button event.
	 *@throws NumberFormatException If specified generation is outside history span.
	 */
	public void actionPerformed(ActionEvent event){
	    if(event.getSource() == done){
		dispose();
	    }else if(event.getSource() == see){
		txt_current.setText("0 - " + (simulator.historyLength()-1));
		int gen = -1;
		try{
		    gen = Integer.parseInt(txt_generation.getText());
		    if(gen < 0 || gen >= simulator.historyLength()) throw new NumberFormatException();
		}catch(NumberFormatException e){
		    JOptionPane.showMessageDialog(null, "Generation outside history span." , "Generation", JOptionPane.ERROR_MESSAGE);
		    return;
		}

		simulator.printToUI(gen);
	    }else if(event.getSource() == lbl_current){
		txt_current.setText("0 - " + (simulator.historyLength()-1));
	    }
	}

	//@Override
	public void windowClosing(WindowEvent event){
	    windowClosed(event);
	}

	//@Override
	public void windowClosed(WindowEvent event){
	    simulator.setSkipPrint(false);
	    mainMenu.setFindGenerationEnabled(true);
	}
    }

    /**
     *Window for displaying individual units` count and percentage.
     */
    private class StatisticsInformation extends JFrameWithClose{
	private JLabel lbl_unitA, txt_colorA, lbl_countA, txt_countA, lbl_percentA, txt_percentA;
	private JLabel lbl_unitB, txt_colorB, lbl_countB, txt_countB, lbl_percentB, txt_percentB;
	private JLabel lbl_unitC, txt_colorC, lbl_countC, txt_countC, lbl_percentC, txt_percentC;
	private JTextField lbl_colorA, lbl_colorB, lbl_colorC;

	/**
	 *Creates a new instance of the statistics window. Only 1 at a time.
	 */
	public StatisticsInformation(){
	    super("Statistics");
	    setResizable(false);
	    setLayout(new GridLayout(3, 12, 10, 5));
	    addWindowListener(this);

	    lbl_unitA = new JLabel("    " + Unit.A + " :");
	    lbl_colorA = new JTextField();
	    lbl_colorA.setEditable(false);
	    lbl_colorA.setBackground(Unit.A.getColor());
	    lbl_countA = new JLabel("numerical:");
	    txt_countA = new JLabel("      n/a      ");
	    lbl_percentA = new JLabel("percentage:");
	    txt_percentA = new JLabel("             n/a                ");

	    add(lbl_unitA);
	    add(lbl_colorA);
	    add(lbl_countA);
	    add(txt_countA);
	    add(lbl_percentA);
	    add(txt_percentA);

	    lbl_unitB = new JLabel("    " + Unit.B + " :");
	    lbl_colorB = new JTextField();
	    lbl_colorB.setEditable(false);
	    lbl_colorB.setBackground(Unit.B.getColor());
	    lbl_countB = new JLabel("numerical:");
	    txt_countB = new JLabel("      n/a      ");
	    lbl_percentB = new JLabel("percentage:");
	    txt_percentB = new JLabel("             n/a                ");

	    add(lbl_unitB);
	    add(lbl_colorB);
	    add(lbl_countB);
	    add(txt_countB);
	    add(lbl_percentB);
	    add(txt_percentB);

	    lbl_unitC = new JLabel("    " + Unit.C + " :");
	    lbl_colorC = new JTextField();
	    lbl_colorC.setEditable(false);
	    lbl_colorC.setBackground(Unit.C.getColor());
	    lbl_countC = new JLabel("numerical:");
	    txt_countC = new JLabel("      n/a      ");
	    lbl_percentC = new JLabel("percentage:");
	    txt_percentC = new JLabel("             n/a                 ");

	    add(lbl_unitC);
	    add(lbl_colorC);
	    add(lbl_countC);
	    add(txt_countC);
	    add(lbl_percentC);
	    add(txt_percentC);
	    
	    
	    pack();
	    setLocation(0, screenHeight-getHeight());
	}

	/**
	 *Updates the information.
	 *@param unit The Unit to be updated.
	 *@param total The total number of Units on the map.
	 *@param newcount The latest count for this Unit.
	 *@param oldcount The previous count for this unit.
	 */
	public void update(Unit unit, int total, int newcount, int oldcount){
	    if(unit == Unit.A){
		update(total, newcount, oldcount, txt_countA, txt_percentA);
	    }else if(unit == Unit.B){
		update(total, newcount, oldcount, txt_countB, txt_percentB);
	    }else{
		update(total, newcount, oldcount, txt_countC, txt_percentC);
	    }
	}

	/**
	 *Renders an update for a specified unit to screen.
	 *@param total The total number of Units on the map.
	 *@param newcount The latest count for this Unit.
	 *@param oldcount The previous count for this unit.
	 *@param count The label to display the count in.
	 *@param percent The label to display percentace in.
	 */
	private void update(int total, int newcount, int oldcount, JLabel count, JLabel percent){
	    String sign;
	    int countIncrease = newcount-oldcount;
	    if(countIncrease >= 0)
		sign = "+";
	    else
		sign = "-";
	    
	    countIncrease = (countIncrease >= 0) ? newcount-oldcount : (newcount-oldcount)*-1;
	    double percentage = ((double)(newcount*100))/(total), percentIncrease = (percentage-((double)(oldcount*100))/(total));
	    percentage = (percentage >= 0) ? percentage : percentage*-1;
	    percentIncrease = (percentIncrease >= 0) ? percentIncrease : percentIncrease*-1;

	    count.setText(String.format("%6d (%s %4d)", newcount, sign, countIncrease));
	    percent.setText(String.format("%3.2f%s (%s %3.2f%s)", percentage, "%", sign, percentIncrease, "%"));
	}
    }

    /**
     *The map window.
     */
    private class MapInformation extends JFrameWithClose{
	private final int radius = 1, space = 0, edge = 0;
	private int sizex, sizey, dimx, dimy;
	private Unit[][] map;
	private boolean first;
	
	/**
	 *Creates a new instance of the map window. Only 1 at a time.
	 */
	public MapInformation(){
	    super("Map");
	    setResizable(false);
	    addWindowListener(this);
	    setBackground(BACK);

	    setNewSize();

	    repaint();
	    setLocation(screenWidth-getWidth(), 0);
	}

	/**
	 *Updates the map.
	 *@param map The map to be printed to screen.
	 */
	public void update(Unit[][] map){
	    this.map = map;
	    repaint();
	}

	/**
	 *Resizes the window to fit new dimensions. Dependand on Generation`s X and Y sizes.
	 */
	public void setNewSize(){
	    sizex = Generation.getSIZEX();
	    sizey = Generation.getSIZEY();

	    dimx = sizey*(radius+space)+2*edge;
	    dimy = sizex*(radius+space)+2*edge;

	    map = new Unit[sizex][sizey];
	    map[0][0] = null;
	    first = true;
	    setSize(new Dimension(dimx, dimy));
	    repaint();
	}

	/**
	 *Draws all the units to the map.
	 *@param g The Graphics.
	 */
	public void paint(Graphics g){
	    Graphics2D g2d = (Graphics2D)g;

	    if(map[0][0] == null){
		g2d.setPaint(BACK);
		g2d.fillRect(0, 0, dimx, dimy);
		g2d.setPaint(Color.WHITE);
		g2d.drawString("n/a", dimx/2, dimy/2);
	    }else{
		if(first){
		    g2d.setPaint(BACK);
		    g2d.drawString("n/a", dimx/2, dimy/2);
		    first = false;
		}
		for(int x = 0; x < sizex; x++){
		    for(int y = 0; y < sizey; y++){
			g2d.setPaint(map[x][y].getColor());
			g2d.fillRect(y*(radius+space)+edge, x*(radius+space)+edge, radius, radius);
		    }
		}
	    }
	}
    }
    
    /**
     *Custom simulation setup window.
     */
    private class CustomSimulation extends JFrameWithClose{
	private final int radius = 10, space = 2, edge = 50;
	private int sizex, sizey, dimx, dimy;
	private Unit[][] map;
	private Menu menu;
	
	/**
	 *Creates and displays a new instance of the custom simulation setup window. Only 1 at a time.
	 */
	public CustomSimulation(){
	    super("Custom Simulation");
	    setResizable(false);
	    setBackground(BACK);

	    menu = new Menu(this);
	    addMouseListener((MouseListener)menu);
	    addWindowListener(this);
	    
	    setSize();
	    reset();
	    master.setMapVisible(false);
	    repaint();
	    setLocation(150, 150);
	    setVisible(true);
	}
	
	/**
	 *Resets the value of all Units on the new map to Unit C.
	 */
	public void reset(){
	    map = new Unit[sizex][sizey];
	    for(int x = 0; x < sizex; x++)
		for(int y = 0; y < sizey; y++)
		    map[x][y] = Unit.C;
	    repaint();
	}
	
	/**
	 *Sets the size of the map.
	 */
	public void setSize(){
	    sizex = Generation.getSIZEX();
	    sizey = Generation.getSIZEY();
	    dimx = sizey*(radius+space)+2*edge;
	    dimy = sizex*(radius+space)+2*edge;
	    setSize(new Dimension(dimx, dimy));
	}
	
	/**
	 *Draws all the units to the map.
	 *@param g The Graphics.
	 */
	public void paint(Graphics g){
	    Graphics2D g2d = (Graphics2D)g;
	    for(int x = 0; x < sizex; x++){
		for(int y = 0; y < sizey; y++){
		    g2d.setPaint(map[x][y].getColor());
		    g2d.fillRect(y*(radius+space)+edge, x*(radius+space)+edge, radius, radius);
		}
	    }	   
	}

	/**
	 *Re-draws the map image.
	 */
	protected void update(){
	    repaint();
	}

	/**
	 *Closes this and the dependant menu window.
	 */
	protected void close(boolean start){
	    master.setMapVisible(true);
	    master.setRunningMode(start);
	    menu.dispose();
	    dispose();
	}

	//@override
	public void windowClosing(WindowEvent event){
	    close(false);
	}

	/**
	 *Menu controls for the custom simulation setup window.
	 */
	private class Menu extends JFrameWithClose implements ActionListener, MouseListener{
	    private JButton reset, use, cancel;
	    private CustomSimulation superMap;

	    /**
	     *Creates and displays a new instance of the menu window. Only 1 at a time.
	     */
	    public Menu(CustomSimulation superMap){
		super("");
		this.superMap = superMap;
		setResizable(false);
		GridLayout grid = new GridLayout(3, 0, 5, 5);
		setLayout(grid);

		addWindowListener(this);
		
		reset = new JButton("Reset");
		reset.addActionListener(this);
		use = new JButton("Use");
		use.addActionListener(this);
		cancel = new JButton("Cancel");
		cancel.addActionListener(this);

		add(reset);
		add(use);
		add(cancel);

		pack();
		setLocation(0, 150);
		setVisible(true);
	    }

	    /**
	     *Button event manager. Either reset all units, cancel the new simulation or start a new simualtion with the current map.
	     *@param event The button event.
	     */
	    public void actionPerformed(ActionEvent event){
		if(event.getSource() == reset){
		    reset();
		}else if(event.getSource() == cancel){
		    superMap.close(false);
		}else if(event.getSource() == use){
		    master.setRunningMode(true);
		    simulator = new Core(master, new Generation(map, -1));
		    simulator.start();
		    superMap.close(true);
		}
	    }

	    /**
	     *Updates the selected Unit.
	     *@param event The mouse event.
	     */
	    public void mouseReleased(MouseEvent event){
		int x = event.getY()-edge, y = event.getX()-edge;
		shuffleUnit(x/(radius+space), y/(radius+space));
		repaint();
	    }

	    /**
	     *Shuffles the selected unit in the order of A-B-C;
	     *@param x The x coordinate of the Unit.
	     *@param y The y coordinate of the Unit.
	     */
	    private void shuffleUnit(int x, int y){
		if(x < 0 || x >= sizex || y < 0 || y >= sizey)
		    return;

		if(map[x][y] == Unit.A)
		    map[x][y] = Unit.B;
		else if(map[x][y] == Unit.B)
		    map[x][y] = Unit.C;
		else if(map[x][y] == Unit.C)
		    map[x][y] = Unit.A;
		superMap.update();
	    }

	    //@override
	    public void windowClosing(WindowEvent event){
		superMap.close(false);
	    }

	    //Method required from interface, but not used.
	    public void mouseClicked(MouseEvent event){}
	    public void mouseEntered(MouseEvent event){}
	    public void mouseExited(MouseEvent event){}
	    public void mousePressed(MouseEvent event){}
	}
    }
    
    /**
     *General information window. For simulation and history length.
     */
    private class GeneralInformation extends JFrameWithClose{
	private JLabel lbl_generation, txt_generation, lbl_totalunits, txt_totalunits, lbl_stabilized, txt_stabilized;

	/**
	 *Creates a new instance of the general information window. Only 1 at a time.
	 */
	public GeneralInformation(){
	    super("General");
	    setResizable(false);
	    GridLayout grid = new GridLayout(0, 6, 5, 5);
	    setLayout(grid);
	    addWindowListener(this);

	    lbl_generation = new JLabel("    Generation:");
	    txt_generation = new JLabel(" n/a ");
	    lbl_totalunits = new JLabel("Total Units:");
	    txt_totalunits = new JLabel(" n/a ");
	    lbl_stabilized = new JLabel("Stabilized:");
	    txt_stabilized = new JLabel(" n/a ");

	    add(lbl_generation);
	    add(txt_generation);
	    add(lbl_totalunits);
	    add(txt_totalunits);
	    add(lbl_stabilized);
	    add(txt_stabilized);

	    pack();
	    StatisticsInformation statFrame;
	    if(statisticsFrame != null){
		statFrame = statisticsFrame;
	    }else{
		statFrame = new StatisticsInformation();
	    }

	    setLocation(0, screenHeight-getHeight()-statFrame.getHeight());
	}

	/**
	 *Updates the general information.
	 *@param generation The current generation number.
	 *@param totalunits Total number of Units on the map.
	 *@param 
	 */
	public void update(int generation, int totalunits, boolean stabilized){
	    txt_generation.setText(String.format("%d", generation));
	    txt_totalunits.setText(String.format("%d", totalunits));
	    if(stabilized){
		txt_stabilized.setText("YES");
		txt_stabilized.setForeground(Color.GREEN);
	    }else{
		txt_stabilized.setText("NO");
		txt_stabilized.setForeground(Color.RED);
	    }
	}
    }

    /**
     *General generation and statistics window.
     */
    private class GenerationInformation extends JFrameWithClose{
	private JLabel lbl_rounds, txt_rounds, lbl_generations, txt_generations, lbl_mingeneration, txt_mingeneration, lbl_maxgeneration, txt_maxgeneration, lbl_average, txt_average;

	/**
	 *Creates a new instance of the generaion window. Only 1 at a time.
	 */
	public GenerationInformation(){
	    super("Generation");
	    setResizable(false);
	    GridLayout grid = new GridLayout(5, 2, 5, 5);
	    setLayout(grid);
	    addWindowListener(this);

	    lbl_rounds = new JLabel("    Total rounds:");
	    txt_rounds = new JLabel("  n/a");
	    lbl_generations = new JLabel("    Total Generations:");
	    txt_generations = new JLabel("  n/a");
	    lbl_mingeneration = new JLabel("    Minimum Generations:");
	    txt_mingeneration = new JLabel("  n/a");
	    lbl_maxgeneration = new JLabel("    Maximum Generations");
	    txt_maxgeneration = new JLabel("  n/a");
	    lbl_average = new JLabel("    Average:");
	    txt_average = new JLabel("  n/a");

	    add(lbl_rounds);
	    add(txt_rounds);
	    add(lbl_generations);
	    add(txt_generations);
	    add(lbl_mingeneration);
	    add(txt_mingeneration);
	    add(lbl_maxgeneration);
	    add(txt_maxgeneration);
	    add(lbl_average);
	    add(txt_average);

	    pack();
	    setLocation(0, 100);
	}

	/**
	 *Updates the generation statistics.
	 *@param rounds Total number of simulations with the set.
	 *@param totalgenerations The total number of all generation within the set.
	 *@param mingeneration The shortest genertaion count with the set.
	 *@param maxgeneration The longest generation count with the set
	 */
	public void update(int rounds, int totalgenerations, int mingeneration, int maxgeneration){
	    txt_rounds.setText(String.format("%d", rounds));
	    txt_generations.setText(String.format("%d", totalgenerations));
	    txt_mingeneration.setText(String.format("%d", mingeneration));
	    txt_maxgeneration.setText(String.format("%d", maxgeneration));
	    txt_average.setText(String.format("%4.1f", ((double)totalgenerations/rounds)));
	}
    }

    /**
     *Write progression window.
     */
    private class WriteInformation extends JFrameWithClose implements ActionListener{
	private JLabel lbl_message, txt_message, lbl_progress;
	private JProgressBar progressbar;
	private JButton close;

	/**
	 *Creates and displays a new instance of the write file progress window.
	 */
	public WriteInformation(String filename){
	    super("Writing " + filename);
	    setResizable(false);
	    GridLayout grid = new GridLayout(3, 2, 5, 5);
	    setLayout(grid);

	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	    lbl_message = new JLabel("    Status:");
	    txt_message = new JLabel("    n/a    ");
	    lbl_progress = new JLabel("    Progress:");
	    close = new JButton("Close");
	    close.addActionListener(this);
	    progressbar = new JProgressBar(0, 100);
	    progressbar.setStringPainted(true);

	    add(lbl_message);
	    add(txt_message);
	    add(lbl_progress);
	    add(progressbar);
	    add(close);


	    setPreferredSize(new Dimension(500, 80));
	    pack();
	    setLocation((screenWidth-getWidth())/2, (screenHeight-getHeight())/2);
	    setVisible(true);
	}

	/**
	 *Updates the progressbar progression.
	 *@param progress The progress to set it to.
	 */
	public void update(double progress){
	    progressbar.setValue((int)progress);
	}

	/**
	 *Updates the message for the write status.
	 *@param message The message to be displayed.
	 */
	public void update(String message){
	    txt_message.setText(message);
	}

	public void actionPerformed(ActionEvent event){
	    dispose();
	}
    }

    /**
     *Used for handling close events with the main windows.
     */
    private class JFrameWithClose extends JFrame implements WindowListener{
	/**
	 *Access to super, JFrame.
	 *@param name The name of the window to be displayed in the title.
	 */
	public JFrameWithClose(String name){
	    super(name);
	}

	public void windowActivated(WindowEvent event){}
	public void windowClosed(WindowEvent event){}

	/**
	 *Calls the ControlSystems custom dispose set.
	 *@param event The window event.
	 */
	public void windowClosing(WindowEvent event){
	    master.dispose(this);
	}
	public void windowDeactivated(WindowEvent event){}
	public void windowDeiconified(WindowEvent event){}
	public void windowIconified(WindowEvent event){}
	public void windowOpened(WindowEvent event){}
    }

    //Method from UserInterface
    public void printStatistics(final Unit unit, final int total, final int newcount, final int oldcount){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    if(statisticsFrame != null)
			statisticsFrame.update(unit, total, newcount, oldcount);
		}
	    });
    }
    
    //Method from UserInterface
    public void printMap(final Unit [][] map){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    if(mapFrame != null)
			mapFrame.update(map);
		}
	    });
    }
    
    //Method from UserInterface
    public void printGeneral(final int generation, final int totalunits, final boolean stabilized){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    if(generalFrame != null)
			generalFrame.update(generation, totalunits, stabilized);
		}
	    });
    }
    
    //Method from UserInterface 
    public void printGenerationStatistics(final int rounds, final int totalgenerations, final int mingeneration, final int maxgeneration){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    if(generationFrame != null)
			generationFrame.update(rounds, totalgenerations, mingeneration, maxgeneration);
		}
	    });
	
    }
    
    //Method from UserInterface
    public void printSimulationStatus(final String message){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    simulationStatus.setText(message);
		}
	    });
    }
    
    //Method from UserInterface
    public void printWriteFileProgress(final double percent){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    if(writeFrame != null)
			writeFrame.update(percent);
		}
	    });
	
    }
    
    //Method from UserInterface
    public  void printWriteFileStatus(final String message){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    if(writeFrame != null)
			writeFrame.update(message);
		}
	    });
	
    }
}
