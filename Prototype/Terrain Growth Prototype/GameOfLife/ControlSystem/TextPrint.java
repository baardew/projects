package GameOfLife.ControlSystem;

import GameOfLife.Simulator.Unit;

/**
 *Used for handling printout to ControlSystems and for Core to write files.
 *@author baardew
 *@version 1.0
 */
public class TextPrint{
    /**
     *Generates a plain visualization text version of the map using Unit`s defined char symbol.
     *@param map The map to be generated from.
     *@return The String to be printed to file or Control System.
     */
    public static String generateMap(Unit [][] map){
	String generatedString = new String();
	int sizex = map.length, sizey = map[sizex-1].length;
	for(int x = 0; x < sizex; x++){;
	    generatedString += "\n";
	    for(int y = 0; y < sizey; y++){;
		generatedString += " " + map[x][y].getUnit();
		
	    }
	}
	return generatedString;
    }

    /**
     *Generates a text version of general simulation statistics. Displays Generation number, map`s max capacity and whether the simulation is stabilized or not.
     *@param Generation The current Generation number.
     *@param totalunits The total number of Units on the map.
     *@param stabilized Indicating if this map has been stabilized.
     *@return The String to be printed to file or Control System.
     */    
    public static String generateGeneral(int Generation, int totalunits, boolean stabilized){
	if(stabilized){
	    return "\nGENERATION: " + Generation + "\tTOTAL UNITS: " + totalunits + "\tSTABILIZED!";
	}else{
	    return "\nGENERATION: " + Generation + "\tTOTAL UNITS: " + totalunits;
	}
    }
    
    /**
     *Generates a text version of general Generation statistics. Displays total number of simulations, total Generations, the fastest simulation, the slowest simulation and the average for all simulations.
     *@param rounds The total count of simulation runs.
     *@param totalGenerations The total Generation count for all simulation runs.
     *@param minGeneration The count of Generation for the fastest Generation run.
     *@param maxGeneration The count of Generation for the slowest Generation run.
     *@return The String to be printed to file or Control System.
     */  
    public static String generateGenerationStatistics(int rounds, int totalGenerations, int minGeneration, int maxGeneration){
	return String.format("\nTOTAL ROUNDS: %4d\tTOTAL GENERATIONS: %5d"
			  + "\nMINIMUM GENERATIONS: %3d\tMAXIMUM GENERATIONS: %4d\tAVERAGE: %4.1f",
		      rounds, totalGenerations, minGeneration, maxGeneration, (double)totalGenerations/rounds);
    }
    
   /**
     *Generates a text version of the statistics for a specified unit within a Generation. Displays the Unit, current count, count increase, current percentage and percent increase.
     *@param unit The Unit to use symbol for display.
     *@param total The total count of all Units.
     *@param newcount The count for the current Generation of the specified Unit.
     *@param oldcount The previous count of the current Generation of the specified Unit.
     *@return The String to be printed to file or Control System.
     */  
    public static String generateStatistics(Unit unit, int total, int newcount, int oldcount){
	String sign;
	int countIncrease = newcount-oldcount;
	if(countIncrease >= 0)
	    sign = "+";
	else
	    sign = "-";
	
	countIncrease = (countIncrease >= 0) ? newcount-oldcount : (newcount-oldcount)*-1;
	double percent = ((double)(newcount*100))/(total), percentIncrease = (percent-((double)(oldcount*100))/(total));
	percent = (percent >= 0) ? percent : percent*-1;
	percentIncrease = (percentIncrease >= 0) ? percentIncrease : percentIncrease*-1;
	
	return String.format("\n%s : %s\tnumerical: %6d (%s %4d)\tpercentage: %3.2f%s (%s %3.2f%s)",
				   unit, unit.getUnit(), newcount, sign, countIncrease, percent, "%", sign, percentIncrease, "%");
    }
}