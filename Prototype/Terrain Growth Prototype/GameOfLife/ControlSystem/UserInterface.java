package GameOfLife.ControlSystem;

import GameOfLife.Simulator.Unit;

/**
 *Interface for combining the Core with the Control System. Calls are from Core to Control System in order to send display information of the simulation and its data. Required for Control System to recive data from simulation.
 *@author baardew
 *@version 1.0
 */
public interface UserInterface{
    /**
     *Sends information about a given Unit for displaying statistics.
     *@param unit The unit to be used for display.
     *@param total The total Unit count on the map.
     *@param newcount The current count in this Generation of this Unit.
     *@param oldcount The count in the previous Generation of this Unit.
     */
    public abstract void printStatistics(Unit unit, int total, int newcount, int oldcount);

    /**
     *Sends the map containing the Units to be displayed in a Control System.
     *@param map The map containing all the units.
     */
    public abstract void printMap(Unit [][] map);

    /**
     *Sends information about the current Generation for displaying statistics.
     *@param generation The Generation`s generation number.
     *@param totalunits Total number of Units on the map.
     *@param stabilized Indicate whether the simulation run has stabilized.
     */
    public abstract void printGeneral(int generation, int totalunits, boolean stabilized);

    /**
     *Send information about general simulation runs statistics.
     *@param rounds The total number of simulation runds.
     *@param totalgenerations The total number of Generations in all of the simulation runs.
     *@param mingeneration The smallest number of Generations within a simulation run.
     *@param maxgeneration The largest number of Generations within a simulation run.
     */
    public abstract void printGenerationStatistics(int rounds, int totalgenerations, int mingeneration, int maxgeneration);
    
    /**
     *Sends a message regarding the simulation` status.
     *@param message The message to be displayed.
     */
    public abstract void printSimulationStatus(String message);
    
    /**
     *Sends a percentage of the current write progression.
     *@param percent The current progress to be displayed.
     */
    public abstract void printWriteFileProgress(double percent);
    
    /**
     *Sends a message about the write status.
     *@param message The message to be displayed.
     */
    public abstract void printWriteFileStatus(String message);
}
