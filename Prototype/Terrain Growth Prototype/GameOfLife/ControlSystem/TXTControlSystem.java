package GameOfLife.ControlSystem;

import GameOfLife.ControlSystem.TextPrint;
import GameOfLife.ControlSystem.UserInterface;
import GameOfLife.Simulator.Core;
import GameOfLife.Simulator.Generation;
import GameOfLife.Simulator.Unit;
import GameOfLife.Simulator.CoreIllegalStateException;

import java.util.Scanner;
import java.text.DecimalFormat;
import java.io.FileNotFoundException;

/**
 *@author baardew
 *@version 1.0
 *A Text based User Interface and Control System for use with Core to be run in a terminal/shell window.
 */
public class TXTControlSystem implements UserInterface{
    private final String menuMessage = "\nPress ENTER to access menu. ",
	wrongCmd = "\t> Invalid command.",
	wrongValue = "\t> Wrong input. Using previous value.";

    private final char TRUE = 't', FALSE = 'f';
    private Scanner ukey;
    private Core simulation;
    private char cmd;

    /**
     *Initializes a new TXT Control System.
     */
    public TXTControlSystem(){
	cmd = '$';
	ukey = new Scanner(System.in);
    }

    /**
     *The Main Menu to the system and its starting point. All other menus are accessed from here.
     */
    public void mainMenu(){
	String gameMenu =
	    "\n[c] - Continue current"
	    + "\n[h] - Pause current"
	    + "\n[e] - End current "
	    + "\n[f] - Find generation"
	    + "\n[p] - Preferences"
	    + "\n[m] - Display menu"
	    + "\n[q] - Quit";
	String mainMenu =
	    "\n[f] - Find generation"
	    + "\n[n] - New simulation setup"
	    + "\n[w] - Write simulation to file"
	    + "\n[p] - Preferences"
	    + "\n[c] - Continue current"
	    + "\n[m] - Display menu"
	    + "\n[q] - Quit";
	String pauseMenu =
	    "\n[f] - Find generation"
	    + "\n[w] - Write simulation to file"
	    + "\n[p] - Preferences"
	    + "\n[c] - Continue current"
	    + "\n[m] - Display menu"
	    + "\n[q] - Quit";
	printSimulationStatus(menuMessage);
	while(cmd != 'q'){
	    cmd = '$';
	    ukey.nextLine(); //Main thread wait line
	    Core.setSkipPrint(true);
	    
	    if(Core.isGame())
		System.out.print(gameMenu);
	    else
		System.out.print(mainMenu);
	    
	    while(cmd != 'q' && cmd != 'c' && cmd != 'n' && cmd != 'd'){ //q = quit, c = custom or continue, n = new, d = default board
		System.out.print("\nCommand: ");
		String Tcmd = ukey.nextLine();
		if(Tcmd.length() != 0)
		    cmd = Tcmd.charAt(0);
		else{;
		    continue;
		}
		switch(cmd){
		case 'c':
		    if(Core.isGame()){
			if(simulation != null){
			    System.out.print("\t> Waiting for next generation..." + "\n" + menuMessage);
			    Core.setSkipPrint(false);
			}
			else{
			    printSimulationStatus(wrongCmd);
			    cmd = '$';
			}
		    }else if(simulation == null){
			printSimulationStatus(wrongCmd);
			cmd = '$';
		    }else{
			System.out.print("\t> Waiting for next generation..." + "\n" + menuMessage);
			Core.setSkipPrint(false);
			try{
			    simulation.continueSimulation();
			}catch(CoreIllegalStateException e){
			    printSimulationStatus(e.getMessage());
			}
		    }
		    break;
		case 'h':
		    if(Core.isGame() && !simulation.isPaused()){
			try{
			    simulation.hold();
			}catch(CoreIllegalStateException e){
			    printSimulationStatus(e.getMessage());
			}
		    }else{
			printSimulationStatus(wrongCmd);
		    }
		    break;
		case 'e':
		    if(simulation.isGame()){
			try{
			    simulation.end();
			}catch(CoreIllegalStateException e){
			    printSimulationStatus(e.getMessage());
			}
		    }
		    else
			printSimulationStatus(wrongCmd);
		    break;
		case 'f':
		    if(simulation != null){
			findGeneration();
		    }else{
			printSimulationStatus("\t> No Simulation stored.");
		    }
		    break;
		case 'p':
			preferences();
		    break;
		case 'm':
		    if(Core.isGame())
			System.out.print(gameMenu);
		    else if(simulation.isPaused())
			System.out.print(pauseMenu);
		    break;
		case 'q':
		    if(simulation != null && Core.isGame()){
			try{
			    simulation.end();
			}catch(CoreIllegalStateException e){
			    printSimulationStatus("No simulation to end.");
			}
			simulation = null;
		    }
		    printSimulationStatus("\tProgram terminated.");
		    break;
		case 'w':
		    if(!Core.isGame() && simulation.isPaused()){
			if(simulation != null){
			    saveHistory();
			}else{
			    printSimulationStatus("\t> No Simulation stored.");
			}
		    }
		    else{
			printSimulationStatus(wrongCmd);
		    }
		    break;
		case 'n':
		    if(!Core.isGame() && !simulation.isPaused()){
			newSimulation();
		    }
		    else{
			printSimulationStatus(wrongCmd);
			cmd = '$';
		    }
		    break;
		default:
		    printSimulationStatus(wrongCmd);
		    cmd = '$';
		    break;
		}
	    }
	}
    }

    /**
     *Menu for setting up a new simulation. Either custom, from file or default(random).
     */
    private void newSimulation(){
	Generation newR = null;
	String menuList =
	    "\n[c] - Custom map"
	    + "\n[f] - From file"
	    + "\n[d] - Default map"
	    + "\n[s] - Setup"
	    + "\n[m] - Display menu"
	    + "\n[r] - Return to Main menu";
	System.out.print(menuList);
	while(cmd != 'r' && cmd != 'd' && cmd != 'c'){
	    System.out.print("\nCommand: ");
	    cmd = ukey.next().charAt(0);
	    switch(cmd){
	    case 'm' :
		System.out.print(menuList);
		break;
	    case 'c':
		Unit[][] newM = customMap();
		if(newM[0][0] != null){
		    newR = new Generation(newM, -1);
		    Core.setLONGSIMULATION(false);
		    Core.setCUSTOMMODE(true);
		}else{
		    printSimulationStatus("\t> New simulation aborted.");
		    break;
		}
	    case 'f':
		if(cmd == 'f'){
		    newR = fileMap();
		    if(newR == null){
			printSimulationStatus("\t> New simulation aborted.");
			break;
		    }
		    cmd = 'c';
		}
	    case 'd':
		displayScreenRequirements();
		if(newR == null)
		    simulation = new Core(this);
		else
		    simulation = new Core(this, newR);
		Core.setSkipPrint(false);
		simulation.start();
		break;
	    case 's':
		if(!Core.isGame())
		    setup();
		else{
		    printSimulationStatus(wrongCmd);
		}
		break;
	    case 'r':
		ukey.nextLine();
		printSimulationStatus("\t> New simulation aborted.");
		break;
	    case 'w':
		saveHistory();
		break;
	    default:
		printSimulationStatus(wrongCmd);
		break;
	    }
	}
    }
    
    /**
     *Setup regarding the map properties. Change map X or Y size and/or the spawn ratio for the default map.
     */
    private void setup(){
	String menuList =
	    "\n[x] - Set map size X"
	    + "\n[y] - Set map size Y"
	    + "\n[s] - Set spawn ratio"
	    + "\n[m] - Display menu"
	    + "\n[r] - Return to new simulation menu and save";
	int sizeX = -1, sizeY = -1, spawnRatio = -1;
	System.out.print(menuList);
	while(cmd != '$'){
	    System.out.print("\nCommand: ");
	    cmd = ukey.next().charAt(0);
	    switch(cmd){
	    case 'm' :
		System.out.print(menuList);
		break;
	    case 'x':
		try{
		    System.out.print("Map size X [currently " + Generation.getSIZEY() + "]: ");
		    sizeY = ukey.nextInt();
		}catch(Exception e){
		    printSimulationStatus(wrongValue);
		    sizeY = -1;
		}
		break;
	    case 'y':
		try{
		    System.out.print("Map size Y [currently " + Generation.getSIZEX() + "]: ");
		    sizeX = ukey.nextInt();
		}catch(Exception e){
		    printSimulationStatus(wrongValue);
		    sizeX = -1;
		}
		break;

	    case 's':
		try{
		    System.out.print("Spawn ratio [currently " + Generation.getSPAWNRATIO() + "]: ");
		    spawnRatio = ukey.nextInt();
		}catch(Exception e){
		    printSimulationStatus(wrongValue);
		    spawnRatio = -1;
		}
		break;
	    case 'r':
		Generation.setSizeXY(sizeX, sizeY);
		Generation.setSPAWNRATIO(spawnRatio);
		ukey.nextLine();
		cmd = '$';
		printSimulationStatus("\t> Settings saved.");
		break;
	    default:
		printSimulationStatus(wrongCmd);
		break;
	    }
	}	
    }

    /**
     *The preferences for display options. Selectable enable/disable for displaying map, statistics, long simulation, generation statistics and next generation delay time.
     */
    private void preferences(){
	String menuList = "\n[b] - Set show map"
	    + "\n[s] - Set show statistics"
	    + "\n[l] - Set long simulation"
	    + "\n[g] - Set show generation statistics"
	    + "\n[d] - Set next generation delay time"
	    + "\n[m] - Display menu"
	    + "\n[r] - Return to previous menu and save";
	boolean longSimulation = Core.getLONGSIMULATION(),
	    pMap = Core.getPMAP(),
	    pStatistics = Core.getPSTATISTICS(),
	    pAverage = Core.getPGENERATIONSTATISTICS();
	int holdTime = -1;
	boolean currentValue;
	char input = '$';
	System.out.print(menuList);
	while(cmd != 'r'){
	    System.out.print("\nCommand: ");
	    cmd = ukey.next().charAt(0);
	    switch(cmd){
	    case 'g':
		currentValue = Core.getPGENERATIONSTATISTICS();
		System.out.print("Display Genration statistics [t/f, currently: " + currentValue  + "]: ");
		input = ukey.next().charAt(0);
		if(input == TRUE){
		    pAverage = true;
		}else if(input == FALSE){
		    pAverage = false;
		}else{
		    printSimulationStatus(wrongValue);
		}
		break;
	    case 'b':
		currentValue = Core.getPMAP();
		System.out.print("Display map [t/f, currently: " + currentValue + "]: ");
		input = ukey.next().charAt(0);
		if(input == TRUE){
		    pMap = true;
		}else if(input == FALSE){
		    pMap = false;
		}else{
		    printSimulationStatus(wrongValue);
		}
		break;
	    case 'm' :
		System.out.print(menuList);
		break;
	    case 'l':
		currentValue = Core.getLONGSIMULATION();
		System.out.print("Long simulation [t/f, currently: " + currentValue + "]: ");
		input = ukey.next().charAt(0);
		if(input == TRUE){
		    longSimulation = true;
		}else if(input == FALSE){
		    longSimulation = false;
		}else{
		    printSimulationStatus(wrongValue);
		}
		break;
	    case 's':
		currentValue = Core.getPSTATISTICS();
		System.out.print("Display statistics [t/f, currently: " + currentValue + "]: ");
		input = ukey.next().charAt(0);
		if(input == TRUE){
		    pStatistics = true;
		}else if(input == FALSE){
		    pStatistics = false;
		}else{
		    printSimulationStatus(wrongValue);
		}
		break;
	    case 'd':
		try{
		    System.out.print("Next generation delay time [1/10th seconds, currently " + (Core.getHOLDTIME()) + "]: ");
		    holdTime = ukey.nextInt();
		}catch(Exception e){
		    printSimulationStatus(wrongValue);
		    holdTime = -1;
		}
		break;
	    case 'r':
		Core.setPGENERATIONSTATISTICS(pAverage);
		Core.setPSTATISTICS(pStatistics);
		Core.setPMAP(pMap);
		Core.setLONGSIMULATION(longSimulation);
		Core.setHOLDTIME(holdTime);
		System.out.print("\t> Settings saved.");
		displayScreenRequirements();
		break;
	    default:
		printSimulationStatus(wrongCmd);
		break;
	    }
	}
	cmd = '$';
    }

    /**
     *Used for displaying required terminal/shell window. Draws a box the user must match.
     */
    public void displayScreenRequirements(){
	int X = Generation.getSIZEX()+7, Y = Generation.getSIZEY();
	if(Generation.getSIZEY() < 40 && Core.getPMAP() == true)
	    Y = 40;
	if(Core.getPMAP() == false)
	    Y = 50;
	if(Core.getPSTATISTICS() == true && Core.getPMAP() == false)
	    X = 7;
	if(Core.getPSTATISTICS() == false && Core.getPMAP() == true)
	    X -= 6;
	if(Core.getPSTATISTICS() == true && Core.getPGENERATIONSTATISTICS() == false)
	    X -= 2;
	    

	System.out.print("\n");
	for(int x = 0; x < X; x++){
	    for(int y = 0; y < Y; y++){
		System.out.print(" @");
	    }
	    System.out.print("\n");
	}
	printSimulationStatus("Make the terminal/shell window to fit the '@' figured box only!");
	System.out.print("Press ENTER to continue.");
	ukey.skip("\n");
	ukey.nextLine();
    }

    private boolean findround = false; //For use with printStatistics and menuMessage
    /**
     *Menu for finding and displaying a specified Generation.
     */
    private void findGeneration(){
	findround = true;
	String genInput = "", menuList = "\n[m] - Display menu\n[e] - End search";
	boolean end = false; 
	int fGeneration = -1;
	printSimulationStatus(menuList);
	while(!end){
	    System.out.print("\nDisplay generation [length: " + (simulation.historyLength()-1) + "]: ");
	    genInput = ukey.next();
	    if(genInput.length() != 0 && genInput.charAt(0) == 'e'){
		end = true;
	    }else if(genInput.length() != 0 && genInput.charAt(0) == 'm'){
		System.out.print(menuList);
	    }else{
		try{
		    fGeneration = Integer.parseInt(genInput);
		}catch(Exception e){
		    printSimulationStatus("\t> Wrong input.");
		    fGeneration = -1;
		}
		if(fGeneration >= 0 && fGeneration < simulation.historyLength()){
		    simulation.printToUI(fGeneration);
		}else{
		    printSimulationStatus("\t> Generation outside history span.");
		}
	    }
	}
	cmd = '$';
	findround = false;
    }

    /**
     *Menu for writing simulation data to file. Can write all Generations, specified Generation, create Data file, graph files or display a Generation.
     */
    private void saveHistory(){
	String fileName;
	DecimalFormat dF = new DecimalFormat("#.##");
	int pGeneration = -1;
	String menuList =
	    "\n[a] - All generations"
	    + "\n[s] - Specify generation"
	    + "\n[f] - Find generation"
	    + "\n[d] - Data file"
	    + "\n[p] - Preferences"
	    + "\n[m] - Display menu"
	    + "\n[g] - Files for graphs"
	    + "\n[r] - Return to main menu";
	System.out.print(menuList);
	while(cmd != 'r' && cmd != 'd' && cmd != 'c'){
	    System.out.print("\nCommand: ");
	    cmd = ukey.next().charAt(0);
	    switch(cmd){
	    case 'm' :
		System.out.print(menuList);
		break;
	    case 'g':
		System.out.print("Specify filename: ");
		ukey.skip("[\n]");
		fileName = ukey.nextLine();
		try{
		    simulation.generateGraphFiles(fileName);
		}catch(CoreIllegalStateException e){
		    printSimulationStatus(e.getMessage());
		}
		break;
	    case 'a':
		int total = simulation.historyLength();
		System.out.print("Specify filename: ");	
		ukey.skip("[\n]");
		fileName = ukey.nextLine();
		try{
		    simulation.writeToFile(fileName);
		}catch(CoreIllegalStateException e){
		    printSimulationStatus(e.getMessage());
		}
		break;
	    case 'p':
		preferences();
		break;
	    case 'f':
		findGeneration();
		break;
	    case 's':
		System.out.print("Specify generation: ");
		pGeneration = -1;
		try{
		    pGeneration = ukey.nextInt();
		}catch(Exception e){
		    printSimulationStatus(wrongCmd);
		    ukey.next();
		    pGeneration = -1;
		}
		if(pGeneration >= 0 && pGeneration < simulation.historyLength()){
			System.out.print("Specify filename: ");
			ukey.skip("[\n]");
			fileName = ukey.nextLine();
			try{
			    simulation.writeToFile(fileName, pGeneration);
			}catch(CoreIllegalStateException e){
			    printSimulationStatus(e.getMessage());
			}
		}else if(pGeneration != -1){
		    printSimulationStatus("Generation outside history span.");
		}
		break;
	    case 'd':
		System.out.print("Specify generation: ");
		pGeneration = -1;
		try{
		    pGeneration = ukey.nextInt();
		}catch(Exception e){
		    printSimulationStatus(wrongCmd);
		    ukey.next();
		    pGeneration = -1;
		}
		if(pGeneration >= 0 && pGeneration < simulation.historyLength()){
			System.out.print("Specify filename: ");
			ukey.skip("[\n]");
			fileName = ukey.nextLine();
			try{
			    simulation.writeToData(fileName, pGeneration);
			}catch(CoreIllegalStateException e){
			    printSimulationStatus(e.getMessage());
			}
		}else if(pGeneration != -1){
		    printSimulationStatus("Generation outside history span.");
		}
		break;
	    case 'r':
		ukey.nextLine();
		break;
	    default:
		printSimulationStatus(wrongCmd);
		break;
	    }
	}
    }

    /**
     *Control for creating a new custom map.
     *@return The new custom map.
     */
    private Unit[][] customMap(){
	//ukey = scanner
	String menuList =
	    "\n[0] - Unit A"
	    + "\n[1] - Unit B"
	    + "\n['any'] - Unit C"
	    + "\n[x-y] - Coordinates"
	    + "\n[m] - Display menu"
	    + "\n[d] - Finished"
	    + "\n[r] - Return to Simulation menu";
	System.out.print(menuList);
	cmd = '$';
	ukey.nextLine();
	Unit[][] map = new Unit[Generation.getSIZEX()][Generation.getSIZEY()];
	boolean continueBoolean = true;
	while(cmd != 'r' && cmd != 'd'){
	    System.out.print("\nCommand: ");
	    String cmdLine = ukey.nextLine();

	    if(cmdLine.length() == 1){
		cmd = cmdLine.charAt(0);
		if(cmd == 'm')
		    printSimulationStatus(menuList);
		else if(!(cmd == 'r' || cmd == 'd'))
		    printSimulationStatus(wrongCmd);
	    }else{
		int x = -1, y = -1;
		try{
		    String[] split = cmdLine.split("-");
		    x = Integer.parseInt(split[0]);
		    y = Integer.parseInt(split[1]);
		    if(x < 0 || x >= Generation.getSIZEX() || y < 0 || y >= Generation.getSIZEY()){
			throw new Exception();
		    }
		}catch(Exception e){
		    printSimulationStatus(wrongCmd);
		    continueBoolean = false;
		}

		if(continueBoolean){
		    System.out.print("Unit: ");
		    cmd = ukey.nextLine().charAt(0);
		    
		    if(cmd == '0')
			map[x][y] = Unit.A;
		    else if(cmd == '1')
			map[x][y] = Unit.B;
		    else
			map[x][y] = Unit.C;
		}
		continueBoolean = true;
	    }
	}

	if(cmd == 'r'){
	    cmd = '$';
	    map[0][0] = null;
	    return map;
	}else{
	    cmd = 'c';
	    System.out.print("Prepearing ...");
	    for(int i = 0; i < Generation.getSIZEX(); i++){
		for(int q = 0; q < Generation.getSIZEY(); q++){
		    if(map[i][q] == null){
			map[i][q] = Unit.C;
		    }
		}
	    }
	    printSimulationStatus(" Done.");
	    return map;
	}
    }

    /**
     *Protocol for stating a new simulation from a file.
     *@return The Generation from that file.
     */
    private Generation fileMap(){
	System.out.print("Filename (cancel type [#]): ");
	ukey.skip("\n");
	String filename = ukey.nextLine();
	if(filename.compareTo("#") == 0){
	    return null;
	}
	Generation newG = null;
	try{
	    newG = new Generation(filename);
	}catch(FileNotFoundException e){
	    printSimulationStatus("File not found.");
	    return null;
	}catch(Exception e){
	    printSimulationStatus("The file has incorrect format.");
	    return null;
	}

	return newG;
    }
    
    private int line = 0; //For controlling printout of menuMessage. Private to method only.
    //Method from UserInterface
    public void printStatistics(Unit unit, int total, int newcount, int oldcount){
	System.out.print(TextPrint.generateStatistics(unit, total, newcount, oldcount));
	if(Core.getPGENERATIONSTATISTICS() != true && ++line % 3 == 0){
	    System.out.print(menuMessage);
	}
    }
    
    //Method from UserInterface
    public void printMap(Unit [][] map){
	System.out.print(TextPrint.generateMap(map));
	if(Core.getPSTATISTICS() != true){
	    System.out.print(menuMessage);
	}
    }

    //Method from UserInterface
    public void printGeneral(int generation, int totalunits, boolean stabilized){
	System.out.print(TextPrint.generateGeneral(generation, totalunits, stabilized));
    }

    //Method from UserInterface
    public void printSimulationStatus(String message){
	System.out.println("" + message);
    }

    //Method from UserInterface
    public void printWriteFileProgress(double percent){
	System.out.print("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
	System.out.printf("(%3.2f%s done)", percent, "%");
    }

    //Method from UserInterface
    public void printWriteFileStatus(String message){
	printSimulationStatus("\t> " + message);
    }
    
    //Method from UserInterface
    public void printGenerationStatistics(int rounds, int totalgenerations, int mingeneration, int maxgeneration){
	System.out.print(TextPrint.generateGenerationStatistics(rounds, totalgenerations, mingeneration, maxgeneration));
	System.out.print(menuMessage);
    }
}