package GameOfLife;

import GameOfLife.ControlSystem.TXTControlSystem;
import GameOfLife.ControlSystem.GUIControlSystem;

/**
 *Startup class containing the main method.
 *@author baardew
 *@version 1.0
 */
public class GameOfLife{
    /**
     *Start method for running GameOfLife.
     *@param args  Which Control System to use. Either TXT or GUI.
     */
    public static void main(String[] args){
	if(args[0].compareTo("-t") == 0){
	    System.out.print("\t* * * * * THE GAME OF LIFE * * * * *\n");
	    TXTControlSystem TXT = new TXTControlSystem();
	    TXT.mainMenu();
	}
	if(args[0].compareTo("-g") == 0){
	    new GUIControlSystem(args[1].compareTo("-m") == 0);
	}
    }
}
