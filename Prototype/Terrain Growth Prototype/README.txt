THE GAME OF LIFE README
Developer Edition

written pretty much by baardew@ifi@uio.
Developed as INF1000 and INF1010 practice at University of Oslo.

v. 1.0

IMPORTANT NOTICE:
This version includes a Graphilcal and a Text based user interface. They both have all the features.
If the Text based user interface is stopped, press ENTER one at a time until it continues. It is thereby recommended to use the Graphical User Interface, since is is less buggy.
The GUI included here is only verified to work with Linux OS and the Mac OS, however, Linux is the recommended OS.
The system is currently untested with Window OS.
Many different parts can be seen in the source files, but refactoring should minimize such occurences.
There are some instability when using a large Map on a low-end computer system.

KNOWN ISSUE - Double display of 'Command: ' during some operations with the Text based Control System (due to scanner complexity)

System reqirements:
minimum
	- Java SE 6 (java v.1.6) (Works with Java 5, but not recommended)
	- ANT build system support (free)
	- Linux OS with 1 monitor conntected
recommended
	- 1GB or more free space on the hard disk drive
	- Two or more cores
	- 1.25 GHz or faster processor
	- 1GB or more RAM
	- Terminal/shell resolution of 101x47 for standard map size when using Text based UI
	- 1280*1024 resolution or higher when using Graphical based UI

Installing:
	- Run "ant install". (ANT support required)

Using the applcation:
	- Start the GUIRun.sh from the Terminal/shell window.
	- Optional TXTRun.sh for terminal/shell based interface.

Manual/documentation:
	- UML graphs created with Violet UML editor. To read theese files, use Violet UML editor (free).
	- Javadoc avaible. Run the javadoc.sh for access.
	- Manual avaible in Library/Resources/ or root folder after install.

Uninstalling:
	Drag the folder GameOfLife to the trash bin.

NOTE:
src   = GameOfLife (source files)
build = Build (class files)
lib   = Library (additional resources)
