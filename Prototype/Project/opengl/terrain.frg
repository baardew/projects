uniform sampler2D tex_terrain;
uniform sampler2D tex_fog;
uniform sampler2D tex_tile;
uniform sampler2D tex_loc;

void main()
{
    vec4 terrain = texture2D(tex_terrain, gl_TexCoord[0].xy);
    vec4 fog     = texture2D(tex_fog,     gl_TexCoord[0].xy);
    vec4 tile    = texture2D(tex_tile,    gl_TexCoord[0].xy);
    vec4 loc     = texture2D(tex_loc,     gl_TexCoord[0].xy);

    if (fog.r == 0.0) {
        if (terrain.r == 0.0)
            gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
        else if (terrain.r == 1.0/255.0)
            // brown: R = 2*G, G = x and B = 0
            gl_FragColor = vec4(0.36, 0.60, 0.0, 1.0);
//        gl_FragColor = vec4(0.33, 0.66, 0.0, 1.0);
        else if (terrain.r == 2.0/255.0)
            gl_FragColor = vec4(0.25, 0.25, 0.25, 1.0);
        else if (terrain.r == 3.0/255.0)
            gl_FragColor = vec4(0.00, 0.90, 0.10, 1.0);
        else if (terrain.r == 4.0/255.0)
            gl_FragColor = vec4(1.00, 0.00, 0.00, 1.0);
        else if (terrain.r == 5.0/255.0)
            gl_FragColor = vec4(0.00, 0.00, 1.00, 1.0);
        else
            gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    } else {
        gl_FragColor = vec4(0.075, 0.075, 0.075, 0.0);
    }

    if (loc.r != 0.0)
    	gl_FragColor = vec4(0.0, 1.0, 1.0, 0.0);

    if (tile.r != 0.0) {
        if (tile.r == 1.0/255.0) // GROUND
            gl_FragColor = vec4(1.0, 1.0, 0.0, 0.0);
        else if (tile.r == 2.0/255.0) // ROCK
            gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
        else if (tile.r == 3.0/255.0) // BOTH
            gl_FragColor = vec4(0.0, 0.75, 0.75, 0.0);
        else
            gl_FragColor = vec4(1.0, 1.0, 1.0, 0.0);
    }
}
