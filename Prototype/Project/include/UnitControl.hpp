#ifndef UNIT_CONTROL_H
#define UNIT_CONTROL_H

#include "World.hpp"

class UnitControl : public QWidget
{
    Q_OBJECT

public:
    UnitControl(World &world);

private slots:
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
    void computeVision();

private:
    int x;
    int y;

    World &world;
};

#endif
