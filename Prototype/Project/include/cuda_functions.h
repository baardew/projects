#ifndef CUDA_FUNCTIONS_H
#define CUDA_FUNCTIONS_H

#include "terrain.h"

extern "C" {
    void cuda_generate(t_terrain *terrain, int widht, int height, unsigned int *seed);
    void cuda_fog(t_terrain *fog, t_terrain *terrain, int widht, int height);
    void cuda_startslot(t_terrain *location, t_terrain *terrain, int widht, int height);
    void cuda_tile(t_terrain *terrain, int widht, int height, struct tile *grid, t_terrain *mapping);
}

#endif
