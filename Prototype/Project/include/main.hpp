#include "terrain.h"
#include "World.hpp"

#include <QWidget>
#include <QCheckBox>

class Prototype : public QWidget
{
    Q_OBJECT

public:
    Prototype(int w, int h);

private slots:
    void push_gen();
    void place_unit();

private:
    World *world;
    QCheckBox *fog, *tile, *start;
};
