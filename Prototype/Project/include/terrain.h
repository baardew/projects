#ifndef TERRAIN_H
#define TERRAIN_H

typedef unsigned char t_terrain;

enum Terrain
{
    VOID      =  0,
    GROUND    =  1,
    ROCK      =  2,
    RESOURCE  =  3,
    BUILDING  =  4,
    UNIT      =  5,
};

#define TPB 32

#define SLOT_SEARCH TPB

/*
 * Tiles are constant setup, but have definitions to ease reading.
 * Changes here will require hard changes in other parts of the code.
 */
#define TILE_SIZE TPB
#define TILE_LEVELS (TPB == 16 ? 2 : 3)
#define TILE_SUBS (1+4+16)

struct tile {
    int level;
    int complexity[TILE_SUBS];
};

#endif
