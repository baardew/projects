typedef unsigned char t_terrain;

enum Terrain
{
    VOID = 0,
    GROUND = 1,
    ROCK = 2,
};


class Rule
{

public:
    Rule(t_terrain **terrain, int width, int height, int x, int y);
    t_terrain compute();
    int getX() { return x; }
    int getY() { return y; }
    void setX(int new_x) { x = new_x; }
    void setY(int new_y) { y = new_y; }
    
private:
    int range(Terrain t, int r);
    int neighboursInner(Terrain t);
    int neighboursOuter(Terrain t);
    int far(Terrain t);
    
    t_terrain **ref;
    int width, height;
    int x, y;
};
