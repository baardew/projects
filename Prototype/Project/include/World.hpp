#ifndef WORLD_H
#define WORLD_H

#include "terrain.h"
#include "Map.hpp"

#include <QSize>
#include <QPoint>

#define GL_GLEXT_PROTOTYPES
#include <QGLWidget>
#include <QGLShader>
#include <QGLShaderProgram>
#include <QGLFunctions>

class World : public QGLWidget, public Map
{
    Q_OBJECT

public:
    World(int, int);
    ~World();
	  
public slots:
    void toggle_generate();
    void toggle_light(bool on);
    void toggle_coordinate(bool on);
    void toggle_startslot(bool on);
    void toggle_simulate();
    void toggle_terminate();
    
public:
    void initializeGL();
    void paintGL();
    void resizeGL(int, int);
    QSize getSize();
    QPoint getUnitProductionStart();
    void setTerrain(t_terrain type, int x, int y);
    void setPoV(int x, int y, int size);

    // Line-of-Sight
    Tile get_tile(int x, int y);
    void reset_visibility();
    void set_visible(int x, int y);
    bool is_visible(int x, int y);
    bool is_transparent(int x, int y);


private:
    // Common
    int width, height, ready;
    t_terrain **terrain;
    t_terrain **fog;
    t_terrain **tiles;
    t_terrain **foundation;
    t_terrain **nulltex;
    struct tile **grid;

    t_terrain** t_terrain2Dcalloc(int width, int height);
    unsigned int** uint2Dcalloc(int width, int height);
    struct tile** stile2Dcalloc(int width, int height);
    void configureTexture(size_t idx);
    void generate();
    void light();
    void coordinate();
    void startslot();
    void terrainShader();
    void clearShader();
    void clearFog();
    void clearCoordinate();
    void clearStartslot();
    void start();
    void updateTerrain();

    // OpenGL
    int screenTextureCount;
    GLuint screenTexture[4];
    QGLFunctions glfunction;
    QGLShaderProgram shaderProgram;
    void checkGLError(const char*);
};

#endif
