#ifndef MAP_H
#define MAP_H

class Tile
{
public:
    Tile() : wall(false), visible(false) {};
    
    bool wall;
    bool visible;
};

class Map
{
public: 
    virtual Tile get_tile(int x, int y) = 0;
    virtual void reset_visibility() = 0;
    virtual void set_visible(int x, int y) = 0;
    virtual bool is_visible(int x, int y) = 0;
    virtual bool is_transparent(int x, int y) = 0;
};

#endif
