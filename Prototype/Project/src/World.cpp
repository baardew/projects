#include "timetaker.hpp"
#include "World.hpp"
#include "cuda_functions.h"
#include "KernelLoader.hpp"

#include <stdio.h>
#include <sys/time.h>

#include <iostream>
#include <string>

#include <QString>

World::World(int width, int height) :
    width(width),
    height(height),
    ready(0),
    screenTextureCount(0)
{
    if (width < 1500 && height < 900) {
        setMinimumSize(width, height);
        setMaximumSize(width, height);
    }

    glfunction.initializeGLFunctions(context());

    resizeGL(width, height);
    updateGL();

    terrain    = t_terrain2Dcalloc(width, height);
    fog        = t_terrain2Dcalloc(width, height);
    tiles      = t_terrain2Dcalloc(width, height);
    foundation = t_terrain2Dcalloc(width, height);
    
    nulltex    = t_terrain2Dcalloc(width, height);
    
    grid       = stile2Dcalloc(width, height);

    terrainShader();
}

World::~World()
{
    clearShader();
    free(terrain[0]);
    free(terrain);

    free(fog[0]);
    free(fog);

    free(tiles[0]);
    free(tiles);

    free(foundation[0]);
    free(foundation);

    free(nulltex[0]);
    free(nulltex);

    free(grid[0]);
    free(grid);
}

QSize World::getSize()
{
    return QSize(width, height);
}

QPoint World::getUnitProductionStart()
{
    int y = 0, x = 0;
    for (y = 0; y < height; y++)
        for (x = 0; x < width; x++)
            if (terrain[y][x] == BUILDING)
                return QPoint(x, y);

    return QPoint(-1, -1);
}

void World::setTerrain(t_terrain type, int x, int y)
{
    terrain[y][x] = type;
    updateTerrain();
}

Tile World::get_tile(int x, int y)
{
    Tile t;

    if (x < 0 || x >= width || y < 0 || y >= height)
        return t;
    
    t.wall = terrain[y][x] != GROUND && terrain[y][x] != BUILDING;
    t.visible = !fog[y][x];
    return t;
}

void World::reset_visibility()
{
    for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++)
            fog[y][x] = 1;
}

void World::set_visible(int x, int y)
{
    if (x < 0 || x >= width || y < 0 || y >= height)
        return;

    fog[y][x] = 0;
}

bool World::is_visible(int x, int y)
{
    return get_tile(x, y).visible;
}

bool World::is_transparent(int x, int y)
{
    return !get_tile(x, y).wall;
}

void World::toggle_generate()
{
    generate();
    light();
    coordinate();
    startslot();
    updateGL();
}

void World::toggle_light(bool on)
{
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, screenTexture[1]);
    GLint U = glfunction.glGetUniformLocation(shaderProgram.programId(), "tex_fog");
    glUniform1i(U, 1);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
		    GL_RED, GL_UNSIGNED_BYTE, on ? fog[0] : nulltex[0]);
    checkGLError("Terrain bind texture: fog");

    updateGL();
}

void World::toggle_coordinate(bool on)
{
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, screenTexture[2]);
    GLint U = glfunction.glGetUniformLocation(shaderProgram.programId(), "tex_tile");
    glUniform1i(U, 2);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
		    GL_RED, GL_UNSIGNED_BYTE, on ? tiles[0] : nulltex[0]);
    checkGLError("Terrain bind texture: tile");

    updateGL();
}

void World::toggle_startslot(bool on)
{

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, screenTexture[3]);
    GLint U = glfunction.glGetUniformLocation(shaderProgram.programId(), "tex_loc");
    glUniform1i(U, 3);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
		    GL_RED, GL_UNSIGNED_BYTE, on ? foundation[0] : nulltex[0]);
    checkGLError("Terrain bind texture: foundation");

    updateGL();
}

void World::updateTerrain()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, screenTexture[0]);
    GLint Y = glfunction.glGetUniformLocation(shaderProgram.programId(), "tex_terrain");
    glUniform1i(Y, 0);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
		    GL_RED, GL_UNSIGNED_BYTE, terrain[0]);
    checkGLError("Terrain bind texture: terrain");
}

void World::toggle_simulate()
{
    start();
    light();
    coordinate();
    startslot();
    updateGL();
}

void World::toggle_terminate()
{

}

t_terrain** World::t_terrain2Dcalloc(int width, int height)
{
    t_terrain *mem = (t_terrain*)calloc(width * height, sizeof(t_terrain));
    t_terrain **ptr = (t_terrain**)calloc(height, sizeof(t_terrain*));

    for (int i = 0; i < height; i++)
	ptr[i] = &(mem[i * width]);

    return ptr;
}

struct tile** World::stile2Dcalloc(int width, int height)
{
    struct tile *mem = (struct tile*)calloc(width * height, sizeof(struct tile));
    struct tile **ptr = (struct tile**)calloc(height, sizeof(struct tile*));

    for (int i = 0; i < height; i++)
	ptr[i] = &(mem[i * width]);

    return ptr;
}

unsigned int** World::uint2Dcalloc(int width, int height)
{
    unsigned int *mem = (unsigned int*)calloc(width * height, sizeof(unsigned int));
    unsigned int **ptr = (unsigned int**)calloc(height, sizeof(unsigned int*));

    for (int i = 0; i < height; i++)
	ptr[i] = &(mem[i * width]);

    return ptr;
}

void World::initializeGL()
{
    glShadeModel(GL_FLAT);
    glEnable(GL_TEXTURE_2D);

    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    checkGLError("InitializeGL");

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
    checkGLError("Render settings.\n");
}

void World::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
}

void World::setPoV(int x, int y, int size)
{
    glViewport(x, y, size, size);
}

void World::checkGLError(const char *msg)
{
    int err = glGetError();
    if (err) {
	fprintf(stderr, "OpenGL Error 0x%x: %s.\n", err, msg);
	std::exit(err);
    }
}

void World::paintGL()
{
    if (!ready) {
	glClear(GL_COLOR_BUFFER_BIT);
	return;
    }

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);       glVertex2f(0, 0);
    glTexCoord2f(1.0f, 0);    glVertex2f(width, 0);
    glTexCoord2f(1.0f, 1.0f); glVertex2f(width, height);
    glTexCoord2f(0, 1.0f);    glVertex2f(0, height);
    glEnd();

}

void World::startslot()
{
    cuda_startslot(foundation[0], terrain[0], width, height);
}

void World::clearStartslot()
{
    memset(foundation[0], 0, width * height * sizeof(t_terrain));
    toggle_startslot(false);
}


void World::coordinate()
{
    struct timeval start, stop;
    timetaker_start(&start);

    cuda_tile(terrain[0], width, height, grid[0], tiles[0]);

    timetaker_stop(&stop);

    long int sec, usec;
    timetaker_time(&start, &stop, &sec, &usec);

    int levels = 0;
    for (int y = 0; y < height; y++)
	for (int x = 0; x < width; x++)
	    levels += grid[y][x].level + 1;

    printf("Coordinate grid in %02ld:%02ld.%06ld (%.1fKlvl)\n",
	   sec / 60, sec % 60, usec, ((double)levels) / 1000);
}

void World::clearCoordinate()
{
    memset(tiles[0], 0, width * height * sizeof(t_terrain));
    toggle_coordinate(false);
}

void World::light()
{
    struct timeval start, stop;
    timetaker_start(&start);
    
    cuda_fog(fog[0], terrain[0], width, height);

    timetaker_stop(&stop);
	
    long int sec, usec;
    timetaker_time(&start, &stop, &sec, &usec);

    
    printf("Fog pixels in %02ld:%02ld.%06ld (%.1fKP/s)\n",
	   sec / 60, sec % 60, usec, ((float)width * height)/(float)1000.0);
}

void World::clearFog()
{
    memset(tiles[0], 0, width * height * sizeof(t_terrain));
    toggle_light(false);
}

void World::generate()
{
    ready = 1;
    clearFog();
    clearCoordinate();
    clearStartslot();

    struct timeval start, stop;
    timetaker_start(&start);

    unsigned int **seed = uint2Dcalloc(width, height);

    for (int y = 0; y < height; y++)
	for (int x = 0; x < width; x++)
	    seed[y][x] = rand();

    cuda_generate(terrain[0], width, height, seed[0]);
    timetaker_stop(&stop);

    long int sec, usec;
    timetaker_time(&start, &stop, &sec, &usec);

    printf("Generation computed in %02ld:%02ld.%06ld (%.1fMP/s)\n",
	   sec / 60, sec % 60, usec, ((double)width * height / sec) / 1000000);

    updateTerrain();
}

void World::configureTexture(size_t idx)
{
    glBindTexture(GL_TEXTURE_2D, screenTexture[idx]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
}

void World::terrainShader()
{
    screenTextureCount = 4;
    glGenTextures(screenTextureCount, screenTexture);

    configureTexture(0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
		 GL_RED, GL_UNSIGNED_BYTE, NULL);
    checkGLError("Terrain shader: terrain");


    configureTexture(1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
		 GL_RED, GL_UNSIGNED_BYTE, NULL);
    checkGLError("Terrain shader: fog");

    configureTexture(2);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
		 GL_RED, GL_UNSIGNED_BYTE, NULL);
    checkGLError("Terrain shader: tiles");

    configureTexture(3);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
		 GL_RED, GL_UNSIGNED_BYTE, NULL);
    checkGLError("Terrain shader: foundation");

    bool src_c = shaderProgram.addShaderFromSourceCode(
						       QGLShader::Fragment,
						       KernelLoader::readKernel("opengl/terrain.frg").c_str()
						       );
    if (!src_c)
	fprintf(stderr, "OpenGL Error: Terrain shader compilation failed.\n");

    shaderProgram.bind();
}

void World::clearShader() {
    glDeleteTextures(screenTextureCount, screenTexture);
    shaderProgram.release();
    shaderProgram.removeAllShaders();
}

void World::start()
{
    for (int y = 0; y < height; y++) {
	for (int x = 0; x < width; x++) {
	    if (foundation[y][x]) {
		printf("Start location at x=%d y=%d\n", x, y);

		for (int yy = y; yy < y + 10; yy++)
		    for (int xx = x; xx < x + 10; xx++)
			terrain[yy][xx] = BUILDING;

		updateTerrain();
		return;
	    }
	}
    }
}
