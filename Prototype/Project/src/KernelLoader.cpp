#include "KernelLoader.hpp"

#include <fstream>
#include <iostream>

std::string KernelLoader::readKernel(const char* filename)
{
    // Does not check for errors

    std::string src, in;
    std::ifstream file;
    
    file.open(filename);
    if (!file.is_open())
	return src;

    while (getline(file, in)) {
	src += in;
	src += '\n';
    }
    
    return src;
}
