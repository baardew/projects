#include "main.hpp"
#include "UnitControl.hpp"

#include <QApplication>
#include <QGridLayout>
#include <QPushButton>
#include <QColor>
#include <QLabel>

#include <stdio.h>
#include <stdlib.h>

Prototype::Prototype(int w, int h)
{
    world = new World(w, h);

    QGridLayout *layout = new QGridLayout;
    QGridLayout *control = new QGridLayout;

    fog = new QCheckBox("LOS");
    tile = new QCheckBox("Map");
    start = new QCheckBox("Start");
    QPushButton *generate = new QPushButton("Generate");
    QPushButton *simulate = new QPushButton("Simulate");
    QPushButton *terminate = new QPushButton("Terminate");
    QPushButton *placeUnit = new QPushButton("Place Unit");

    int i = 0;
    control->addWidget(generate, 1, i++);
    control->addWidget(simulate, 1, i++);
    control->addWidget(terminate, 1, i++);
    control->addWidget(placeUnit, 1, i++);

    i = 0;
    control->addWidget(fog, 2, i++);
    control->addWidget(tile, 2, i++);
    control->addWidget(start, 2, i++);
    control->addWidget(new QLabel(QString("Resolution %1 x %2 | TPB %3").arg(w).arg(h).arg(TPB)), 2, i++);

    layout->addWidget(world, 0, 0);
    layout->addLayout(control, 1, 0);

    connect(fog, SIGNAL(toggled(bool)), world, SLOT(toggle_light(bool)));
    connect(tile, SIGNAL(toggled(bool)), world, SLOT(toggle_coordinate(bool)));
    connect(start, SIGNAL(toggled(bool)), world, SLOT(toggle_startslot(bool)));
    connect(generate, SIGNAL(released()), this, SLOT(push_gen()));
    connect(simulate, SIGNAL(released()), world, SLOT(toggle_simulate()));
    connect(terminate, SIGNAL(released()), world, SLOT(toggle_terminate()));
    connect(placeUnit, SIGNAL(released()), this, SLOT(place_unit()));
    
    setLayout(layout);
}

void Prototype::push_gen()
{
    world->toggle_generate();
    fog->setChecked(false);
    tile->setChecked(false);
    start->setChecked(false);
}

void Prototype::place_unit()
{    
    UnitControl *uc = new UnitControl(*world);
    uc->show();
}

int main(int argc, char **argv)
{
    
    QApplication app(argc, argv);

    if (argc != 3) {
	printf("Missing resolution.\n");
	return EXIT_FAILURE;
    }

    int w = atoi(argv[1]);
    int h = atoi(argv[2]);

    if (w % TPB != 0 || h % TPB != 0) {
	fprintf(stderr, "Size (%d x %d) not compatible with TPB(%d)!\n", w, h, TPB);
	return EXIT_FAILURE;
    }
    
    Prototype mainWin(w, h);
    mainWin.show();
    return app.exec();
}
