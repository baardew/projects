#include "timetaker.hpp"
#include "UnitControl.hpp"
#include "MRPAS.hpp"

#include <QGridLayout>
#include <QPushButton>
#include <QShortcut>

#include <stdio.h>

UnitControl::UnitControl(World &world) :
    world(world)
{
    x = world.getUnitProductionStart().x();
    y = world.getUnitProductionStart().y();

    QGridLayout *control = new QGridLayout;

    QPushButton *up = new QPushButton("UP");
    QPushButton *down = new QPushButton("DOWN");
    QPushButton *left = new QPushButton("LEFT");
    QPushButton *right = new QPushButton("RIGHT");

    int i = 0;
    control->addWidget(up, 0, i++);
    control->addWidget(down, 0, i++);
    control->addWidget(left, 0, i++);
    control->addWidget(right, 0, i++);

    connect(up, SIGNAL(released()), this, SLOT(moveUp()));
    connect(down, SIGNAL(released()), this, SLOT(moveDown()));
    connect(left, SIGNAL(released()), this, SLOT(moveLeft()));
    connect(right, SIGNAL(released()), this, SLOT(moveRight()));

    new QShortcut(QKeySequence(Qt::Key_Up), this, SLOT(moveUp()));
    new QShortcut(QKeySequence(Qt::Key_Down), this, SLOT(moveDown()));
    new QShortcut(QKeySequence(Qt::Key_Left), this, SLOT(moveLeft()));
    new QShortcut(QKeySequence(Qt::Key_Right), this, SLOT(moveRight()));


    setLayout(control);
    world.setTerrain(UNIT, x, y);
}

void UnitControl::moveUp()
{
    if (y == 0)
        return;

    world.setTerrain(GROUND, x, y);
    y--;
    world.setTerrain(UNIT, x, y);
    computeVision();
}

void UnitControl::moveDown()
{
    if (y >= world.getSize().height() - 1)
        return;

    world.setTerrain(GROUND, x, y);
    y++;
    world.setTerrain(UNIT, x, y);
    computeVision();
}

void UnitControl::moveLeft()
{
    if (x == 0)
        return;

    world.setTerrain(GROUND, x, y);
    x--;
    world.setTerrain(UNIT, x, y);
    computeVision();
}

void UnitControl::moveRight()
{
    if (x >= world.getSize().width() - 1)
        return;

    world.setTerrain(GROUND, x, y);
    x++;
    world.setTerrain(UNIT, x, y);
    computeVision();
}

void UnitControl::computeVision()
{
    t_player p;
    p.x = x;
    p.y = y;

    world.set_visible(x, y);
    MRPAS m(world.getSize().width(), world.getSize().height());

    struct timeval start, stop;
    long int sec, usec;

    timetaker_start(&start);
    m.computeFoV(world, p, 50);
    timetaker_stop(&stop);

    timetaker_time(&start, &stop, &sec, &usec);
    float total_usec = usec + sec * 1000000.0f;
    printf("MRPAS::computeFoV took %ld.%06lds (%.2fFPS)\n", sec, usec, ( 1 / (total_usec / 1000000)));

    world.toggle_light(true);
}
