#include "MRPAS.hpp"

#include <algorithm>

using namespace std;

MRPAS::MRPAS(int width, int height) :
    width(width),
    height(height)
{

}

MRPAS::~MRPAS()
{

}

void MRPAS::computeFoV(World &map, t_player player, int radius)
{
    this->session.map = &map;
    this->session.position = player;
    this->session.maxRadius = radius * radius + 2 * radius + 1;

    session.startAngle = new float[radius * radius];
    session.endAngle = new float[radius * radius];

    map.reset_visibility();
    map.set_visible(player.x, player.y);

    session.dx = 1;
    session.dy = 1;
    quadrant();

    session.dx = 1;
    session.dy = -1;
    quadrant();

    session.dx = -1;
    session.dy = 1;
    quadrant();

    session.dx = -1;
    session.dy = -1;
    quadrant();

    delete[] session.endAngle;
    delete[] session.startAngle;
}

void MRPAS::quadrant()
{
    session.y = session.position.y + session.dy;
    if (session.y >= 0 && session.y < height) {
        session.ddx = 0;
        session.ddy = session.dy;
        session.iteration = 1;
        session.totalObstacles = 0;
        session.obstaclesInLastLine = 0;
        session.minAngle = 0.0f;
        vertical();
    }

    session.x = session.position.x + session.dx;
    if (session.x >= 0 && session.x < width) {
        session.ddx = session.dx;
        session.ddy = 0;
        session.iteration = 1;
        session.totalObstacles = 0;
        session.obstaclesInLastLine = 0;
        session.minAngle = 0.0;
        horizontal();
    }
}

void MRPAS::horizontal()
{
    while (true) {
        if (slope(MRPAS::Horizontal))
            break;

        session.x += session.dx;
        if (session.x < 0 || session.x >= width)
            break;

        session.iteration++;
        session.obstaclesInLastLine = session.totalObstacles;
    }
}

void MRPAS::vertical()
{
    while (true) {
        if (slope(MRPAS::Vertical))
            break;

        session.y += session.dy;
        if (session.y < 0 || session.y >= height)
            break;

        session.iteration++;
        session.obstaclesInLastLine = session.totalObstacles;
    }
}

bool MRPAS::slope(Direction direction)
{
    float slopesPerCell = 1.0f / (session.iteration + 1.0f);
    float halfSlopes = slopesPerCell * 0.5f;
    int processedCell = session.minAngle / slopesPerCell;

    bool done = true;

    int from;
    int to;
    float *value;
    int addition;

    if (direction == Vertical) {
        session.x = session.position.x + (processedCell * session.dx);
        from = max(0, session.position.x - session.iteration);
        to = min(width - 1, session.position.x + session.iteration);
        value = &session.x;
        addition = session.dx;
    } else {
        session.y = session.position.y + (processedCell * session.dy);
        from = max(0, session.position.y - session.iteration);
        to = min(height - 1, session.position.y + session.iteration);
        value = &session.y;
        addition = session.dy;
    }

    while (*value >= from && *value <= to) {
        if (!inRange())
            break;

        bool visible = true;
        float startSlope = processedCell * slopesPerCell;
        float centerSlope = startSlope + halfSlopes;
        float endSlope = startSlope + slopesPerCell;
        bool transparentPosition = session.map->is_transparent(session.x, session.y);

        if (session.obstaclesInLastLine > 0 && !session.map->is_visible(session.x, session.y))
            visible = visibility(transparentPosition, startSlope, centerSlope, endSlope);

        if (visible) {
            session.map->set_visible(session.x, session.y);
            done = false;

            if (!transparentPosition) {
                if (session.minAngle >= startSlope) {
                    if (endSlope >= 1.0f)
                        return true;

                    session.minAngle = endSlope;

                } else {
                    session.startAngle[session.totalObstacles] = startSlope;
                    session.endAngle[session.totalObstacles] = endSlope;
                    session.totalObstacles++;
                }
            }
        }

        processedCell++;
        *value += addition;
    }

    return done;
}

bool MRPAS::inRange()
{
    int diffx = session.position.x - session.x;
    int diffy = session.position.y - session.y;

    return (diffx * diffx) + (diffy * diffy) < session.maxRadius;
}

bool MRPAS::visibility(bool transparentPosition, float startSlope, float centerSlope, float endSlope)
{
    for (int idx = 0; idx < session.obstaclesInLastLine; idx++) {
        if (transparentPosition && centerSlope > session.startAngle[idx] && centerSlope < session.endAngle[idx])
            return false;

        if (startSlope >= session.startAngle[idx] && endSlope <= session.endAngle[idx])
            return false;
    }

    bool regionVisible
        = session.map->is_visible(session.x - session.ddx, session.y - session.ddy)
        && session.map->is_transparent(session.x - session.ddx, session.y - session.ddy);
    bool visibleOrTransparent
        = session.map->is_visible(session.x - session.dx, session.y - session.dy)
        && session.map->is_transparent(session.x - session.dx, session.y - session.dy);

    if (!regionVisible && !visibleOrTransparent)
        return false;

    return true;
}
