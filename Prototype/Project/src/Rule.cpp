#include "Rule.hpp"

#define ON_TERR(cy, cx, y, x) (!(cy == y && cx == x) && cy > -1 && cy < height && cx > -1 && cx < width)
#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)

Rule::Rule(t_terrain **terrain, int width, int height, int x, int y) :
    ref(terrain),
    width(width),
    height(height),
    x(x),
    y(y)
{

}

t_terrain Rule::compute()
{
    int GRND_small = range(GROUND, 1);
    int ROCK_small = range(ROCK, 1);

    if (GRND_small && ROCK_small) {
	if (neighboursOuter(GROUND) >= neighboursOuter(ROCK) * 2)
	    return GROUND;
	else if (far(ROCK))
	    return ROCK;
    }

    if (!GRND_small && ROCK_small)
	return ROCK;

    if (GRND_small || ( range(GROUND, 2) && !(ROCK_small || range(ROCK, 2)) ))
	return GROUND;

    return VOID;
}

int Rule::range(Terrain t, int r)
{
    int checkY = MAX(y - r, 0);
    int endY   = MIN(y + r, height - 1);
    int startX = MAX(x - r, 0);
    int endX   = MIN(x + r, width - 1);

    for (; checkY <= endY; checkY++)
	for (int checkX = startX; checkX <= endX; checkX++)
	    if (!(checkY == y && checkX == x) && ref[checkY][checkX] == t)
		return 1;
    return 0;
}

int Rule::far(Terrain t)
{
    for (int checkY = y - 5; checkY <= y + 5; checkY++)
	for (int checkX = x - 5; checkX <= x + 5; checkX++)
	    if (ON_TERR(checkX, checkY, x, y)
		&& ref[checkX][checkY] == t
		&& (checkX == x - 5 || checkX == x + 5)
		&& (checkY == y - 5 || checkY == y + 5))
		return 1;
    return 0;
}

int Rule::neighboursInner(Terrain t)
{
    int checkY = MAX(y - 1, 0);
    int endY   = MIN(y + 1, height - 1);
    int startX = MAX(x - 1, 0);
    int endX   = MIN(x + 1, width - 1);

    int count = 0;
    for (; checkY <= endY; checkY++)
	for (int checkX = startX; checkX <= endX; checkX++)
	    if (!(checkY == y && checkX == x) && ref[checkY][checkX] == t)
		count++;

    return count;
}

int Rule::neighboursOuter(Terrain t)
{
    int countIn = neighboursInner(t);

    int checkY = MAX(y - 2, 0);
    int endY   = MIN(y + 2, height - 1);
    int startX = MAX(x - 2, 0);
    int endX   = MIN(x + 2, width - 1);

    int count = 0;
    for (; checkY <= endY; checkY++)
	for (int checkX = startX; checkX <= endX; checkX++)
	    if (!(checkY == y && checkX == x) && ref[checkY][checkX] == t)
		count++;

    int diff = count - countIn;
    return diff > 0 ? diff : 0;
}
