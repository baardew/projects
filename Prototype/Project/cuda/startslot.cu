#include "cuda_functions.h"

#include <stdio.h>

#define CUDA_ERR_VAR cudaError_t _cuda_err
#define CUDA_CHECK(_cuda)                                               \
    _cuda_err = _cuda;                                                  \
    if (_cuda_err != cudaSuccess) {                                     \
	printf("CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
	exit(EXIT_FAILURE);                                             \
    }

#define CUDA_CHECK_KERNEL						\
    _cuda_err = cudaGetLastError();					\
    if (_cuda_err != cudaSuccess) {					\
	printf("CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
	exit(EXIT_FAILURE);						\
    }

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)
#define COORD_XY int x = blockIdx.x * TPB + threadIdx.x; int y = (blockIdx.y * TPB  + threadIdx.y) * width;

__device__ t_terrain *terr;
__device__ int width, height;

__device__
int range_space(t_terrain t, int r)
{
    COORD_XY;
    int checkY = MAX(y - (r * width), 0);
    int endY   = MIN(y + (r * width), (height - 1) * width);
    int startX = MAX(x - r, 0);
    int endX   = MIN(x + r, width - 1);
    int checkX;
    
    for (; checkY <= endY; checkY += width)
	for (checkX = startX; checkX <= endX; checkX++)
	    if (!(checkY == y && checkX == x) && terr[checkY + checkX] != t)
		return 1;
    return 0;
}

__global__
void locate_terrain(t_terrain *l, t_terrain *t, int w, int h)
{
    width = w;
    height = h;
    terr = t;

    COORD_XY;
    
    int d_x = blockIdx.x * TPB + threadIdx.x;
    int d_y = blockIdx.y * TPB + threadIdx.y;
    if (d_x < SLOT_SEARCH || d_x > w - SLOT_SEARCH || d_y < SLOT_SEARCH || d_y > h - SLOT_SEARCH) {
        l[y + x] = 0;
	return;
    }

    l[y + x] = range_space(GROUND, SLOT_SEARCH) != 1;
}

__host__
void cuda_startslot(t_terrain *location, t_terrain *terrain, int width, int height)
{
    CUDA_ERR_VAR;
    int size = width * height * sizeof(t_terrain);    
    t_terrain *d_terr, *d_loc;
    dim3 gridDim(width / TPB, height / TPB);
    dim3 blockDim(TPB, TPB);
    
    CUDA_CHECK(cudaMalloc((void**)&d_terr, size));
    CUDA_CHECK(cudaMalloc((void**)&d_loc, size));
    CUDA_CHECK(cudaMemcpy(d_terr, terrain, size, cudaMemcpyHostToDevice));

    locate_terrain<<<gridDim, blockDim>>>(d_loc, d_terr, width, height);
    CUDA_CHECK_KERNEL;

    CUDA_CHECK(cudaMemcpy(location, d_loc, size, cudaMemcpyDeviceToHost));

    CUDA_CHECK(cudaFree(d_terr));
    CUDA_CHECK(cudaFree(d_loc));
}
