#include "cuda_functions.h"

#include <stdio.h>

#define CUDA_ERR_VAR cudaError_t _cuda_err
#define CUDA_CHECK(_cuda)                                               \
    _cuda_err = _cuda;                                                  \
    if (_cuda_err != cudaSuccess) {                                     \
	printf("CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
	exit(EXIT_FAILURE);                                             \
    }

#define CUDA_CHECK_KERNEL						\
    _cuda_err = cudaGetLastError();					\
    if (_cuda_err != cudaSuccess) {					\
	printf("CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
	exit(EXIT_FAILURE);						\
    }

#define COORD_XY int x = blockIdx.x * TPB + threadIdx.x; int y = (blockIdx.y * TPB  + threadIdx.y) * w;

__device__
int complexity_index(int level)
{
    if (level == 0)
	return 0;

    if (level == 1 && TPB/2 >= 8) {
	int block = (TPB / 2);
	int y = threadIdx.y / block;
	int x = threadIdx.x / block;
	return 1 + y * 2 + x;
    }

    if (level == 2 && TPB/4 >= 8) {
	int block = (TPB / 4);
	int y = threadIdx.y / block;
	int x = threadIdx.x / block;
	return 1 + 4 + y * 4 + x;	
    }

    return -1;
}

__device__
int clean_tile(struct tile *mapping, int *has_ground, int *has_rock)
{
    if (mapping->level == 0) {
        if (has_rock[0]) {
            if (has_ground[0])
                mapping->complexity[0] = 3;
            else
                mapping->complexity[0] = 2;
        } else {
            mapping->complexity[0] = 1;
        }
        
	return mapping->complexity[0] != 3;
    }

    if (mapping->level == 1 && TPB/2 >= 8) {
	for (int i = 1; i <= 4; i++) {
            if (has_rock[i]) {
                if (has_ground[i]) {
                    mapping->complexity[i] = 3;
                } else {
                    mapping->complexity[i] = 2;
                }
            } else {
                mapping->complexity[i] = 1;
            }
        }

        int clear = 1;
        int base = mapping->complexity[1];
        for (int i = 1; i <= 4; i++) {
            clear = clear && mapping->complexity[i] == base;
        }

	return clear;
    }

    if (mapping->level == 2 && TPB/4 >= 8) {
	for (int i = 5; i <= 20; i++) {
            if (has_rock[i]) {
                if (has_ground[i]) {
                    mapping->complexity[i] = 3;
                } else {
                    mapping->complexity[i] = 2;
                }
            } else {
                mapping->complexity[i] = 1;
            }
        }

        int clear = 1;
        int base = mapping->complexity[5];
        for (int i = 5; i <= 20; i++) {
            clear = clear && mapping->complexity[i] == base;
        }

	return clear;
    }

    // Can never come here
    return 1;
}

__device__
int color_tile(struct tile *mapping)
{
    int idx_c = complexity_index(mapping->level);
    t_terrain pixel = (t_terrain)mapping->complexity[idx_c];

    if (idx_c == 0) {
        if (threadIdx.x == 0 || threadIdx.x == TPB - 1
            || threadIdx.y == 0 || threadIdx.y == TPB - 1)
            return pixel;
    }

    if (idx_c >= 1 && idx_c <= 4) {
        if (threadIdx.x == 0 || threadIdx.x == TPB - 1
            || threadIdx.y == 0 || threadIdx.y == TPB - 1
            || threadIdx.x == TPB/2 || threadIdx.x == (TPB/2)-1
            || threadIdx.y == TPB/2 || threadIdx.y == (TPB/2)-1)
            return pixel;        
    }

    if (idx_c >= 5 && idx_c <= 20) {
        if (threadIdx.x == 0 || threadIdx.x == TPB - 1
            || threadIdx.y == 0 || threadIdx.y == TPB - 1)
            return pixel;
    }

    return 0;
}

__global__
void tile_terrain(t_terrain *t, int w, int h, struct tile *g, t_terrain *m)
{
    __shared__ struct tile mapping;
    __shared__ int has_ground[TILE_SUBS];
    __shared__ int has_rock[TILE_SUBS];
    COORD_XY;
    
    if (threadIdx.x == 0 && threadIdx.y == 0) {
	mapping = g[blockIdx.y * (w / TILE_SIZE) + blockIdx.x];
	mapping.level = 0;
    }
    
    has_ground[(y+x) % TILE_SUBS] = 0;
    has_rock[(y+x) % TILE_SUBS] = 0;

    __syncthreads();

    for (int i = 0; i < TILE_LEVELS; i++) {
        mapping.level = i;
        __syncthreads();
        
        int idx_c = complexity_index(mapping.level);
        if (t[y + x] == GROUND)
            has_ground[idx_c] = 1;
        else if (t[y + x] != GROUND)
            has_rock[idx_c] = 1;
    }
    __syncthreads();

    
    if (threadIdx.x == 0 && threadIdx.y == 0) {
        mapping.level = 0;
        int lvl_0 = clean_tile(&mapping, has_ground, has_rock);

        mapping.level = 1;
        int lvl_1 = clean_tile(&mapping, has_ground, has_rock);

        mapping.level = 2;
        int lvl_2 = clean_tile(&mapping, has_ground, has_rock);

        if (!lvl_0) {
            if (!lvl_1) {
                if (!lvl_2) {
                    mapping.level = 2;
                } else {
                    mapping.level = 1;
                }
            } else {
                mapping.level = 0;
            }
        } else {
            mapping.level = 0;
        }
        

        g[blockIdx.y * (w / TILE_SIZE) + blockIdx.x] = mapping;
    }

    __syncthreads();
    
    m[y + x] = color_tile(&mapping);
}

__host__
void cuda_tile(t_terrain *terrain, int width, int height, struct tile *grid, t_terrain *mapping)
{
    CUDA_ERR_VAR;
    int size = width * height;
    t_terrain *d_terr, *d_map;
    struct tile *d_grid;
    dim3 gridDim(width / TPB, height / TPB);
    dim3 blockDim(TPB, TPB);
    
    CUDA_CHECK(cudaMalloc((void**)&d_terr, size * sizeof(t_terrain)));
    CUDA_CHECK(cudaMalloc((void**)&d_map,  size * sizeof(t_terrain)));
    CUDA_CHECK(cudaMalloc((void**)&d_grid, size * sizeof(struct tile)));
    CUDA_CHECK(cudaMemcpy(d_map,  mapping, size * sizeof(t_terrain), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(d_terr, terrain, size * sizeof(t_terrain), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(d_grid, grid,    size * sizeof(struct tile), cudaMemcpyHostToDevice));

    tile_terrain<<<gridDim, blockDim>>>(d_terr, width, height, d_grid, d_map);
    CUDA_CHECK_KERNEL;

    CUDA_CHECK(cudaMemcpy(grid, d_grid, size * sizeof(struct tile), cudaMemcpyDeviceToHost));
    CUDA_CHECK(cudaMemcpy(mapping, d_map, size * sizeof(t_terrain), cudaMemcpyDeviceToHost));

    CUDA_CHECK(cudaFree(d_terr));
    CUDA_CHECK(cudaFree(d_map));
    CUDA_CHECK(cudaFree(d_grid));
}
