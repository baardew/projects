#include "cuda_functions.h"

#include <stdio.h>

#define CUDA_ERR_VAR cudaError_t _cuda_err
#define CUDA_CHECK(_cuda)                                               \
    _cuda_err = _cuda;                                                  \
    if (_cuda_err != cudaSuccess) {                                     \
	printf("CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
	exit(EXIT_FAILURE);                                             \
    }

#define CUDA_CHECK_KERNEL						\
    _cuda_err = cudaGetLastError();					\
    if (_cuda_err != cudaSuccess) {					\
	printf("CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
	exit(EXIT_FAILURE);						\
    }

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)
#define COORD_XY int x = blockIdx.x * TPB + threadIdx.x; int y = (blockIdx.y * TPB  + threadIdx.y) * width;

#define SPWN 48000

__device__ t_terrain *cur, *ref;
__device__ int width, height;

__device__
int range(t_terrain t, int r)
{
    COORD_XY;
    int checkY = MAX(y - (r * width), 0);
    int endY   = MIN(y + (r * width), (height - 1) * width);
    int startX = MAX(x - r, 0);
    int endX   = MIN(x + r, width - 1);
    int checkX;

    for (; checkY <= endY; checkY += width)
	for (checkX = startX; checkX <= endX; checkX++)
	    if (!(checkY == y && checkX == x) && ref[checkY + checkX] == t)
		return 1;
    return 0;
}

__device__
int far(t_terrain t)
{
    COORD_XY;
    int checkY = MAX(y - (5 * width), 0);
    int endY   = MIN(y + (5 * width), (height - 1) * width);
    int startX = MAX(x - 5, 0);
    int endX   = MIN(x + 5, width - 1);
    int checkX;
    for (; checkY <= endY; checkY += width)
	for (checkX = startX; checkX <= endX; checkX++)
	    if (ref[checkY + checkX] == t
		&& (checkX == x - 5 || checkX == x + 5)
		&& (checkY == y - 5 || checkY == y + 5))
		return 1;
    return 0;
}

__device__
int neighboursInner(t_terrain t)
{
    COORD_XY;
    int checkY = MAX(y - width, 0);
    int endY   = MIN(y + width, (height - 1) * width);
    int startX = MAX(x - 1, 0);
    int endX   = MIN(x + 1, width - 1);
    int checkX;

    int count = 0;
    for (; checkY <= endY; checkY += width)
	for (checkX = startX; checkX <= endX; checkX++)
	    if (!(checkY == y && checkX == x) && ref[checkY + checkX] == t)
		count++;

    return count;
}

__device__
int neighboursOuter(t_terrain t)
{
    COORD_XY;
    int countIn = neighboursInner(t);

    int checkY = MAX(y - (2 * width), 0);
    int endY   = MIN(y + (2 * width), (height - 1) * width);
    int startX = MAX(x - 2, 0);
    int endX   = MIN(x + 2, width - 1);
    int checkX;

    int count = 0;
    for (; checkY <= endY; checkY += width)
	for (checkX = startX; checkX <= endX; checkX++)
	    if (!(checkY == y && checkX == x) && ref[checkY + checkX] == t)
		count++;

    int diff = count - countIn;
    return diff > 0 ? diff : 0;
}

__device__
t_terrain compute(void)
{
    COORD_XY;
    if (ref[y + x] != VOID)
	return ref[y + x];
    
    int GRND_small = range(GROUND, 1);
    int ROCK_small = range(ROCK, 1);

    if (GRND_small && ROCK_small) {
	if (neighboursOuter(GROUND) >= neighboursOuter(ROCK) * 2)
	    return GROUND;
	else if (far(ROCK))
	    return ROCK;
    }

    if (!GRND_small && ROCK_small)
	return ROCK;

    if (GRND_small || ( range(GROUND, 2) && !(ROCK_small || range(ROCK, 2)) ))
	return GROUND;

    return VOID;
}

__global__
void generate_terrain(t_terrain *t, t_terrain *b, int w, int h, int *c)
{
    ref = b;
    cur = t;
    width = w;
    height = h;

    COORD_XY;
    cur[y + x] = compute();
    if (cur[y + x] != ref[y + x])
	*c = 1;
}

__device__
float simple_rand(unsigned int *seed) {
    // constants for random no gen.
    unsigned long a = 16807;  		
    unsigned long m = 2147483647;   	// 2^31 - 1
    unsigned long x = (unsigned long) *seed;
    
    x = (a * x) % m;
    
    *seed = (unsigned int) x;
    return ((float)x)/m;
}

__global__
void seed_terrain(t_terrain *t, t_terrain *b, int w, int h, unsigned int *seed)
{
    ref = b;
    cur = t;
    width = w;
    height = h;

    COORD_XY;
    int r = (seed[y + x] % SPWN) + 1;
    simple_rand(&seed[y + x]);
    if (r < 10) {
	cur[y + x] = GROUND;
	ref[y + x] = GROUND;
    } else if (r < 35) {
	cur[y + x] = ROCK;
	ref[y + x] = ROCK;
    } else {
	cur[y + x] = VOID;
	ref[y + x] = VOID;
    }
}

__global__
void erode_terrain(t_terrain *t, t_terrain *b, int w, int h, unsigned int *seed)
{
    ref = b;
    cur = t;
    width = w;
    height = h;

    COORD_XY;
    
    float r = simple_rand(&seed[y + x]);
    
    if (ref[y + x] == GROUND && range(ROCK, 1) && r < (float)0.3)
	cur[y + x] = ROCK;
    else if (ref[y + x] == ROCK && range(GROUND, 1) && r < (float)0.3)
	cur[y + x] = ROCK;

}

__global__
void resource_terrain(t_terrain *t, t_terrain *b, int w, int h, unsigned int *seed)
{
    ref = b;
    cur = t;
    width = w;
    height = h;

    COORD_XY;
    
    float r = simple_rand(&seed[y + x]);
    if (ref[y + x] == ROCK && r < (float)0.004)
	cur[y + x] = RESOURCE;
}

__host__
void cuda_generate(t_terrain *terrain, int width, int height, unsigned int *seed)
{
    CUDA_ERR_VAR;
    int size = width * height * sizeof(t_terrain);    
    t_terrain *d_terr, *d_buf;
    unsigned int *d_seed;
    int *d_compute, h_compute = 1;
    dim3 gridDim(width / TPB, height / TPB);
    dim3 blockDim(TPB, TPB);
    int iters;
    
    CUDA_CHECK(cudaMalloc((void**)&d_terr, size));
    CUDA_CHECK(cudaMalloc((void**)&d_buf, size));
    CUDA_CHECK(cudaMalloc((void**)&d_compute, sizeof(int)));
    CUDA_CHECK(cudaMalloc((void**)&d_seed, width * height * sizeof(unsigned int)));
    CUDA_CHECK(cudaMemcpy(d_terr, terrain, size, cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(d_seed, seed, width * height * sizeof(unsigned int), cudaMemcpyHostToDevice));

    // seed
    seed_terrain<<<gridDim, blockDim>>>(d_terr, d_buf, width, height, d_seed);
    CUDA_CHECK_KERNEL;

    // grow
    while (h_compute) {
	h_compute = 0;
	CUDA_CHECK(cudaMemcpy(d_compute, &h_compute, sizeof(int), cudaMemcpyHostToDevice));
	CUDA_CHECK(cudaMemcpy(d_buf, d_terr, size, cudaMemcpyDeviceToDevice));
	
	generate_terrain<<<gridDim, blockDim>>>(d_terr, d_buf, width, height, d_compute);
	CUDA_CHECK_KERNEL;
	
	CUDA_CHECK(cudaMemcpy(&h_compute, d_compute, sizeof(int), cudaMemcpyDeviceToHost));
    }

    // erosion
    for (iters = 0; iters < 3; iters++) {
	CUDA_CHECK(cudaMemcpy(d_buf, d_terr, size, cudaMemcpyDeviceToDevice));
	
	erode_terrain<<<gridDim, blockDim>>>(d_terr, d_buf, width, height, d_seed);
	CUDA_CHECK_KERNEL;
    }

    // resources
    CUDA_CHECK(cudaMemcpy(d_buf, d_terr, size, cudaMemcpyDeviceToDevice));
    resource_terrain<<<gridDim, blockDim>>>(d_terr, d_buf, width, height, d_seed);
    CUDA_CHECK_KERNEL;

    // result
    CUDA_CHECK(cudaMemcpy(terrain, d_terr, size, cudaMemcpyDeviceToHost));

    CUDA_CHECK(cudaFree(d_terr));
    CUDA_CHECK(cudaFree(d_buf));
    CUDA_CHECK(cudaFree(d_compute));
}
