#include "cuda_functions.h"

#include <stdio.h>

#define CUDA_ERR_VAR cudaError_t _cuda_err
#define CUDA_CHECK(_cuda)                                               \
    _cuda_err = _cuda;                                                  \
    if (_cuda_err != cudaSuccess) {                                     \
	printf("CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
	exit(EXIT_FAILURE);                                             \
    }

#define CUDA_CHECK_KERNEL						\
    _cuda_err = cudaGetLastError();					\
    if (_cuda_err != cudaSuccess) {					\
	printf("CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
	exit(EXIT_FAILURE);						\
    }

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)
#define COORD_XY int x = blockIdx.x * TPB + threadIdx.x; int y = (blockIdx.y * TPB  + threadIdx.y) * width;

__device__ t_terrain *terr, *fog;
__device__ int width, height;

#define R TPB*2

__device__
int have_los(int x, int y, int tx, int ty)
{
    if (x > tx) {
	int tmp = x;
	x = tx;
	tx = tmp;
    }

    if (y > ty) {
	int tmp = y;
	y = tx;
	tx = tmp;
    }

    int diffX = tx - x;
    int diffY = ty - y;

    int incX = 1;
    int incY = width;

    while (x < tx && y < ty) {
	if (terr[y + x] != GROUND && terr[y + x] != BUILDING)
	    return 0;

	if (x < tx)
	    x += incX;
	if (y < ty)
	    y += incY;
    }

    return 1;
}

__device__
int range_fog()
{
    COORD_XY;
    int checkY = MAX(y - (R * width), 0);
    int endY   = MIN(y + (R * width), (height - 1) * width);
    int startX = MAX(x - R, 0);
    int endX   = MIN(x + R, width - 1);
    int checkX;
    int visible = 0;
    
    for (; checkY <= endY; checkY += width) {
	for (checkX = startX; checkX <= endX; checkX++) {
	    if (terr[checkY + checkX] == BUILDING) {
		visible |= have_los(x, y, checkX, checkY);
	    }
        }
    }

    return visible;
}

__global__
void fog_terrain(t_terrain *f, t_terrain *t, int w, int h)
{
    width = w;
    height = h;
    terr = t;
    fog = f;

    COORD_XY;
    f[y + x] = range_fog() != 1;
}

__host__
void cuda_fog(t_terrain *fog, t_terrain *terrain, int width, int height)
{
    CUDA_ERR_VAR;
    int size = width * height * sizeof(t_terrain);    
    t_terrain *d_terr, *d_fog;
    dim3 gridDim(width / TPB, height / TPB);
    dim3 blockDim(TPB, TPB);
    
    CUDA_CHECK(cudaMalloc((void**)&d_terr, size));
    CUDA_CHECK(cudaMalloc((void**)&d_fog, size));
    CUDA_CHECK(cudaMemcpy(d_terr, terrain, size, cudaMemcpyHostToDevice));

    fog_terrain<<<gridDim, blockDim>>>(d_fog, d_terr, width, height);
    CUDA_CHECK_KERNEL;

    CUDA_CHECK(cudaMemcpy(fog, d_fog, size, cudaMemcpyDeviceToHost));

    CUDA_CHECK(cudaFree(d_terr));
    CUDA_CHECK(cudaFree(d_fog));
}
