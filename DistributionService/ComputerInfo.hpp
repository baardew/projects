#ifndef COMPUTER_INFO_H
#define COMPUTER_INFO_H

#define MAX_NAME_LENGTH 64

#include "Serializer.hpp"

#include <unistd.h>
#include <sys/sysctl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <cmath>
#include <cstring>

class ComputerInfo
{
public:
    ComputerInfo();

    int   getCores();
    int   getMemory();
    char* getName();
    bool  hasCuda();

    unsigned int serialize(unsigned char *buf);
    unsigned int deserialize(unsigned char *buf);

private:
    struct Data {
	int cores;
	int memory;
	bool cuda;
	char name[MAX_NAME_LENGTH];
    };

    Data d;

    size_t getMemoryAmount();
};

#endif
