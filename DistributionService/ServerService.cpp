#include "ServerService.hpp"

ServerService::ServerService() :
    waitForClients(true),
    clientSock(-1)
{
    socketHandle = createConnection(0, SOCKET_TCP);
    int ret = listen(socketHandle, MAX_CLIENTS_QUEUE);
    PERROR(ret, "Socket listening");
}

ServerService::~ServerService()
{
    closeConnection(socketHandle);
}

void ServerService::halt()
{
    waitForClients = false;
}

void ServerService::awaitClient(int sockBCast)
{
    int ret, len;
    int selfPort = getSockPort(socketHandle);

    struct ComputerInfo client;
    struct timeval tv;
    tv.tv_sec = 10;
    tv.tv_usec = 0;

    struct sockaddr_in recvAddr;
    socklen_t addrLen =  sizeof(struct sockaddr_in);
    memset(&recvAddr, 0, sizeof(struct sockaddr_in));

    ret = setsockopt(sockBCast, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
    PERROR(ret, "Servers await timeout");

    log("DS:server", "Ready for clients");
    while (waitForClients) {
	ret = recvfrom(sockBCast, buf_net, BUF_SIZE, 0, (struct sockaddr*)&recvAddr, &addrLen);
	if (ret <= 0)
	    continue;

	len = ret;
	len = unpackBuffer(buf_data, header, buf_net, len);

	if (!(header.flag & IS_COMMAND))
	    continue;

	switch (header.flag) {
	case REQUEST:
	    client.deserialize(buf_data);
	    log("DS:server", "Got request from %s:%d", client.getName(), ntohs(recvAddr.sin_port));

	    len = computer.serialize(buf_data);
	    len += Serializer::serialize(buf_data + len, "h", selfPort);

	    header.flag = REPLY;
	    header.len  = len;

	    len = packBuffer(buf_net, header, buf_data, len);

	    ret = sendto(sockBCast, buf_net, len, 0, (struct sockaddr*)&recvAddr, addrLen);
	    PERROR(ret, "Client reply");
	    break;

	case AQUIRE:
	    log("DS:server", "Aqustition received");

	    clientSock = linkClient();
	    supportClient();
	    unlinkClient();
	    clientSock = -1;
	    break;

	default:
	    break;
	}
    }
}

int ServerService::linkClient()
{
    int ret;
    unsigned int len;
    socklen_t sockLen = sizeof(struct sockaddr_in);

    struct sockaddr_in  client;
    struct ComputerInfo clientInfo;

    memset(&client, 0,  sizeof(struct sockaddr_in));

    int clientSock = accept(socketHandle, (struct sockaddr*)&client, &sockLen);
    PERROR(clientSock, "Accept client");

    len = ret = netRecv(clientSock, header, &buf_tmp);
    PERROR(ret, "Linkup client");

    if (header.flag != LINKUP) {
	ret = close(clientSock);
	PERROR(ret, "Close disqualified linkup");
	return -1;
    }

    clientInfo.deserialize(buf_tmp);
    log("DS:server", "Got aquired:%d by %s:%d", ntohs(getSockPort(clientSock)), clientInfo.getName(), ntohs(client.sin_port));
    free(buf_tmp);

    header.flag = RENTED;
    len = computer.serialize(buf_data);
    ret = netSend(clientSock, header, buf_data, len);
    PERROR(ret, "Reply Rented");

    return clientSock;
}

void ServerService::supportClient()
{
    log("DS:server", "Supporting client");
    handleService();
}

void ServerService::unlinkClient()
{
    log("DS:server", "Client finished");
    closeConnection(clientSock);
}

ssize_t ServerService::networkSend(unsigned char *buf, size_t size)
{
    if (clientSock == -1) {
	errno = EBADF;
	return -1;
    }

    struct DSHeader header;
    header.flag = DATA;
    header.len  = size;

    return netSend(clientSock, header, buf, size);
}

ssize_t ServerService::networkRecv(unsigned char **buf)
{
    if (clientSock == -1) {
	errno = EBADF;
	return -1;
    }

    ssize_t len;
    struct DSHeader header;

    while (true) {
	len = netRecv(clientSock, header, buf);
	if (len <= 0 || header.flag == DATA)
	    break;
    }

    return len;
}

void ServerService::publish(void (*service)(void))
{
    handleService = service;
    int sockBCast = createConnection(SOCKET_BCAST_PORT, SOCKET_UDP);
    awaitClient(sockBCast);
    closeConnection(sockBCast);
}
