#include "DistributionService.hpp"
#include "ClientService.hpp"
#include "ServerService.hpp"
#include "Serializer.hpp"

#include <vector>
#include <iostream>
#include <cstring>

static ServerService *ss;

static void service(void)
{
    while (true) {
	unsigned char *buf;
	ssize_t ret = ss->networkRecv(&buf);
	PERROR(ret, "Retrieve");
	if (ret == 0) {
	    ss->halt();
	    free(buf);
	    return;
	}

	char *msg = (char*)buf;
	std::cout << "Got a message of " << ret << " bytes that contain '" <<  msg << "'." << std::endl;

	ret = ss->networkRecv(&buf);
	PERROR(ret, "Retrieve");
	if (ret == 0) {
	    ss->halt();
	    free(buf);
	    return;
	}

	float value;
	Serializer::deserialize(buf, "f", &value);
	std::cout << "Got a float with value " << value << std::endl;

	ret = ss->networkSend((unsigned char*)"ACK", 3);
	PERROR(ret, "Acking the ret");

	free(buf);
    }
}

int main(int argc, char **argv)
{
    if (argc != 2) {
	std::cout << argv[0] << " <client|server>" << std::endl;
	return EXIT_SUCCESS;
    }

    if (strcmp(argv[1], "-client") == 0) {
	ClientService *cs = new ClientService();

	std::vector<struct ComputerInfo> servers = cs->getServers();
	if (servers.size() == 0) {
	    delete cs;
	    return EXIT_SUCCESS;
	}

	ComputerInfo self;
	int select = 0;
	int s = 0;
	for (std::vector<struct ComputerInfo>::iterator it = servers.begin(); it != servers.end(); it++, s++)
	    if (strcmp(self.getName(), it->getName()))
		select = s;

	cs->aquire(select);

	const char *msg = "This is an automatically configured transmission example message to provide services at a simple level.";
	std::cout << "Sent a message of " << strlen(msg) << " bytes that contain '" <<  msg << "'." << std::endl;
	ssize_t ret = cs->networkSend(select, (unsigned char*)msg, strlen(msg) + 1);
	PERROR(ret, "tramsission error");

	unsigned char value[10];
	float val = 1337.42;
	ret = Serializer::serialize(value, "f", val);
	std::cout << "Sent a float with value " << val << std::endl;
	ret = cs->networkSend(select, value, ret);

	unsigned char *buf;
	ret = cs->networkRecv(select, &buf);
	PERROR(ret, "Ackking");

	std::cout << "Got a " << buf << std::endl;

	free(buf);

	cs->release(select);
	delete cs;

    } else {
	ss = new ServerService();
	ss->publish(&service);
	delete ss;
    }

    return EXIT_SUCCESS;
}
