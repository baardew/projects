#ifndef SERVERSERVICE_H
#define SERVERSERVICE_H

#include "DistributionService.hpp"

class ServerService : public DistributionService
{
private:
    static const int MAX_CLIENTS_QUEUE = 1;

public:
    ServerService();
    ~ServerService();

    void    publish(void (*service)(void));
    void    halt();
    ssize_t networkRecv(unsigned char **buf);
    ssize_t networkSend(unsigned char *buf, size_t size);

private:
    void awaitClient(int sockBCast);
    int	 linkClient();
    void supportClient();
    void unlinkClient();

    void (*handleService)(void);
    bool waitForClients;
    int socketHandle;
    int clientSock;
};

#endif
