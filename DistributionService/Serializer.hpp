#ifndef SERIALIZER_H
#define SERIALIZER_H

#include <cstdarg>
#include <cstring>

/**
 * Only support 64-bit.
 *
 * fmt = (bytes) <arguments>
 * c = (1) <char>
 * h = (2) <short>
 * i = (4) <int>
 * f = (4) <float>
 * l = (8) <long>
 * d = (8) <double>
 * s = (variable) <char*> <int> (String with length. Null-terminator '\0' must be included explicitly if required)
 */

class Serializer
{
public:
    static size_t bytesize(const char *fmt, ...);
    static size_t serialize(unsigned char *buf, const char *fmt, ...);
    static size_t deserialize(unsigned char *buf, const char *fmt, ...);
};

#endif
