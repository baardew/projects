#ifndef CLIENTSERVICE_H
#define CLIENTSERVICE_H

#include "DistributionService.hpp"

#include <ifaddrs.h>
#include <net/if.h>

#include <vector>

class ClientService : public DistributionService
{
private:
    struct ServerInfo {
	int socket;
	socklen_t len;
	struct ComputerInfo server;
	struct sockaddr_in addr;
    };

public:
    ClientService();
    ~ClientService();

    void mapServers();
    void aquire(int serverID);
    void release(int serverID);
    ssize_t networkRecv(int serverID, unsigned char **buf);
    ssize_t networkSend(int serverID, unsigned char *buf, size_t size);
    std::vector<struct ComputerInfo> getServers();

private:
    void requestServers();
    void addServer(ComputerInfo server, struct sockaddr_in addr, socklen_t len);

    std::vector<struct ServerInfo> servers;
    int sockBCast;
};

#endif
