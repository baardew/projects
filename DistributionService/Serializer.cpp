#include "Serializer.hpp"

#include <cstring>
#include <cstdio>

size_t Serializer::serialize(unsigned char *buf, const char *fmt, ...)
{
    int strlen;
    char *src;
    size_t len = 0;
    va_list args;
    unsigned long i;
    double d;
    float s;
    const char *f;

    va_start(args, fmt);
    for (f = fmt; *f != '\0'; f++) {
        switch (*f) {
        case 'c':
            buf[len++] = va_arg(args, int);
            break;
        case 'h':
            i = va_arg(args, unsigned long);
            memcpy(buf + len, (unsigned char*)&i, 2);
            len += 2;
            break;
        case 'i':
            i = va_arg(args, unsigned long);
            memcpy(buf + len, (unsigned char*)&i, 4);
            len += 4;
            break;
        case 'l':
            i = va_arg(args, unsigned long);
            memcpy(buf + len, (unsigned char*)&i, 8);
            len += 8;
            break;
        case 'f':
            s = va_arg(args, double);
            memcpy(buf + len, (unsigned char*)&s, 4);
            len += 4;
            break;
        case 'd':
            d = va_arg(args, double);
            memcpy(buf + len, (unsigned char*)&d, 8);
            len += 8;
            break;
        case 's':
            src    = va_arg(args, char*);
            strlen = va_arg(args, int);
            memcpy(buf + len, src, strlen);
            len += strlen;
            break;
        default:
            fprintf(stderr, "Serializer::serialize error: Unknwon parameter name %c.\n", *f);
            break;
        }
    }
    va_end(args);

    return len;
}

size_t Serializer::deserialize(unsigned char *buf, const char *fmt, ...)
{
    int strlen;
    char *src;
    size_t len = 0;
    va_list args;
    unsigned int *i;
    unsigned short *h;
    unsigned long *l;
    double *d;
    float *s;
    const char *f;

    va_start(args, fmt);
    for (f = fmt; *f != '\0'; f++) {
        switch (*f) {
        case 'c':
            *(va_arg(args, char*)) = buf[len++];
            break;
        case 'h':
            h = va_arg(args, unsigned short*);
            memcpy((unsigned char*)h, buf + len, 2);
            len += 2;
            break;
        case 'i':
            i = va_arg(args, unsigned int*);
            memcpy((unsigned char*)i, buf + len, 4);
            len += 4;
            break;
        case 'l':
            l = va_arg(args, unsigned long*);
            memcpy((unsigned char*)l, buf + len, 8);
            len += 8;
            break;
        case 'f':
            s = va_arg(args, float*);
            memcpy((unsigned char*)s, buf + len, 4);
            len += 4;
            break;
        case 'd':
            d = va_arg(args, double*);
            memcpy((unsigned char*)d, buf + len, 8);
            len += 8;
            break;
        case 's':
            src    = va_arg(args, char*);
            strlen = va_arg(args, int);
            memcpy(src, buf + len, strlen);
            len += strlen;
            break;
        default:
            fprintf(stderr, "Serializer::deserialize error: Unknwon parameter name %c.\n", *f);
            break;
        }
    }
    va_end(args);

    return len;
}

size_t Serializer::bytesize(const char *fmt, ...)
{
    int strlen;
    size_t len = 0;
    va_list args;
    const char *f;

    va_start(args, fmt);
    for (f = fmt; *f != '\0'; f++) {
        switch (*f) {
        case 'c':
            len += 1;
            break;
        case 'h':
            len += 2;
            break;
        case 'i':
        case 'f':
            len += 4;
            break;
        case 'l':
        case 'd':
            len += 8;
            break;
        case 's':
            strlen = va_arg(args, int);
            len += strlen;
            break;
        default:
            fprintf(stderr, "Serializer::bytesize error: Unknwon parameter name %c.\n", *f);
            break;
        }
    }
    va_end(args);

    return len;
}
