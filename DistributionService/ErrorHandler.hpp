#ifndef ERRORHANDLER_H
#define ERRORHANDLER_H

#include <stdio.h>
#include <stdarg.h>

#define PERROR(err, msg) {						\
	if (err == -1) {						\
	    char format[256];						\
	    sprintf(format, "%s:%d %s", __FILE__, __LINE__, msg);	\
	    perror(format);						\
	    exit(EXIT_FAILURE);						\
	}								\
    }

#define LOG(loc, fmt, argv) {			\
	fprintf(stderr, "[%s]  ", loc);		\
	vfprintf(stderr, fmt, argv);		\
	fprintf(stderr, "\n");			\
    }

#ifdef DS_LOG
#undef DS_LOG
#define DS_LOG 1
#else
#define DS_LOG 0
#endif

#endif
