#ifndef DISTRIBUTIONSERVICE_H
#define DISTRIBUTIONSERVICE_H

#include "ErrorHandler.hpp"
#include "ComputerInfo.hpp"
#include "Serializer.hpp"

#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include <cstring>
#include <iostream>
#include <cstdlib>
#include <cstdio>

class DistributionService
{
protected:
    static const size_t BUF_SIZE = 128;
    static const int SOCKET_BCAST_PORT = 4545;
    static const unsigned int LOCALHOST_IPV4 = 2130706433; // 127.0.0.1 in actual value
    
    enum SocketMode {
	SOCKET_TCP   = 0,
	SOCKET_UDP   = 1,
	SOCKET_BCAST = 2,
    };

    enum DSFlag {
	// COMMAND:
	IS_COMMAND = 0xC0,
	REQUEST    = 0xC1,
	REPLY      = 0xC2,
	AQUIRE     = 0xC3,
	LINKUP     = 0xC4,
	RENTED     = 0xC5,
	UNLINK     = 0xC6,

	// DATA:
	IS_DATA    = 0xD0,
	DATA       = 0xDD,
    };

    struct DSHeader {
	DSFlag flag; // 4B
	size_t len;  // 4B
    };

public:
    DistributionService();
    ~DistributionService();
    
protected:
    int      getSockPort(int sock);
    int      createConnection(int port, SocketMode mode);
    void     closeConnection(int sock);
    size_t   sizeofDSHeader();
    ssize_t  netSend(int sock, DSHeader &header, unsigned char *buf, size_t size);
    ssize_t  netRecv(int sock, DSHeader &header, unsigned char **buf);
    
    void log(const char *loc, const char *fmt, ...);

    unsigned int packBuffer(unsigned char *buf, DSHeader &header,
			    unsigned char *data, unsigned int len);
    unsigned int unpackBuffer(unsigned char *data, DSHeader &header,
			      unsigned char *buf, unsigned int len);

    struct ComputerInfo computer;
    struct DSHeader header;
    unsigned char buf_data[BUF_SIZE];
    unsigned char buf_net[BUF_SIZE];
    unsigned char *buf_tmp;
};

#endif
