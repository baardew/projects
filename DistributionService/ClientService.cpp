#include "ClientService.hpp"

ClientService::ClientService()
{
    int ret;

    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;

    sockBCast = createConnection(0, SOCKET_BCAST);

    ret = setsockopt(sockBCast, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));
    PERROR(ret, "Servers request timeout");

    requestServers();
    mapServers();
}

ClientService::~ClientService()
{
    closeConnection(sockBCast);
}

void ClientService::aquire(int serverID)
{
    int ret;
    struct ServerInfo *server = &servers[serverID];

    server->socket = createConnection(0, SOCKET_TCP);
    ret = connect(server->socket, (struct sockaddr*)&server->addr, server->len);

    if (ret == -1) {
	if (errno != ECONNREFUSED && errno != ETIMEDOUT) {
	    PERROR(ret, "Connect to server");
	} else {
	    log("DS:client", "Cannot connect to %s:%d", server->server.getName(), ntohs(server->addr.sin_port));
	    return;
	}
    }

    // UDP notification
    int serverTCP = server->addr.sin_port;
    server->addr.sin_port = htons(SOCKET_BCAST_PORT);
    header.flag = AQUIRE;
    header.len  = 0;

    unsigned int len = packBuffer(buf_net, header, NULL, 0);
    ret = sendto(sockBCast, buf_net, len, 0, (struct sockaddr*)&server->addr, server->len);
    PERROR(ret, "Notify connection");

    // TCP connection
    server->addr.sin_port = serverTCP;
    len = computer.serialize(buf_data);
    header.flag = LINKUP;
    header.len  = len;

    ret = netSend(server->socket, header, buf_data, len);
    PERROR(ret, "Linkup server");

    while (true) {
	ret = netRecv(server->socket, header, &buf_tmp);
	PERROR(ret, "Linkup accept");
	free(buf_tmp);
	
	if (header.flag == RENTED)
	    break;
    }

    log("DS:client", "Aquired:%d server %s:%d", ntohs(getSockPort(server->socket)), server->server.getName(), ntohs(server->addr.sin_port));
}

void ClientService::release(int serverID)
{
    struct ServerInfo *server = &servers[serverID];
    header.flag = UNLINK;
    header.len  = 0;

    int ret = netSend(server->socket, header, NULL, 0);
    PERROR(ret, "Unlink server");

    log("DS:client", "Unlinked server %s:%d", server->server.getName(), ntohs(server->addr.sin_port));
    
    closeConnection(server->socket);
    server->socket = -1;
}

std::vector<struct ComputerInfo> ClientService::getServers()
{
    std::vector<struct ComputerInfo> ser;

    for (std::vector<ServerInfo>::iterator it = servers.begin(); it != servers.end(); it++)
	ser.push_back(it->server);

    return ser;
}

void ClientService::requestServers()
{
    int len, ret;
    bool needLocal = true;

    struct sockaddr_in sendAddr;
    struct ifaddrs *ifa, *ifa_root;

    memset(&sendAddr, 0, sizeof(struct sockaddr_in));
    sendAddr.sin_family = AF_INET;
    sendAddr.sin_port   = htons(SOCKET_BCAST_PORT);
    sendAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    len = computer.serialize(buf_data);
    header.flag = REQUEST;
    header.len  = len;
    len = packBuffer(buf_net, header, buf_data, len);

    ret = getifaddrs(&ifa);
    PERROR(ret, "Get BCast interfaces");
    ifa_root = ifa;

    log("DS:client", "Requesting servers");
    while (ifa != NULL) {
	if (ifa->ifa_addr == NULL)
	    continue;

	if (ifa->ifa_flags & IFF_UP
	    && ifa->ifa_flags & IFF_BROADCAST
	    && ifa->ifa_addr->sa_family == AF_INET)
	{
	    needLocal = false;
	    sendAddr.sin_addr.s_addr = ((struct sockaddr_in*)ifa->ifa_broadaddr)->sin_addr.s_addr;
	    ret = sendto(sockBCast, buf_net, len, 0, (struct sockaddr*)&sendAddr, sizeof(struct sockaddr_in));
	    PERROR(ret, "Request servers");
	}

	ifa = ifa->ifa_next;
    }

    if (needLocal) {
	sendAddr.sin_addr.s_addr = htonl(LOCALHOST_IPV4);
	ret = sendto(sockBCast, buf_net, len, 0, (struct sockaddr*)&sendAddr, sizeof(struct sockaddr_in));
	PERROR(ret, "Request servers");
    }

    freeifaddrs(ifa_root);
}

void ClientService::addServer(ComputerInfo server, struct sockaddr_in addr, socklen_t len)
{
    ServerInfo si;
    si.socket = -1;
    si.len = len;
    si.server = server;
    si.addr = addr;

    for (std::vector<struct ServerInfo>::iterator it = servers.begin(); it != servers.end(); it++) {
	if (strncmp(it->server.getName(), si.server.getName(), MAX_NAME_LENGTH) == 0
	    && addr.sin_port == it->addr.sin_port
	    && addr.sin_addr.s_addr == it->addr.sin_addr.s_addr)
	{
	    return;
	}
    }

    servers.push_back(si);
}

void ClientService::mapServers()
{
    int len, ret;
    socklen_t recvLen = sizeof(struct sockaddr_in);

    struct sockaddr_in  recvInfo;
    struct ComputerInfo server;

    while (true) {
	ret = recvfrom(sockBCast, buf_net, BUF_SIZE, 0, (struct sockaddr*)&recvInfo, &recvLen);
	if (ret <= 0)
	    break;

	len = ret;
	len = unpackBuffer(buf_data, header, buf_net, len);

	if (header.flag != REPLY)
	    continue;

	unsigned int off = server.deserialize(buf_data);
	off += Serializer::deserialize(buf_data + off, "h", &recvInfo.sin_port);

	addServer(server, recvInfo, recvLen);
	log("DS:client", "\tGot reply: %s:%d, cores = %d, memory = %d, CUDA = %s",
	    server.getName(), ntohs(recvInfo.sin_port), server.getCores(), server.getMemory(), server.hasCuda() ? "Yes" : "No");
    }

    log("DS:client", "\tServers discovered: %d", servers.size());
}

ssize_t ClientService::networkRecv(int serverID, unsigned char **buf)
{
    if (servers[serverID].socket == -1) {
	errno = EBADF;
	return -1;
    }

    ssize_t len;
    struct DSHeader header;

    while (true) {
	len = netRecv(servers[serverID].socket, header, buf);
	if (len <= 0 || header.flag == DATA)
	    break;
    }

    return len;
}

ssize_t ClientService::networkSend(int serverID, unsigned char *buf, size_t size)
{
    if (servers[serverID].socket == -1) {
	errno = EBADF;
	return -1;
    }

    struct DSHeader header;
    header.flag = DATA;
    header.len  = size;

    return netSend(servers[serverID].socket, header, buf, size);
}
