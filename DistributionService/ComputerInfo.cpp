#include "ComputerInfo.hpp"

#include <stdio.h>

ComputerInfo::ComputerInfo()
{
    gethostname(d.name, MAX_NAME_LENGTH);
    d.cores  = (int)sysconf(_SC_NPROCESSORS_ONLN);
    d.memory = (int)round(((double)getMemoryAmount() / 1000000000) - 0.1); //-0.1 for precision loss
#ifdef HAS_CUDA
    d.cuda = true;
#else
    d.cuda = false;
#endif
}

size_t ComputerInfo::getMemoryAmount()
{
#if defined(_SC_PHYS_PAGES)
  return (size_t)sysconf(_SC_PHYS_PAGES) * (size_t)sysconf(_SC_PAGE_SIZE);
#else
  int mib[2];
  mib[0] = CTL_HW;
  mib[1] = HW_MEMSIZE;

  int64_t size = 0;
  size_t len = sizeof(size);
  if (sysctl(mib, 2, &size, &len, NULL, 0) == 0)
    return (size_t)size;
  return 0L;
#endif
}

int ComputerInfo::getCores()
{
    return d.cores;
}

int ComputerInfo::getMemory()
{
    return d.memory;
}

bool ComputerInfo::hasCuda()
{
    return d.cuda;
}

char* ComputerInfo::getName()
{
    return d.name;
}

unsigned int ComputerInfo::serialize(unsigned char *buf)
{
    return Serializer::serialize(buf, "iics", d.cores, d.memory,
				 d.cuda, d.name, MAX_NAME_LENGTH);
}

unsigned int ComputerInfo::deserialize(unsigned char *buf)
{
    return Serializer::deserialize(buf, "iics", &d.cores, &d.memory,
				   &d.cuda, d.name, MAX_NAME_LENGTH);
}
