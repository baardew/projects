#include "DistributionService.hpp"

DistributionService::DistributionService()
{

}

DistributionService::~DistributionService()
{

}

void DistributionService::log(const char *loc, const char *fmt, ...)
{
    if (DS_LOG) {
	va_list argv;
	va_start(argv, fmt);
	LOG(loc, fmt, argv);
	va_end(argv);
    }
}

int DistributionService::getSockPort(int sock)
{
    struct sockaddr_in selfAddr;
    socklen_t selfLen = sizeof(struct sockaddr_in);
    
    int ret = getsockname(sock, (struct sockaddr*)&selfAddr, &selfLen);
    PERROR(ret, "Get socket port");

    return selfAddr.sin_port;
}

int DistributionService::createConnection(int port, SocketMode mode)
{
    int ret;
    int type = -1;
    int protocol = -1;
    int sock = -1;
    int broadcastEnable = 1;
    struct sockaddr_in socketInfo;
    memset(&socketInfo, 0, sizeof(struct sockaddr_in));

    socketInfo.sin_family      = AF_INET;
    socketInfo.sin_port        = htons(port);
    socketInfo.sin_addr.s_addr = htonl(INADDR_ANY);

    switch (mode) {
    case SOCKET_TCP:
        type     = SOCK_STREAM;
        protocol = IPPROTO_TCP;
        break;
    case SOCKET_UDP:
    case SOCKET_BCAST:
        type     = SOCK_DGRAM;
        protocol = IPPROTO_UDP;
        break;
    default:
        fprintf(stderr, "Socket mode undefined");
	break;
    }

    sock = socket(AF_INET, type, protocol);
    PERROR(sock, "Socket creation");

    ret = bind(sock, (struct sockaddr*)&socketInfo, sizeof(struct sockaddr_in));
    PERROR(ret, "Socket bind");

    if (mode == SOCKET_BCAST) {
	ret = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(int));
	PERROR(ret, "Socket BCast enable");
    }
    
    /* LOG START */
    std::string modename;
    int selfPort = getSockPort(sock);

    switch (mode) {
    case SOCKET_TCP:
	modename = "TCP";
	break;
    case SOCKET_UDP:
	modename = "UDP";
	break;
    case SOCKET_BCAST:
	modename = "BCAST";
	break;
    default:
	modename = "<UNKNOWN>";
	break;
    }
    
    log("DS:common", "Created socked %s:%d", modename.c_str(), ntohs(selfPort));
    /* LOG END */
    
    return sock;
}

void DistributionService::closeConnection(int sock)
{
    int port = getSockPort(sock);
    int ret = close(sock);
    PERROR(ret, "Socket close");

    log("DS:common", "Closed socket:%d", ntohs(port));
}


unsigned int DistributionService::packBuffer(unsigned char *buf, DSHeader &header,
					     unsigned char *data, unsigned int len)
{
    int hLen = Serializer::serialize(buf, "il", header.flag, header.len);
    if (len > 0 && data != NULL)
	memcpy(buf + hLen, data, len);
    return len + hLen;
}

unsigned int DistributionService::unpackBuffer(unsigned char *data, DSHeader &header,
					       unsigned char *buf, unsigned int len)
{
    header.len = 0;
    int hLen = Serializer::deserialize(buf, "il", &header.flag, &header.len);
    int actualLen = len - hLen;
    if (actualLen > 0 && buf != NULL)
	memcpy(data, buf + hLen, actualLen);
    return actualLen;
}

size_t DistributionService::sizeofDSHeader()
{
    return Serializer::bytesize("il");
}

ssize_t DistributionService::netSend(int sock, DSHeader &header, unsigned char *buf, size_t size)
{
    unsigned char *buffer = (unsigned char*)malloc(size + sizeofDSHeader());
    size_t len = packBuffer(buffer, header, buf, size);

    ssize_t ret;
    ssize_t sent = 0;
    while ((size_t)sent < len) {
	ret = send(sock, buffer + sent, len - sent, 0);
	if (ret <= 0) {
	    free(buffer);
	    return ret;
	}

	sent += ret;
    }
    
    log("DS:common", "Sent:%d %ld bytes", ntohs(getSockPort(sock)), sent);
    free(buffer);
    return size;
}

ssize_t DistributionService::netRecv(int sock, DSHeader &header, unsigned char **buf)
{
    ssize_t ret;
    ret = recv(sock, buf_data, BUF_SIZE, MSG_PEEK);
    if (ret <= 0)
	return ret;

    if ((size_t)ret < sizeofDSHeader())
	return 0;
    
    unpackBuffer(NULL, header, buf_data, 0);

    unsigned char *buffer = (unsigned char*)malloc(sizeofDSHeader() + header.len);
    *buf = (unsigned char*)malloc(header.len);

    size_t total = header.len + sizeofDSHeader();
    ssize_t rec = 0;
    while ((size_t)rec < total) {
	ret = recv(sock, buffer + rec, total - rec, 0);
	if (ret <= 0) {
	    free(buffer);
	    return ret;
	}

	rec += ret;
    }
    
    ret = unpackBuffer(*buf, header, buffer, rec);
    log("DS:common", "Received:%d %d bytes", ntohs(getSockPort(sock)), rec);
    free(buffer);
    return ret;
}
