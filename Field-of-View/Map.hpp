#ifndef MAP_TILE_H
#define MAP_TILE_H

#include "arraymemory.hpp"

class Tile
{
public:
    Tile() : wall(false), visible(false) {};
    
    bool wall;
    bool visible;
};

class Map
{
public:
    Map(int x, int y, char **map, char **shader);
    ~Map();
    
    Tile get_tile(int x, int y);
    void reset_visibility();
    void set_visible(int x, int y);
    bool is_visible(int x, int y);
    bool is_transparent(int x, int y);

private:
    int width;
    int height;

    Tile **tiles;
    char **shader;
    char **map;

    char *prev;
    char value;
};

#endif
