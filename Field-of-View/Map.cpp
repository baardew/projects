#include "Map.hpp"

#include <stdio.h>

Map::Map(int x, int y, char **map, char **shader) :
    width(x),
    height(y),
    shader(shader),
    map(map),
    prev(NULL)
{
    tiles = (Tile**)allocate_memory_2d(sizeof(Tile), x, y);

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            tiles[i][j];
            tiles[i][j].wall = map[i][j] == 64;
        }
    }
}

Map::~Map()
{
    free_memory((void*)tiles, 2);
}

Tile Map::get_tile(int x, int y)
{
    return tiles[y][x];
}

void Map::reset_visibility()
{
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            tiles[i][j].visible = false;
            shader[i][j] = 0;
        }
    }
}

void Map::set_visible(int x, int y)
{
    tiles[y][x].visible = true;
    shader[y][x] = 1;

    return;

    if (prev != NULL)
        *prev = value;

    prev = &(map[y][x]);
    value = map[y][x];

    map[y][x] = 'T';
}

bool Map::is_visible(int x, int y)
{
    if (x < 0 || y < 0 || x >= width || y >= height)
        return false;

    return tiles[y][x].visible;
}

bool Map::is_transparent(int x, int y)
{
    if (x < 0 || y < 0 || x >= width || y >= height)
        return false;

    return !tiles[y][x].wall;
}
