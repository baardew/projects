#ifndef MRPAS_H
#define MRPAS_H

#include "Map.hpp"
#include "IndexMemory.hpp"

// HACK
#ifndef PLAYER_T
#define PLAYER_T
typedef struct player {
    int x;
    int y;
} t_player;
#endif

class MRPAS
{
public:
    MRPAS(int width, int height);
    ~MRPAS();

    void computeFoV(Map &map, t_player player, int radius);

    enum Direction {
        Vertical,
        Horizontal
    };

private:

    // Per object
    int width;
    int height;

    void quadrant();
    void vertical();
    void horizontal();

    bool inRange();
    bool visibility(bool transparentPosition, float startSlope, float centerSlope, float endSlope);
    bool slope(Direction direction);

    // Per session
    struct ComputeSession {
        int dx;
        int dy;
        int ddx;
        int ddy;
        int maxRadius;
        t_player position;
        Map *map;
        IndexMemory startAngle;
        IndexMemory endAngle;
        float x;
        float y;
        int iteration;
        int totalObstacles;
        int obstaclesInLastLine;
        float minAngle;
    };

    ComputeSession session;
};

#endif
