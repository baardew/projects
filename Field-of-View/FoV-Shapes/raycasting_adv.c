/*

  PSEDUO:

  1. Find all eucludian points
  2. Check line between point and all distance points to eucludian
  3. For every possible box that line can hit, test if it hits
  4. Test hit by checking if any of the edges of the box intersects with line.

*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#define LARGE

#ifdef LARGE
int width = 25;
int height = 25;
int R = 7;

char shader[25][25] = {0};
char map[25][25] = {
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0, 64, 64, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0,  0, 64, 64,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0, 64, 64,  0,  0,  0,  0,  0, 64,  0, 64, 64, 64, 64,  0,  0,  0, 64,  0,  0,  0, 64,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0, 64,  0,  0,  0,  0, 64,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0, 64, 64, 64,  0,  0},
    {0,  0, 64, 64, 64,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0, 64,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0, 64,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64, 64, 64,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
};

#else
int width = 3;
int height = 3;
int R = 2;

char shader[3][3] = {0};
char map[3][3] = {
    {0,  0, 64},
    {0,  0, 64},
    {0,  0, 64},
};

#endif

typedef struct player {
    int x;
    int y;
} t_player;

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)

// Source:: http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
typedef struct Point
{
    float x;
    float y;
} Point;

int doIntersect(Point A, Point B, Point C, Point D)
{
    Point CmP;
    CmP.x = C.x - A.x;
    CmP.y = C.y - A.y;
    
    Point r;
    r.x = B.x - A.x;
    r.y = B.y - A.y;
    
    Point s;
    s.x = D.x - C.x;
    s.y = D.y - C.y;
 
    float CmPxr = CmP.x * r.y - CmP.y * r.x;
    float CmPxs = CmP.x * s.y - CmP.y * s.x;
    float rxs = r.x * s.y - r.y * s.x;
    
    if (CmPxr == 0.0f)
    {
	// Lines are collinear, and so intersect if they have any overlap
	
	return ((C.x - A.x < 0.0f) != (C.x - B.x < 0.0f))
	    || ((C.y - A.y < 0.0f) != (C.y - B.y < 0.0f));
    }
    
    if (rxs == 0.0f)
	return 0; // Lines are parallel.
    
    float rxsr = 1.0f / rxs;
    float t = CmPxs * rxsr;
    float u = CmPxr * rxsr;
    
    return (t >= 0.0f) && (t <= 1.0f) && (u >= 0.0f) && (u <= 1.0f);
}

// 4 Check if two lines (obstacle and ray) intersect. Use code found elsewhere
int ray_intersect_line(float x1, float y1, float x2, float y2, float startX, float endX, float startY, float endY)
{
    Point p1, q1, p2, q2;
    p1.x = x1;
    p1.y = y1;

    q1.x = x2;
    q1.y = y2;

    p2.x = startX;
    p2.y = startY;

    q2.x = endX;
    q2.y = endY;

    int intersect = doIntersect(p1, q1, p2, q2);

#ifndef LARGE
    printf("[(%f, %f) -> (%f, %f)] >-< [(%f, %f) -> (%f, %f)] == %d\n", x1, y1, x2, y2, startX, startY, endX, endY, intersect);
#endif
    return intersect;
}

float eucludianDistance_float(Point p1, Point p2)
{
    float diffx = p1.x - p2.x;
    float diffy = p1.y - p2.y;
    if (diffx < 0)
	diffx *= -1;
    if (diffy < 0)
	diffy *= -1;

    return (float)sqrt(((diffx * diffx) + (diffy * diffy)));
}

typedef struct min_vert {
    Point p;
    float euc;
} t_mtx;

void find_min_vertex(t_mtx **f, t_mtx **t, t_mtx *v1, t_mtx *v2, t_mtx *v3, t_mtx *v4)
{
    if ((*f)->euc > v1->euc && (*t) != v1) {
#ifndef LARGE
	printf("MIN-F-1: (%f, %f)=%f < (%f, %f)=%f\n", (*f)->p.x, (*f)->p.y, (*f)->euc, v1->p.x, v1->p.y, v1->euc);
#endif
	*f = v1;
    } if ((*f)->euc > v2->euc && (*t) != v2) {
#ifndef LARGE
	printf("MIN-F-2: (%f, %f)=%f < (%f, %f)=%f\n", (*f)->p.x, (*f)->p.y, (*f)->euc, v2->p.x, v2->p.y, v2->euc);
#endif
	*f = v2;
    } if ((*f)->euc > v3->euc && (*t) != v3) {
#ifndef LARGE
	printf("MIN-F-3: (%f, %f)=%f < (%f, %f)=%f\n", (*f)->p.x, (*f)->p.y, (*f)->euc, v3->p.x, v3->p.y, v3->euc);
#endif
	*f = v3;
    } if ((*f)->euc > v4->euc && (*t) != v4) {
#ifndef LARGE
	printf("MIN-F-4: (%f, %f)=%f < (%f, %f)=%f\n", (*f)->p.x, (*f)->p.y, (*f)->euc, v4->p.x, v4->p.y, v4->euc);
#endif
	*f = v4;
    }

    if ((*t)->euc > v1->euc && (*f) != v1) {
#ifndef LARGE
	printf("MIN-T-1: (%f, %f)=%f < (%f, %f)=%f\n", (*t)->p.x, (*t)->p.y, (*t)->euc, v1->p.x, v1->p.y, v1->euc);
#endif
	*t = v1;
    } if ((*t)->euc > v2->euc && (*f) != v2) {
#ifndef LARGE
	printf("MIN-T-2: (%f, %f)=%f < (%f, %f)=%f\n", (*t)->p.x, (*t)->p.y, (*t)->euc, v2->p.x, v2->p.y, v2->euc);
#endif
	*t = v2;
    } if ((*t)->euc > v3->euc && (*f) != v3) {
#ifndef LARGE
	printf("MIN-T-3: (%f, %f)=%f < (%f, %f)=%f\n", (*t)->p.x, (*t)->p.y, (*t)->euc, v3->p.x, v3->p.y, v3->euc);
#endif
	*t = v3;
    } if ((*t)->euc > v4->euc && (*f) != v4) {
#ifndef LARGE
	printf("MIN-T-4: (%f, %f)=%f < (%f, %f)=%f\n", (*t)->p.x, (*t)->p.y, (*t)->euc, v4->p.x, v4->p.y, v4->euc);
#endif
	*t = v4;
    }

#ifndef LARGE
    printf("MIN's: (%f, %f)=%f and (%f, %f)=%f\n", (*f)->p.x, (*f)->p.y, (*f)->euc, (*t)->p.x, (*t)->p.y, (*t)->euc);
#endif
}


int ray_intersect_open(int x1, int y1, int x2, int y2, int ox, int oy)
{
    Point player, target, object;
    player.x = (float)x1 + .5f;
    player.y = (float)y1 + .5f;
    target.x = (float)x2 + .5f;
    target.y = (float)y2 + .5f;
    object.x = (float)ox + .5f;
    object.y = (float)oy + .5f;
    
    t_mtx v1;
    v1.p.x = object.x - .5f;
    v1.p.y = object.y - .5f;
    v1.euc = eucludianDistance_float(player, v1.p);

    t_mtx v2;
    v2.p.x = object.x - .5f;
    v2.p.y = object.y + .5f;
    v2.euc = eucludianDistance_float(player, v2.p);

    t_mtx v3;
    v3.p.x = object.x + .5f;
    v3.p.y = object.y - .5f;
    v3.euc = eucludianDistance_float(player, v3.p);

    t_mtx v4;
    v4.p.x = object.x + .5f;
    v4.p.y = object.y + .5f;
    v4.euc = eucludianDistance_float(player, v4.p);

    t_mtx *from = &v1, *to = &v2;

    find_min_vertex(&from, &to, &v1, &v2, &v3, &v4);
    return ray_intersect_line(player.x, player.y, target.x, target.y, from->p.x, to->p.x, from->p.y, to->p.y);
}

int ray_intersect_north(int x1, int y1, int x2, int y2, int ox, int oy)
{
    Point player, target, object;
    player.x = (float)x1 + .5f;
    player.y = (float)y1 + .5f;
    target.x = (float)x2 + .1f;
    target.y = (float)y2 + .1f;
    object.x = (float)ox + .5f;
    object.y = (float)oy + .5f;
    
    t_mtx v1;
    v1.p.x = object.x - .5f;
    v1.p.y = object.y - .5f;
    v1.euc = eucludianDistance_float(player, v1.p);

    t_mtx v2;
    v2.p.x = object.x - .5f;
    v2.p.y = object.y + .5f;
    v2.euc = eucludianDistance_float(player, v2.p);

    t_mtx v3;
    v3.p.x = object.x + .5f;
    v3.p.y = object.y - .5f;
    v3.euc = eucludianDistance_float(player, v3.p);

    t_mtx v4;
    v4.p.x = object.x + .5f;
    v4.p.y = object.y + .5f;
    v4.euc = eucludianDistance_float(player, v4.p);

    t_mtx *from = &v1, *to = &v2;

    find_min_vertex(&from, &to, &v1, &v2, &v3, &v4);
    return ray_intersect_line(player.x, player.y, target.x, target.y, from->p.x, to->p.x, from->p.y, to->p.y);
}

int ray_intersect_south(int x1, int y1, int x2, int y2, int ox, int oy)
{
    Point player, target, object;
    player.x = (float)x1 + .5f;
    player.y = (float)y1 + .5f;
    target.x = (float)x2 + .9f;
    target.y = (float)y2 + .9f;
    object.x = (float)ox + .5f;
    object.y = (float)oy + .5f;
    
    t_mtx v1;
    v1.p.x = object.x - .5f;
    v1.p.y = object.y - .5f;
    v1.euc = eucludianDistance_float(player, v1.p);

    t_mtx v2;
    v2.p.x = object.x - .5f;
    v2.p.y = object.y + .5f;
    v2.euc = eucludianDistance_float(player, v2.p);

    t_mtx v3;
    v3.p.x = object.x + .5f;
    v3.p.y = object.y - .5f;
    v3.euc = eucludianDistance_float(player, v3.p);

    t_mtx v4;
    v4.p.x = object.x + .5f;
    v4.p.y = object.y + .5f;
    v4.euc = eucludianDistance_float(player, v4.p);

    t_mtx *from = &v1, *to = &v2;

    find_min_vertex(&from, &to, &v1, &v2, &v3, &v4);
    return ray_intersect_line(player.x, player.y, target.x, target.y, from->p.x, to->p.x, from->p.y, to->p.y);
}

int ray_intersect_east(int x1, int y1, int x2, int y2, int ox, int oy)
{
    Point player, target, object;
    player.x = (float)x1 + .5f;
    player.y = (float)y1 + .5f;
    target.x = (float)x2 + .9f;
    target.y = (float)y2 + .1f;
    object.x = (float)ox + .5f;
    object.y = (float)oy + .5f;
    
    t_mtx v1;
    v1.p.x = object.x - .5f;
    v1.p.y = object.y - .5f;
    v1.euc = eucludianDistance_float(player, v1.p);

    t_mtx v2;
    v2.p.x = object.x - .5f;
    v2.p.y = object.y + .5f;
    v2.euc = eucludianDistance_float(player, v2.p);

    t_mtx v3;
    v3.p.x = object.x + .5f;
    v3.p.y = object.y - .5f;
    v3.euc = eucludianDistance_float(player, v3.p);

    t_mtx v4;
    v4.p.x = object.x + .5f;
    v4.p.y = object.y + .5f;
    v4.euc = eucludianDistance_float(player, v4.p);

    t_mtx *from = &v1, *to = &v2;

    find_min_vertex(&from, &to, &v1, &v2, &v3, &v4);
    return ray_intersect_line(player.x, player.y, target.x, target.y, from->p.x, to->p.x, from->p.y, to->p.y);
}



int ray_intersect_west(int x1, int y1, int x2, int y2, int ox, int oy)
{
    Point player, target, object;
    player.x = (float)x1 + .5f;
    player.y = (float)y1 + .5f;
    target.x = (float)x2 + .1f;
    target.y = (float)y2 + .9f;
    object.x = (float)ox + .5f;
    object.y = (float)oy + .5f;
    
    t_mtx v1;
    v1.p.x = object.x - .5f;
    v1.p.y = object.y - .5f;
    v1.euc = eucludianDistance_float(player, v1.p);

    t_mtx v2;
    v2.p.x = object.x - .5f;
    v2.p.y = object.y + .5f;
    v2.euc = eucludianDistance_float(player, v2.p);

    t_mtx v3;
    v3.p.x = object.x + .5f;
    v3.p.y = object.y - .5f;
    v3.euc = eucludianDistance_float(player, v3.p);

    t_mtx v4;
    v4.p.x = object.x + .5f;
    v4.p.y = object.y + .5f;
    v4.euc = eucludianDistance_float(player, v4.p);

    t_mtx *from = &v1, *to = &v2;

    find_min_vertex(&from, &to, &v1, &v2, &v3, &v4);
    return ray_intersect_line(player.x, player.y, target.x, target.y, from->p.x, to->p.x, from->p.y, to->p.y);
}


// 3. For every object, check if any of the edges intersect our line
int ray_intersect_obstacle(int x1, int y1, int x2, int y2, int ox, int oy)
{
#ifndef LARGE
    printf("\nCheck obstacle [(%d, %d) -> (%d, %d)] | (%d, %d)\n", x1, y1, x2, y2, ox, oy);
#endif


    // Now, we cannot take if ANY of them intersects, as it will eliminate too many obstacles.
    // Instead, we must check that only the intersections that is facing the player are the one to decide the fate.
    // We do this by finding the two cornes of the obstacle that is closest to the player's origin.


    // Set inital distance to something stupidly large: width*height; should always be larger than any value inside the map.
    // To be clever, only two test are needed: 4 cournes with two pre-initialized, just test if larger than that ones.


    
    // Must see center to see what is there
    if (map[y2][x2] == 64)
	return ray_intersect_open(x1, y1, x2, y2, ox, oy);
    
    // Must se any edge if wall is visible
    int intersect = 0;
    intersect |= ray_intersect_north(x1, y1, x2, y2, ox, oy);
    intersect |= ray_intersect_south(x1, y1, x2, y2, ox, oy);
    intersect |= ray_intersect_west(x1, y1, x2, y2, ox, oy);
    intersect |= ray_intersect_east(x1, y1, x2, y2, ox, oy);


    /*
    // There are four lines to cross-check: north, east, west and south.
    int intersects = 0;

    // North
    intersects = ray_intersect_line((float)x1, (float)y1, (float)x2, (float)y2, (float)ox - .5f, (float)ox + .5f, (float)oy - .5f, (float)oy - .5f);
    if (intersects)
    return 1;

    // East
    intersects = ray_intersect_line((float)x1, (float)y1, (float)x2, (float)y2, (float)ox + .5f, (float)ox + .5f, (float)oy - .5f, (float)oy + .5f);
    if (intersects)
    return 1;

    // West
    intersects = ray_intersect_line((float)x1, (float)y1, (float)x2, (float)y2, (float)ox - .5f, (float)ox - .5f, (float)oy - .5f, (float)oy + .5f);
    if (intersects)
    return 1;

    // South
    intersects = ray_intersect_line((float)x1, (float)y1, (float)x2, (float)y2, (float)ox - .5f, (float)ox + .5f, (float)oy + .5f, (float)oy - .5f);
    if (intersects)
    return 1;
    */

    return intersect;
}

// 2. A bit dirty, but try to intersect our line with any possible object in the trajectory
void raycast(t_player player, int x2, int y2)
{
    int startX = MIN(player.x, x2);
    int endX = MAX(player.x, x2);
    int startY = MIN(player.y, y2);
    int endY = MAX(player.y, y2);

    // Check all tiles inside the bounding box
    int y, x;
    for (y = startY; y <= endY; y++) {
	for (x = startX; x <= endX; x++) {

	    // We are testing if this is visible, so not include this
	    // What we care for is that nothing else is in the way
	    if (y == y2 && x == x2)
		continue;

	    // We do not test self either: we know we can see ourself!
	    if (y == player.y && x == player.x)
		continue;

	    if (ray_intersect_obstacle(player.x, player.y, x2, y2, x, y) && map[y][x] == 64)
		return;
	}
    }

    shader[y2][x2] = 1;
}

int eucludianDistance(t_player player, int tx, int ty)
{
    int diffx = player.x - tx;
    int diffy = player.y - ty;
    if (diffx < 0)
	diffx *= -1;
    if (diffy < 0)
	diffy *= -1;

    return sqrt(((diffx * diffx) + (diffy * diffy)));
}

void raytrace(t_player player)
{
    int x = player.x;
    int y = player.y;
    int checkY = MAX(y - R, 0);
    int endY   = MIN(y + R, height-1);
    int startX = MAX(x - R, 0);
    int endX   = MIN(x + R, width - 1);
    int checkX;

    int rx, ry; // Reset
    for (ry = 0; ry < height; ry++)
	for (rx = 0; rx < width; rx++)
	    shader[ry][rx] = 0;


    // 1. Check every square that can potentially be visible from player
    for (; checkY <= endY; checkY++)
	for (checkX = startX; checkX <= endX; checkX++)
	    if (eucludianDistance(player, checkX, checkY) <= R)
		raycast(player, checkX, checkY);

}

void renderDisplay(t_player player)
{
    int y, x;

    printf("+--");
    for (x = 1; x < width; x++)
	printf("--");
    printf("-+\n");

    for (y = 0; y < height; y++) {
	printf("| ");
	for (x = 0; x < width; x++) {
	    if (x == player.x && player.y == y) {
		printf("X ");
		continue;
	    }

	    if (!shader[y][x]) {
		printf("  ");
		continue;
	    }

	    switch (map[y][x]) {
	    case 0:
		printf(". ");
		break;
	    case 64:
		printf("@ ");
		break;
	    case 'T':
		printf("T ");
		break;
	    default:
		printf("? ");
		break;
	    }
	}

	printf("|\n");
    }

    printf("+--");
    for (x = 1; x < width; x++)
	printf("--");
    printf("-+\n");
}

void movePlayer(t_player *player, char keyPressed)
{
    switch (keyPressed) {
    case 'a':
	if (player->x - 1 < 0)
	    return;

	if (map[player->y][player->x-1] != 64)
	    player->x--;
	break;
    case 's':
	if (player->y + 1 >= height)
	    return;

	if (map[player->y+1][player->x] != 64)
	    player->y++;

	break;
    case 'd':
	if (player->x + 1 >= width)
	    return;

	if (map[player->y][player->x+1] != 64)
	    player->x++;
	break;
    case 'w':
	if (player->y - 1 < 0)
	    return;

	if (map[player->y-1][player->x] != 64)
	    player->y--;

	break;
	break;
    default:
	break;
    }
}

int main()
{
    t_player player;
    player.x = 0;
    player.y = 0;

    char keyPressed = 0;

    while (keyPressed != 'q') {
	raytrace(player);
	renderDisplay(player);

	keyPressed = getchar();

	movePlayer(&player, keyPressed);
    }


    return 0;
}
