#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int width = 25;
int height = 25;
int R = 7;

char shader[25][25] = {0};
char map[25][25] = {
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0, 64, 64, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0,  0, 64, 64,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0, 64, 64,  0,  0,  0,  0,  0, 64,  0, 64, 64, 64, 64,  0,  0,  0, 64,  0,  0,  0, 64,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0, 64,  0,  0,  0,  0, 64,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0, 64, 64, 64,  0,  0},
    {0,  0, 64, 64, 64,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0, 64,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0, 64,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
};

typedef struct player {
    int x;
    int y;
} t_player;

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)

static void run_is_zero(int x1, int y1, int x2, int y2)
{
    if (y2 < y1) {
        int y;
        int visible = 1;
        for (y = y1; y >= y2; y--) {
            shader[y][x1] = visible;
            if (map[y][x1] == 64)
                visible = 0;
        }	
    } else {
        int y;
        int visible = 1;
        for (y = y1; y <= y2; y++) {
            shader[y][x1] = visible;
            if (map[y][x1] == 64)
                visible = 0;
        }
    }
}

void line1(x, y, tx, ty)
{
//    printf("line1\n");

    int rise = ty - y;
    int run = tx - x;
    float offset = 0.0f;
    float treshold = 0.5f;
    float delta = (float)rise / run;
    if (delta < 0)
        delta *= -1;

    int visible = 1;
    for (; x <= tx; x++) {
        shader[y][x] = visible;
        if (map[y][x] == 64)
            visible = 0;

        offset += delta;
        if (offset >= treshold) {
            y++;
            treshold++;
        }
    }
}

void line2(x, y, tx, ty)
{
//    printf("line2\n");
    
    int rise = ty - y;
    int run = tx - x;
    float offset = 0.0f;
    float treshold = 0.5f;
    float delta = ((float)rise / run);
    if (delta < 0)
        delta *= -1;

    int visible = 1;
    for (; x <= tx; x++) {
        shader[y][x] = visible;
        if (map[y][x] == 64)
            visible = 0;

        offset += delta;
        if (offset >= treshold) {
            y--;
            treshold++;
        }
    }
}

void line3(x, y, tx, ty)
{
//    printf("line3\n");
	
    int rise = ty - y;
    int run = tx - x;
    float offset = 0.0f;
    float treshold = 0.5f;
    float delta = (float)rise / run;
    if (delta < 0)
        delta *= -1;

    int visible = 1;
    for (; x >= tx; x--) {
        shader[y][x] = visible;
        if (map[y][x] == 64)
            visible = 0;

        offset += delta;
        if (offset >= treshold) {
            y++;
            treshold++;
        }
    }
}

void line4(x, y, tx, ty)
{
//    printf("line4\n");

    int rise = ty - y;
    int run = tx - x;
    float offset = 0.0f;
    float treshold = 0.5f;
    float delta = ((float)rise / run);
    if (delta < 0)
        delta *= -1;

    int visible = 1;
    for (; x >= tx; x--) {
        shader[y][x] = visible;
        if (map[y][x] == 64)
            visible = 0;

        offset += delta;
        if (offset >= treshold) {
            y--;
            treshold++;
        }
    }
}

void line5(x, y, tx, ty)
{
//    printf("line5\n");

    int rise = ty - y;
    int run = tx - x;
    float offset = 0.0f;
    float treshold = 0.5f;
    float delta = ((float)run / rise);
    if (delta < 0)
        delta *= -1;

    int visible = 1;
    for (; y >= ty; y--) {
        shader[y][x] = visible;
        if (map[y][x] == 64)
            visible = 0;

        offset += delta;
        if (offset >= treshold) {
            x--;
            treshold++;
        }
    }
}

void line6(x, y, tx, ty)
{
//    printf("line6\n");
    
    int rise = ty - y;
    int run = tx - x;
    float offset = 0.0f;
    float treshold = 0.5f;
    float delta = ((float)run / rise);
    if (delta < 0)
        delta *= -1;

    int visible = 1;
    for (; y >= ty; y--) {
        shader[y][x] = visible;
        if (map[y][x] == 64)
            visible = 0;

        offset += delta;
        if (offset >= treshold) {
            x++;
            treshold++;
        }
    }
}

void line7(x, y, tx, ty)
{
//    printf("line7\n");
    
    int rise = ty - y;
    int run = tx - x;
    float offset = 0.0f;
    float treshold = 0.5f;
    float delta = ((float)run / rise);
    if (delta < 0)
        delta *= -1;

    int visible = 1;
    for (; y <= ty; y++) {
        shader[y][x] = visible;
        if (map[y][x] == 64)
            visible = 0;

        offset += delta;
        if (offset >= treshold) {
            x++;
            treshold++;
        }
    }
}

void line8(x, y, tx, ty)
{
//    printf("line8\n");
    
    int rise = ty - y;
    int run = tx - x;
    float offset = 0.0f;
    float treshold = 0.5f;
    float delta = ((float)run / rise);
    if (delta < 0)
        delta *= -1;

    int visible = 1;
    for (; y <= ty; y++) {
        shader[y][x] = visible;
        if (map[y][x] == 64)
            visible = 0;

        offset += delta;
        if (offset >= treshold) {
            x--;
            treshold++;
        }
    }
}

void raycast(t_player player, int x2, int y2)
{
    int x1 = player.x;
    int y1 = player.y;
    int rise = y2 - y1;
    int run = x2 - x1;
    float slope = (float)rise / run;

//    printf("run=%d, rise=%d, slope=%f, (%d, %d) -> (%d, %d)\n", run, rise, slope, x1, y1, x2, y2);

    if (run == 0) {
        run_is_zero(x1, y1, x2, y2); // case 9 and 10
    } else {
        // Translate to function pointer matrix?
        // Could the decision be taken easilier?
        if (slope <= 1 && slope >= -1) {
            if (x1 < x2) {
                if (y1 < y2) {
                    line1(x1, y1, x2, y2);
                } else {
                    line2(x1, y1, x2, y2);
                }
            } else {
                if (y1 < y2) {
                    line3(x1, y1, x2, y2);
                } else {
                    line4(x1, y1, x2, y2);
                }
            }
        } else {
            if (x1 < x2) {
                if (y1 < y2) {
                    line7(x1, y1, x2, y2);
                } else {
                    line6(x1, y1, x2, y2);
                }
            } else {
                if (y1 < y2) {
                    line8(x1, y1, x2, y2);
                } else {
                    line5(x1, y1, x2, y2);
                }
            }
        }
    }
}

int manhattanDistance(t_player player, int tx, int ty)
{
    int diffx = player.x - tx;
    int diffy = player.y - ty;
    if (diffx < 0)
        diffx *= -1;
    if (diffy < 0)
        diffy *= -1;
    return diffy + diffx;
}

int eucludianDistance(t_player player, int tx, int ty)
{
    int diffx = player.x - tx;
    int diffy = player.y - ty;
    if (diffx < 0)
        diffx *= -1;
    if (diffy < 0)
        diffy *= -1;

    return sqrt(((diffx * diffx) + (diffy * diffy)));
}

void raytrace(t_player player)
{
    int x = player.x;
    int y = player.y;
    int checkY = MAX(y - R, 0);
    int endY   = MIN(y + R, height-1);
    int startX = MAX(x - R, 0);
    int endX   = MIN(x + R, width - 1);
    int checkX;

    int rx, ry; // Reset
    for (ry = 0; ry < height; ry++)
        for (rx = 0; rx < width; rx++)
            shader[ry][rx] = 0;

    // Square radius calculus
#ifdef SQUARE
    for (; checkY <= endY; checkY++)
        for (checkX = startX; checkX <= endX; checkX++)
            shader[checkY][checkX] = 1;
#endif

    // Circle radius calculus manhattan
#ifdef CIRCLE_MAN
    for (; checkY <= endY; checkY++)
        for (checkX = startX; checkX <= endX; checkX++)
            if (manhattanDistance(player, checkX, checkY) <= R)
                shader[checkY][checkX] = 1;
#endif  

    // Circle radius calculus eucludian
#ifdef CIRCLE_EUC
    for (; checkY <= endY; checkY++)
        for (checkX = startX; checkX <= endX; checkX++)
            if (eucludianDistance(player, checkX, checkY) <= R)
                shader[checkY][checkX] = 1;
#endif
    
    // Raycasting radius calculus naive
#ifdef RAYCAST_NAY
    for (; checkY <= endY; checkY++)
        for (checkX = startX; checkX <= endX; checkX++)
            if (eucludianDistance(player, checkX, checkY) <= R)
                raycast(player, checkX, checkY);
#endif
    
}

void renderDisplay(t_player player)
{
    int y, x;

    printf("+--");
    for (x = 1; x < width; x++)
        printf("--");
    printf("-+\n");

    for (y = 0; y < height; y++) {
        printf("| ");
        for (x = 0; x < width; x++) {
            if (x == player.x && player.y == y) {
                printf("X ");
                continue;
            }

            if (!shader[y][x]) {
                printf("  ");
                continue;
            }

            switch (map[y][x]) {
            case 0:
                printf(". ");
                break;
            case 64:
                printf("@ ");
                break;
            case 'T':
                printf("T ");
                break;
            default:
                printf("? ");
                break;
            }
        }

        printf("|\n");
    }

    printf("+--");
    for (x = 1; x < width; x++)
        printf("--");
    printf("-+\n");
}

void movePlayer(t_player *player, char keyPressed)
{
    switch (keyPressed) {
    case 'a':
        if (player->x - 1 < 0)
            return;

        if (map[player->y][player->x-1] != 64)
            player->x--;
        break;
    case 's':
        if (player->y + 1 >= height)
            return;

        if (map[player->y+1][player->x] != 64)
            player->y++;

        break;
    case 'd':
        if (player->x + 1 >= width)
            return;

        if (map[player->y][player->x+1] != 64)
            player->x++;
        break;
    case 'w':
        if (player->y - 1 < 0)
            return;

        if (map[player->y-1][player->x] != 64)
            player->y--;

        break;
        break;
    default:
        break;
    }
}

int main()
{
    t_player player;
    player.x = 15;
    player.y = 15;

    char keyPressed = 0;

    while (keyPressed != 'q') {
        raytrace(player);
        renderDisplay(player);

        keyPressed = getchar();

        movePlayer(&player, keyPressed);
    }


    return 0;
}
