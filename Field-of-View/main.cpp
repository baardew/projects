#include "Map.hpp"
#include "arraymemory.hpp"
#include "MRPAS.hpp"
#include "timetaker.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <vector>
#include <algorithm>

int width = 25;
int height = 25;
int R = 7;

char **shader;
char **shader2;
char **map;

long int sec_js;
long int usec_js;
long int sec_cpp;
long int usec_cpp;

const char seed_1[25][25] = {
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
};

const char seed[25][25] = {
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64},
    {0,  0, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0, 64, 64, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0,  0, 64, 64,  0,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0,  0},
    {0,  0,  0, 64,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0, 64, 64,  0,  0,  0,  0,  0, 64,  0, 64, 64, 64, 64,  0,  0,  0, 64,  0,  0,  0, 64,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0, 64,  0,  0,  0,  0, 64,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0,  0},
    {0,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64,  0,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0, 64, 64, 64,  0,  0},
    {0,  0, 64, 64, 64,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0, 64,  0,  0,  0, 64, 64,  0,  0, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    {0,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0, 64,  0, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64, 64, 64, 64, 64, 64, 64,  0,  0,  0,  0, 64,  0,  0, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 64, 64, 64, 64,  0,  0,  0},
    {0,  0,  0,  0,  0,  0, 64, 64, 64, 64, 64, 64, 64, 64,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
};

// HACK
#ifndef PLAYER_T
#define PLAYER_T
typedef struct player {
    int x;
    int y;
} t_player;
#endif


void renderDisplay(t_player player);
int eucludianDistance(t_player player, int tx, int ty);

void compute_quadrant(Map &map, t_player position, int maxRadius, int dx, int dy)
{
    float *startAngle = (float*)allocate_memory_1d(sizeof(float), maxRadius+1);
    float *endAngle = (float*)allocate_memory_1d(sizeof(float), maxRadius+1);

    int iteration = 1;
    bool done = false;
    int totalObstacles = 0;
    int obstaclesInLastLine = 0;
    float minAngle = 0.0f;
    float x = 0.0f;
    float y = position.y + dy;

    float slopesPerCell;
    float halfSlopes;
    int processedCell = 0;
    int minx;
    int maxx;
    int miny;
    int maxy;
    t_player pos;
    bool visible;
    float startSlope;
    float centerSlope;
    float endSlope;
    int idx;

    if (y < 0 || y >= height)
        done = true;

    while (!done) {
        slopesPerCell = 1.0f / (iteration + 1.0f);
        halfSlopes = slopesPerCell * 0.5;
        processedCell = minAngle / slopesPerCell;
        minx = std::max(0, position.x - iteration);
        maxx = std::min(width - 1, position.x + iteration);
        done = true;
        x = position.x + (processedCell * dx);

        while (x >= minx && x <= maxx) {
            pos.x = x;
            pos.y = y;
            visible = true;
            startSlope = processedCell * slopesPerCell;
            centerSlope = startSlope + halfSlopes;
            endSlope = startSlope + slopesPerCell;

            if (obstaclesInLastLine > 0 && !map.is_visible(pos.x, pos.y)) {
                idx = 0;

                while (visible && idx < obstaclesInLastLine) {
                    if (map.is_transparent(pos.x, pos.y)) {
                        if (centerSlope > startAngle[idx] && centerSlope < endAngle[idx])
                            visible = false;
                    } else if (startSlope >= startAngle[idx] && endSlope <= endAngle[idx]) {
                        visible = false;
                    }

                    if (visible
                        && (!map.is_visible(x, y-dy) || !map.is_transparent(x, y-dy))
                        && (x - dx >= 0 && x - dx < width
                            && (!map.is_visible(x-dx, y-dy) || !map.is_transparent(x-dx, y-dy)))
                        )
                    {
                        visible = false;
                    }

                    idx += 1;
                }
            }

            if (visible) {
                if (eucludianDistance(position, pos.x, pos.y) <= maxRadius) {
                    map.set_visible(pos.x, pos.y);
                    done = false;
                } else {
                    iteration == maxRadius;
                    break;
                }
                //if the cell is opaque, block the adjacent slopes
                if(!map.is_transparent(pos.x, pos.y)){
                    if (minAngle >= startSlope) {
                        minAngle = endSlope;
                    } else {
                        startAngle[totalObstacles] = startSlope;
                        endAngle[totalObstacles] = endSlope;
                        totalObstacles += 1;
                    }
                }
            }

            processedCell++;
            x += dx;
        }

        if (iteration == maxRadius)
            done = true;

        iteration++;
        obstaclesInLastLine = totalObstacles;
        y += dy;

        if (y < 0 || y >= height || minAngle == 1.0f)
            done = true;
    }

    //octant: horizontal edge
    iteration = 1; //iteration of the algo for this octant
    done = false;
    totalObstacles = 0;
    obstaclesInLastLine = 0;
    minAngle = 0.0;
    x = position.x + dx; //the outer slope's coordinates (first processed line)
    y = 0;

    //do while there are unblocked slopes left and the algo is within the map's boundaries
    //scan progressive lines/columns from the PC outwards
    if (x < 0 || x >= width)
        done = true;

    while (!done) {
        //process cells in the line
        slopesPerCell = 1.0 / (iteration + 1);
        halfSlopes = slopesPerCell * 0.5;
        processedCell = minAngle / slopesPerCell;
        miny = std::max(0, position.y - iteration);
        maxy = std::min(height - 1, position.y + iteration);
        done = true;
        y = position.y + (processedCell * dy);

        while (y >= miny && y <= maxy) {
            //calculate slopes per cell
            pos.x = x;
            pos.y = y;
            visible = true;
            startSlope = (processedCell * slopesPerCell);
            centerSlope = startSlope + halfSlopes;
            endSlope = startSlope + slopesPerCell;

            if (obstaclesInLastLine > 0 && !map.is_visible(pos.x, pos.y)) {
                idx = 0;

                while (visible && idx < obstaclesInLastLine) {
                    if (map.is_transparent(pos.x, pos.y)) {
                        if(centerSlope > startAngle[idx] && centerSlope < endAngle[idx])
                            visible = false;
                    } else if(startSlope >= startAngle[idx] && endSlope <= endAngle[idx]) {
                        visible = false;
                    }

                    if (visible
                        && (!map.is_visible(x-dx, y) || !map.is_transparent(x-dx, y))
                        && (y - dy >= 0 && y - dy < height
                            && (!map.is_visible(x-dx, y-dy) || !map.is_transparent(x-dx, y-dy)))) {
                        visible = false;
                    }

                    idx += 1;
                }
            }

            if (visible) {
                if (eucludianDistance(position, pos.x, pos.y) <= maxRadius) {
                    map.set_visible(pos.x, pos.y);
                    done = false;
                } else {
                    iteration == maxRadius;
                    break;
                }
                //if the cell is opaque, block the adjacent slopes
                if(!map.is_transparent(pos.x, pos.y)) {
                    if(minAngle >= startSlope) {
                        minAngle = endSlope;
                    } else {
                        startAngle[totalObstacles] = startSlope;
                        endAngle[totalObstacles] = endSlope;
                        totalObstacles += 1;
                    }
                }
            }

            processedCell += 1;
            y += dy;
        }

        if (iteration == maxRadius)
            done = true;

        iteration += 1;
        obstaclesInLastLine = totalObstacles;
        x += dx;

        if (x < 0 || x >= width || minAngle == 1.0f)
            done = true;
    }

    free_memory((void*)startAngle, 1);
    free_memory((void*)endAngle, 1);
}

void __attribute__ ((noinline)) compute(Map &map, t_player player)
{
    map.reset_visibility();
    map.set_visible(player.x, player.y); //player can see himself
    //compute the 4 quadrants of the map
    compute_quadrant(map, player, R, 1, 1);
    compute_quadrant(map, player, R, 1, -1);
    compute_quadrant(map, player, R, -1, 1);
    compute_quadrant(map, player, R, -1, -1);
};

int eucludianDistance(t_player player, int tx, int ty)
{
    int diffx = player.x - tx;
    int diffy = player.y - ty;
    if (diffx < 0)
        diffx *= -1;
    if (diffy < 0)
        diffy *= -1;

    return sqrt(((diffx * diffx) + (diffy * diffy)));
}

void renderDisplay(t_player player)
{
    int y, x;

    printf("Original port from JS to CPP                             Refactored to CPP class\n");

    printf("+--");
    for (x = 1; x < width; x++)
        printf("--");
    printf("-+    ");

    printf("+--");
    for (x = 1; x < width; x++)
        printf("--");
    printf("-+\n");

    for (y = 0; y < height; y++) {
        printf("| ");
        for (x = 0; x < width; x++) {
            if (x == player.x && player.y == y) {
                printf("X ");
                continue;
            }

            if (!shader[y][x]) {
                printf("  ");
                continue;
            }

            switch (map[y][x]) {
            case 0:
                printf(". ");
                break;
            case 64:
                printf("@ ");
                break;
            case 'T':
                printf("T ");
                break;
            default:
                printf("? ");
                break;
            }
        }

        printf("|    | ");
        for (x = 0; x < width; x++) {
            if (x == player.x && player.y == y) {
                printf("X ");
                continue;
            }

            if (!shader2[y][x]) {
                printf("  ");
                continue;
            }

            switch (map[y][x]) {
            case 0:
                printf(". ");
                break;
            case 64:
                printf("@ ");
                break;
            case 'T':
                printf("T ");
                break;
            default:
                printf("? ");
                break;
            }
        }

        printf("|\n");
    }

    printf("+--");
    for (x = 1; x < width; x++)
        printf("--");
    printf("-+    ");

    printf("+--");
    for (x = 1; x < width; x++)
        printf("--");
    printf("-+\n");

    printf("TIME: %02ld.%06lds                                          TIME: %02ld.%06lds\n", sec_js, usec_js, sec_cpp, usec_cpp);
}

void movePlayer(t_player *player, char keyPressed)
{
    switch (keyPressed) {
    case 'a':
        if (player->x - 1 < 0)
            return;

        if (map[player->y][player->x-1] != 64)
            player->x--;
        break;
    case 's':
        if (player->y + 1 >= height)
            return;

        if (map[player->y+1][player->x] != 64)
            player->y++;

        break;
    case 'd':
        if (player->x + 1 >= width)
            return;

        if (map[player->y][player->x+1] != 64)
            player->x++;
        break;
    case 'w':
        if (player->y - 1 < 0)
            return;

        if (map[player->y-1][player->x] != 64)
            player->y--;

        break;
        break;
    default:
        break;
    }
}

void run_interactive(Map &algo, Map &algo2, t_player &player)
{
    std::vector<char> path;
    char keyPressed = 0;

    while (keyPressed != 'q') {
        struct timeval from, to;
        timetaker_start(&from);
        compute(algo, player);
        timetaker_stop(&to);
        timetaker_time(&from, &to, &sec_js, &usec_js);
        MRPAS mrpas(25, 25);


        timetaker_start(&from);
        mrpas.computeFoV(algo2, player, R);
        timetaker_stop(&to);
        timetaker_time(&from, &to, &sec_cpp, &usec_cpp);

        for (int y = 0; y < 25; y++)
            for (int x = 0; x < 25; x++)
                if (shader2[y][x] != shader[y][x])
                    printf("FoV differs at %d x %d\n", y, x);


        if (keyPressed == '\n')
            renderDisplay(player);
        else
            path.push_back(keyPressed);

        keyPressed = getchar();

        movePlayer(&player, keyPressed);
    }

    printf("Movement sequence: ");
    for (int i = 0; i < path.size(); i++)
        printf("%c", path[i]);
    printf("\n");
}

void run_automated(Map &algo, Map &algo2, t_player &player, char *sequence)
{
    int length = strlen(sequence);
    MRPAS mrpas(25, 25);

    for (int i = 0; i < length; i++) {
        struct timeval from, to;
        timetaker_start(&from);
        compute(algo, player);
        timetaker_stop(&to);
        timetaker_time(&from, &to, &sec_js, &usec_js);

        timetaker_start(&from);
        mrpas.computeFoV(algo2, player, R);
        timetaker_stop(&to);
        timetaker_time(&from, &to, &sec_cpp, &usec_cpp);

        movePlayer(&player, sequence[i]);
    }
}

int main(int argc, char **argv)
{
    map = (char**)allocate_memory_2d(sizeof(char), 25, 25);
    shader = (char**)allocate_memory_2d(sizeof(char), 25, 25);
    shader2 = (char**)allocate_memory_2d(sizeof(char), 25, 25);

    for (int i = 0; i < 25; i++)
        for (int j = 0; j < 25; j++)
            map[i][j] = seed[i][j];

    t_player player;
    player.x = 15;
    player.y = 15;

    Map algo(25, 25, map, shader);
    Map algo2(25, 25, map, shader2);

    if (argc == 1)
        run_interactive(algo, algo2, player);
    else
        run_automated(algo, algo2, player, argv[1]);

    free_memory((void*)map, 2);
    free_memory((void*)shader, 2);
    free_memory((void*)shader2, 2);

    return 0;
}
