
#include <cstdlib>
#include <cstdio>

class IndexMemory
{
public:
    IndexMemory() : max_index(0) {
        mem = new float[512];
    }

    ~IndexMemory() {
        delete[] mem;
        printf("Largest Access: %d\n", max_index);
    }

    float& operator[](const int i) {
        if (i > max_index)
            max_index = i;
        return mem[i];
    }
    
private:
    int max_index;
    float *mem;
};
