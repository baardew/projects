/**
 * This file contains method for creating, accessing and freeing arrays of
 * up to three dimensions, using a linear data array and seperate pointer arrays.
 *
 * Compatible with C, C++ and (soon) CUDA.
 * Compatible with 32-bit and 64-bit.
 *
 * This implementation is approx 12-15% slower than multi-allocated base memory,
 * but should have advantages with memory transfers.
 * However, this is more suited to use with GPUs.
 *
 * The arrays are created to optimize memory access;
 * this results in the array structure:
 *
 * Access pattern is:
 * 1D = [x]
 * 2D = [y][x]
 * 3D = [z][y][x]
 *
 * Memory format is pointers first, then actual data memory.
 * If every memory slot in 3D is to be processed, acces them using 1D
 * as memory is laid of to maximize memory coalesced.
 */

#ifndef ARRAY_MEMORY_H
#define ARRAY_MEMORY_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

    void free_memory(void* memory, int dimensions);

    void*   allocate_memory_1d(size_t bytesize, size_t width);
    void**  allocate_memory_2d(size_t bytesize, size_t width, size_t height);
    void*** allocate_memory_3d(size_t bytesize, size_t width, size_t height, size_t depth);

    /**
     * Development tools and used for debugging.
     * Prints to stdout only.
     */
    void array_print_int_1d(char   *array, size_t x);
    void array_print_int_2d(char  **array, size_t x, size_t y);
    void array_print_int_3d(char ***array, size_t x, size_t y, size_t z);


#ifdef __cplusplus
}
#endif

#endif /* ARRAY_MEMORY_H */
