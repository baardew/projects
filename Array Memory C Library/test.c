#include "arraymemory.h"

#include <stdio.h>

int main(void)
{
    int ***mem = (int***)allocate_memory_3d(sizeof(int), 10, 10, 2);

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 10; j++)
            for (int k = 0; k < 10; k++)
                mem[i][j][k] = i * 10 * 10 + j * 10 + k;

    array_print_int_3d((void*)mem, 10, 10, 2);
    free_memory((void*)mem, 3);
}
